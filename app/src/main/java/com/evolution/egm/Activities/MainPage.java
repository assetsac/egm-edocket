package com.evolution.egm.Activities;

import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.astuetz.PagerSlidingTabStrip;
import com.evolution.egm.Fragments.Fleet;
import com.evolution.egm.Fragments.Jobs;
import com.evolution.egm.Fragments.More;
import com.evolution.egm.Fragments.Safety;
import com.evolution.egm.Fragments.VideoTest;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Utils.SessionManager;
import com.evolution.egm.R;

public class MainPage extends AppCompatActivity {
    public static final String TAG = MainPage.class.getSimpleName();

    private SessionManager session;
    private Toolbar toolbar;
    private String token;
    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        db = new DatabaseHelper(this);
        initToolbar();
        initSession();
        initViewPager();

        final LocationManager manager = (LocationManager) getSystemService(this.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void initSession() {
        session = new SessionManager(getApplicationContext());
        token = session.getToken();
        if (!session.isLoggedIn()) {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public void initViewPager() {
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new SampleFragmentPagerAdapter(getSupportFragmentManager()));

        PagerSlidingTabStrip tabsStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabsStrip.setViewPager(viewPager);

        tabsStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        getSupportActionBar().setTitle("Job List");
                        break;
                    case 1:
                        getSupportActionBar().setTitle("Video");
                        break;
                    case 2:
                        getSupportActionBar().setTitle("Safety");
                        break;
                    case 3:
                        getSupportActionBar().setTitle("After Care");
                        break;
                    case 4:
                        getSupportActionBar().setTitle("More");
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public class SampleFragmentPagerAdapter extends FragmentPagerAdapter implements PagerSlidingTabStrip.IconTabProvider {

        final int PAGE_COUNT = 5;
        private int tabIcons[] = {R.drawable.ic_action_searchicon, R.drawable.ic_action_video, R.drawable.ic_action_safetyicon, R.drawable.ic_action_aftercare, R.drawable.ic_action_moreicon};

        public SampleFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment da = null;
            switch (position) {
                case 0:
                    da = Jobs.newInstance(position);
                    break;
                case 1:
                    da = VideoTest.newInstance(position);
                    break;
                case 2:
                    da = Safety.newInstance(position);
                    break;
                case 3:
                    da = Fleet.newInstance(position);
                    break;
                case 4:
                    da = More.newInstance(position);
                    break;
            }
            return da;
        }

        @Override
        public int getPageIconResId(int position) {
            return tabIcons[position];
        }
    }

    public void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Job Lists ");
    }
}
