package com.evolution.egm.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.evolution.egm.Adapters.AdapterSignagesList;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Models.Job;
import com.evolution.egm.Models.SignagesMeterModel;
import com.evolution.egm.Models.SignagesObject;
import com.evolution.egm.R;

import org.parceler.Parcels;

import java.util.ArrayList;

public class SignsList extends AppCompatActivity {
    Toolbar toolbar;
    ArrayList<SignagesMeterModel> item = new ArrayList<>();

    Job mJobObject;
    SignagesObject listSelected = null;
    private RecyclerView mRecyclerView;
    private AdapterSignagesList mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public DatabaseHelper db;
    String types = "";
    ArrayList<SignagesMeterModel> passingData;
    SignagesObject objects;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signs);
        initToolBar();

        db = new DatabaseHelper(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.signagesphoto);
        mLayoutManager = new GridLayoutManager(getApplicationContext(), 4);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));
        types = getIntent().getExtras().getString("type");
        passingData = Parcels.unwrap(getIntent().getExtras().getParcelable("selected"));
        ArrayList<SignagesMeterModel> signlists = new ArrayList<SignagesMeterModel>();

        if (types.equals("1")) {
            Log.e("pass counter1", passingData.size() + "");
            objects = new SignagesObject(db.getAllSignages(mJobObject.getJobID()));
            for (int c = 0; c < objects.getSignages().size(); c++) {
                for (int i = 0; i < passingData.size(); i++) {
                    if (passingData.get(i).get_id().equals(objects.getSignages().get(c).get_id())) {
                        if (passingData.get(i).isSelected()) {
                            objects.getSignages().get(c).setIsSelected(true);
                        }
                    }
                }
                signlists.add(objects.getSignages().get(c));
            }
        } else {

            Log.e("pass counter", passingData.size() + "");
            String jj = String.valueOf(passingData.size());

            objects = new SignagesObject(db.getAllSignages2(mJobObject.getJobID()));

            for (int c = 0; c < objects.getSignages().size(); c++) {
                for (int i = 0; i < passingData.size(); i++) {
                    if (passingData.get(i).get_id().equals(objects.getSignages().get(c).get_id())) {
                        if (passingData.get(i).isSelected()) {
                            objects.getSignages().get(c).setIsSelected(true);
                        }
                    }
                }
                signlists.add(objects.getSignages().get(c));
            }
        }
        mAdapter = new AdapterSignagesList(new SignagesObject(signlists), this);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.munusignages, menu);
        return true;
    }

    public void initToolBar() {

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Add Signages");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.addsignstolist:
                Intent intent = getIntent();
                intent.putExtra("type", types);
                intent.putExtra("selecteditem", Parcels.wrap(mAdapter.listofSelectedItem().getSignages()));
                setResult(Activity.RESULT_OK, intent);
                finish();
                return true;
            case android.R.id.home:
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
