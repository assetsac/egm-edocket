package com.evolution.egm.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.evolution.egm.R;

public class WebViewAcitivy extends AppCompatActivity {
    Toolbar toolbar;
    WebView url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        initToolBar();

        url = (WebView) findViewById(R.id.url);
        url.loadUrl("http://tutorialspoint.com/android/android_tutorial.pdf");
        WebSettings webSettings = url.getSettings();
        webSettings.setJavaScriptEnabled(true);
        url.setWebViewClient(new WebViewClient());

    }


    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Photo");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                return true;
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
