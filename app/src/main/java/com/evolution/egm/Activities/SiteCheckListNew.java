package com.evolution.egm.Activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.evolution.egm.Fragments.CreateJob1;
import com.evolution.egm.Fragments.CreateJob2;
import com.evolution.egm.Fragments.CreateJob3;
import com.evolution.egm.Fragments.CreateJob5;
import com.evolution.egm.Fragments.SiteChecklistSignature;
import com.evolution.egm.Utils.CreateJobSharedPreference;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Utils.SessionManager;
import com.evolution.egm.Models.Job;
import com.evolution.egm.R;

import org.parceler.Parcels;

import java.util.HashMap;


public class SiteCheckListNew extends AppCompatActivity implements View.OnClickListener, CreateJob1.OnFirstFragmentListener, CreateJob2.OnnextandPre, CreateJob5.OnnextandPre,  CreateJob3.OnnextandPre, SiteChecklistSignature.OnnextandPre {

    public static final String TAG = SiteCheckListNew.class.getSimpleName();

    Toolbar toolbar;
    RelativeLayout nextfirst, nextButton, previos, save;
    ViewPager viewPager;
    Job mJobObject;
    DatabaseHelper db;
    public String CompanyName, Address, JobID;
    private String token;
    public SessionManager session;
    public int currentPage;
    CreateJobSharedPreference shared;
    CreateJob1 job1;
    String b = "b";
    Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_job);

        DatabaseHelper db = new DatabaseHelper(this);
        initToolBar();
        shared = new CreateJobSharedPreference(this);
        extras = getIntent().getExtras();
        db = new DatabaseHelper(this);

        if (extras != null) {
            mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));
        }

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new SampleFragmentPagerAdapter(getSupportFragmentManager()));

        nextButton = (RelativeLayout) findViewById(R.id.nextButton);
        previos = (RelativeLayout) findViewById(R.id.previos);
        nextfirst = (RelativeLayout) findViewById(R.id.nextfirst);
        save = (RelativeLayout) findViewById(R.id.save);

        session = new SessionManager(this);
        token = session.getToken();
        job1 = new CreateJob1();
        nextfirst.setOnClickListener(this);
        save.setOnClickListener(this);

        initViewPager();

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventNext();
            }
        });

        previos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(getItem(-1), true);
                if (viewPager.getCurrentItem() >= 0) {

                    if (viewPager.getCurrentItem() <= 1) {
                        Log.e("", "Previous button deleted");
                    }

                } else {

                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        // your code.
        finish();
        closeCreateJob();
    }

    public void initViewPager() {
        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                switch (position) {
                    case 0:
                        break;
                    case 1:
                        break;
                }
            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
                switch (position) {
                    case 0:
                        getSupportActionBar().setTitle("Site Checklist");
                        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                        nextfirst.setVisibility(nextfirst.VISIBLE);
                        save.setVisibility(save.GONE);
                        nextButton.setVisibility(nextButton.GONE);
                        break;
                    default:
                        getSupportActionBar().setTitle("Site Checklist");
                        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                        nextfirst.setVisibility(nextfirst.GONE);
                        save.setVisibility(save.GONE);
                        nextButton.setVisibility(nextButton.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Site Checklist");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void setFragment(Fragment frag) {
    }

    public void closeCreateJob() {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nextfirst:
                eventNext();
                job1.getCheckBox();
                break;
            case R.id.save:
                saveJob();
                break;
        }
    }


    public void eventNext() {
        viewPager.setCurrentItem(getItem(+1), true);
        if (viewPager.getCurrentItem() <= 0) {
            if (viewPager.getCurrentItem() <= 1) {
                Log.e("", "Previous button created");
            }
        } else {

        }
    }

    public void saveJob() {
        Toast.makeText(getApplication(), "test", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNext() {
        eventNext();
    }


    @Override
    public void onPre() {
        prev();
    }

    @Override
    public void saving() {
        db = new DatabaseHelper(this);
        String m = db.checkExistCreateJObs(mJobObject.getJobID());
        if (db.checkExistCreateJObs2(mJobObject.getJobID()) == null) {
            db.addCreateJobs(mJobObject.getJobID());
        }
        db.updateContact(mJobObject.getJobID(), shared.getCreateJobFirst(), shared.getCreateJobSecond2(), shared.getSignatures(), "", "1");
        this.finish();

    }

    public void prev() {
        viewPager.setCurrentItem(getItem(-1), true);
        if (viewPager.getCurrentItem() >= 0) {
            if (viewPager.getCurrentItem() <= 1) {
            }
        } else {

        }
    }

    @Override
    public void onFirst(HashMap<String, String> data) {
        eventNext();
    }

    public class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 2;

        public SampleFragmentPagerAdapter(FragmentManager fm) {
            super(fm);

        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment da = null;
            switch (position) {
                case 0:
                    da = CreateJob2.newInstance(position);
                    break;
                case 1:
                    da = SiteChecklistSignature.newInstance(position);
                    break;
            }
            return da;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }


}
