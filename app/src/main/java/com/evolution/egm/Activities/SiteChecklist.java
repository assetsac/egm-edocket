package com.evolution.egm.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.evolution.egm.Adapters.SignaturesList;
import com.evolution.egm.Utils.ConnectionDetector;
import com.evolution.egm.Utils.Constants;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Utils.RecyclerItemClickListener;
import com.evolution.egm.Utils.SessionManager;
import com.evolution.egm.Interface.APIInterface;
import com.evolution.egm.Models.Job;
import com.evolution.egm.Models.SiteCheckListSignatureModel;
import com.evolution.egm.Models.siteChecklistAddmodel;
import com.evolution.egm.R;
import com.google.gson.Gson;
import com.rey.material.app.Dialog;
import com.squareup.okhttp.OkHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

public class SiteChecklist extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    LinearLayout answerlayout;
    DatabaseHelper db;
    Bundle extras;
    Job mJobObject;
    RadioButton sitecheclist, signature;
    LinearLayout checklist, signatures;
    Dialog dialog;
    private static final String ARG_SECTION_NUMBER = "section_number";
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private ConnectionDetector connection;
    ArrayList<String> myDataset = new ArrayList<>();
    ArrayList<String> signaturesd = new ArrayList<>();
    ProgressDialog progressDoalog;
    OkHttpClient okHttpClient;
    public SessionManager session;
    ArrayList<SiteCheckListSignatureModel> mDataset = new ArrayList<SiteCheckListSignatureModel>();
    final CharSequence ifsubjob = ".";
    Dialog dialogdd;
    ProgressBar progresssaving;
    TextView statusbaritem;
    TextView statuspercentage;
    ArrayList<siteChecklistAddmodel> aditionalhazzard = new ArrayList<>();
     boolean isGenerated;
    Menu resync;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_site_checklist);
        initToolBar();

        extras = getIntent().getExtras();

        if (extras != null) {
            mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));
        }

        session = new SessionManager(this);
        okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(60 * 1000, TimeUnit.MILLISECONDS);
        connection = new ConnectionDetector(this);
        dialogdd = new Dialog(this, R.style.FullHeightDialog);

        dialog = new Dialog(this, R.style.FullHeightDialog);
        db = new DatabaseHelper(this);
        String timeshet = db.getSiteChecklist(mJobObject.getJobID());

        sitecheclist = (RadioButton) findViewById(R.id.sitecheclist);
        signature = (RadioButton) findViewById(R.id.signature);
        sitecheclist.setOnClickListener(this);
        signature.setOnClickListener(this);

        signatures = (LinearLayout) findViewById(R.id.signatures);
        checklist = (LinearLayout) findViewById(R.id.checklist);
        signatures.setVisibility(signatures.GONE);

        mRecyclerView = (RecyclerView) findViewById(R.id.signatures_list);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        Log.e("testsdfsdfsdf", "onCreate: " + db.getTCAttachment(mJobObject.getJobID()));
        try {
            JSONArray jso = new JSONArray(db.getTCAttachment(mJobObject.getJobID()));
            for (int c = 0; c < jso.length(); c++) {
                JSONObject d = jso.getJSONObject(c);
                SiteCheckListSignatureModel m = new SiteCheckListSignatureModel();
                m.setAttachedOn(d.getString("AttachedOn"));
                m.setAttachment(d.getString("Attachment"));
                m.setJobID(d.getString("JobID"));
                m.setTCName(d.getString("TCName"));
                mDataset.add(m);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        mAdapter = new SignaturesList(mDataset, this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {
                        Intent m = new Intent(SiteChecklist.this, SignatureView.class);
                        Log.e("haha", mDataset.get(position).getAttachment());
                        m.putExtra("signature", mDataset.get(position).getAttachment());
                        m.putExtra("name", mDataset.get(position).getTCName());
                        m.putExtra("timesheet", "false");
                        startActivity(m);
                    }
                })
        );

        getSignatures();
        String abc = timeshet.replace("~", "#");
        abc = abc.replace("|#", "#");
        abc = abc.replace("|", "  \n - ");
        String[] splitter = abc.split("#");

        answerlayout = (LinearLayout) findViewById(R.id.answerlayout);

        for (int c = 0; c < splitter.length; c++) {
            String xx = null;
            String[] cde = splitter[c].split(":");
            String abcd = cde[1].replace("^", "#");
            String[] inner = abcd.split("#");

            if (c == 0) {
                TextView title2 = new TextView(this);
                title2.setText("Normal Road Configuration");
                title2.setPadding(0, 16, 0, 26);
                title2.setTextSize(20);
                title2.setTextColor(getResources().getColor(R.color.skyblue));
                title2.setId(c);
                title2.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT));
                ((LinearLayout) answerlayout).addView(title2);
            }

            if (c == 6) {
                View m = new View(this);
                m.setBackground(getResources().getDrawable(R.color.colorDivider));
                m.setMinimumHeight(3);
                ((LinearLayout) answerlayout).addView(m);

                TextView title2 = new TextView(this);
                title2.setText("Onsite Conditions");
                title2.setPadding(0, 16, 0, 26);
                title2.setTextSize(20);
                title2.setTextColor(getResources().getColor(R.color.skyblue));
                title2.setId(c);
                title2.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT));
                ((LinearLayout) answerlayout).addView(title2);
            }

            if (c == 11) {
                View m = new View(this);
                m.setBackground(getResources().getDrawable(R.color.colorDivider));
                m.setMinimumHeight(3);
                ((LinearLayout) answerlayout).addView(m);
                TextView title2 = new TextView(this);
                title2.setText("Control Measure as per SWMS");
                title2.setPadding(0, 16, 0, 26);
                title2.setTextSize(20);
                title2.setTextColor(getResources().getColor(R.color.skyblue));
                title2.setId(c);
                title2.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT));
                ((LinearLayout) answerlayout).addView(title2);
            }

            TextView valueTV = new TextView(this);
            String substring = inner[0].substring(Math.max(inner[0].length() - 5, 0));
            if (substring.equals("_Note")) {
                valueTV.setText("Note:");
            } else {
                valueTV.setText(inner[0]);
            }

            valueTV.setPadding(0, 0, 0, 16);
            valueTV.setTextSize(16);
            valueTV.setId(c);
            valueTV.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT));

            TextView valueTV2 = new TextView(this);

            if (inner.length >= 2) {
                valueTV2.setText(" - " + methodRemove(inner[1]));
            } else {
                valueTV2.setText(" - ");
            }

            valueTV2.setTextColor(getResources().getColor(R.color.skyblue));
            valueTV2.setPadding(16, 0, 0, 30);
            valueTV.setTextSize(13);
            valueTV2.setId(400 + c);

            valueTV2.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT));
            ((LinearLayout) answerlayout).addView(valueTV);
            ((LinearLayout) answerlayout).addView(valueTV2);

        }
        Gson mmm = new Gson();
        Log.e("data addition", mmm.toJson(db.checkhazard(mJobObject.getJobID())));

        try {
            JSONArray jsons = new JSONArray(db.checkhazard(mJobObject.getJobID()));
            for (int d = 0; d < jsons.length(); d++) {
                JSONObject c = jsons.getJSONObject(d);

                TextView titles13 = new TextView(this);
                titles13.setText("" + " - Additional Hazard #1");
                titles13.setTextSize(18);
                titles13.setTextColor(getResources().getColor(R.color.skyblue));
                ((LinearLayout) answerlayout).addView(titles13);


                TextView titles1 = new TextView(this);
                titles1.setText("\nAny additional hazards identified");
                ((LinearLayout) answerlayout).addView(titles1);

                TextView titlesunder = new TextView(this);
                titlesunder.setText("- " + c.getString("AddHazard"));
                titlesunder.setPadding(0, 16, 0, 26);
                titlesunder.setTextSize(14);
                titlesunder.setTextColor(getResources().getColor(R.color.skyblue));
                ((LinearLayout) answerlayout).addView(titlesunder);


                TextView titles3 = new TextView(this);
                titles3.setText("Initial Risks");
                ((LinearLayout) answerlayout).addView(titles3);

                TextView initials = new TextView(this);
                String residual2 = "";

                switch (c.getString("InitialRisk")) {
                    case "0":
                        residual2 = "High";
                        break;
                    case "1":
                        residual2 = "Med";
                        break;
                    case "2":
                        residual2 = "Low";
                        break;
                    default:
                        residual2 = c.getString("InitialRisk");
                        break;
                }

                initials.setText("- " + residual2);
                initials.setPadding(0, 16, 0, 26);
                initials.setTextSize(14);
                initials.setTextColor(getResources().getColor(R.color.skyblue));
                ((LinearLayout) answerlayout).addView(initials);

                TextView titles4 = new TextView(this);
                titles4.setText("Control measures to minimise risk");
                ((LinearLayout) answerlayout).addView(titles4);

                TextView titles4sunder = new TextView(this);
                titles4sunder.setText("- " + c.getString("ControlMeasures"));
                titles4sunder.setPadding(0, 16, 0, 26);
                titles4sunder.setTextSize(14);
                titles4sunder.setTextColor(getResources().getColor(R.color.skyblue));
                ((LinearLayout) answerlayout).addView(titles4sunder);

                TextView titles5 = new TextView(this);
                titles5.setText("Residual Risk");
                ((LinearLayout) answerlayout).addView(titles5);

                TextView residuals = new TextView(this);
                String residual = "";

                switch (c.getString("ResidualRisk")) {
                    case "0":
                        residual = "High";
                        break;
                    case "1":
                        residual = "Med";
                        break;
                    case "2":
                        residual = "Low";
                        break;
                    default:
                        residual = c.getString("ResidualRisk");
                        break;
                }

                residuals.setText("- " + residual);
                residuals.setPadding(0, 16, 0, 26);
                residuals.setTextSize(14);
                residuals.setTextColor(getResources().getColor(R.color.skyblue));
                ((LinearLayout) answerlayout).addView(residuals);

                aditionalhazzard = db.getAdditionalHazard(mJobObject.getJobID());

                for (int i = 0; i < aditionalhazzard.size(); i++) {

                    TextView titles134 = new TextView(this);
                    titles134.setText("Additional Hazard #" + (i + 2));
                    titles134.setTextSize(18);
                    titles134.setTextColor(getResources().getColor(R.color.skyblue));
                    ((LinearLayout) answerlayout).addView(titles134);


                    TextView titles14 = new TextView(this);
                    titles14.setText("\nAny additional hazards identified");
                    ((LinearLayout) answerlayout).addView(titles14);

                    TextView titlesunder1 = new TextView(this);
                    titlesunder1.setText("- " + aditionalhazzard.get(i).getAddHazard());
                    titlesunder1.setPadding(0, 16, 0, 26);
                    titlesunder1.setTextSize(14);
                    titlesunder1.setTextColor(getResources().getColor(R.color.skyblue));
                    ((LinearLayout) answerlayout).addView(titlesunder1);

                    TextView titles35 = new TextView(this);
                    titles35.setText("Initial Risks");
                    ((LinearLayout) answerlayout).addView(titles35);

                    TextView initials3 = new TextView(this);
                    String residual23 = "";

                    switch (aditionalhazzard.get(i).getInitialRisk()) {
                        case "0":
                            residual23 = "High";
                            break;
                        case "1":
                            residual23 = "Med";
                            break;
                        case "2":
                            residual23 = "Low";
                            break;
                        default:
                            residual23 = aditionalhazzard.get(i).getInitialRisk();
                            break;
                    }

                    initials3.setText("- " + residual23);
                    initials3.setPadding(0, 16, 0, 26);
                    initials3.setTextSize(14);
                    initials3.setTextColor(getResources().getColor(R.color.skyblue));
                    ((LinearLayout) answerlayout).addView(initials3);

                    TextView titles136 = new TextView(this);
                    titles136.setText("Control measures to minimise risk");
                    ((LinearLayout) answerlayout).addView(titles136);

                    TextView titlesunder13 = new TextView(this);
                    titlesunder13.setText("- " + aditionalhazzard.get(i).getControlMeasures());
                    titlesunder13.setPadding(0, 16, 0, 26);
                    titlesunder13.setTextSize(14);
                    titlesunder13.setTextColor(getResources().getColor(R.color.skyblue));
                    ((LinearLayout) answerlayout).addView(titlesunder13);

                    TextView titles355 = new TextView(this);
                    titles355.setText("Residual Risks");
                    ((LinearLayout) answerlayout).addView(titles355);

                    TextView initials35 = new TextView(this);
                    String residual235 = "";

                    switch (aditionalhazzard.get(i).getInitialRisk()) {
                        case "0":
                            residual235 = "High";
                            break;
                        case "1":
                            residual235 = "Med";
                            break;
                        case "2":
                            residual235 = "Low";
                            break;
                        default:
                            residual235 = aditionalhazzard.get(i).getInitialRisk();
                            break;
                    }

                    initials35.setText("- " + residual235);
                    initials35.setPadding(0, 16, 0, 26);
                    initials35.setTextSize(14);
                    initials35.setTextColor(getResources().getColor(R.color.skyblue));
                    ((LinearLayout) answerlayout).addView(initials35);

                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        isGenerated = false;

    }

    public String methodRemove(String str) {
        if (str.length() > 0 && str.charAt(str.length() - 1) == 'x') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Site Checklist");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_wasignage, menu);
        this.resync = menu;

            menu.findItem(R.id.resync).setVisible(false);

        if (connection.isConnectedToInternet()) {
            Log.e("sdfsdf", "sdfsdfbla");
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Constants.API_URL)
                    .build();
            APIInterface interfacem = restAdapter.create(APIInterface.class);
            interfacem.checReport(session.getToken(), Constants.REQUEST_JOBINFO, mJobObject.getJobID(), new Callback<Object>() {
                @Override
                public void success(Object o, Response response) {
                    String jsonObjectResponse = new Gson().toJson(o);
                    try {
                        JSONObject jsonObj = new JSONObject(jsonObjectResponse);
                        if (jsonObj.getString("status").equals("true")) {
                            menu.findItem(R.id.resync).setVisible(false);
                            Log.e("Checks ", " check true" + jsonObjectResponse);
                        } else {
                            menu.findItem(R.id.resync).setVisible(true);
                            Log.e("Checks ", " check false" + jsonObjectResponse);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("Check ", " check false" + e);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Response r = error.getResponse();
                    Log.e("error ", "error while connectiong to HTTP" + r.getStatus());
                    menu.findItem(R.id.resync).setVisible(true);
                }
            });
        } else {
            menu.findItem(R.id.resync).setVisible(true);
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        }


        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                return true;
            case R.id.resync:
                if (!connection.isConnectedToInternet()) {
                    Toast.makeText(SiteChecklist.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                } else {

                    dialogdd.setCanceledOnTouchOutside(false);
                    dialogdd.setContentView(R.layout.loadingsyncsite);

                    statusbaritem = (TextView) dialogdd.findViewById(R.id.statusbaritem);
                    statuspercentage = (TextView) dialogdd.findViewById(R.id.statuspercentage);
                    progresssaving = (ProgressBar) dialogdd.findViewById(R.id.progressBar7);

                    progresssaving.getProgressDrawable().setColorFilter(Color.LTGRAY, PorterDuff.Mode.SRC_IN);
                    progresssaving.setProgress(5);
                    dialogdd.contentMargin(0, 0, 0, -25);

                    syncSiteChecklist();
                }

                break;
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sitecheclist:
                checklist.setVisibility(checklist.VISIBLE);
                signatures.setVisibility(signatures.GONE);
                break;
            case R.id.signature:
                checklist.setVisibility(checklist.GONE);
                signatures.setVisibility(signatures.VISIBLE);
                break;
        }
    }

    public String getAllAdditionalHazard() {
        Gson mac = new Gson();
        String re = "";
        String de = db.checkhazard(mJobObject.getJobID()).replace("]", "");
        if (db.checkhazard(mJobObject.getJobID()).contains("\"AddHazard\":\"\"")) {
            re = "[]";
        } else {
            if (db.getAdditionalHazard(mJobObject.getJobID()).size() != 0) {
                de = de + ",";
            }
            re = de + mac.toJson(db.getAdditionalHazard(mJobObject.getJobID())).replace("[", "");
        }
        return re;
    }

    public String getSiteChecklist() {
        String temp = "";
        if (db.getSiteChecklist(mJobObject.getJobID()) == null) {

        } else {
            temp = db.getSiteChecklist(mJobObject.getJobID());
        }
        return temp;
    }


    public void syncSiteChecklist() {
        dialogdd.show();

        boolean cc = mJobObject.getJobID().contains(ifsubjob);
        String Checklist2 = getSiteChecklist().replace("|~", "~");
        String Checklist = Checklist2.replace("^x", "^n/a");
        Map<String, String> params = new HashMap<>();
        params.put("operatorID", mJobObject.getOperatorID().toString());
        params.put("jobID", mJobObject.getJobID());
        params.put("timesheets", "[]");
        params.put("assets", "[]");
        params.put("requirements", "[]");
        params.put("dockets", "[]");
        params.put("siteCheckList", Checklist);
        params.put("signageAudit", "[]");
        params.put("tcAttachment", "[]");
        params.put("orderNumber", mJobObject.getOrderNumber());

        if (cc) {
            params.put("parentJob", mJobObject.getId());
        } else {
            params.put("parentJob", "");
        }
        params.put("additionalHazards", getAllAdditionalHazard().replace("\n", ""));
        if (!db.getotherfield(mJobObject.getJobID()).equals("wala")) {
            String data = db.getotherfield(mJobObject.getJobID());
            String[] arr = data.split("~");
            params.put("clientEmail", "");
            params.put("clientName", "");
        } else {
            params.put("clientEmail", "");
            params.put("clientName", "");
        }

        params.put("geotag", "-27.4996872,153.0354553");
        String hashmap = "operatorID :" + params.get("operatorID").toString() + "\n\n" +
                "jobID :" + params.get("jobID").toString() + "\n\n" +
                "assets :" + params.get("assets").toString() + "\n\n" +
                "tcAttachment :" + params.get("tcAttachment").toString() + "\n\n" +
                "timesheets :" + params.get("timesheets").toString() + "\n\n" +
                "requirements :" + params.get("requirements").toString() + "\n\n" +
                "dockets :" + params.get("dockets").toString() + "\n\n" +
                "siteCheckList :" + params.get("siteCheckList").toString() + "\n\n" +
                "signageAudit :" + params.get("signageAudit").toString() + "\n\n" +
                "additionalHazards :" + params.get("additionalHazards").toString() + "\n\n" +
                "parentJob :" + params.get("parentJob").toString() + "\n\n" +
                "clientEmail :" + params.get("clientEmail").toString() + "\n\n" +
                "geotag :" + "-27.4996872,153.0354553\n\n";

        try {
            File myFile = new File("/sdcard/test-" + mJobObject.getJobID() + ".txt");
            myFile.createNewFile();
            FileOutputStream fOut = new FileOutputStream(myFile);
            OutputStreamWriter myOutWriter =
                    new OutputStreamWriter(fOut);
            myOutWriter.append(hashmap);
            myOutWriter.close();
            fOut.close();
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setClient(new OkClient(okHttpClient))
                .setEndpoint(Constants.API_URL)
                .build();
        APIInterface upload = restAdapter.create(APIInterface.class);
        upload.siteChecklistSync(session.getToken(), Constants.REQUEST_JOBINFO, params, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                String jsonObjectResponse = new Gson().toJson(o);
                Log.e("Output", "" + jsonObjectResponse.toString());

                JSONObject jsonObject = null;
                dialogdd.dismiss();
                try {
                    jsonObject = new JSONObject(jsonObjectResponse.toString());
                    if (jsonObject.get("status").toString().equals("false")) {
                        Toast.makeText(getApplicationContext(), "Uploading site checklist was unsuccessful.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Site Checklist has been uploaded.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("ifsuccess", "failure: Wrong" + error.toString());
                Toast.makeText(getApplicationContext(), "Error while uploading site checklist", Toast.LENGTH_SHORT).show();
                dialogdd.dismiss();
            }
        });
    }

    public void getSignatures() {
        String temp = "";
        if (db.getTCAttachment(mJobObject.getJobID()) == null) {
        } else {
            temp = db.getTCAttachment(mJobObject.getJobID());
            try {
                JSONArray m = new JSONArray(temp);
                for (int r = 0; r < m.length(); r++) {
                    JSONObject c = m.getJSONObject(r);
                    myDataset.add(c.getString("TCName"));
                    signaturesd.add(c.getString("Attachment"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }
}
