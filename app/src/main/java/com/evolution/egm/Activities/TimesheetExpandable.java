package com.evolution.egm.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.Switch;

import com.evolution.egm.Adapters.ExpandableListAdapter;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Models.Job;
import com.evolution.egm.Models.Timesheet;
import com.evolution.egm.Models.Timesheet_Upload;
import com.evolution.egm.R;
import com.google.gson.Gson;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TimesheetExpandable extends AppCompatActivity {

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    private ArrayList<Timesheet> timesheetmem = new ArrayList<Timesheet>();
    Toolbar toolbar;
    Job mJobObject;
    DatabaseHelper db;
    Switch nosignature;
    MenuItem signaturenow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dubplicate_listview);
        db = new DatabaseHelper(this);
        initToolBar();
        Bundle extras = getIntent().getExtras();
        mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));
        expListView = (ExpandableListView) findViewById(R.id.lvExp);
        nosignature = (Switch) findViewById(R.id.nosignature);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (db.getClientStatus(mJobObject.getJobID()).equals("null")) {
            nosignature.setChecked(true);
        } else if (db.getClientStatus(mJobObject.getJobID()).equals("1")) {
            nosignature.setChecked(true);
        } else {
            nosignature.setChecked(false);
        }

        nosignature.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (db.getClientStatus(mJobObject.getJobID()).equals("null")) {
                    db.addJobStatusTable(mJobObject.getJobID());
                }
                db.updateJobClientSignatureStatus(mJobObject.getJobID(), b);

                if (b){
                    signaturenow.setVisible(true);
                }else{
                    db.timesheetsign(mJobObject.getJobID());
                    signaturenow.setVisible(false);
                    db.updateDockets("1", "null", "null", mJobObject.getJobID());
//                    db.updateDockets("2", "null", "null", mJobObject.getJobID());
                }
            }
        });

        Display newDisplay = getWindowManager().getDefaultDisplay();
        int width = newDisplay.getWidth();
        expListView.setIndicatorBounds(width - 50, width);

        prepareListData();
        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
        // setting list adapter
        expListView.setAdapter(listAdapter);

        // Listview Group click listener
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                return false;
            }
        });

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                return false;
            }
        });

    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("TimeSheet Summary");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /*
     * Preparing the list data
     */
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        timesheetmem = db.getallTimesheets(mJobObject.getJobID()).getTimesheets();

        for (int c = 0; c < timesheetmem.size(); c++) {
            listDataHeader.add(timesheetmem.get(c).getName());
            List<String> top2502 = new ArrayList<String>();

            ArrayList<Timesheet_Upload> top250 = db.getAllTimeByID(mJobObject.getJobID(), timesheetmem.get(c).getContactID());
            Gson m = new Gson();
            top2502.add(m.toJson(db.getAllTimeByID(mJobObject.getJobID(), timesheetmem.get(c).getContactID())));
            Log.e("testestset","" + m.toJson(db.getAllTimeByID(mJobObject.getJobID(), timesheetmem.get(c).getContactID())));
            listDataChild.put(listDataHeader.get(c), top2502);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.timesheetsummary, menu);
        signaturenow = (MenuItem) menu.findItem(R.id.signaturenow);

        if (db.getClientStatus(mJobObject.getJobID()).equals("null")) {
            signaturenow.setVisible(true);
        } else if (db.getClientStatus(mJobObject.getJobID()).equals("1")) {
            signaturenow.setVisible(true);
        } else {
            signaturenow.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.signaturenow:

                Intent clint = new Intent(TimesheetExpandable.this, SignatureActivity.class);
                clint.putExtra("type", "1");
                clint.putExtra("data", Parcels.wrap(mJobObject));
                startActivity(clint);

                break;
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
