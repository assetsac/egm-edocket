package com.evolution.egm.Activities.signs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.evolution.egm.Utils.CreateJobSharedPreference;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Utils.DateHelper;
import com.evolution.egm.Models.Job;
import com.evolution.egm.R;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NSWSignage extends AppCompatActivity implements View.OnTouchListener, View.OnClickListener {

    Toolbar toolbar;
    RadioButton slowlane, fastlane;
    LinearLayout slowlanel, fastlanel;
    MaterialEditText cwevolution4, cwlocallandmark4, TimeErected, TimeCollected, TimeChecked1, TimeChecked2, TimeChecked3, TimeChecked4;
    Spinner spinner;
    String Time1, Time2, Time3, Time4, Time5, Time6;
    String TimeErected2, TimeCollected2, TimeChecked12, TimeChecked22, TimeChecked32, TimeChecked42;
    DateHelper date;
    DatabaseHelper db;
    Dialog dialog;
    DatePicker datepicker;
    TimePicker timepicker;
    List<String> categories;

    final CharSequence cs1 = "-";

    Dialog.Builder builder2 = null;
    Job mJobObject;
    CreateJobSharedPreference shared;

    EditText n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41;
    EditText na1, na2, na3, na4, na5, na6, na7, na8, na9, na10, na11, na12, na13, na14, na15, na16, na17, na18, na19, na20, na21, na22, na23, na24, na25, na26, na27, na28, na29, na30, na31, na32, na33, na34, na35, na36, na37, na38, na39, na40, na41;

    String aftercares = "null";
    RadioButton afteryes, afterno;
    RadioGroup aftercare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nswsignage);
        initToolBar();

        date = new DateHelper();
        db = new DatabaseHelper(this);
        dialog = new Dialog(this, R.style.FullHeightDialog);


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));
        }
        shared = new CreateJobSharedPreference(this);

        initViews();
        initSpinner();
        populateData();

    }

    public void initSpinner() {
        // Spinner click listener
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // On selecting a spinner item
                String item = parent.getItemAtPosition(position).toString();

                // Showing selected spinner item
//                Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Spinner Drop down elements
        categories = new ArrayList<String>();
        categories.add("North");
        categories.add("South");
        categories.add("East");
        categories.add("West");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinnerlocation, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.spinnerlocation);

        spinner.setAdapter(dataAdapter);

    }

    public void initViews() {

        TimeErected = (MaterialEditText) findViewById(R.id.TimeErected);
        TimeCollected = (MaterialEditText) findViewById(R.id.TimeCollected);
        TimeChecked1 = (MaterialEditText) findViewById(R.id.TimeChecked1);
        TimeChecked2 = (MaterialEditText) findViewById(R.id.TimeChecked2);
        TimeChecked3 = (MaterialEditText) findViewById(R.id.TimeChecked3);
        TimeChecked4 = (MaterialEditText) findViewById(R.id.TimeChecked4);

        TimeErected.setOnTouchListener(this);
        TimeCollected.setOnTouchListener(this);
        TimeChecked1.setOnTouchListener(this);
        TimeChecked2.setOnTouchListener(this);
        TimeChecked3.setOnTouchListener(this);
        TimeChecked4.setOnTouchListener(this);

        spinner = (Spinner) findViewById(R.id.cwdirectionlist4);

        slowlane = (RadioButton) findViewById(R.id.slowlane);
        fastlane = (RadioButton) findViewById(R.id.fastlane);

        slowlanel = (LinearLayout) findViewById(R.id.slowlanel);
        fastlanel = (LinearLayout) findViewById(R.id.fastlanel);

        cwevolution4 = (MaterialEditText) findViewById(R.id.cwevolution4);
        cwlocallandmark4 = (MaterialEditText) findViewById(R.id.cwlocallandmark4);

        slowlane.setOnClickListener(this);
        fastlane.setOnClickListener(this);


        n1 = (EditText) findViewById(R.id.n1);
        n2 = (EditText) findViewById(R.id.n2);
        n3 = (EditText) findViewById(R.id.n3);
        n4 = (EditText) findViewById(R.id.n4);
        n5 = (EditText) findViewById(R.id.n5);
        n6 = (EditText) findViewById(R.id.n6);
        n7 = (EditText) findViewById(R.id.n7);
        n8 = (EditText) findViewById(R.id.n8);
        n9 = (EditText) findViewById(R.id.n9);
        n10 = (EditText) findViewById(R.id.n10);
        n11 = (EditText) findViewById(R.id.n11);
        n12 = (EditText) findViewById(R.id.n12);
        n13 = (EditText) findViewById(R.id.n13);
        n14 = (EditText) findViewById(R.id.n14);
        n15 = (EditText) findViewById(R.id.n15);
        n16 = (EditText) findViewById(R.id.n16);
        n17 = (EditText) findViewById(R.id.n17);
        n18 = (EditText) findViewById(R.id.n18);
        n19 = (EditText) findViewById(R.id.n19);
        n20 = (EditText) findViewById(R.id.n20);
        n21 = (EditText) findViewById(R.id.n21);
        n22 = (EditText) findViewById(R.id.n22);
        n23 = (EditText) findViewById(R.id.n23);
        n24 = (EditText) findViewById(R.id.n24);
        n25 = (EditText) findViewById(R.id.n25);
        n26 = (EditText) findViewById(R.id.n26);
        n27 = (EditText) findViewById(R.id.n27);
        n28 = (EditText) findViewById(R.id.n28);
        n29 = (EditText) findViewById(R.id.n29);
        n30 = (EditText) findViewById(R.id.n30);
        n31 = (EditText) findViewById(R.id.n31);
        n32 = (EditText) findViewById(R.id.n32);
        n33 = (EditText) findViewById(R.id.n33);
        n34 = (EditText) findViewById(R.id.n34);
        n35 = (EditText) findViewById(R.id.n35);
        n36 = (EditText) findViewById(R.id.n36);
        n37 = (EditText) findViewById(R.id.n37);
        n38 = (EditText) findViewById(R.id.n38);
        n39 = (EditText) findViewById(R.id.n39);
        n40 = (EditText) findViewById(R.id.n40);
        n41 = (EditText) findViewById(R.id.n41);

        na1 = (EditText) findViewById(R.id.na1);
        na2 = (EditText) findViewById(R.id.na2);
        na3 = (EditText) findViewById(R.id.na3);
        na4 = (EditText) findViewById(R.id.na4);
        na5 = (EditText) findViewById(R.id.na5);
        na6 = (EditText) findViewById(R.id.na6);
        na7 = (EditText) findViewById(R.id.na7);
        na8 = (EditText) findViewById(R.id.na8);
        na9 = (EditText) findViewById(R.id.na9);
        na10 = (EditText) findViewById(R.id.na10);
        na11 = (EditText) findViewById(R.id.na11);
        na12 = (EditText) findViewById(R.id.na12);
        na13 = (EditText) findViewById(R.id.na13);
        na14 = (EditText) findViewById(R.id.na14);
        na15 = (EditText) findViewById(R.id.na15);
        na16 = (EditText) findViewById(R.id.na16);
        na17 = (EditText) findViewById(R.id.na17);
        na18 = (EditText) findViewById(R.id.na18);
        na19 = (EditText) findViewById(R.id.na19);
        na20 = (EditText) findViewById(R.id.na20);
        na21 = (EditText) findViewById(R.id.na21);
        na22 = (EditText) findViewById(R.id.na22);
        na23 = (EditText) findViewById(R.id.na23);
        na24 = (EditText) findViewById(R.id.na24);
        na25 = (EditText) findViewById(R.id.na25);
        na26 = (EditText) findViewById(R.id.na26);
        na27 = (EditText) findViewById(R.id.na27);
        na28 = (EditText) findViewById(R.id.na28);
        na29 = (EditText) findViewById(R.id.na29);
        na30 = (EditText) findViewById(R.id.na30);
        na31 = (EditText) findViewById(R.id.na31);
        na32 = (EditText) findViewById(R.id.na32);
        na33 = (EditText) findViewById(R.id.na33);
        na34 = (EditText) findViewById(R.id.na34);
        na35 = (EditText) findViewById(R.id.na35);
        na36 = (EditText) findViewById(R.id.na36);
        na37 = (EditText) findViewById(R.id.na37);
        na38 = (EditText) findViewById(R.id.na38);
        na39 = (EditText) findViewById(R.id.na39);
        na40 = (EditText) findViewById(R.id.na40);
        na41 = (EditText) findViewById(R.id.na41);

        afteryes = (RadioButton) findViewById(R.id.afteryes);
        afterno = (RadioButton) findViewById(R.id.afterno);
        aftercare = (RadioGroup) findViewById(R.id.aftercarer);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_qldsignages, menu);
        return true;
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Signage Audit");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.saveqld:

//                if (checkifEmpty(cwevolution4, "Rego No. is required.")) {
//                } else {
//                    if (checkifEmpty(cwlocallandmark4, "Local Landmark is required.")) {
//                    } else {
//                        if (checkifEmpty(TimeErected, "Time Erected is required.")) {
//                        } else {
//                            if (checkifEmpty(TimeChecked1, "Time Collected is required.")) {
//                            } else {
//                                if (checkifEmpty(TimeChecked2, "Time Checked is required.")) {
//                                } else {
//                                    if (checkifEmpty(TimeChecked3, "Time Checked is required.")) {
//                                    } else {
//                                        if (checkifEmpty(TimeChecked4, "Time Checked is required.")) {
//                                        } else {
//                                            if (checkifEmpty(TimeCollected, "Time Checked is required.")) {
//                                            } else {
                saveSings();
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }

                break;
            case android.R.id.home:
                finish();
                overridePendingTransition(0, 0);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean checkifEmpty(MaterialEditText isEmpty, String messageError) {
        boolean temp = false;
        if (isEmpty.getText().toString().equals("")) {
            temp = true;
            isEmpty.setError(messageError);
        }
        return temp;
    }

    public void timedate(final int to, String whoClicked) {

        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.activity_timesheetpicker);

        datepicker = (DatePicker) dialog.findViewById(R.id.datePicker);
        datepicker.setVisibility(datepicker.GONE);
        timepicker = (TimePicker) dialog.findViewById(R.id.timePicker);

        LinearLayout save = (LinearLayout) dialog.findViewById(R.id.save);
        LinearLayout cancel = (LinearLayout) dialog.findViewById(R.id.cancel);
        TextView title = (TextView) dialog.findViewById(R.id.title);
        title.setText(whoClicked);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (to) {
                    case 1: {
                        TimeErected.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeErected2 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                    case 2: {
                        TimeCollected.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeCollected2 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                    case 3: {
                        TimeChecked1.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeChecked12 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                    case 4: {
                        TimeChecked2.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeChecked22 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                    case 5: {
                        TimeChecked3.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeChecked32 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                    case 6: {
                        TimeChecked4.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeChecked42 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    public boolean checkifhave(String toValidate) {
        boolean temp = false;
        boolean ishave = toValidate.contains(cs1);
        if (ishave) {
            temp = true;
        }
        return temp;
    }


    public String getTimeDifference(String first, String second) {
        long diffMinutes = 0;
        long diffHours = 0;
        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(first);
            d2 = format.parse(second);
            long diff = d2.getTime() - d1.getTime();
            diffMinutes = diff / (60 * 1000) % 60;
            diffHours = diff / (60 * 60 * 1000);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return diffHours + "h  " + diffMinutes + "m";
    }

    public String getcontetn(String gets, int position) {
        String getz = "";
        String[] splitters = gets.split("~");

        String abc = splitters[position].replace("^", "#");
        String[] splitter = abc.split("#");

        if (splitter.length >= 2) {
            getz = splitter[1];
        } else {
            getz = "";
        }

        return getz.replace("Metres:", "");
    }


    public Long generateMili(DatePicker datem, TimePicker timem) {
        int day = datem.getDayOfMonth();
        int month = datem.getMonth() + 1;
        int year = datem.getYear();
        String returns = day + " " + month + " " + year + " " + timem.getCurrentHour() + ":" + timem.getCurrentMinute() + "";
        return Long.parseLong(convertDate(returns));
    }

    public String convertDate(String date) {
        String temp = "";
        long timeInMilliseconds = 0;
        String givenDateString = date;
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
        try {
            Date mDate = sdf.parse(givenDateString);
            timeInMilliseconds = mDate.getTime();
            System.out.println("Date in milli :: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds + "";
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.TimeErected: {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    initTimepicker(1);
                }
                return true;
            }
            case R.id.TimeCollected:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeChecked4.getText().toString().equals("")) {
                        Toast.makeText(NSWSignage.this, "Please add Time Checked.", Toast.LENGTH_SHORT).show();
                    } else {
                        initTimepicker(2);
                    }
                }
                return true;
            case R.id.TimeChecked1:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeErected.getText().toString().equals("")) {
                        Toast.makeText(NSWSignage.this, "Please add Time Collected.", Toast.LENGTH_SHORT).show();
                    } else {
                        initTimepicker(3);
                    }
                }
                return true;
            case R.id.TimeChecked2:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeChecked1.getText().toString().equals("")) {
                        Toast.makeText(NSWSignage.this, "Please add Time Checked.", Toast.LENGTH_SHORT).show();
                    } else {
                        initTimepicker(4);
                    }
                }
                return true;
            case R.id.TimeChecked3:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeChecked2.getText().toString().equals("")) {
                        Toast.makeText(NSWSignage.this, "Please add Time Checked.", Toast.LENGTH_SHORT).show();
                    } else {
                        initTimepicker(5);
                    }
                }
                return true;
            case R.id.TimeChecked4:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeChecked3.getText().toString().equals("")) {
                        Toast.makeText(NSWSignage.this, "Please add Time Checked.", Toast.LENGTH_SHORT).show();
                    } else {
                        initTimepicker(6);
                    }
                }
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.slowlane:
                slowlanel.setVisibility(slowlanel.VISIBLE);
                fastlanel.setVisibility(slowlanel.GONE);
                break;
            case R.id.fastlane:
                slowlanel.setVisibility(slowlanel.GONE);
                fastlanel.setVisibility(slowlanel.VISIBLE);
                break;
        }
    }


    public static String hourConverter(String dime) {
        Log.e("tsetses", dime);

        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm:ss a");
        Date date = null;
        try {
            date = parseFormat.parse(dime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return displayFormat.format(date) + "";
    }

    public void initTimepicker(final int to) {
        builder2 = new TimePickerDialog.Builder(6, 00) {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                final TimePickerDialog dialogr = (TimePickerDialog) fragment.getDialog();
                switch (to) {
                    case 1:
                        if (!TimeChecked1.getText().toString().equals("")) {
                            if (checkifhave(getTimeDifference(dialogr.getFormattedTime(DateFormat.getDateTimeInstance()), TimeErected2))) {
                                Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                            } else {
                                TimeErected.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                                TimeErected2 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());
                                Time1 = convertDate(TimeErected2);
                            }
                        } else {
                            TimeErected.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                            TimeErected2 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());
                            Time1 = convertDate(TimeErected2);
                        }
                        break;
                    case 3:
                        TimeChecked12 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());

                        if (checkifhave(getTimeDifference(TimeErected2, TimeChecked12))) {
                            TimeChecked12 = "";
                            Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                        } else {
                            Time2 = convertDate(TimeChecked12);
                            TimeChecked1.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                        }
                        break;
                    case 4:
                        TimeChecked22 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());
                        if (checkifhave(getTimeDifference(TimeChecked12, TimeChecked22))) {
                            TimeChecked22 = "";
                            Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                        } else {
                            Time3 = convertDate(TimeChecked22);
                            TimeChecked2.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                        }
                        break;
                    case 5:
                        TimeChecked32 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());
                        if (checkifhave(getTimeDifference(TimeChecked22, TimeChecked32))) {
                            TimeChecked32 = "";
                            Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                        } else {
                            Time4 = convertDate(TimeChecked32);
                            TimeChecked3.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                        }
                        break;
                    case 6:
                        TimeChecked42 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());

                        if (checkifhave(getTimeDifference(TimeChecked32, TimeChecked42))) {
                            TimeChecked42 = "";
                            Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                        } else {
                            Time5 = convertDate(TimeChecked42);
                            TimeChecked4.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                        }
                        break;
                    case 2:
                        TimeCollected2 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());

                        if (checkifhave(getTimeDifference(TimeChecked42, TimeCollected2))) {
                            TimeCollected2 = "";
                            Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                        } else {
                            Time6 = convertDate(TimeCollected2);
                            TimeCollected.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                        }
                        break;
                }

                super.onPositiveActionClicked(fragment);
                super.onNegativeActionClicked(fragment);

            }
        };

        builder2.positiveAction("OK")
                .negativeAction("CANCEL");

        DialogFragment fragment2 = DialogFragment.newInstance(builder2);
        fragment2.show(getSupportFragmentManager(), null);

    }

    public void saveSings() {


        int radioButtonID2 = aftercare.getCheckedRadioButtonId();
        RadioButton rb2 = (RadioButton) aftercare.findViewById(radioButtonID2);

        if (aftercare.getCheckedRadioButtonId() != -1) {
            aftercares = aftercare.indexOfChild(rb2) + "";
        }

        String re = "[" +
                "{" +
                "\"JobID\": \"" + mJobObject.getJobID() + "\"," +
                "\"RegoNo\": \"" + cwevolution4.getText().toString() + "\"," +
                "\"Landmark\": \"" + cwlocallandmark4.getText().toString() + "\"," +
                "\"CarriageWay\": \"1\"," +
                "\"Direction\": \"" + spinner.getSelectedItemPosition() + "\"," +
                "    \"TimeErected\": \"" + Time1 + "\",\n" +
                "    \"TimeCollected\": \"" + Time6 + "\",\n" +
                "    \"TimeChecked1\": \"" + Time2 + "\",\n" +
                "    \"TimeChecked2\": \"" + Time3 + "\",\n" +
                "    \"TimeChecked3\": \"" + Time4 + "\",\n" +
                "    \"TimeChecked4\": \"" + Time5 + "\",\n" +
                "    \"aftercare\": \"" + aftercares + "\",\n" +
                "    \"AuditSigns\": \"SignID:1^Metres:" + n1.getText() + "" +
                "~SignID:2^Metres:" + n2.getText().toString() + "~SignID:3^Metres:" + n3.getText() + "~SignID:4^Metres:" + n4.getText() + "~SignID:5^Metres:" + n5.getText() + "~" +
                "SignID:6^Metres:" + n6.getText() + "~SignID:7^Metres:" + n7.getText() + "~SignID:8^Metres:" + n8.getText() + "~SignID:9^Metres:" + n9.getText() + "~" +
                "SignID:10^Metres:" + n10.getText() + "~SignID:11^Metres:" + n11.getText() + "~SignID:12^Metres:" + n12.getText() + "~SignID:13^Metres:" + n13.getText() + "~" +
                "SignID:14^Metres:" + n14.getText() + "~SignID:15^Metres:" + n15.getText() + "~SignID:16^Metres:" + n16.getText() + "~SignID:17^Metres:" + n17.getText() + "~" +
                "SignID:18^Metres:" + n18.getText() + "~SignID:19^Metres:" + n19.getText() + "~SignID:20^Metres:" + n20.getText() + "~SignID:21^Metres:" + n21.getText() + "~" +
                "SignID:22^Metres:" + n22.getText() + "~SignID:23^Metres:" + n23.getText() + "~SignID:24^Metres:" + n24.getText() + "~SignID:25^Metres:" + n25.getText() + "~" +
                "SignID:26^Metres:" + n26.getText() + "~SignID:27^Metres:" + n27.getText() + "~SignID:28^Metres:" + n28.getText() + "~SignID:29^Metres:" + n29.getText() + "~" +
                "SignID:30^Metres:" + n30.getText() + "~SignID:31^Metres:" + n31.getText() + "~SignID:32^Metres:" + n32.getText() + "~SignID:33^Metres:" + n33.getText() + "~" +
                "SignID:34^Metres:" + n34.getText() + "~SignID:35^Metres:" + n35.getText() + "~SignID:36^Metres:" + n36.getText() + "~SignID:37^Metres:" + n37.getText() + "~" +
                "SignID:38^Metres:" + n38.getText() + "~SignID:39^Metres:" + n39.getText() + "~SignID:40^Metres:" + n40.getText() + "~SignID:41^Metres:" + n41.getText() +
                "\"}," + "{" +
                "\"JobID\": \"" + mJobObject.getJobID() + "\",\n" +
                "\"RegoNo\": \"" + cwevolution4.getText().toString() + "\",\n" +
                "\"Landmark\": \"" + cwlocallandmark4.getText().toString() + "\",\n" +
                "\"CarriageWay\":\"2\"," +
                "\"Direction\": \"" + spinner.getSelectedItemPosition() + "\",\n" +
                "    \"TimeErected\": \"" + Time1 + "\",\n" +
                "    \"TimeCollected\": \"" + Time6 + "\",\n" +
                "    \"TimeChecked1\": \"" + Time2 + "\",\n" +
                "    \"TimeChecked2\": \"" + Time3 + "\",\n" +
                "    \"TimeChecked3\": \"" + Time4 + "\",\n" +
                "    \"TimeChecked4\": \"" + Time5 + "\",\n" +
                "    \"aftercare\": \"" + aftercares + "\",\n" +
                "    \"AuditSigns\": \"SignID:1^Metres:" + na1.getText() + "" +
                "~SignID:2^Metres:" + na2.getText().toString() + "~SignID:3^Metres:" + na3.getText() + "~SignID:4^Metres:" + na4.getText() + "~SignID:5^Metres:" + na5.getText() + "~" +
                "SignID:6^Metres:" + na6.getText() + "~SignID:7^Metres:" + na7.getText() + "~SignID:8^Metres:" + na8.getText() + "~SignID:9^Metres:" + na9.getText() + "~" +
                "SignID:10^Metres:" + na10.getText() + "~SignID:11^Metres:" + na11.getText() + "~SignID:12^Metres:" + na12.getText() + "~SignID:13^Metres:" + na13.getText() + "~" +
                "SignID:14^Metres:" + na14.getText() + "~SignID:15^Metres:" + na15.getText() + "~SignID:16^Metres:" + na16.getText() + "~SignID:17^Metres:" + na17.getText() + "~" +
                "SignID:18^Metres:" + na18.getText() + "~SignID:19^Metres:" + na19.getText() + "~SignID:20^Metres:" + na20.getText() + "~SignID:21^Metres:" + na21.getText() + "~" +
                "SignID:22^Metres:" + na22.getText() + "~SignID:23^Metres:" + na23.getText() + "~SignID:24^Metres:" + na24.getText() + "~SignID:25^Metres:" + na25.getText() + "~" +
                "SignID:26^Metres:" + na26.getText() + "~SignID:27^Metres:" + na27.getText() + "~SignID:28^Metres:" + na28.getText() + "~SignID:29^Metres:" + na29.getText() + "~" +
                "SignID:30^Metres:" + na30.getText() + "~SignID:31^Metres:" + na31.getText() + "~SignID:32^Metres:" + na32.getText() + "~SignID:33^Metres:" + na33.getText() + "~" +
                "SignID:34^Metres:" + na34.getText() + "~SignID:35^Metres:" + na35.getText() + "~SignID:36^Metres:" + na36.getText() + "~SignID:37^Metres:" + na37.getText() + "~" +
                "SignID:38^Metres:" + na38.getText() + "~SignID:39^Metres:" + na39.getText() + "~SignID:40^Metres:" + na40.getText() + "~SignID:41^Metres:" + na41.getText() +
                "\"}\n" +
                "]";

        if (db.checkExistCreateJObs(mJobObject.getJobID()) == null) {
            db.addCreateJobs(mJobObject.getJobID());
        }

        db.updateContact(mJobObject.getJobID(), "", "", "", re.toString(), "");
        Toast.makeText(NSWSignage.this, "Changes have been saved!", Toast.LENGTH_SHORT).show();
    }

    public String getSignageAudit() {
        String temp = "";
        temp = "{\"mac\":" + db.getSignageAudt(mJobObject.getJobID()) + "}";
        return temp;
    }


    public void populateData() {
        JSONArray data = null;
        Log.e("haha", getSignageAudit());
        if (getSignageAudit().equals("{\"mac\":null}")) {

        } else {
            try {
                JSONObject jsonObj = new JSONObject(getSignageAudit());

                // Getting JSON Array node
                data = jsonObj.getJSONArray("mac");

                JSONObject c = data.getJSONObject(0);
                JSONObject x = data.getJSONObject(1);

                Log.e("JOB ID", c.getString("JobID"));
                cwevolution4.setText(c.getString("RegoNo"));
                cwlocallandmark4.setText(c.getString("Landmark"));

                spinner.setSelection(Integer.parseInt(c.getString("Direction")), true);

                if (!c.getString("TimeErected").equals("null")) {
                    TimeErected.setText(date.miliSecondConverter(c.getString("TimeErected"), "HH:mm:ss a"));
                    TimeErected2 = c.getString("TimeErected");
                    Time1 = c.getString("TimeErected");
                }
                if (!c.getString("TimeCollected").equals("null")) {
                    TimeCollected.setText(date.miliSecondConverter(c.getString("TimeCollected"), "HH:mm:ss a"));
                    TimeCollected2 = c.getString("TimeCollected");
                    Time6 = c.getString("TimeErected");
                }
                if (!c.getString("TimeChecked1").equals("null")) {
                    TimeChecked1.setText(date.miliSecondConverter(c.getString("TimeChecked1"), "HH:mm:ss a"));
                    TimeChecked12 = c.getString("TimeChecked1");
                    Time2 = c.getString("TimeChecked1");
                }
                if (!c.getString("TimeChecked2").equals("null")) {
                    TimeChecked2.setText(date.miliSecondConverter(c.getString("TimeChecked2"), "HH:mm:ss a"));
                    TimeChecked22 = c.getString("TimeChecked2");
                    Time3 = c.getString("TimeChecked2");
                }
                if (!c.getString("TimeChecked3").equals("null")) {
                    TimeChecked3.setText(date.miliSecondConverter(c.getString("TimeChecked3"), "HH:mm:ss a"));
                    TimeChecked32 = c.getString("TimeChecked3");
                    Time4 = c.getString("TimeChecked3");
                }
                if (!c.getString("TimeChecked4").equals("null")) {
                    TimeChecked4.setText(date.miliSecondConverter(c.getString("TimeChecked4"), "HH:mm:ss a"));
                    TimeChecked42 = c.getString("TimeChecked4");
                    Time5 = c.getString("TimeChecked3");
                }

                if (!c.getString("aftercare").equals("null")) {
                    ((RadioButton) aftercare.getChildAt(Integer.parseInt(c.getString("aftercare")))).setChecked(true);
                }

                n1.setText(getcontetn(c.getString("AuditSigns"), 0));
                n2.setText(getcontetn(c.getString("AuditSigns"), 1));
                n3.setText(getcontetn(c.getString("AuditSigns"), 2));
                n4.setText(getcontetn(c.getString("AuditSigns"), 3));
                n5.setText(getcontetn(c.getString("AuditSigns"), 4));
                n6.setText(getcontetn(c.getString("AuditSigns"), 5));
                n7.setText(getcontetn(c.getString("AuditSigns"), 6));
                n8.setText(getcontetn(c.getString("AuditSigns"), 7));
                n9.setText(getcontetn(c.getString("AuditSigns"), 8));
                n10.setText(getcontetn(c.getString("AuditSigns"), 9));
                n11.setText(getcontetn(c.getString("AuditSigns"), 10));
                n12.setText(getcontetn(c.getString("AuditSigns"), 11));
                n13.setText(getcontetn(c.getString("AuditSigns"), 12));
                n14.setText(getcontetn(c.getString("AuditSigns"), 13));
                n15.setText(getcontetn(c.getString("AuditSigns"), 14));
                n16.setText(getcontetn(c.getString("AuditSigns"), 15));
                n17.setText(getcontetn(c.getString("AuditSigns"), 16));
                n18.setText(getcontetn(c.getString("AuditSigns"), 17));
                n19.setText(getcontetn(c.getString("AuditSigns"), 18));
                n20.setText(getcontetn(c.getString("AuditSigns"), 19));
                n21.setText(getcontetn(c.getString("AuditSigns"), 20));
                n22.setText(getcontetn(c.getString("AuditSigns"), 21));
                n23.setText(getcontetn(c.getString("AuditSigns"), 22));
                n24.setText(getcontetn(c.getString("AuditSigns"), 23));
                n25.setText(getcontetn(c.getString("AuditSigns"), 24));
                n26.setText(getcontetn(c.getString("AuditSigns"), 25));
                n27.setText(getcontetn(c.getString("AuditSigns"), 26));
                n28.setText(getcontetn(c.getString("AuditSigns"), 27));
                n29.setText(getcontetn(c.getString("AuditSigns"), 28));
                n30.setText(getcontetn(c.getString("AuditSigns"), 29));
                n31.setText(getcontetn(c.getString("AuditSigns"), 30));
                n32.setText(getcontetn(c.getString("AuditSigns"), 31));
                n33.setText(getcontetn(c.getString("AuditSigns"), 32));
                n34.setText(getcontetn(c.getString("AuditSigns"), 33));
                n35.setText(getcontetn(c.getString("AuditSigns"), 34));
                n36.setText(getcontetn(c.getString("AuditSigns"), 35));
                n37.setText(getcontetn(c.getString("AuditSigns"), 36));
                n38.setText(getcontetn(c.getString("AuditSigns"), 37));
                n39.setText(getcontetn(c.getString("AuditSigns"), 38));
                n40.setText(getcontetn(c.getString("AuditSigns"), 39));
                n41.setText(getcontetn(c.getString("AuditSigns"), 40));

                na1.setText(getcontetn(x.getString("AuditSigns"), 0));
                na2.setText(getcontetn(x.getString("AuditSigns"), 1));
                na3.setText(getcontetn(x.getString("AuditSigns"), 2));
                na4.setText(getcontetn(x.getString("AuditSigns"), 3));
                na5.setText(getcontetn(x.getString("AuditSigns"), 4));
                na6.setText(getcontetn(x.getString("AuditSigns"), 5));
                na7.setText(getcontetn(x.getString("AuditSigns"), 6));
                na8.setText(getcontetn(x.getString("AuditSigns"), 7));
                na9.setText(getcontetn(x.getString("AuditSigns"), 8));
                na10.setText(getcontetn(x.getString("AuditSigns"), 9));
                na11.setText(getcontetn(x.getString("AuditSigns"), 10));
                na12.setText(getcontetn(x.getString("AuditSigns"), 11));
                na13.setText(getcontetn(x.getString("AuditSigns"), 12));
                na14.setText(getcontetn(x.getString("AuditSigns"), 13));
                na15.setText(getcontetn(x.getString("AuditSigns"), 14));
                na16.setText(getcontetn(x.getString("AuditSigns"), 15));
                na17.setText(getcontetn(x.getString("AuditSigns"), 16));
                na18.setText(getcontetn(x.getString("AuditSigns"), 17));
                na19.setText(getcontetn(x.getString("AuditSigns"), 18));
                na20.setText(getcontetn(x.getString("AuditSigns"), 19));
                na21.setText(getcontetn(x.getString("AuditSigns"), 20));
                na22.setText(getcontetn(x.getString("AuditSigns"), 21));
                na23.setText(getcontetn(x.getString("AuditSigns"), 22));
                na24.setText(getcontetn(x.getString("AuditSigns"), 23));
                na25.setText(getcontetn(x.getString("AuditSigns"), 24));
                na26.setText(getcontetn(x.getString("AuditSigns"), 25));
                na27.setText(getcontetn(x.getString("AuditSigns"), 26));
                na28.setText(getcontetn(x.getString("AuditSigns"), 27));
                na29.setText(getcontetn(x.getString("AuditSigns"), 28));
                na30.setText(getcontetn(x.getString("AuditSigns"), 29));
                na31.setText(getcontetn(x.getString("AuditSigns"), 30));
                na32.setText(getcontetn(x.getString("AuditSigns"), 31));
                na33.setText(getcontetn(x.getString("AuditSigns"), 32));
                na34.setText(getcontetn(x.getString("AuditSigns"), 33));
                na35.setText(getcontetn(x.getString("AuditSigns"), 34));
                na36.setText(getcontetn(x.getString("AuditSigns"), 35));
                na37.setText(getcontetn(x.getString("AuditSigns"), 36));
                na38.setText(getcontetn(x.getString("AuditSigns"), 37));
                na39.setText(getcontetn(x.getString("AuditSigns"), 38));
                na40.setText(getcontetn(x.getString("AuditSigns"), 39));
                na41.setText(getcontetn(x.getString("AuditSigns"), 40));


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }


}
