package com.evolution.egm.Activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Models.Job;
import com.evolution.egm.Models.PhotoItem;
import com.evolution.egm.R;
import com.rey.material.app.Dialog;

import org.parceler.Parcels;

import java.util.ArrayList;

public class Photoview extends AppCompatActivity {
    Toolbar toolbar;
    public int imageposition;
    ImageView img;
    TextView textView14;
    CustomPagerAdapter mCustomPagerAdapter;
    ViewPager mViewPager;
    DatabaseHelper db;
    Job mJobObject;
    Dialog dialog;
    public ArrayList<PhotoItem> photo = new ArrayList<PhotoItem>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_photoview);
        img = (ImageView) findViewById(R.id.imageView3);
        textView14 = (TextView) findViewById(R.id.textView14);
        initToolBar();

        Bundle extras = getIntent().getExtras();
        db = new DatabaseHelper(this);

        if (extras != null) {
            imageposition = Integer.parseInt(extras.getString("position"));
            mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));
        }

        photo = db.getAllPhotos(mJobObject.getJobID());
        mCustomPagerAdapter = new CustomPagerAdapter(photo, this);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mCustomPagerAdapter);
        mViewPager.setCurrentItem(imageposition);

        dialog = new Dialog(this, R.style.FullHeightDialog);
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Photo");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_image_views, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                
                dialog.show();
                dialog.setCanceledOnTouchOutside(false);
                dialog.setContentView(R.layout.activity_deletephoto);
                dialog.contentMargin(0, 0, 0, -20);

                LinearLayout save = (LinearLayout) dialog.findViewById(R.id.save);
                LinearLayout cancel = (LinearLayout) dialog.findViewById(R.id.cancel);

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        db.removeImageById(photo.get(mViewPager.getCurrentItem()).getImage_url());
                        dialog.dismiss();
                        finish();
                    }
                });

                return true;
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    class CustomPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;
        ArrayList<PhotoItem> item;

        public CustomPagerAdapter(ArrayList<PhotoItem> item, Context context) {
            mContext = context;
            this.item = item;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return item.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);
            PhotoItem m = new PhotoItem();
            ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
            TextView textView14 = (TextView) itemView.findViewById(R.id.textView14);

            Glide.with(getApplicationContext()).load(item.get(position).getImage_url()).into(imageView);
            textView14.setText(item.get(position).getImage_url());
            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }

}





