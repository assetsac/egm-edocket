package com.evolution.egm.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.evolution.egm.Adapters.Employeelist_Duplicate;
import com.evolution.egm.Utils.ConnectionDetector;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Utils.SessionManager;
import com.evolution.egm.Models.Timesheet;
import com.evolution.egm.Models.Timesheet_Upload;
import com.evolution.egm.R;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;

import java.util.ArrayList;

public class EmployeeList extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView mRecyclerView;
    private Employeelist_Duplicate mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    Toolbar toolbar;
    Timesheet item;
    Timesheet mJobObject;
    private String token;
    ConnectionDetector connection;
    DatabaseHelper db;
    public SessionManager session;
    LinearLayout duplicate;

    Timesheet_Upload mTimesheet;
    String id = "";
    int position = 0;
    String status = "";
    private ArrayList<Timesheet> timesheetmem = new ArrayList<Timesheet>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_list);
        Intent intent = getIntent();
        session = new SessionManager(this);
        token = session.getToken();
        connection = new ConnectionDetector(this);

        initToolBar();
        db = new DatabaseHelper(getApplicationContext());
        id = intent.getStringExtra("ID");
        duplicate = (LinearLayout) findViewById(R.id.duplicate);
        duplicate.setOnClickListener(this);
        position = Integer.parseInt(intent.getStringExtra("position"));
        status = intent.getStringExtra("status");
        getAllEmployee();
        getJobInfo();
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Employee List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void getAllEmployee() {
        mRecyclerView = (RecyclerView) findViewById(R.id.fleetmembers);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
    }

    public void getJobInfo() {
        timesheetmem = db.getallTimesheets(id).getTimesheets();
        mAdapter = new Employeelist_Duplicate(db.getallTimesheets(id).getTimesheets(), getApplicationContext(), db.getallTimesheets(id).getTimesheets().get(position).getContactID());
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.removeItem(position);
    }

    public void duplicatedData() {

        for (int c = 0; c < mAdapter.getallSelectedEmployee().size(); c++) {
            if (mAdapter.getallSelectedEmployee().get(c).isSelected()) {
                ArrayList<Timesheet_Upload> timetoDubplicate = db.getAllTimeByID(mAdapter.getallSelectedEmployee().get(0).getJobID(), mAdapter.getWorkerID());
                mTimesheet = new Timesheet_Upload();

                mTimesheet.setJobID(mAdapter.getallSelectedEmployee().get(0).getJobID());
                mTimesheet.setWorkerID(mAdapter.getallSelectedEmployee().get(c).getContactID());
                mTimesheet.setShiftID(timetoDubplicate.get(0).getShiftID());
                mTimesheet.setTravelStartDateTime(timetoDubplicate.get(0).getTravelStartDateTime());
                mTimesheet.setTravelStartDateTimeEnd(timetoDubplicate.get(0).getTravelStartDateTimeEnd());
                mTimesheet.setTravelStartDistance(timetoDubplicate.get(0).getTravelStartDistance());
                mTimesheet.setJobStartDateTime(timetoDubplicate.get(0).getJobStartDateTime());
                mTimesheet.setJobFinishDateTime(timetoDubplicate.get(0).getJobFinishDateTime());
                mTimesheet.setBreakStartDateTime(timetoDubplicate.get(0).getBreakStartDateTime());
                mTimesheet.setBreakFinishDateTime(timetoDubplicate.get(0).getBreakFinishDateTime());
                mTimesheet.setTravelFinishDateTimeStart(timetoDubplicate.get(0).getTravelFinishDateTimeStart());
                mTimesheet.setTravelFinishDateTime(timetoDubplicate.get(0).getTravelFinishDateTime());
                mTimesheet.setTravelFinishDistance(timetoDubplicate.get(0).getTravelFinishDistance());
                mTimesheet.setFatigueCompliance(timetoDubplicate.get(0).getFatigueCompliance());
                mTimesheet.setAttendDepot(timetoDubplicate.get(0).getAttendDepot());
                mTimesheet.setIsHaveMeal(timetoDubplicate.get(0).getIsHaveMeal());
                if(!status.equals("Driver")){
                    mTimesheet.setRegoNo(timetoDubplicate.get(0).getRegoNo());
                    mTimesheet.setTraveledKilometers(timetoDubplicate.get(0).getTraveledKilometers());
                }else{
                    mTimesheet.setRegoNo("");
                    mTimesheet.setTraveledKilometers("");
                }
                db.addUpdateToUploadTimeSheet(mTimesheet);
                db.removeSigTIme(mAdapter.getallSelectedEmployee().get(0).getJobID(), mAdapter.getallSelectedEmployee().get(c).getContactID());
            }
        }
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.duplicatedata:
                int b = 0;
                for (int d = 0; d < mAdapter.getallSelectedEmployee().size(); d++) {
                    if (mAdapter.getallSelectedEmployee().get(d).isSelected()) {
                        b++;
                        Log.e("size", "" + d + "@@" + mAdapter.getallSelectedEmployee().size());
                    } else {
                        Log.e("size", "" + d + "@@d" + mAdapter.getallSelectedEmployee().size());
                    }
                }
                if (b > 0) {
                    duplicate();
                } else {
                    Toast.makeText(EmployeeList.this, "Please select employee to proceed.", Toast.LENGTH_SHORT).show();
                }
                break;
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void duplicate() {

        Dialog.Builder builder = null;
        builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                duplicatedData();
                Toast.makeText(EmployeeList.this, "Changes have been saved.", Toast.LENGTH_SHORT).show();
                super.onPositiveActionClicked(fragment);
            }
            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                super.onNegativeActionClicked(fragment);
            }
        };

        ((SimpleDialog.Builder) builder).message("Dou you want to duplicate the timesheet?")
                .title("Duplicate Timesheet")
                .positiveAction("DUPLICATE")
                .negativeAction("CANCEL");

        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(getSupportFragmentManager(), null);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.duplicate:

                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_employee_list, menu);
        return true;
    }

}
