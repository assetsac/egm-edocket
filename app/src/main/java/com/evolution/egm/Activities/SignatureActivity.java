package com.evolution.egm.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Utils.Encoder;
import com.evolution.egm.Models.Job;
import com.evolution.egm.R;
import com.github.gcacace.signaturepad.views.SignaturePad;

import org.parceler.Parcels;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignatureActivity extends ActionBarActivity implements View.OnClickListener {
    Button save, clear, cancel;
    private SignaturePad mSignaturePad;
    Toolbar toolbar;
    String type;
    Job mJobObject;
    DatabaseHelper db;
    ImageView signatureview;
    MenuItem saved;
    Bitmap decodedByte;
    String v4;
    com.rengwuxian.materialedittext.MaterialEditText signame, sigemail;
    boolean signed = false;
    boolean issigned = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signature);
        initToolBar();
        clear = (Button) findViewById(R.id.clear);
        clear.setOnClickListener(this);
        Bundle extras = getIntent().getExtras();
        String userName;

        signame = (com.rengwuxian.materialedittext.MaterialEditText) findViewById(R.id.signame);
        sigemail = (com.rengwuxian.materialedittext.MaterialEditText) findViewById(R.id.sigemail);
        signame.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                issigned = true;
            }
        });

        sigemail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                issigned = true;
            }
        });

        if (extras != null) {
            mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));
            type = extras.getString("type");
            if (type.equals("2")) {
                signame.setVisibility(signame.GONE);
                sigemail.setVisibility(sigemail.GONE);
            }
        }

        signatureview = (ImageView) findViewById(R.id.signatureview);

        db = new DatabaseHelper(this);
        mSignaturePad = (SignaturePad) findViewById(R.id.signaturearea);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onSigned() {
                signed = true;
                clear.setEnabled(true);
                saved.setEnabled(true);
                issigned = true;
            }

            @Override
            public void onClear() {
                signed = false;
                clear.setEnabled(false);
                saved.setEnabled(false);
                issigned = false;
            }
        });
    }

    public void initToolBar() {

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Signature Area");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        populateSignature();
        populateNameEmail();

    }

    public void populateSignature() {

        String timeshet = db.gelSingleDocket(mJobObject.getJobID(), "1");
        String abc = timeshet.replace("|~~~~|", "###");
        String[] splitter = abc.split("###");

        Log.e("timesheet1", timeshet);

        String timeshet2 = db.gelSingleDocket(mJobObject.getJobID(), "2");
        String abc2 = timeshet2.replace("|~~~~|", "###");
        String[] splitter2 = abc2.split("###");

        Log.e("timesheet2", timeshet2);

        if (type.equals("1")) {
            if (!splitter[4].equals("null")) {
                Log.e("sig", splitter[4]);
                v4 = splitter[4];
                byte[] decodedString = Base64.decode(splitter[4], Base64.DEFAULT);
                decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                signatureview.setImageBitmap(decodedByte);
                signatureview.setVisibility(signatureview.VISIBLE);
                clear.setEnabled(true);
            }
        }
        if (type.equals("2")) {
            if (!splitter2[4].equals("null")) {
                v4 = splitter[4];
                byte[] decodedString = Base64.decode(splitter2[4], Base64.DEFAULT);
                decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                signatureview.setImageBitmap(decodedByte);
                signatureview.setVisibility(signatureview.VISIBLE);
                clear.setEnabled(true);
            }
        }
    }

    private boolean validMail(String yourEmailString) {
        Pattern emailPattern = Pattern.compile(".+@.+\\.[a-z]+");
        Matcher emailMatcher = emailPattern.matcher(yourEmailString);
        return emailMatcher.matches();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_signature, menu);
        saved = (MenuItem) menu.findItem(R.id.save);
        if (issigned) {
            saved.setEnabled(true);
        } else {
            saved.setEnabled(false);
        }
        return true;
    }

    public void populateNameEmail() {

        if (!db.getotherfield(mJobObject.getJobID()).equals("wala")) {
            String data = db.getotherfield(mJobObject.getJobID());
            String[] arr = data.split("~");
            signame.setText(arr[1].toString());
            sigemail.setText(arr[2].toString());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {

            case R.id.save:
                if (type.equals("1")) {
                    if ((!signame.getText().toString().equals("")) && !sigemail.getText().toString().equals("")) {
                        if (validMail(sigemail.getText().toString())) {
                            if (db.getotherfield(mJobObject.getJobID()).equals("wala")) {
                                db.addotherfield(mJobObject.getJobID(), signame.getText().toString(), sigemail.getText().toString());
                            } else {
                                db.updateotherfield(mJobObject.getJobID(), signame.getText().toString(), sigemail.getText().toString());
                            }
                            if (signed) {
                                Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                                String m = Encoder.encodeTobase64(signatureBitmap);
                                String filename = "Signature_" + System.currentTimeMillis() + ".jpg";
                                if (addSignatureToGallery(signatureBitmap, filename)) {
                                    Toast.makeText(getApplicationContext(), "Changes have been saved.", Toast.LENGTH_SHORT).show();
                                    createDockets(m);
                                    finish();
                                } else {
                                    Toast.makeText(SignatureActivity.this, "Unable to store the signature", Toast.LENGTH_SHORT).show();
                                    createDockets(m);
                                }
                            } else {
                                if (clear.isEnabled()) {
                                    createDockets(v4);
                                }
                                finish();
                            }
                        } else {
                            sigemail.setError("Incorrect Email Address");
                        }
                    } else {
                        if (signame.getText().toString().equals("")) {
                            signame.setError("Name is required.");
                        }
                        if (sigemail.getText().toString().equals("")) {
                            sigemail.setError("Email Address is required.");
                        }
                    }
                } else {

                    if (signed) {
                        Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                        String m = Encoder.encodeTobase64(signatureBitmap);
                        String filename = "Signature_" + System.currentTimeMillis() + ".jpg";
                        Log.e("sd", m.length() + "");
                        if (addSignatureToGallery(signatureBitmap, filename)) {
                            Toast.makeText(getApplicationContext(), "Changes have been saved.", Toast.LENGTH_SHORT).show();
                            createDockets(m);
                            finish();
                        } else {
                            Toast.makeText(SignatureActivity.this, "Unable to store the signature", Toast.LENGTH_SHORT).show();
                            createDockets(m);
                        }
                    } else {
                        if (clear.isEnabled()) {
                            createDockets(v4);
                        }
                        finish();
                    }
                }
                break;
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.clear:
                signatureview.setVisibility(signatureview.GONE);
                mSignaturePad.clear();
                break;
        }
    }


    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        }
        return file;
    }

    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        stream.close();
    }

    public boolean addSignatureToGallery(Bitmap signature, String filename) {
        boolean result = false;
        try {
            File photo = new File(getAlbumStorageDir("SignaturePad"), String.format(filename, ""));
            saveBitmapToJPG(signature, photo);
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(photo);
            mediaScanIntent.setData(contentUri);
            SignatureActivity.this.sendBroadcast(mediaScanIntent);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void createDockets(String base64file) {
        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        Date beginupd = new Date(currentDateTimeString);
        long milli = beginupd.getTime();

        if (type.equals("1")) {
            db.updateDockets("1", milli + "", base64file, mJobObject.getJobID());
        }
        if (type.equals("2")) {
            db.updateDockets("2", milli + "", base64file, mJobObject.getJobID());
        }

    }
}
