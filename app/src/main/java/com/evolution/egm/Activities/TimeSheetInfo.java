package com.evolution.egm.Activities;

import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.evolution.egm.Utils.CustomTimePickerDialog;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Utils.DateHelper;
import com.evolution.egm.Utils.Encoder;
import com.evolution.egm.Utils.SessionManager;
import com.evolution.egm.Models.TimeSheetSignature;
import com.evolution.egm.Models.Timesheet;
import com.evolution.egm.Models.Timesheet_Upload;
import com.evolution.egm.R;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.rey.material.app.DatePickerDialog;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.ThemeManager;
import com.rey.material.widget.Spinner;
import com.rey.material.widget.TimePicker;

import org.parceler.Parcels;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class TimeSheetInfo extends AppCompatActivity implements View.OnClickListener {

    Timesheet mJobObject;
    DateHelper date;
    private String token;
    public SessionManager session;
    DatabaseHelper db;
    Toolbar toolbar;
    Bundle extras;
    private NumberPicker minutePicker;
    public TimePicker timesp;
    boolean mealbreak = false;
    boolean odometerre = false;
    boolean isLightTheme = ThemeManager.getInstance().getCurrentTheme() == 0;
    int position;
    String startjob, endjob;
    CustomTimePickerDialog timePic;
    //Views
    RadioButton yesismeal, noismeal;
    RadioGroup fatigue, mealbreakrgoup2, attenddepot;
    RadioButton fat1, fat2, attenddepotyes, attenddepotno;
    LinearLayout ismeal, layouodometer;
    MenuItem savemenu, editmenu, canceledit;
    TextView travel_date, travel_date_end, onsite_date, onsite_date_end, meal_date, meal_date_end, travel_from_date, travel_from_date_end;
    TextView name, descripion, startdate, enddate, comapny, statuss, testtest;
    TextView starttotime, starttotimeend, onsiteStarttime, onsiteEndttime, mealStartttime, mealEndttime, travelFromtime, travelFromtimeend, traveltotal, onsitetotalfinal, mealtotalfinal, travelfromtotalfinal;
    EditText odmeter, regoNo, traveltofinalkm, kilomitertravelfrom;
    RelativeLayout setSignature, viewSignature;
    private static final String TAG = "TimeSheetInfo";
    //Animation
    //Dialog
    Dialog.Builder builder = null;
    Dialog dialogsig;
    TimeSheetSignature timesig;
    com.rey.material.app.Dialog dialogdd;
    //Arrays
    Timesheet_Upload mTimesheet;

    String timeGenerated = "";
    int whoisUsingTimepicter;

    // TEST

    Bitmap signatureBitmap = null;
    String mdd = "";
    String sigold = "";
    boolean signatureare = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_time_sheet_info);
        initToolBar();

        db = new DatabaseHelper(this);
        date = new DateHelper();
        session = new SessionManager(this);
        token = session.getToken();
        extras = getIntent().getExtras();
        position = Integer.parseInt(extras.getString("position"));
        mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));
        startjob = mJobObject.getStartDateTime();
        endjob = mJobObject.getEndDateTime();
        //RadioButon
        yesismeal = (RadioButton) findViewById(R.id.yesismeal);
        noismeal = (RadioButton) findViewById(R.id.noismeal);

        fatigue = (RadioGroup) findViewById(R.id.fatigue);
        fat1 = (RadioButton) findViewById(R.id.fat1);
        fat2 = (RadioButton) findViewById(R.id.fat2);

        attenddepot = (RadioGroup) findViewById(R.id.attenddepot);
        attenddepotyes = (RadioButton) findViewById(R.id.attenddepotyes);
        attenddepotno = (RadioButton) findViewById(R.id.attenddepotno);

        mealbreakrgoup2 = (RadioGroup) findViewById(R.id.mealbreakrgoup);

        //LinearLayout
        ismeal = (LinearLayout) findViewById(R.id.ismeal);
        setSignature = (RelativeLayout) findViewById(R.id.setSignature);
        viewSignature = (RelativeLayout) findViewById(R.id.viewSignature);

        //TextView
        traveltotal = (TextView) findViewById(R.id.traveltotal);
        onsitetotalfinal = (TextView) findViewById(R.id.onsitetotalfinal);
        mealtotalfinal = (TextView) findViewById(R.id.mealtotalfinal);
        travelfromtotalfinal = (TextView) findViewById(R.id.travelfromtotalfinal);

        starttotime = (TextView) findViewById(R.id.starttotime);
        starttotimeend = (TextView) findViewById(R.id.starttotimeend);
        onsiteStarttime = (TextView) findViewById(R.id.onsiteStarttime);
        onsiteEndttime = (TextView) findViewById(R.id.onsiteEndttime);
        mealStartttime = (TextView) findViewById(R.id.mealStartttime);
        mealEndttime = (TextView) findViewById(R.id.mealEndttime);
        travelFromtime = (TextView) findViewById(R.id.travelFromtime);
        travelFromtimeend = (TextView) findViewById(R.id.travelFromtimeend);

        travel_date = (TextView) findViewById(R.id.travel_date);
        travel_date_end = (TextView) findViewById(R.id.travel_date_end);
        onsite_date = (TextView) findViewById(R.id.onsite_date);
        onsite_date_end = (TextView) findViewById(R.id.onsite_date_end);
        meal_date = (TextView) findViewById(R.id.meal_date);
        meal_date_end = (TextView) findViewById(R.id.meal_date_end);
        travel_from_date = (TextView) findViewById(R.id.travel_from_date);
        travel_from_date_end = (TextView) findViewById(R.id.travel_from_date_end);
        name = (TextView) findViewById(R.id.name);
        descripion = (TextView) findViewById(R.id.discription);
        startdate = (TextView) findViewById(R.id.startdate);
        enddate = (TextView) findViewById(R.id.enddate);
        comapny = (TextView) findViewById(R.id.comapny);
        statuss = (TextView) findViewById(R.id.statuss);

        //EditText
        odmeter = (EditText) findViewById(R.id.odmeter);
        regoNo = (EditText) findViewById(R.id.regornoti);
        traveltofinalkm = (EditText) findViewById(R.id.traveltofinalkm);
        kilomitertravelfrom = (EditText) findViewById(R.id.kilomitertravelfrom);

        //Event
        starttotime.setOnClickListener(this);
        starttotimeend.setOnClickListener(this);
        onsiteStarttime.setOnClickListener(this);
        onsiteEndttime.setOnClickListener(this);
        mealStartttime.setOnClickListener(this);
        mealEndttime.setOnClickListener(this);
        travelFromtime.setOnClickListener(this);
        travelFromtimeend.setOnClickListener(this);
        yesismeal.setOnClickListener(this);
        noismeal.setOnClickListener(this);
        travel_date.setOnClickListener(this);
        travel_date_end.setOnClickListener(this);
        onsite_date.setOnClickListener(this);
        onsite_date_end.setOnClickListener(this);
        meal_date.setOnClickListener(this);
        meal_date_end.setOnClickListener(this);
        travel_from_date.setOnClickListener(this);
        travel_from_date_end.setOnClickListener(this);
        viewSignature.setOnClickListener(this);
        setSignature.setOnClickListener(this);
        dialogsig = new Dialog(this, R.style.FullHeightDialog2);
        initTIme();
        timesig = new TimeSheetSignature();
        mTimesheet = new Timesheet_Upload();
        timePic.setCancelable(false);
    }

    public int getCurrentMinInterval(int current) {
        int currentTimeSelected = 0;
        int intervalmod = current % 15;
        if (intervalmod < 7) {
            currentTimeSelected = current - intervalmod;
            Log.e("Current mod -", intervalmod + "");
        } else {
            currentTimeSelected = current + intervalmod;
            Log.e("Current mod +", intervalmod + "");
        }
        return currentTimeSelected;
    }

    public void initTIme() {
        Calendar c = Calendar.getInstance();
        timePic = new CustomTimePickerDialog(this, mTimeSetListener, c.get(Calendar.HOUR_OF_DAY), getCurrentMinInterval(c.get(Calendar.MINUTE)), true);
        timePic.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE) {

                }
            }
        });
    }
    public void enableDisable(boolean enablenatin) {
        if (enablenatin) {
            starttotime.setEnabled(true);
            starttotimeend.setEnabled(true);
            onsiteStarttime.setEnabled(true);
            onsiteEndttime.setEnabled(true);
            mealStartttime.setEnabled(true);
            mealEndttime.setEnabled(true);
            travelFromtime.setEnabled(true);
            travelFromtimeend.setEnabled(true);
            yesismeal.setEnabled(true);
            noismeal.setEnabled(true);
            travel_date.setEnabled(true);
            travel_date_end.setEnabled(true);
            onsite_date.setEnabled(true);
            onsite_date_end.setEnabled(true);
            meal_date.setEnabled(true);
            meal_date_end.setEnabled(true);
            travel_from_date.setEnabled(true);
            travel_from_date_end.setEnabled(true);
            odmeter.setEnabled(true);
            regoNo.setEnabled(true);
            traveltofinalkm.setEnabled(true);
            kilomitertravelfrom.setEnabled(true);
            attenddepotyes.setEnabled(true);
            attenddepotno.setEnabled(true);
            fat1.setEnabled(true);
            fat2.setEnabled(true);
        } else {
            starttotime.setEnabled(false);
            starttotimeend.setEnabled(false);
            onsiteStarttime.setEnabled(false);
            onsiteEndttime.setEnabled(false);
            mealStartttime.setEnabled(false);
            mealEndttime.setEnabled(false);
            travelFromtime.setEnabled(false);
            travelFromtimeend.setEnabled(false);
            yesismeal.setEnabled(false);
            noismeal.setEnabled(false);
            travel_date.setEnabled(false);
            travel_date_end.setEnabled(false);
            onsite_date.setEnabled(false);
            onsite_date_end.setEnabled(false);
            meal_date.setEnabled(false);
            meal_date_end.setEnabled(false);
            travel_from_date.setEnabled(false);
            travel_from_date_end.setEnabled(false);
            odmeter.setEnabled(false);
            regoNo.setEnabled(false);
            traveltofinalkm.setEnabled(false);
            kilomitertravelfrom.setEnabled(false);
            attenddepotyes.setEnabled(false);
            attenddepotno.setEnabled(false);
            fat1.setEnabled(false);
            fat2.setEnabled(false);
        }
    }

    TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {
            switch (whoisUsingTimepicter) {
                case 1:
                    starttotime.setText(addZero(hourOfDay + "", minute + ""));
                    if (!travel_date_end.getText().toString().equals("Date") && !starttotime.getText().toString().equals("HH:mm")) {
                        traveltotal.setText(getTimeDifference(travel_date.getText().toString() + " " + starttotime.getText().toString(), travel_date_end.getText().toString() + " " + starttotimeend.getText().toString()));
                    } else {

                    }
                    break;
                case 2:
                    if (!isvalid2(getTimeDifference(travel_date.getText().toString() + " " + starttotime.getText().toString(), travel_date_end.getText().toString() + " " + addZero(hourOfDay + "", minute + "")))) {
                        starttotimeend.setText(addZero(hourOfDay + "", minute + ""));
                        onsiteStarttime.setText(addZero(hourOfDay + "", minute + ""));
                        traveltotal.setText(getTimeDifference(travel_date.getText().toString() + " " + starttotime.getText().toString(), travel_date_end.getText().toString() + " " + starttotimeend.getText().toString()));
                    } else {
                        Toast.makeText(TimeSheetInfo.this, "Invalid time.", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case 3:
                    if (!isvalid2(getTimeDifference(travel_date_end.getText().toString() + " " + starttotimeend.getText().toString(), onsite_date.getText().toString() + " " + addZero(hourOfDay + "", minute + "")))) {
                        onsiteStarttime.setText(addZero(hourOfDay + "", minute + ""));
                        if (!onsite_date_end.getText().toString().equals("Date") && !onsiteStarttime.getText().toString().equals("HH:mm")) {
                            onsitetotalfinal.setText(getTimeDifference(onsite_date.getText().toString() + " " + onsiteStarttime.getText().toString(), onsite_date_end.getText().toString() + " " + onsiteEndttime.getText().toString()));
                        } else {
                        }
                    } else {
                        Toast.makeText(TimeSheetInfo.this, "Invalid time.", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case 4:
                    if (!isvalid2(getTimeDifference(onsite_date.getText().toString() + " " + onsiteStarttime.getText().toString(), onsite_date_end.getText().toString() + " " + addZero(hourOfDay + "", minute + "")))) {
                        onsiteEndttime.setText(addZero(hourOfDay + "", minute + ""));
                        travelFromtime.setText(addZero(hourOfDay + "", minute + ""));
                        onsitetotalfinal.setText(getTimeDifference(onsite_date.getText().toString() + " " + onsiteStarttime.getText().toString(), onsite_date_end.getText().toString() + " " + onsiteEndttime.getText().toString()));
                    } else {
                        Toast.makeText(TimeSheetInfo.this, "Invalid time.", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case 5:
                    if (!isvalid2(getTimeDifference(travel_date.getText().toString() + " " + starttotime.getText().toString(), meal_date.getText().toString() + " " + addZero(hourOfDay + "", minute + "")))) {
//                        if (!isvalid(compareDates2(dateMil(meal_date.getText().toString() + " " + addZero(hourOfDay + "", minute + "")), dateMil(onsite_date_end.getText().toString() + " " + onsiteEndttime.getText().toString())))) {
                            mealStartttime.setText(addZero(hourOfDay + "", minute + ""));
                            distributeplus(dateMil(meal_date.getText().toString() + " " + addZero(hourOfDay + "", minute + "")), mealEndttime, meal_date_end);
                            mealtotalfinal.setText(getTimeDifference(meal_date.getText().toString() + " " + mealStartttime.getText().toString(), meal_date_end.getText().toString() + " " + mealEndttime.getText().toString()));
                            if (!meal_date_end.getText().toString().equals("Date") && !mealStartttime.getText().toString().equals("HH:mm")) {
                                mealtotalfinal.setText(getTimeDifference(meal_date.getText().toString() + " " + mealStartttime.getText().toString(), meal_date_end.getText().toString() + " " + mealEndttime.getText().toString()));
                            } else {

                            }
//                        } else {
//                            Toast.makeText(TimeSheetInfo.this, "Invalid Time, please make sure meal break is under job start and end.", Toast.LENGTH_SHORT).show();
//                        }
                    } else {
                        Toast.makeText(TimeSheetInfo.this, "Invalid Time.", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case 6:
                    if (!isvalid2(getTimeDifference(meal_date.getText().toString() + " " + mealStartttime.getText().toString(), meal_date_end.getText().toString() + " " + addZero(hourOfDay + "", minute + "")))) {
                        if (!isvalid(compareDates2(dateMil(meal_date.getText().toString() + " " + addZero(hourOfDay + "", minute + "")), dateMil(onsite_date_end.getText().toString() + " " + onsiteEndttime.getText().toString())))) {
                            mealEndttime.setText(addZero(hourOfDay + "", minute + ""));
                            mealtotalfinal.setText(getTimeDifference(meal_date.getText().toString() + " " + mealStartttime.getText().toString(), meal_date_end.getText().toString() + " " + mealEndttime.getText().toString()));
                        } else {
                            Toast.makeText(TimeSheetInfo.this, "Invalid Time, please make sure meal break is under job start and end.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(TimeSheetInfo.this, "Invalid Time, please make sure meal break is under job start and end.", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case 7:
                    if (!isvalid2(getTimeDifference(travel_from_date.getText().toString() + " " + travelFromtime.getText().toString(), travel_from_date_end.getText().toString() + " " + addZero(hourOfDay + "", minute + "")))) {
                        travelFromtime.setText(addZero(hourOfDay + "", minute + ""));
                        if (!travel_from_date_end.getText().toString().equals("Date") && !travelFromtime.getText().toString().equals("HH:mm")) {
                            travelfromtotalfinal.setText(getTimeDifference(travel_from_date.getText().toString() + " " + travelFromtime.getText().toString(), travel_from_date_end.getText().toString() + " " + travelFromtimeend.getText().toString()));
                        } else {
                        }
                    } else {
                        Toast.makeText(TimeSheetInfo.this, "Invalid time.", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case 8:
                    if (!isvalid2(getTimeDifference(travel_from_date.getText().toString() + " " + travelFromtime.getText().toString(), travel_from_date_end.getText().toString() + " " + addZero(hourOfDay + "", minute + "")))) {
                        travelFromtimeend.setText(addZero(hourOfDay + "", minute + ""));
                        travelfromtotalfinal.setText(getTimeDifference(travel_from_date.getText().toString() + " " + travelFromtime.getText().toString(), travel_from_date_end.getText().toString() + " " + travelFromtimeend.getText().toString()));
                    } else {
                        Toast.makeText(TimeSheetInfo.this, "Invalid time.", Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    };

    public String getTimeDifference(String first, String second) {
        long diffMinutes = 0;
        long diffHours = 0;
        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy HH:mm");
        Date d1 = null;
        Date d2 = null;
        try {
            d1 = format.parse(first);
            d2 = format.parse(second);
            long diff = d2.getTime() - d1.getTime();
            diffMinutes = diff / (60 * 1000) % 60;
            diffHours = diff / (60 * 60 * 1000);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return diffHours + "h  " + diffMinutes + "m";
    }

    public String addZero(String n, String c) {

        String re = "";
        String re2 = "";

        if (n.length() == 1) {
            re = "0" + n;
        } else {
            re = n;
        }
        if (c.length() == 1) {
            re2 = "0" + c;
        } else {
            re2 = c;
        }
        return re + ":" + re2;
    }

    @Override
    protected void onStart() {
        super.onStart();
        populateData();
    }



    public void populateData() {

        statuss.setText(extras.getString("status"));
        descripion.setText(mJobObject.getDescription());
        name.setText(mJobObject.getName());
        comapny.setText(mJobObject.getShiftLocation());
        ArrayList<Timesheet_Upload> timeToDisplay = db.getAllTimeByID(mJobObject.getJobID(), mJobObject.getContactID());

        if (extras.getString("status").equals("Driver")) {
            layouodometer = (LinearLayout) findViewById(R.id.layouodometer);
            layouodometer.setVisibility(layouodometer.VISIBLE);
            odometerre = true;
        }

        if (timeToDisplay.size() != 0) {
            odmeter.setText(timeToDisplay.get(0).getTraveledKilometers());
            regoNo.setText(timeToDisplay.get(0).getRegoNo());
            traveltofinalkm.setText(timeToDisplay.get(0).getTravelStartDistance());
            kilomitertravelfrom.setText(timeToDisplay.get(0).getTravelFinishDistance());

            if (!(timeToDisplay.get(0).getAttendDepot().toString().equals(""))) {
                ((RadioButton) attenddepot.getChildAt(Integer.parseInt(timeToDisplay.get(0).getAttendDepot().toString()))).setChecked(true);
            }
            if (!(timeToDisplay.get(0).getFatigueCompliance().toString().equals(""))) {
                ((RadioButton) fatigue.getChildAt(Integer.parseInt(timeToDisplay.get(0).getFatigueCompliance().toString()))).setChecked(true);
            } else {

            }
            if (timeToDisplay.get(0).getIsHaveMeal().equals("true")) {
                ismeal.setVisibility(ismeal.VISIBLE);
                ((RadioButton) mealbreakrgoup2.getChildAt(0)).setChecked(true);
                mealbreak = true;
            } else {
                ismeal.setVisibility(ismeal.GONE);
                ((RadioButton) mealbreakrgoup2.getChildAt(1)).setChecked(true);
                mealbreak = false;
            }

            if (!timeToDisplay.get(0).getTravelStartDateTime().toString().equals("")) {
                travel_date.setText(dateReadable(Long.parseLong(timeToDisplay.get(0).getTravelStartDateTime().toString())));
                starttotime.setText(dateReadable2(Long.parseLong(timeToDisplay.get(0).getTravelStartDateTime().toString())));
            }
            if (!timeToDisplay.get(0).getTravelStartDateTimeEnd().toString().equals("")) {
                travel_date_end.setText(dateReadable(Long.parseLong(timeToDisplay.get(0).getTravelStartDateTimeEnd().toString())));
                starttotimeend.setText(dateReadable2(Long.parseLong(timeToDisplay.get(0).getTravelStartDateTimeEnd().toString())));
                traveltotal.setText(getTimeDifference(travel_date.getText().toString() + " " + starttotime.getText().toString(), travel_date_end.getText().toString() + " " + starttotimeend.getText().toString()));
            }
            if (timeToDisplay.get(0).getJobStartDateTime().toString().equals("")) {
                onsite_date.setText(dateReadable(Long.parseLong(startjob.replace("/Date(", "").replace(")/", ""))));
            } else {
                onsite_date.setText(dateReadable(Long.parseLong(timeToDisplay.get(0).getJobStartDateTime().toString())));
                onsiteStarttime.setText(dateReadable2(Long.parseLong(timeToDisplay.get(0).getJobStartDateTime().toString())));
            }
            if (!timeToDisplay.get(0).getJobFinishDateTime().toString().equals("")) {
                onsite_date_end.setText(dateReadable(Long.parseLong(timeToDisplay.get(0).getJobFinishDateTime().toString())));
                onsiteEndttime.setText(dateReadable2(Long.parseLong(timeToDisplay.get(0).getJobFinishDateTime().toString())));
                onsitetotalfinal.setText(getTimeDifference(onsite_date.getText().toString() + " " + onsiteStarttime.getText().toString(), onsite_date_end.getText().toString() + " " + onsiteEndttime.getText().toString()));
            }
            if (!timeToDisplay.get(0).getBreakStartDateTime().toString().equals("")) {
                meal_date.setText(dateReadable(Long.parseLong(timeToDisplay.get(0).getBreakStartDateTime().toString())));
                mealStartttime.setText(dateReadable2(Long.parseLong(timeToDisplay.get(0).getBreakStartDateTime().toString())));
            }
            if (!timeToDisplay.get(0).getBreakFinishDateTime().toString().equals("")) {
                meal_date_end.setText(dateReadable(Long.parseLong(timeToDisplay.get(0).getBreakFinishDateTime().toString())));
                mealEndttime.setText(dateReadable2(Long.parseLong(timeToDisplay.get(0).getBreakFinishDateTime().toString())));
                mealtotalfinal.setText(getTimeDifference(meal_date.getText().toString() + " " + mealStartttime.getText().toString(), meal_date_end.getText().toString() + " " + mealEndttime.getText().toString()));
            }
            if (!timeToDisplay.get(0).getTravelFinishDateTimeStart().toString().equals("")) {
                travel_from_date.setText(dateReadable(Long.parseLong(timeToDisplay.get(0).getTravelFinishDateTimeStart().toString())));
                travelFromtime.setText(dateReadable2(Long.parseLong(timeToDisplay.get(0).getTravelFinishDateTimeStart().toString())));
            }
            if (!timeToDisplay.get(0).getTravelFinishDateTime().toString().equals("")) {
                travel_from_date_end.setText(dateReadable(Long.parseLong(timeToDisplay.get(0).getTravelFinishDateTime().toString())));
                travelFromtimeend.setText(dateReadable2(Long.parseLong(timeToDisplay.get(0).getTravelFinishDateTime().toString())));
                travelfromtotalfinal.setText(getTimeDifference(travel_from_date.getText().toString() + " " + travelFromtime.getText().toString(), travel_from_date_end.getText().toString() + " " + travelFromtimeend.getText().toString()));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_timesheet, menu);
        savemenu = (MenuItem) menu.findItem(R.id.savetime);
        editmenu = (MenuItem) menu.findItem(R.id.edittime);
        canceledit = (MenuItem) menu.findItem(R.id.canceledit);
        editmenu.setVisible(false);
        canceledit.setVisible(false);

        if (db.checkTimesheetUploadSigExisting(mJobObject.getJobID(), mJobObject.getContactID())) {
            savemenu.setVisible(false);
            editmenu.setVisible(true);
            enableDisable(false);
            setSignature.setVisibility(setSignature.GONE);
            viewSignature.setVisibility(viewSignature.VISIBLE);
        }

        return true;
    }

    public void saveData() {

        String fatiguecompliance = "";
        String attenddepotcs = "";
        int radioButtonID2 = fatigue.getCheckedRadioButtonId();
        RadioButton rb2 = (RadioButton) fatigue.findViewById(radioButtonID2);
        int attenddepot2 = attenddepot.getCheckedRadioButtonId();
        RadioButton rb4 = (RadioButton) attenddepot.findViewById(attenddepot2);

        if (fatigue.getCheckedRadioButtonId() != -1) {
            fatiguecompliance = fatigue.indexOfChild(rb2) + "";
        }

        if (attenddepot.getCheckedRadioButtonId() != -1) {
            attenddepotcs = attenddepot.indexOfChild(rb4) + "";
        }

        mTimesheet.setJobID(mJobObject.getJobID());
        mTimesheet.setWorkerID(mJobObject.getContactID());
        mTimesheet.setShiftID(mJobObject.getId());
        mTimesheet.setFatigueCompliance(fatiguecompliance);
        mTimesheet.setAttendDepot(attenddepotcs);

        if (regoNo.getText().toString().equals("")) {
            mTimesheet.setRegoNo("0");
        } else {
            mTimesheet.setRegoNo(regoNo.getText().toString());
        }
        if (odmeter.getText().toString().equals("")) {
            mTimesheet.setTraveledKilometers("0");
        } else {
            mTimesheet.setTraveledKilometers(odmeter.getText().toString());
        }

        mTimesheet.setTravelStartDistance(traveltofinalkm.getText().toString());
        mTimesheet.setTravelFinishDistance(kilomitertravelfrom.getText().toString());
        mTimesheet.setIsHaveMeal(mealbreak + "");

        if (travel_date.getText().toString().equals("Date")) {
            mTimesheet.setTravelStartDateTime("");
        } else {
            if (starttotime.getText().toString().equals("HH:mm")) {
                mTimesheet.setTravelStartDateTime("");
            } else {
                mTimesheet.setTravelStartDateTime(dateMil(travel_date.getText().toString() + " " + starttotime.getText().toString()) + "");
            }
        }

        if (travel_date_end.getText().toString().equals("Date")) {
            mTimesheet.setTravelStartDateTimeEnd("");
        } else {
            if (starttotimeend.getText().toString().equals("HH:mm")) {
                mTimesheet.setTravelStartDateTimeEnd("");
            } else {
                mTimesheet.setTravelStartDateTimeEnd(dateMil(travel_date_end.getText().toString() + " " + starttotimeend.getText().toString()) + "");
            }
        }
        if (onsite_date.getText().toString().equals("Date")) {
            mTimesheet.setJobStartDateTime("");
        } else {
            if (onsiteStarttime.getText().toString().equals("HH:mm")) {
                mTimesheet.setJobStartDateTime("");
            } else {
                mTimesheet.setJobStartDateTime(dateMil(onsite_date.getText().toString() + " " + onsiteStarttime.getText().toString()) + "");
            }
        }
        if (onsite_date_end.getText().toString().equals("Date")) {
            mTimesheet.setJobFinishDateTime("");
        } else {
            if (onsiteEndttime.getText().toString().equals("HH:mm")) {
                mTimesheet.setJobFinishDateTime("");
            } else {
                mTimesheet.setJobFinishDateTime(dateMil(onsite_date_end.getText().toString() + " " + onsiteEndttime.getText().toString()) + "");
            }
        }
        //check if break is false and return 0
        if (meal_date.getText().toString().equals("Date")) {
            mTimesheet.setBreakStartDateTime("");
        } else {
            if (mealStartttime.getText().toString().equals("HH:mm")) {
                mTimesheet.setBreakStartDateTime("");
            } else {
                mTimesheet.setBreakStartDateTime(dateMil(meal_date.getText().toString() + " " + mealStartttime.getText().toString()) + "");
            }
        }
        if (meal_date_end.getText().toString().equals("Date")) {
            mTimesheet.setBreakFinishDateTime("");
        } else {
            if (mealEndttime.getText().toString().equals("")) {
                mTimesheet.setBreakFinishDateTime("");
            } else {
                mTimesheet.setBreakFinishDateTime(dateMil(meal_date_end.getText().toString() + " " + mealEndttime.getText().toString()) + "");
            }
        }
        if (travel_from_date.getText().toString().equals("Date")) {
            mTimesheet.setTravelFinishDateTimeStart("");
        } else {
            if (travelFromtime.getText().toString().equals("HH:mm")) {
                mTimesheet.setTravelFinishDateTimeStart("");
            } else {
                mTimesheet.setTravelFinishDateTimeStart(dateMil(travel_from_date.getText().toString() + " " + travelFromtime.getText().toString()) + "");
            }
        }
        if (travel_from_date_end.getText().toString().equals("Date")) {
            mTimesheet.setTravelFinishDateTime("");
        } else {
            if (travelFromtimeend.getText().toString().equals("HH:mm")) {
                mTimesheet.setTravelFinishDateTime("");
            } else {
                mTimesheet.setTravelFinishDateTime(dateMil(travel_from_date_end.getText().toString() + " " + travelFromtimeend.getText().toString()) + "");
            }
        }

        db.addUpdateToUploadTimeSheet(mTimesheet);
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.savetime:
                if (db.checkTimesheetUploadSigExisting(mJobObject.getJobID(), mJobObject.getContactID())) {
                    savemenu.setVisible(false);
                    setSignature.setVisibility(setSignature.GONE);
                    viewSignature.setVisibility(viewSignature.VISIBLE);
                    editmenu.setVisible(true);
                    db.deleteTimesheetInfoEdited(mJobObject.getJobID(), mJobObject.getContactID());
                    db.timesheetsign(mJobObject.getJobID());
                    db.updateDockets("1", "null", "null", mJobObject.getJobID());
                    db.updateDockets("2", "null", "null", mJobObject.getJobID());
                    enableDisable(false);
                }
                saveData();
                Toast.makeText(getApplicationContext(), "Changes have been saved.", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.edittime:
                db.removeSigTIme(mJobObject.getJobID(), mJobObject.getContactID());
                savemenu.setVisible(true);
                editmenu.setVisible(false);
                setSignature.setVisibility(setSignature.VISIBLE);
                viewSignature.setVisibility(viewSignature.GONE);

                db.updateDockets("1", "null", "null", mJobObject.getJobID());
                db.updateDockets("2", "null", "null", mJobObject.getJobID());

                enableDisable(true);
                break;
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Timesheet");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.yesismeal:
                initTIme();
                ismeal.setVisibility(ismeal.VISIBLE);
                mealbreak = true;
                break;
            case R.id.noismeal:
                mealbreak = false;
                ismeal.setVisibility(ismeal.GONE);
                meal_date.setText("Date");
                meal_date_end.setText("Date");
                mealStartttime.setText("00:00");
                mealEndttime.setText("00:00");
                mealtotalfinal.setText("0h 0m");
                mTimesheet.setBreakStartDateTime("");
                mTimesheet.setBreakFinishDateTime("");
                initTIme();
                break;
            case R.id.travel_date:
                initTIme();
                datePicker(travel_date, 1);
                break;
            case R.id.travel_date_end:
                initTIme();
                if (!travel_date.getText().equals("Date")) {
                    datePicker(travel_date_end, 2);
                } else {
                    Toast.makeText(TimeSheetInfo.this, "Please add start travel to.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.onsite_date:
                initTIme();
                if (!travel_date_end.getText().equals("Date")) {
                    datePicker(onsite_date, 3);
                } else {
                    Toast.makeText(TimeSheetInfo.this, "Please add start travel end.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.onsite_date_end:
                initTIme();
                if (mealbreak) {
                    if (!meal_date.getText().equals("Date")) {
                        datePicker(onsite_date_end, 4);
                    } else {
                        Toast.makeText(TimeSheetInfo.this, "Please add onsite start.", Toast.LENGTH_SHORT).show();
                    }

                }else{
                    if (!onsite_date.getText().equals("Date")) {
                        datePicker(onsite_date_end, 4);
                    } else {
                        Toast.makeText(TimeSheetInfo.this, "Please add onsite start.", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.meal_date:
                initTIme();
                if (!onsite_date.getText().equals("Date")) {
                    datePicker(meal_date, 5);
                } else {
                    Toast.makeText(TimeSheetInfo.this, "Please add onsite end. ", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.meal_date_end:
                initTIme();
                if (!meal_date.getText().equals("Date")) {
                    datePicker(meal_date_end, 6);
                } else {
                    Toast.makeText(TimeSheetInfo.this, "Please add meal start. ", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.travel_from_date:
                initTIme();
                if (!onsite_date_end.getText().equals("Date")) {
                    datePicker(travel_from_date, 7);
                } else {
                    Toast.makeText(TimeSheetInfo.this, "Please add onsite end.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.travel_from_date_end:
                initTIme();
                if (!travel_from_date.getText().equals("Date")) {
                    datePicker(travel_from_date_end, 8);
                } else {
                    Toast.makeText(TimeSheetInfo.this, "Please add travel from.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.starttotime:
                initTIme();
                if (!travel_date.getText().equals("Date")) {
                    timePic.show();
                    whoisUsingTimepicter = 1;
                } else {
                    Toast.makeText(TimeSheetInfo.this, "Please add date start travel to.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.starttotimeend:
                initTIme();
                if (!travel_date_end.getText().equals("Date")) {
                    timePic.show();
                    whoisUsingTimepicter = 2;
                } else {
                    Toast.makeText(TimeSheetInfo.this, "Please add date end travel to.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.onsiteStarttime:
                initTIme();
                if (!onsite_date.getText().equals("Date")) {
                    timePic.show();
                    whoisUsingTimepicter = 3;
                } else {
                    Toast.makeText(TimeSheetInfo.this, "Please add date onsite start.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.onsiteEndttime:
                initTIme();
                if (!onsite_date_end.getText().equals("Date")) {
                    timePic.show();
                    whoisUsingTimepicter = 4;
                } else {
                    Toast.makeText(TimeSheetInfo.this, "Please add date onsite end.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.mealStartttime:
                initTIme();
                if (!meal_date.getText().equals("Date")) {
                    timePic.show();
                    whoisUsingTimepicter = 5;
                } else {
                    Toast.makeText(TimeSheetInfo.this, "Please add date meal start.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.mealEndttime:
                initTIme();
                if (!onsite_date_end.getText().equals("Date")) {
                    timePic.show();
                    whoisUsingTimepicter = 6;
                } else {
                    Toast.makeText(TimeSheetInfo.this, "Please add date onsite end.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.travelFromtime:
                initTIme();
                if (!travel_from_date.getText().equals("Date")) {
                    timePic.show();
                    whoisUsingTimepicter = 7;
                } else {
                    Toast.makeText(TimeSheetInfo.this, "Please addd date travel from.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.travelFromtimeend:
                initTIme();
                if (!travel_date.getText().equals("Date")) {
                    timePic.show();
                    whoisUsingTimepicter = 8;
                } else {
                    Toast.makeText(TimeSheetInfo.this, "Please add date travel from.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.setSignature:
                saveData();
                ArrayList<Timesheet_Upload> timeToDisplay = db.getAllTimeByID(mJobObject.getJobID(), mJobObject.getContactID());
                Gson gson = new Gson();
                Log.e("string return", "" + gson.toJson(timeToDisplay) + "");
                String toValidate = gson.toJson(timeToDisplay);
                if (!extras.getString("status").equals("Driver")) {
                    toValidate = toValidate.replace("\"RegoNo\":\"\",", "").replace("\"TraveledKilometers\":\"\",", "").replace("\"ShiftID\":\"\",", "");
                }
                if (mealbreak) {
                    toValidate = toValidate + "";
                } else {
                    toValidate = toValidate.replace("\"BreakFinishDateTime\":\"\"", "");
                    toValidate = toValidate.replace("\"BreakStartDateTime\":\"\"", "");
                }
                if (!toValidate.contains("\"\"")) {
                    signatureArea();
                } else {
                    Toast.makeText(TimeSheetInfo.this, "Please complete the timesheet to proceed.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.viewSignature:
                ArrayList<TimeSheetSignature> mdf = db.getAllTimeSigByID(mJobObject.getJobID(), mJobObject.getContactID());
                Intent m = new Intent(TimeSheetInfo.this, SignatureView.class);
                if (mdf.size() != 0) {
                    m.putExtra("timesheet", mdf.get(0).getName());
                    m.putExtra("signature", mdf.get(0).getSignature());
                } else {
                    m.putExtra("signature", "none");
                }
                startActivity(m);
                break;
        }
    }

    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate) {

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
        }

        return outputDate;

    }

    public void datePicker(final TextView clicked, final int thewho) {
        Dialog.Builder builder = null;
        boolean isLightTheme = ThemeManager.getInstance().getCurrentTheme() == 0;
        builder = new DatePickerDialog.Builder(isLightTheme ? R.style.Material_App_Dialog_DatePicker_Light : R.style.Material_App_Dialog_DatePicker) {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                initTIme();
                DatePickerDialog dialog = (DatePickerDialog) fragment.getDialog();
                String newDate = "";
                String date = dialog.getFormattedDate(SimpleDateFormat.getDateInstance());
                String date2 = dialog.getFormattedDate(SimpleDateFormat.getDateInstance());
                if (date.contains(",")) {
                    newDate = formateDateFromstring("MMM dd, yyyy", "dd, MMM yyyy", date);
                } else {
                    newDate = date;
                }
                date = newDate.replace(",", "");
                switch (thewho) {
                    case 1:
                        Log.e("date date date", newDate.replace(",", ""));
                        clicked.setText(date);
                        timePic.show();
                        whoisUsingTimepicter = 1;
                        super.onPositiveActionClicked(fragment);
                        break;
                    case 2:
                        if (!isvalid(compareDates(dateMil(date), dateMil(travel_date.getText().toString())))) {
                            clicked.setText(date);
                            onsite_date.setText(date);
                            timePic.show();
                            whoisUsingTimepicter = 2;
                            super.onPositiveActionClicked(fragment);
                        } else {
                            Toast.makeText(TimeSheetInfo.this, "Invalid Date", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 3:
                        if (!isvalid(compareDates(dateMil(date), dateMil(travel_date_end.getText().toString())))) {
                            clicked.setText(date);
                            timePic.show();
                            whoisUsingTimepicter = 3;
                            super.onPositiveActionClicked(fragment);
                        } else {
                            Toast.makeText(TimeSheetInfo.this, "Invalid Date", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 4:
                        if (!isvalid(compareDates(dateMil(date), dateMil(onsite_date.getText().toString())))) {
                            clicked.setText(date);
                            travel_from_date.setText(date);
                            timePic.show();
                            whoisUsingTimepicter = 4;
                            super.onPositiveActionClicked(fragment);
                        } else {
                            Toast.makeText(TimeSheetInfo.this, "Invalid Date", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 5:
                        if (!isvalid(compareDates(dateMil(date), dateMil(onsite_date.getText().toString())))) {
//                            if (!isvalid(compareDates2(dateMil(date), dateMil(onsite_date_end.getText().toString())))) {
                                clicked.setText(date);
                                timePic.show();
                                whoisUsingTimepicter = 5;
                                super.onPositiveActionClicked(fragment);
//                            } else {
//                                Toast.makeText(TimeSheetInfo.this, "Please make sure that the meal is under onsite start and end.", Toast.LENGTH_SHORT).show();
//                            }
                        } else {
                            Toast.makeText(TimeSheetInfo.this, "Please make sure that the meal is under onsite start and end.", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 6:
                        if (!isvalid(compareDates(dateMil(date), dateMil(meal_date.getText().toString())))) {
                            if (!isvalid(compareDates2(dateMil(date), dateMil(onsite_date_end.getText().toString())))) {
                                clicked.setText(date);
                                timePic.show();
                                whoisUsingTimepicter = 6;
                                super.onPositiveActionClicked(fragment);
                            } else {
                                Toast.makeText(TimeSheetInfo.this, "Please make sure that the meal is under onsite start and end.", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(TimeSheetInfo.this, "Invalid Date", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 7:
                        if (!isvalid(compareDates(dateMil(date), dateMil(onsite_date_end.getText().toString())))) {
                            clicked.setText(date);
                            timePic.show();
                            whoisUsingTimepicter = 7;
                            super.onPositiveActionClicked(fragment);
                        } else {
                            Toast.makeText(TimeSheetInfo.this, "Invalid Date", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 8:
                        if (!isvalid(compareDates(dateMil(date), dateMil(travel_from_date.getText().toString())))) {
                            clicked.setText(date);
                            timePic.show();
                            whoisUsingTimepicter = 8;
                            super.onPositiveActionClicked(fragment);
                        } else {
                            Toast.makeText(TimeSheetInfo.this, "Invalid Date", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    default:
                        clicked.setText(date);
                        super.onPositiveActionClicked(fragment);
                        break;
                }
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                super.onNegativeActionClicked(fragment);
            }
        };

        builder.positiveAction("OK")
                .negativeAction("CANCEL");

        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(getSupportFragmentManager(), null);
    }

    public void distributeplus(Long miliseconds, TextView startdate, TextView starttime) {

        DateFormat date = new SimpleDateFormat("dd MMM yyyy");
        DateFormat time = new SimpleDateFormat("HH:mm");
        long milliSeconds = miliseconds + 30 * 60 * 1000;
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        starttime.setText(date.format(calendar.getTime()));
        startdate.setText(time.format(calendar.getTime()));
    }

    public boolean isvalid(long toValidate) {
        boolean isItTrueOrNot = false;
        String val = toValidate + "";
        if (val.contains("-")) {
            isItTrueOrNot = true;
        } else {
            isItTrueOrNot = false;
        }
        return isItTrueOrNot;
    }

    public boolean isvalid2(String toValidate) {
        boolean isItTrueOrNot = false;
        String val = toValidate + "";
        if (val.contains("-")) {
            isItTrueOrNot = true;
        } else {
            isItTrueOrNot = false;
        }
        return isItTrueOrNot;
    }

    public long dateMil(String dateset) {
        Date date = new Date(dateset);
        long utcDateInMilliSeconds = date.getTime();
        Log.e("ready", "dateMil: " + utcDateInMilliSeconds);
        return utcDateInMilliSeconds;
    }

    public String sp(Spinner sp) {
        return sp.getSelectedItem().toString();
    }

    public String dateReadable(long miliseconds) {
        long time = miliseconds;
        return (new SimpleDateFormat("dd MMM yyyy")).format(new Date(time));
    }

    public String dateReadable2(long miliseconds) {
        long time = miliseconds;
        return (new SimpleDateFormat("HH:mm")).format(new Date(time));
    }

    public long compareDates(long miliseconds1, long miliseconds2) {
        return miliseconds1 - miliseconds2;
    }

    public long compareDates2(long miliseconds1, long miliseconds2) {
        return miliseconds2 - miliseconds1;
    }


    public void signatureArea() {

        dialogsig.setContentView(R.layout.dialog_signature_time);
        dialogsig.contentMargin(0);
        dialogsig.contentMargin(0, 0, 0, -25);

        final LinearLayout cancel = (LinearLayout) dialogsig.findViewById(R.id.cancel);
        final LinearLayout save = (LinearLayout) dialogsig.findViewById(R.id.save);

        final SignaturePad mSignaturePad = (SignaturePad) dialogsig.findViewById(R.id.signaturearea);
        final LinearLayout clear = (LinearLayout) dialogsig.findViewById(R.id.clear);
        final ImageView signatureprev = (ImageView) dialogsig.findViewById(R.id.signatureprev);
        final EditText signaturename = (EditText) dialogsig.findViewById(R.id.signaturenamehere);

        TextView signaturename2 = (TextView) dialogsig.findViewById(R.id.signaturename);
        signaturename2.setText("Employee Signature");
        signaturename.setText(mJobObject.getName());

        if (db.checkTimesheetUploadSigExisting(mJobObject.getJobID(), mJobObject.getContactID())) {
            ArrayList<TimeSheetSignature> m = db.getAllTimeSigByID(mJobObject.getJobID(), mJobObject.getContactID());
            byte[] decodedString = Base64.decode(m.get(0).getSignature(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            signatureprev.setImageBitmap(decodedByte);
            sigold = m.get(0).getSignature();
            signatureprev.setVisibility(signatureprev.VISIBLE);
            mSignaturePad.setVisibility(mSignaturePad.GONE);
        } else {
            signatureprev.setVisibility(signatureprev.GONE);
            mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
        }

        save.setEnabled(false);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timesig.setJobID(mJobObject.getJobID());
                timesig.setContactID(mJobObject.getContactID());
                timesig.setName(signaturename.getText().toString());
                String sigTemp = "";
                if (mSignaturePad.isEmpty()) {
                    sigTemp = sigold;
                } else {
                    signatureBitmap = mSignaturePad.getSignatureBitmap();
                    mdd = Encoder.encodeTobase64(signatureBitmap);
                    sigTemp = mdd;
                }
                long millisStart = Calendar.getInstance().getTimeInMillis();
                if (signaturename.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Name is required.", Toast.LENGTH_SHORT).show();
                    signaturename.setFocusable(true);
                } else {
                    timesig.setSignature(sigTemp);
                    if (signatureare == true) {
                        if (addSignatureToGallery(signatureBitmap, "clientSignature" + millisStart)) {
                            db.addUpdateToUploadTimeSheet_SIG(timesig);
                            Toast.makeText(getApplicationContext(), "Signature has been saved.", Toast.LENGTH_SHORT).show();
                            dialogsig.hide();
                        } else {
                            Toast.makeText(getApplicationContext(), "Unable to store the signature", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        db.addUpdateToUploadTimeSheet_SIG(timesig);
                        savemenu.setVisible(false);
                        editmenu.setVisible(true);
                        dialogsig.hide();
                    }
                }
                finish();
            }
        });

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSignaturePad.clear();
                signatureprev.setVisibility(signatureprev.GONE);
                mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogsig.dismiss();
            }
        });
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onSigned() {
                signatureare = true;
                save.setEnabled(true);
                clear.setEnabled(true);
            }

            @Override
            public void onClear() {
                save.setEnabled(false);
                clear.setEnabled(false);
            }
        });
        dialogsig.show();
    }

    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        } else {
            Log.e("SignaturePad", "Directory  created");
        }
        return file;
    }

    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        stream.close();
    }

    public boolean addSignatureToGallery(Bitmap signature, String filename) {
        boolean result = false;
        try {
            File photo = new File(getAlbumStorageDir("Evolution"), String.format(filename + ".jpg", System.currentTimeMillis()));
            saveBitmapToJPG(signature, photo);
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(photo);
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

}
