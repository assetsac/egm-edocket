package com.evolution.egm.Activities;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.evolution.egm.Activities.Forms.EmployeeDeclaratiion;
import com.evolution.egm.Activities.Forms.IncidentBasic;
import com.evolution.egm.Activities.Forms.IncidentDetails;
import com.evolution.egm.Activities.Forms.IncidentLocality;
import com.evolution.egm.Activities.Forms.NationalSafety;
import com.evolution.egm.Adapters.AdapterIncidentReportForm;
import com.evolution.egm.Utils.RecyclerItemClickListener;
import com.evolution.egm.R;
import com.rey.material.app.Dialog;

public class IncidentReportForm extends ActionBarActivity {
    Toolbar toolbar;
    String[] myDataset = {"Basic Information", "Incident Locality Details", "Incident Details", "Employee Declaration(must be completed and witnessed prior to forwarding to the ERMG Safety Manager)", "Depot/Section Manager/ Supervisor Report"};
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incident_report_form);
        initToolBar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initRecycler();
    }

    public void initRecycler() {
        mRecyclerView = (RecyclerView) findViewById(R.id.incidentreportform);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Dialog.Builder builder = null;
                        switch (position) {
                            case 0:
                                Intent m = new Intent(getApplicationContext(), IncidentBasic.class);
                                m.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(m);
                                break;
                            case 1:
                                Intent m2 = new Intent(getApplicationContext(), IncidentLocality.class);
                                m2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(m2);
                                break;
                            case 2:
                                Intent m3 = new Intent(getApplicationContext(), IncidentDetails.class);
                                m3.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(m3);
                                break;
                            case 3:
                                Intent m4 = new Intent(getApplicationContext(), EmployeeDeclaratiion.class);
                                m4.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(m4);
                                break;
                            case 4:
                                Intent m5 = new Intent(getApplicationContext(), NationalSafety.class);
                                m5.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(m5);
                                break;
                        }
                    }
                })
        );
        mAdapter = new AdapterIncidentReportForm(myDataset, getApplicationContext());
        mRecyclerView.setAdapter(mAdapter);
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Incident Report Form");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                return true;
            case android.R.id.home:
                finish();
                overridePendingTransition(0, 0);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(0, 0);
    }
}
