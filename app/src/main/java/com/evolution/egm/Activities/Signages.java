package com.evolution.egm.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.evolution.egm.Adapters.SignAgeMetter;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Interface.SignagesInterface;
import com.evolution.egm.Models.Job;
import com.evolution.egm.Models.SignagesMeterModel;
import com.evolution.egm.Models.SignagesObject;
import com.evolution.egm.R;

import org.parceler.Parcels;

import java.util.ArrayList;

public class Signages extends AppCompatActivity implements View.OnClickListener, SignagesInterface {

    LinearLayout addSigns;
    Toolbar toolbar;
    private RecyclerView mRecyclerView;
    private SignAgeMetter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private RecyclerView mRecyclerView2;
    private SignAgeMetter mAdapter2;
    private RecyclerView.LayoutManager mLayoutManager2;
    SignagesObject listofSelectedItem;
    SignagesObject listofS, listofS2;
    RadioButton slowlane, fastlane;
    LinearLayout slowlanel, fastlanel;
    Job mJobObject;

    SignagesMeterModel listitem;
    public static final int REQUEST_CODE = 1;
    private ArrayList<SignagesMeterModel> meter = new ArrayList<SignagesMeterModel>();
    public ArrayList<SignagesMeterModel> metertopas;
    ArrayList<String> lis = new ArrayList<>();
    public DatabaseHelper db;
    ArrayList<SignagesMeterModel> signages;
    boolean slLane = true;
    CheckBox ishaveaftercare;
    LinearLayout afterheader;
    ArrayList<SignagesMeterModel> passingData;
    ArrayList<SignagesMeterModel> passingData2;

    Parcelable sd;
    int[] nsw = {R.drawable.n1, R.drawable.n2, R.drawable.n3, R.drawable.n4, R.drawable.n5, R.drawable.n6, R.drawable.n7, R.drawable.n8, R.drawable.n9, R.drawable.n10, R.drawable.n11, R.drawable.n12, R.drawable.n13, R.drawable.n14, R.drawable.n15, R.drawable.n16, R.drawable.n17, R.drawable.n18, R.drawable.n181, R.drawable.n182, R.drawable.n183, R.drawable.n184, R.drawable.n185, R.drawable.n186, R.drawable.n187, R.drawable.n188, R.drawable.n19, R.drawable.n20, R.drawable.n21, R.drawable.n22, R.drawable.n23, R.drawable.n24, R.drawable.n25, R.drawable.n26, R.drawable.n27, R.drawable.n28, R.drawable.n29, R.drawable.n30, R.drawable.n31, R.drawable.n32, R.drawable.n33, R.drawable.n34, R.drawable.n35, R.drawable.n36, R.drawable.n37, R.drawable.n38, R.drawable.n39, R.drawable.n40, R.drawable.n4};

    int[] w = {R.drawable.w1, R.drawable.w2, R.drawable.w3, R.drawable.w4, R.drawable.w5, R.drawable.w6, R.drawable.w7, R.drawable.w8, R.drawable.w9, R.drawable.w10, R.drawable.w11, R.drawable.w12, R.drawable.w13, R.drawable.w14, R.drawable.w15, R.drawable.w16, R.drawable.w17, R.drawable.w18, R.drawable.w19, R.drawable.w20, R.drawable.w21, R.drawable.w22, R.drawable.w23, R.drawable.w24, R.drawable.w25, R.drawable.w26, R.drawable.w27, R.drawable.w28, R.drawable.w29, R.drawable.w30, R.drawable.w31, R.drawable.w32, R.drawable.w33, R.drawable.w34, R.drawable.w340, R.drawable.w35, R.drawable.w36, R.drawable.w37, R.drawable.w38, R.drawable.w39, R.drawable.w40,
            R.drawable.w41, R.drawable.w42, R.drawable.w43, R.drawable.w44, R.drawable.w45, R.drawable.w46, R.drawable.n181, R.drawable.n182, R.drawable.n183, R.drawable.n184, R.drawable.n185, R.drawable.n186, R.drawable.n187, R.drawable.n188, R.drawable.w47, R.drawable.w48, R.drawable.w49, R.drawable.w50, R.drawable.w51, R.drawable.w52, R.drawable.w53, R.drawable.w54, R.drawable.w55, R.drawable.w56, R.drawable.w57, R.drawable.w58, R.drawable.w59, R.drawable.w60, R.drawable.w61, R.drawable.w62, R.drawable.w63
    };

    int[] q = {R.drawable.roadworkahead, R.drawable.roadworkahead70, R.drawable.roadworkahead80, R.drawable.roadworkahead90, R.drawable.roadworkahead100, R.drawable.roadworkahead110, R.drawable.reducespeed40, R.drawable.reducespeed50, R.drawable.reducespeed60, R.drawable.reducespeed70, R.drawable.stopdonotovertake, R.drawable.mergelimit, R.drawable.donotovertaker, R.drawable.arrowupmergeleft, R.drawable.arrowuptdonotovertake, R.drawable.q1, R.drawable.q2, R.drawable.q3, R.drawable.q4, R.drawable.q5, R.drawable.q6, R.drawable.q7, R.drawable.q8, R.drawable.q9, R.drawable.q10, R.drawable.q11, R.drawable.q12, R.drawable.q13, R.drawable.q14, R.drawable.q15, R.drawable.q16, R.drawable.n181, R.drawable.n182, R.drawable.n183, R.drawable.n184, R.drawable.n185, R.drawable.n186, R.drawable.n187, R.drawable.n188, R.drawable.q17, R.drawable.q18, R.drawable.q19, R.drawable.q20, R.drawable.q21, R.drawable.q22, R.drawable.q23, R.drawable.q24, R.drawable.q25, R.drawable.q26, R.drawable.q27, R.drawable.q28, R.drawable.q29, R.drawable.q30, R.drawable.q31, R.drawable.q32, R.drawable.q33, R.drawable.q34, R.drawable.q35, R.drawable.q36, R.drawable.q37, R.drawable.q38, R.drawable.q39, R.drawable.q40,
            R.drawable.q41, R.drawable.q42, R.drawable.q43, R.drawable.q44, R.drawable.q45, R.drawable.q46, R.drawable.q47, R.drawable.q48, R.drawable.q52, R.drawable.q49, R.drawable.q50, R.drawable.q51, R.drawable.q53, R.drawable.q54, R.drawable.q55, R.drawable.q56, R.drawable.q57, R.drawable.q58, R.drawable.q59, R.drawable.q60, R.drawable.q61
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signages);

        mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));
        addSigns = (LinearLayout) findViewById(R.id.addSigns);
        addSigns.setOnClickListener(this);
        initToolBar();
        db = new DatabaseHelper(this);
        afterheader = (LinearLayout) findViewById(R.id.afterheader);

        slowlane = (RadioButton) findViewById(R.id.slowlane);
        fastlane = (RadioButton) findViewById(R.id.fastlane);

        slowlanel = (LinearLayout) findViewById(R.id.slowlanelauto);
        fastlanel = (LinearLayout) findViewById(R.id.fastlanelauto);
        slowlane.setOnClickListener(this);
        fastlane.setOnClickListener(this);

        mRecyclerView = (RecyclerView) findViewById(R.id.slowlanerecycler);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView2 = (RecyclerView) findViewById(R.id.slowlanerecycler2);
        mLayoutManager2 = new LinearLayoutManager(this);
        mRecyclerView2.setLayoutManager(mLayoutManager2);
        mRecyclerView2.setHasFixedSize(true);
        ishaveaftercare = (CheckBox) findViewById(R.id.ishaveaftercare);

        loopThis();
        Log.e("onCreate", "on");

        ArrayList<SignagesMeterModel> sings = getSelectedOnly(db.getAllSignages(mJobObject.getJobID()));
        passingData = sings;

        ArrayList<SignagesMeterModel> sings2 = getSelectedOnly(db.getAllSignages2(mJobObject.getJobID()));
        passingData2 = sings2;

        if (db.getAllSignagesSelected(mJobObject.getJobID()).size() <= 0) {
            afterheader.setVisibility(afterheader.GONE);
        } else {
            afterheader.setVisibility(afterheader.VISIBLE);
            if (slLane) {
                if (db.getAllSignage(mJobObject.getJobID()).get(0).getIsAfterCare().equals("1")) {
                    ishaveaftercare.setChecked(true);
                } else {
                    ishaveaftercare.setChecked(false);
                }
            } else {
                if (db.getAllSignage2(mJobObject.getJobID()).get(0).getIsAfterCare().equals("1")) {
                    ishaveaftercare.setChecked(true);
                } else {
                    ishaveaftercare.setChecked(false);
                }
            }
        }

        mAdapter = new SignAgeMetter(passingData, ishaveaftercare.isChecked(), mJobObject.getJobID(), "1", Signages.this);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter2 = new SignAgeMetter(passingData2, ishaveaftercare.isChecked(), mJobObject.getJobID(), "0", Signages.this);
        mRecyclerView2.setAdapter(mAdapter2);

        ishaveaftercare.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    mAdapter = new SignAgeMetter(passingData, true, mJobObject.getJobID(), "1", Signages.this);
                    mRecyclerView.setAdapter(mAdapter);
                    mAdapter2 = new SignAgeMetter(passingData2, true, mJobObject.getJobID(), "0", Signages.this);
                    mRecyclerView2.setAdapter(mAdapter2);
                    if (slLane) {
                        db.updateSignages(true, mJobObject.getJobID(), "1");
                    } else {
                        db.updateSignages(true, mJobObject.getJobID(), "2");
                    }
                } else {
                    mAdapter2 = new SignAgeMetter(passingData2, false, mJobObject.getJobID(), "0", Signages.this);
                    mRecyclerView2.setAdapter(mAdapter2);
                    mAdapter = new SignAgeMetter(passingData, false, mJobObject.getJobID(), "1", Signages.this);
                    mRecyclerView.setAdapter(mAdapter);
                    if (slLane) {
                        db.updateSignages(false, mJobObject.getJobID(), "1");
                    } else {
                        db.updateSignages(false, mJobObject.getJobID(), "2");
                    }
                }
            }
        });
    }

    @Override
    public void returnNewSign(ArrayList<SignagesMeterModel> mDataset, String mode) {
        if (mode.equals("1")) {
            Log.e("modeitemzzz", mode);
            passingData = mDataset;
        } else {
            Log.e("modeitem2zz", mode);
            passingData2 = mDataset;
        }
    }

    public void loopThis() {

        int[] iconlist;
        if (mJobObject.getState().equals("NSW")) {
            iconlist = nsw;
        } else if (mJobObject.getState().equals("QLD")) {
            iconlist = q;
        } else if (mJobObject.getState().equals("WA")) {
            iconlist = w;
        } else {
            iconlist = q;
        }

        ArrayList<SignagesMeterModel> signlists = new ArrayList<SignagesMeterModel>();

        for (int i = 0; i < iconlist.length; i++) {
            SignagesMeterModel temp = new SignagesMeterModel();
            temp.setImage(iconlist[i] + "");
            signlists.add(temp);
        }

        db.addSings(new SignagesObject(signlists), mJobObject.getJobID(), "1");
    }

    public void loopThis2() {
        int[] iconlist;
        if (mJobObject.getState().equals("NSW")) {
            iconlist = nsw;
        } else if (mJobObject.getState().equals("QLD")) {
            iconlist = q;
        } else if (mJobObject.getState().equals("WA")) {
            iconlist = w;
        } else {
            iconlist = q;
        }

        ArrayList<SignagesMeterModel> signlists = new ArrayList<SignagesMeterModel>();

        for (int i = 0; i < iconlist.length; i++) {
            SignagesMeterModel temp = new SignagesMeterModel();
            temp.setImage(iconlist[i] + "");
            signlists.add(temp);
        }
        db.addSings(new SignagesObject(signlists), mJobObject.getJobID(), "0");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.munusignages, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("onResume", "on");
        ishaveaftercare.setChecked(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("onstart", "on");
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Signages");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.addsignstolist:
                saveThis();
                Toast.makeText(getApplicationContext(), "Changes have been saved.", Toast.LENGTH_SHORT).show();
                db.updateDockets("1", "null", "null", mJobObject.getJobID());
                db.updateDockets("2", "null", "null", mJobObject.getJobID());
                return true;
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void saveThis() {
        db.updateSigns(new SignagesObject(passingData), mJobObject.getJobID(), "1");
        db.updateSigns(new SignagesObject(passingData2), mJobObject.getJobID(), "0");
//        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.getExtras().getString("type").equals("1")) {
                    passingData = Parcels.unwrap(data.getExtras().getParcelable("selecteditem"));
                    passingData = getSelectedOnly(passingData);
                    mAdapter = new SignAgeMetter(passingData, ishaveaftercare.isChecked(), mJobObject.getJobID(), "1", Signages.this);
                    mRecyclerView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                } else {
                    passingData2 = Parcels.unwrap(data.getExtras().getParcelable("selecteditem"));
                    passingData2 = getSelectedOnly(passingData2);
                    mAdapter2 = new SignAgeMetter(passingData2, ishaveaftercare.isChecked(), mJobObject.getJobID(), "0", Signages.this);
                    mRecyclerView2.setAdapter(mAdapter2);
                    mAdapter2.notifyDataSetChanged();
                }
                afterheader.setVisibility(afterheader.VISIBLE);
            }

            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    public ArrayList<SignagesMeterModel> getSelectedOnly(ArrayList<SignagesMeterModel> mm) {
        ArrayList<SignagesMeterModel> mmd = new ArrayList<>();
        for (int c = 0; c < mm.size(); c++) {
            if (mm.get(c).isSelected()) {
                mmd.add(mm.get(c));
            } else {

            }
        }
        return mmd;

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addSigns:
                db.updateSigns(new SignagesObject(passingData), mJobObject.getJobID(), "1");
                db.updateSigns(new SignagesObject(passingData2), mJobObject.getJobID(), "0");
                loopThis2();
                Intent s = new Intent(Signages.this, SignsList.class);
                s.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                s.putExtra("data", Parcels.wrap(mJobObject));
                if (slLane) {
                    sd = Parcels.wrap(passingData);
                    s.putExtra("type", "1");
                    s.putExtra("selected", sd);
                } else {
                    sd = Parcels.wrap(passingData2);
                    s.putExtra("type", "0");
                    s.putExtra("selected", sd);
                }
                startActivityForResult(s, 1);
                break;
            case R.id.slowlane:
                slLane = true;
                slowlanel.setVisibility(slowlanel.VISIBLE);
                fastlanel.setVisibility(slowlanel.GONE);
                if (!(db.getAllSignage(mJobObject.getJobID()).size() <= 0)) {
                    if (!(db.getAllSignage(mJobObject.getJobID()).get(0).getIsAfterCare() == null)) {
                        if (db.getAllSignage(mJobObject.getJobID()).get(0).getIsAfterCare().equals("1")) {
                            ishaveaftercare.setChecked(true);
                        } else {
                            ishaveaftercare.setChecked(false);
                        }
                    }
                }
                break;
            case R.id.fastlane:
                slLane = false;
                slowlanel.setVisibility(slowlanel.GONE);
                fastlanel.setVisibility(slowlanel.VISIBLE);
                if (!(db.getAllSignage2(mJobObject.getJobID()).size() <= 0)) {
                    if (!(db.getAllSignage2(mJobObject.getJobID()).get(0).getIsAfterCare() == null)) {
                        if (db.getAllSignage2(mJobObject.getJobID()).get(0).getIsAfterCare().equals("1")) {
                            ishaveaftercare.setChecked(true);
                        } else {
                            ishaveaftercare.setChecked(false);
                        }
                    }
                }
                break;
        }
    }

}