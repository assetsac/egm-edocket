package com.evolution.egm.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.evolution.egm.Utils.Constants;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Utils.SessionManager;
import com.evolution.egm.Interface.APIInterface;
import com.evolution.egm.R;
import com.rey.material.app.Dialog;
import com.squareup.okhttp.OkHttpClient;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.mime.TypedFile;

public class VideoViewActivity extends AppCompatActivity {
    private ImageView videoView;
    private ImageView playvideo;
    Toolbar toolbar;
    private String filePath = null;
    private String idv = "";
    DatabaseHelper db;
    public SessionManager session;
    private String token;
    OkHttpClient okHttpClient;
    Uri uri;
    Dialog dialog;
    Dialog.Builder builder = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_view);

        initToolBar();

        session = new SessionManager(this);
        token = session.getToken();
        videoView = (ImageView) findViewById(R.id.videoview);
        playvideo = (ImageView) findViewById(R.id.playvideo);
        db = new DatabaseHelper(this);
        // Receiving the data from previous activity
        Intent i = getIntent();

        // image or video path that is captured in previous activity
        filePath = i.getStringExtra("filePath");
        idv = i.getStringExtra("id");
        Toast.makeText(VideoViewActivity.this, "" + filePath, Toast.LENGTH_SHORT).show();
        uri = Uri.parse("file://" + filePath);
        if (filePath != null) {
            // Displaying the image or video on the screen
            Glide.with(getApplicationContext()).load(filePath).into(videoView);
            // start playing
        } else {
            Toast.makeText(getApplicationContext(),
                    "Sorry, file path is missing!", Toast.LENGTH_LONG).show();
        }

        playvideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent viewIntent = new Intent("android.intent.action.VIEW", uri);
                viewIntent.setDataAndType(uri, "video/*");
                startActivity(viewIntent);
            }
        });

        dialog = new Dialog(this, R.style.FullHeightDialog);
        okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);

    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Video");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_video, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.save:
                uploadVideo();
                break;
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void uploadVideo() {
        Map<String, String> params = new HashMap<>();
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setClient(new OkClient(okHttpClient))
                .setEndpoint(Constants.API_URL)
                .build();

        Log.e("to send", filePath);
        TypedFile typedFile = new TypedFile("multipart/form-data", new File(filePath));
        APIInterface interfacem = restAdapter.create(APIInterface.class);

    }
}
