package com.evolution.egm.Activities.signs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.evolution.egm.Utils.CreateJobSharedPreference;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Utils.DateHelper;
import com.evolution.egm.Models.Job;
import com.evolution.egm.R;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class QLDSignages extends AppCompatActivity implements View.OnTouchListener, View.OnClickListener {

    Toolbar toolbar;
    RadioButton slowlane, fastlane;
    LinearLayout slowlanel, fastlanel;
    MaterialEditText cwevolution4, cwlocallandmark4, TimeErected, TimeCollected, TimeChecked1, TimeChecked2, TimeChecked3, TimeChecked4;
    Spinner spinner;
    String Time1, Time2, Time3, Time4, Time5, Time6;
    String TimeErected2, TimeCollected2, TimeChecked12, TimeChecked22, TimeChecked32, TimeChecked42;
    DateHelper date;
    DatabaseHelper db;
    Dialog dialog;
    DatePicker datepicker;
    TimePicker timepicker;
    List<String> categories;
    final CharSequence cs1 = "-";
    Dialog.Builder builder2 = null;
    Job mJobObject;
    CreateJobSharedPreference shared;

    RadioButton afteryes, afterno;
    RadioGroup aftercare;
    String aftercares = "null";

    EditText qld1, qld2, qld3, qld4, qld5, qld6, qld7, qld8, qld9, qld10, qld11, qld12, qld13, qld14, qld15, qld16, qld17, qld18, qld19, qld20, qld21, qld22, qld23, qld24, qld25, qld26, qld27, qld28, qld29, qld30, qld31, qld32, qld33, qld34, qld35, qld36, qld37, qld38, qld39, qld40, qld41, qld42, qld43, qld44, qld45, qld46, qld47, qld48, qld49, qld50, qld51, qld52, qld53, qld54, qld55, qld56, qld57, qld58, qld59, qld60, qld61, qld62, qld63, qld64, qld65, qld66, qld67, qld68, qld69, qld70, qld71, qld72, qld73, qld74, qld75, qld76, qld77;
    EditText qlda1, qlda2, qlda3, qlda4, qlda5, qlda6, qlda7, qlda8, qlda9, qlda10, qlda11, qlda12, qlda13, qlda14, qlda15, qlda16, qlda17, qlda18, qlda19, qlda20, qlda21, qlda22, qlda23, qlda24, qlda25, qlda26, qlda27, qlda28, qlda29, qlda30, qlda31, qlda32, qlda33, qlda34, qlda35, qlda36, qlda37, qlda38, qlda39, qlda40, qlda41, qlda42, qlda43, qlda44, qlda45, qlda46, qlda47, qlda48, qlda49, qlda50, qlda51, qlda52, qlda53, qlda54, qlda55, qlda56, qlda57, qlda58, qlda59, qlda60, qlda61, qlda62, qlda63, qlda64, qlda65, qlda66, qlda67, qlda68, qlda69, qlda70, qlda71, qlda72, qlda73, qlda74, qlda75, qlda76, qlda77;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qldsigns);
        initToolBar();
        date = new DateHelper();
        db = new DatabaseHelper(this);
        dialog = new Dialog(this, R.style.FullHeightDialog);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));
        }
        shared = new CreateJobSharedPreference(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initViews();
        initSpinner();
        populateData();
    }

    public void initSpinner() {
        // Spinner click listener
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // On selecting a spinner item
                String item = parent.getItemAtPosition(position).toString();

                // Showing selected spinner item
//                Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Spinner Drop down elements
        categories = new ArrayList<String>();
        categories.add("North");
        categories.add("South");
        categories.add("East");
        categories.add("West");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinnerlocation, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.spinnerlocation);

        spinner.setAdapter(dataAdapter);


    }

    public void initViews() {

        TimeErected = (MaterialEditText) findViewById(R.id.TimeErected);
        TimeCollected = (MaterialEditText) findViewById(R.id.TimeCollected);
        TimeChecked1 = (MaterialEditText) findViewById(R.id.TimeChecked1);
        TimeChecked2 = (MaterialEditText) findViewById(R.id.TimeChecked2);
        TimeChecked3 = (MaterialEditText) findViewById(R.id.TimeChecked3);
        TimeChecked4 = (MaterialEditText) findViewById(R.id.TimeChecked4);

        TimeErected.setOnTouchListener(this);
        TimeCollected.setOnTouchListener(this);
        TimeChecked1.setOnTouchListener(this);
        TimeChecked2.setOnTouchListener(this);
        TimeChecked3.setOnTouchListener(this);
        TimeChecked4.setOnTouchListener(this);

        spinner = (Spinner) findViewById(R.id.cwdirectionlist4);

        slowlane = (RadioButton) findViewById(R.id.slowlane);
        fastlane = (RadioButton) findViewById(R.id.fastlane);

        slowlanel = (LinearLayout) findViewById(R.id.slowlanel);
        fastlanel = (LinearLayout) findViewById(R.id.fastlanel);

        cwevolution4 = (MaterialEditText) findViewById(R.id.cwevolution4);
        cwlocallandmark4 = (MaterialEditText) findViewById(R.id.cwlocallandmark4);

        slowlane.setOnClickListener(this);
        fastlane.setOnClickListener(this);

        qld1 = (EditText) findViewById(R.id.qld1);
        qld2 = (EditText) findViewById(R.id.qld2);
        qld3 = (EditText) findViewById(R.id.qld3);
        qld4 = (EditText) findViewById(R.id.qld4);
        qld5 = (EditText) findViewById(R.id.qld5);
        qld6 = (EditText) findViewById(R.id.qld6);
        qld7 = (EditText) findViewById(R.id.qld7);
        qld8 = (EditText) findViewById(R.id.qld8);
        qld9 = (EditText) findViewById(R.id.qld9);
        qld10 = (EditText) findViewById(R.id.qld10);
        qld11 = (EditText) findViewById(R.id.qld11);
//        qld12 = (EditText) findViewById(R.id.qld12);
        qld13 = (EditText) findViewById(R.id.qld13);
        qld14 = (EditText) findViewById(R.id.qld14);
        qld15 = (EditText) findViewById(R.id.qld15);
        qld16 = (EditText) findViewById(R.id.qld16);
        qld17 = (EditText) findViewById(R.id.qld17);
        qld18 = (EditText) findViewById(R.id.qld18);
        qld19 = (EditText) findViewById(R.id.qld19);
        qld20 = (EditText) findViewById(R.id.qld20);
        qld21 = (EditText) findViewById(R.id.qld21);
        qld22 = (EditText) findViewById(R.id.qld22);
        qld23 = (EditText) findViewById(R.id.qld23);
        qld24 = (EditText) findViewById(R.id.qld24);
        qld25 = (EditText) findViewById(R.id.qld25);
        qld26 = (EditText) findViewById(R.id.qld26);
        qld27 = (EditText) findViewById(R.id.qld27);
        qld28 = (EditText) findViewById(R.id.qld28);
        qld29 = (EditText) findViewById(R.id.qld29);
        qld30 = (EditText) findViewById(R.id.qld30);
        qld31 = (EditText) findViewById(R.id.qld31);
        qld32 = (EditText) findViewById(R.id.qld32);
        qld33 = (EditText) findViewById(R.id.qld33);
        qld34 = (EditText) findViewById(R.id.qld34);
        qld35 = (EditText) findViewById(R.id.qld35);
        qld36 = (EditText) findViewById(R.id.qld36);
        qld37 = (EditText) findViewById(R.id.qld37);
        qld38 = (EditText) findViewById(R.id.qld38);
        qld39 = (EditText) findViewById(R.id.qld39);
        qld40 = (EditText) findViewById(R.id.qld40);
        qld41 = (EditText) findViewById(R.id.qld41);
        qld42 = (EditText) findViewById(R.id.qld42);
        qld43 = (EditText) findViewById(R.id.qld43);
        qld44 = (EditText) findViewById(R.id.qld44);
        qld45 = (EditText) findViewById(R.id.qld45);
        qld46 = (EditText) findViewById(R.id.qld46);
        qld47 = (EditText) findViewById(R.id.qld47);
        qld48 = (EditText) findViewById(R.id.qld48);
        qld49 = (EditText) findViewById(R.id.qld49);
        qld50 = (EditText) findViewById(R.id.qld50);
        qld51 = (EditText) findViewById(R.id.qld51);
        qld52 = (EditText) findViewById(R.id.qld52);
        qld53 = (EditText) findViewById(R.id.qld53);
        qld54 = (EditText) findViewById(R.id.qld54);
        qld55 = (EditText) findViewById(R.id.qld55);
        qld56 = (EditText) findViewById(R.id.qld56);
        qld57 = (EditText) findViewById(R.id.qld57);
        qld58 = (EditText) findViewById(R.id.qld58);
        qld59 = (EditText) findViewById(R.id.qld59);
        qld60 = (EditText) findViewById(R.id.qld60);
        qld61 = (EditText) findViewById(R.id.qld61);
        qld62 = (EditText) findViewById(R.id.qld62);
        qld63 = (EditText) findViewById(R.id.qld63);
        qld64 = (EditText) findViewById(R.id.qld64);
        qld65 = (EditText) findViewById(R.id.qld65);
        qld66 = (EditText) findViewById(R.id.qld66);
        qld67 = (EditText) findViewById(R.id.qld67);
        qld68 = (EditText) findViewById(R.id.qld68);
        qld69 = (EditText) findViewById(R.id.qld69);
        qld70 = (EditText) findViewById(R.id.qld70);
        qld71 = (EditText) findViewById(R.id.qld71);
        qld72 = (EditText) findViewById(R.id.qld72);
        qld73 = (EditText) findViewById(R.id.qld73);
        qld74 = (EditText) findViewById(R.id.qld74);
        qld75 = (EditText) findViewById(R.id.qld75);
        qld76 = (EditText) findViewById(R.id.qld76);
        qld77 = (EditText) findViewById(R.id.qld77);

        qlda1 = (EditText) findViewById(R.id.qlda1);
        qlda2 = (EditText) findViewById(R.id.qlda2);
        qlda3 = (EditText) findViewById(R.id.qlda3);
        qlda4 = (EditText) findViewById(R.id.qlda4);
        qlda5 = (EditText) findViewById(R.id.qlda5);
        qlda6 = (EditText) findViewById(R.id.qlda6);
        qlda7 = (EditText) findViewById(R.id.qlda7);
        qlda8 = (EditText) findViewById(R.id.qlda8);
        qlda9 = (EditText) findViewById(R.id.qlda9);
        qlda10 = (EditText) findViewById(R.id.qlda10);
        qlda11 = (EditText) findViewById(R.id.qlda11);
//        qlda12 = (EditText) findViewById(R.id.qlda12);
        qlda13 = (EditText) findViewById(R.id.qlda13);
        qlda14 = (EditText) findViewById(R.id.qlda14);
        qlda15 = (EditText) findViewById(R.id.qlda15);
        qlda16 = (EditText) findViewById(R.id.qlda16);
        qlda17 = (EditText) findViewById(R.id.qlda17);
        qlda18 = (EditText) findViewById(R.id.qlda18);
        qlda19 = (EditText) findViewById(R.id.qlda19);
        qlda20 = (EditText) findViewById(R.id.qlda20);
        qlda21 = (EditText) findViewById(R.id.qlda21);
        qlda22 = (EditText) findViewById(R.id.qlda22);
        qlda23 = (EditText) findViewById(R.id.qlda23);
        qlda24 = (EditText) findViewById(R.id.qlda24);
        qlda25 = (EditText) findViewById(R.id.qlda25);
        qlda26 = (EditText) findViewById(R.id.qlda26);
        qlda27 = (EditText) findViewById(R.id.qlda27);
        qlda28 = (EditText) findViewById(R.id.qlda28);
        qlda29 = (EditText) findViewById(R.id.qlda29);
        qlda30 = (EditText) findViewById(R.id.qlda30);
        qlda31 = (EditText) findViewById(R.id.qlda31);
        qlda32 = (EditText) findViewById(R.id.qlda32);
        qlda33 = (EditText) findViewById(R.id.qlda33);
        qlda34 = (EditText) findViewById(R.id.qlda34);
        qlda35 = (EditText) findViewById(R.id.qlda35);
        qlda36 = (EditText) findViewById(R.id.qlda36);
        qlda37 = (EditText) findViewById(R.id.qlda37);
        qlda38 = (EditText) findViewById(R.id.qlda38);
        qlda39 = (EditText) findViewById(R.id.qlda39);
        qlda40 = (EditText) findViewById(R.id.qlda40);
        qlda41 = (EditText) findViewById(R.id.qlda41);
        qlda42 = (EditText) findViewById(R.id.qlda42);
        qlda43 = (EditText) findViewById(R.id.qlda43);
        qlda44 = (EditText) findViewById(R.id.qlda44);
        qlda45 = (EditText) findViewById(R.id.qlda45);
        qlda46 = (EditText) findViewById(R.id.qlda46);
        qlda47 = (EditText) findViewById(R.id.qlda47);
        qlda48 = (EditText) findViewById(R.id.qlda48);
        qlda49 = (EditText) findViewById(R.id.qlda49);
        qlda50 = (EditText) findViewById(R.id.qlda50);
        qlda51 = (EditText) findViewById(R.id.qlda51);
        qlda52 = (EditText) findViewById(R.id.qlda52);
        qlda53 = (EditText) findViewById(R.id.qlda53);
        qlda54 = (EditText) findViewById(R.id.qlda54);
        qlda55 = (EditText) findViewById(R.id.qlda55);
        qlda56 = (EditText) findViewById(R.id.qlda56);
        qlda57 = (EditText) findViewById(R.id.qlda57);
        qlda58 = (EditText) findViewById(R.id.qlda58);
        qlda59 = (EditText) findViewById(R.id.qlda59);
        qlda60 = (EditText) findViewById(R.id.qlda60);
        qlda61 = (EditText) findViewById(R.id.qlda61);
        qlda62 = (EditText) findViewById(R.id.qlda62);
        qlda63 = (EditText) findViewById(R.id.qlda63);
        qlda64 = (EditText) findViewById(R.id.qlda64);
        qlda65 = (EditText) findViewById(R.id.qlda65);
        qlda66 = (EditText) findViewById(R.id.qlda66);
        qlda67 = (EditText) findViewById(R.id.qlda67);
        qlda68 = (EditText) findViewById(R.id.qlda68);
        qlda69 = (EditText) findViewById(R.id.qlda69);
        qlda70 = (EditText) findViewById(R.id.qlda70);
        qlda71 = (EditText) findViewById(R.id.qlda71);
        qlda72 = (EditText) findViewById(R.id.qlda72);
        qlda73 = (EditText) findViewById(R.id.qlda73);
        qlda74 = (EditText) findViewById(R.id.qlda74);
        qlda75 = (EditText) findViewById(R.id.qlda75);
        qlda76 = (EditText) findViewById(R.id.qlda76);
        qlda77 = (EditText) findViewById(R.id.qlda77);

        afteryes = (RadioButton) findViewById(R.id.afteryes);
        afterno = (RadioButton) findViewById(R.id.afterno);
        aftercare = (RadioGroup) findViewById(R.id.aftercarer);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_qldsignages, menu);
        return true;
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Signage Audit");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.saveqld:
//                if (checkifEmpty(cwevolution4, "Rego No. is required.")) {
//                } else {
//                    if (checkifEmpty(cwlocallandmark4, "Local Landmark is required.")) {
//                    } else {
//                        if (checkifEmpty(TimeErected, "Time Erected is required.")) {
//                        } else {
//                            if (checkifEmpty(TimeChecked1, "Time Collected is required.")) {
//                            } else {
//                                if (checkifEmpty(TimeChecked2, "Time Checked is required.")) {
//                                } else {
//                                    if (checkifEmpty(TimeChecked3, "Time Checked is required.")) {
//                                    } else {
//                                        if (checkifEmpty(TimeChecked4, "Time Checked is required.")) {
//                                        } else {
//                                            if (checkifEmpty(TimeCollected, "Time Checked is required.")) {
//                                            } else {
                saveSings();
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
                break;
            case android.R.id.home:
                finish();
                overridePendingTransition(0, 0);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean checkifEmpty(MaterialEditText isEmpty, String messageError) {
        boolean temp = false;
        if (isEmpty.getText().toString().equals("")) {
            temp = true;
            isEmpty.setError(messageError);
        }
        return temp;
    }

    public void timedate(final int to, String whoClicked) {

        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.activity_timesheetpicker);

        datepicker = (DatePicker) dialog.findViewById(R.id.datePicker);
        datepicker.setVisibility(datepicker.GONE);
        timepicker = (TimePicker) dialog.findViewById(R.id.timePicker);

        LinearLayout save = (LinearLayout) dialog.findViewById(R.id.save);
        LinearLayout cancel = (LinearLayout) dialog.findViewById(R.id.cancel);
        TextView title = (TextView) dialog.findViewById(R.id.title);
        title.setText(whoClicked);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (to) {
                    case 1: {
                        TimeErected.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeErected2 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                    case 2: {
                        TimeCollected.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeCollected2 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                    case 3: {
                        TimeChecked1.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeChecked12 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                    case 4: {
                        TimeChecked2.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeChecked22 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                    case 5: {
                        TimeChecked3.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeChecked32 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                    case 6: {
                        TimeChecked4.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeChecked42 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    public boolean checkifhave(String toValidate) {
        boolean temp = false;
        boolean ishave = toValidate.contains(cs1);
        if (ishave) {
            temp = true;
        }
        return temp;
    }


    public String getTimeDifference(String first, String second) {
        long diffMinutes = 0;
        long diffHours = 0;
        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(first);
            d2 = format.parse(second);
            long diff = d2.getTime() - d1.getTime();
            diffMinutes = diff / (60 * 1000) % 60;
            diffHours = diff / (60 * 60 * 1000);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return diffHours + "h  " + diffMinutes + "m";
    }


    public Long generateMili(DatePicker datem, TimePicker timem) {
        int day = datem.getDayOfMonth();
        int month = datem.getMonth() + 1;
        int year = datem.getYear();
        String returns = day + " " + month + " " + year + " " + timem.getCurrentHour() + ":" + timem.getCurrentMinute() + "";
        return Long.parseLong(convertDate(returns));
    }

    public String convertDate(String date) {
        String temp = "";
        long timeInMilliseconds = 0;
        String givenDateString = date;
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
        try {
            Date mDate = sdf.parse(givenDateString);
            timeInMilliseconds = mDate.getTime();
            System.out.println("Date in milli :: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds + "";
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.TimeErected: {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    initTimepicker(1);
                }
                return true;
            }
            case R.id.TimeCollected:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeChecked4.getText().toString().equals("")) {
                        Toast.makeText(QLDSignages.this, "Please add Time Checked.", Toast.LENGTH_SHORT).show();
                    } else {
                        initTimepicker(2);
                    }
                }
                return true;
            case R.id.TimeChecked1:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeErected.getText().toString().equals("")) {
                        Toast.makeText(QLDSignages.this, "Please add Time Collected.", Toast.LENGTH_SHORT).show();
                    } else {
                        initTimepicker(3);
                    }
                }
                return true;
            case R.id.TimeChecked2:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeChecked1.getText().toString().equals("")) {
                        Toast.makeText(QLDSignages.this, "Please add Time Checked.", Toast.LENGTH_SHORT).show();
                    } else {
                        initTimepicker(4);
                    }
                }
                return true;
            case R.id.TimeChecked3:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeChecked2.getText().toString().equals("")) {
                        Toast.makeText(QLDSignages.this, "Please add Time Checked.", Toast.LENGTH_SHORT).show();
                    } else {
                        initTimepicker(5);
                    }
                }
                return true;
            case R.id.TimeChecked4:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeChecked3.getText().toString().equals("")) {
                        Toast.makeText(QLDSignages.this, "Please add Time Checked.", Toast.LENGTH_SHORT).show();
                    } else {
                        initTimepicker(6);
                    }
                }
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.slowlane:
                slowlanel.setVisibility(slowlanel.VISIBLE);
                fastlanel.setVisibility(slowlanel.GONE);
                break;
            case R.id.fastlane:
                slowlanel.setVisibility(slowlanel.GONE);
                fastlanel.setVisibility(slowlanel.VISIBLE);
                break;
        }
    }

    public static String hourConverter(String dime) {
        Log.e("tsetses", dime);

        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm:ss a");
        Date date = null;
        try {
            date = parseFormat.parse(dime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return displayFormat.format(date) + "";
    }


    public void initTimepicker(final int to) {
        builder2 = new TimePickerDialog.Builder(6, 00) {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                final TimePickerDialog dialogr = (TimePickerDialog) fragment.getDialog();
                switch (to) {
                    case 1:
                        if (!TimeChecked1.getText().toString().equals("")) {
                            if (checkifhave(getTimeDifference(dialogr.getFormattedTime(DateFormat.getDateTimeInstance()), TimeErected2))) {
                                Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                            } else {
                                TimeErected.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                                TimeErected2 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());
                                Time1 = convertDate(TimeErected2);
                            }
                        } else {
                            TimeErected.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                            TimeErected2 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());
                            Time1 = convertDate(TimeErected2);
                        }
                        break;
                    case 3:
                        TimeChecked12 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());

                        if (checkifhave(getTimeDifference(TimeErected2, TimeChecked12))) {
                            TimeChecked12 = "";
                            Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                        } else {
                            Time2 = convertDate(TimeChecked12);
                            TimeChecked1.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                        }
                        break;
                    case 4:
                        TimeChecked22 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());
                        if (checkifhave(getTimeDifference(TimeChecked12, TimeChecked22))) {
                            TimeChecked22 = "";
                            Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                        } else {
                            Time3 = convertDate(TimeChecked22);
                            TimeChecked2.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                        }
                        break;
                    case 5:
                        TimeChecked32 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());
                        if (checkifhave(getTimeDifference(TimeChecked22, TimeChecked32))) {
                            TimeChecked32 = "";
                            Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                        } else {
                            Time4 = convertDate(TimeChecked32);
                            TimeChecked3.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                        }
                        break;
                    case 6:
                        TimeChecked42 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());

                        if (checkifhave(getTimeDifference(TimeChecked32, TimeChecked42))) {
                            TimeChecked42 = "";
                            Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                        } else {
                            Time5 = convertDate(TimeChecked42);
                            TimeChecked4.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                        }
                        break;
                    case 2:
                        TimeCollected2 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());

                        if (checkifhave(getTimeDifference(TimeChecked42, TimeCollected2))) {
                            TimeCollected2 = "";
                            Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                        } else {
                            Time6 = convertDate(TimeCollected2);
                            TimeCollected.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                        }
                        break;
                }

                super.onPositiveActionClicked(fragment);
                super.onNegativeActionClicked(fragment);

            }
        };

        builder2.positiveAction("OK")
                .negativeAction("CANCEL");

        DialogFragment fragment2 = DialogFragment.newInstance(builder2);
        fragment2.show(getSupportFragmentManager(), null);

    }

    public void saveSings() {
        afteryes = (RadioButton) findViewById(R.id.afteryes);
        afterno = (RadioButton) findViewById(R.id.afterno);
        aftercare = (RadioGroup) findViewById(R.id.aftercarer);

        int radioButtonID2 = aftercare.getCheckedRadioButtonId();
        RadioButton rb2 = (RadioButton) aftercare.findViewById(radioButtonID2);

        if (aftercare.getCheckedRadioButtonId() != -1) {
            aftercares = aftercare.indexOfChild(rb2) + "";
        }

        String re = "[" +
                "{" +
                "\"JobID\": \"" + mJobObject.getJobID() + "\"," +
                "\"RegoNo\": \"" + cwevolution4.getText().toString() + "\"," +
                "\"Landmark\": \"" + cwlocallandmark4.getText().toString() + "\"," +
                "\"CarriageWay\": \"1\"," +
                "\"Direction\": \"" + spinner.getSelectedItemPosition() + "\"," +
                "    \"TimeErected\": \"" + Time1 + "\",\n" +
                "    \"TimeCollected\": \"" + Time6 + "\",\n" +
                "    \"TimeChecked1\": \"" + Time2 + "\",\n" +
                "    \"TimeChecked2\": \"" + Time3 + "\",\n" +
                "    \"TimeChecked3\": \"" + Time4 + "\",\n" +
                "    \"TimeChecked4\": \"" + Time5 + "\",\n" +
                "    \"aftercare\": \"" + aftercares + "\",\n" +
                "    \"AuditSigns\": \"SignID:1^Metres:" + qld1.getText() + "" +
                "~SignID:2^Metres:" + qld2.getText().toString() + "~SignID:3^Metres:" + qld3.getText() + "~SignID:4^Metres:" + qld4.getText() + "~SignID:5^Metres:" + qld5.getText() + "~" +
                "SignID:6^Metres:" + qld6.getText() + "~SignID:7^Metres:" + qld7.getText() + "~SignID:8^Metres:" + qld8.getText() + "~SignID:9^Metres:" + qld9.getText() + "~" +
                "SignID:10^Metres:" + qld10.getText() + "~SignID:11^Metres:" + qld11.getText() + "~SignID:12^Metres:" + qld13.getText() + "~" +
                "SignID:13^Metres:" + qld14.getText() + "~SignID:14^Metres:" + qld15.getText() + "~SignID:15^Metres:" + qld16.getText() + "~SignID:16^Metres:" + qld17.getText() + "~" +
                "SignID:17^Metres:" + qld18.getText() + "~SignID:18^Metres:" + qld19.getText() + "~SignID:19^Metres:" + qld20.getText() + "~SignID:20^Metres:" + qld21.getText() + "~" +
                "SignID:21^Metres:" + qld22.getText() + "~SignID:22^Metres:" + qld23.getText() + "~SignID:23^Metres:" + qld24.getText() + "~SignID:24^Metres:" + qld25.getText() + "~" +
                "SignID:25^Metres:" + qld26.getText() + "~SignID:26^Metres:" + qld27.getText() + "~SignID:27^Metres:" + qld28.getText() + "~SignID:28^Metres:" + qld29.getText() + "~" +
                "SignID:29^Metres:" + qld30.getText() + "~SignID:30^Metres:" + qld31.getText() + "~SignID:31^Metres:" + qld32.getText() + "~SignID:32^Metres:" + qld33.getText() + "~" +
                "SignID:33^Metres:" + qld34.getText() + "~SignID:34^Metres:" + qld35.getText() + "~SignID:35^Metres:" + qld36.getText() + "~SignID:36^Metres:" + qld37.getText() + "~" +
                "SignID:37^Metres:" + qld38.getText() + "~SignID:38^Metres:" + qld39.getText() + "~SignID:39^Metres:" + qld40.getText() + "~SignID:40^Metres:" + qld41.getText() + "~" +
                "SignID:41^Metres:" + qld42.getText() + "~SignID:42^Metres:" + qld43.getText() + "~SignID:43^Metres:" + qld44.getText() + "~SignID:44^Metres:" + qld45.getText() +
                "~SignID:45^Metres:" + qld46.getText() + "~SignID:46^Metres:" + qld47.getText() + "~SignID:47^Metres:" + qld48.getText() + "~SignID:48^Metres:" + qld49.getText() +
                "~SignID:49^Metres:" + qld50.getText() + "~SignID:50^Metres:" + qld51.getText() + "~SignID:51^Metres:" + qld52.getText() + "~SignID:52^Metres:" + qld53.getText() +
                "~SignID:53^Metres:" + qld54.getText() + "~SignID:54^Metres:" + qld55.getText() + "~SignID:55^Metres:" + qld56.getText() + "~SignID:56^Metres:" + qld57.getText() +
                "~SignID:57^Metres:" + qld58.getText() + "~SignID:58^Metres:" + qld59.getText() + "~SignID:59^Metres:" + qld60.getText() + "~SignID:60^Metres:" + qld61.getText() +
                "~SignID:61^Metres:" + qld62.getText() + "~SignID:62^Metres:" + qld63.getText() + "~SignID:63^Metres:" + qld64.getText() + "~SignID:64^Metres:" + qld65.getText() +
                "~SignID:65^Metres:" + qld66.getText() + "~SignID:66^Metres:" + qld67.getText() + "~SignID:67^Metres:" + qld68.getText() + "~SignID:68^Metres:" + qld69.getText() +
                "~SignID:69^Metres:" + qld70.getText() + "~SignID:70^Metres:" + qld71.getText() + "~SignID:71^Metres:" + qld72.getText() + "~SignID:72^Metres:" + qld73.getText() +
                "~SignID:73^Metres:" + qld74.getText() + "~SignID:74^Metres:" + qld75.getText() + "~SignID:75^Metres:" + qld76.getText() + "~SignID:76^Metres:" + qld77.getText() +
                "\"}," + "{" +
                "\"JobID\": \"" + mJobObject.getJobID() + "\",\n" +
                "\"RegoNo\": \"" + cwevolution4.getText().toString() + "\",\n" +
                "\"Landmark\": \"" + cwlocallandmark4.getText().toString() + "\",\n" +
                "\"CarriageWay\":\"2\"," +
                "\"Direction\": \"" + spinner.getSelectedItemPosition() + "\",\n" +
                "    \"TimeErected\": \"" + Time1 + "\",\n" +
                "    \"TimeCollected\": \"" + Time6 + "\",\n" +
                "    \"TimeChecked1\": \"" + Time2 + "\",\n" +
                "    \"TimeChecked2\": \"" + Time3 + "\",\n" +
                "    \"TimeChecked3\": \"" + Time4 + "\",\n" +
                "    \"TimeChecked4\": \"" + Time5 + "\",\n" +
                "    \"aftercare\": \"" + aftercares + "\",\n" +
                "    \"AuditSigns\": \"SignID:1^Metres:" + qlda1.getText() + "" +                "~SignID:2^Metres:" + qld2.getText().toString() + "~SignID:3^Metres:" + qld3.getText() + "~SignID:4^Metres:" + qld4.getText() + "~SignID:5^Metres:" + qld5.getText() + "~" +
                "SignID:6^Metres:" + qld6.getText() + "~SignID:7^Metres:" + qld7.getText() + "~SignID:8^Metres:" + qld8.getText() + "~SignID:9^Metres:" + qld9.getText() + "~" +
                "SignID:10^Metres:" + qld10.getText() + "~SignID:11^Metres:" + qld11.getText() + "~SignID:12^Metres:" + qld13.getText() + "~" +
                "SignID:13^Metres:" + qld14.getText() + "~SignID:14^Metres:" + qld15.getText() + "~SignID:15^Metres:" + qld16.getText() + "~SignID:16^Metres:" + qld17.getText() + "~" +
                "SignID:17^Metres:" + qld18.getText() + "~SignID:18^Metres:" + qld19.getText() + "~SignID:19^Metres:" + qld20.getText() + "~SignID:20^Metres:" + qld21.getText() + "~" +
                "SignID:21^Metres:" + qld22.getText() + "~SignID:22^Metres:" + qld23.getText() + "~SignID:23^Metres:" + qld24.getText() + "~SignID:24^Metres:" + qld25.getText() + "~" +
                "SignID:25^Metres:" + qld26.getText() + "~SignID:26^Metres:" + qld27.getText() + "~SignID:27^Metres:" + qld28.getText() + "~SignID:28^Metres:" + qld29.getText() + "~" +
                "SignID:29^Metres:" + qld30.getText() + "~SignID:30^Metres:" + qld31.getText() + "~SignID:31^Metres:" + qld32.getText() + "~SignID:32^Metres:" + qld33.getText() + "~" +
                "SignID:33^Metres:" + qld34.getText() + "~SignID:34^Metres:" + qld35.getText() + "~SignID:35^Metres:" + qld36.getText() + "~SignID:36^Metres:" + qld37.getText() + "~" +
                "SignID:37^Metres:" + qld38.getText() + "~SignID:38^Metres:" + qld39.getText() + "~SignID:39^Metres:" + qld40.getText() + "~SignID:40^Metres:" + qld41.getText() + "~" +
                "SignID:41^Metres:" + qld42.getText() + "~SignID:42^Metres:" + qld43.getText() + "~SignID:43^Metres:" + qld44.getText() + "~SignID:44^Metres:" + qld45.getText() +
                "~SignID:45^Metres:" + qld46.getText() + "~SignID:46^Metres:" + qld47.getText() + "~SignID:47^Metres:" + qld48.getText() + "~SignID:48^Metres:" + qld49.getText() +
                "~SignID:49^Metres:" + qld50.getText() + "~SignID:50^Metres:" + qld51.getText() + "~SignID:51^Metres:" + qld52.getText() + "~SignID:52^Metres:" + qld53.getText() +
                "~SignID:53^Metres:" + qld54.getText() + "~SignID:54^Metres:" + qld55.getText() + "~SignID:55^Metres:" + qld56.getText() + "~SignID:56^Metres:" + qld57.getText() +
                "~SignID:57^Metres:" + qld58.getText() + "~SignID:58^Metres:" + qld59.getText() + "~SignID:59^Metres:" + qld60.getText() + "~SignID:60^Metres:" + qld61.getText() +
                "~SignID:61^Metres:" + qld62.getText() + "~SignID:62^Metres:" + qld63.getText() + "~SignID:63^Metres:" + qld64.getText() + "~SignID:64^Metres:" + qld65.getText() +
                "~SignID:65^Metres:" + qld66.getText() + "~SignID:66^Metres:" + qld67.getText() + "~SignID:67^Metres:" + qld68.getText() + "~SignID:68^Metres:" + qld69.getText() +
                "~SignID:69^Metres:" + qld70.getText() + "~SignID:70^Metres:" + qld71.getText() + "~SignID:71^Metres:" + qld72.getText() + "~SignID:72^Metres:" + qld73.getText() +
                "~SignID:73^Metres:" + qld74.getText() + "~SignID:74^Metres:" + qld75.getText() + "~SignID:75^Metres:" + qld76.getText() + "~SignID:76^Metres:" + qld77.getText() +
                "\"}\n" +
                "]";

        Log.e("this is a test", re);
        if (db.checkExistCreateJObs(mJobObject.getJobID()) == null) {
            db.addCreateJobs(mJobObject.getJobID());
        }
        db.updateContact(mJobObject.getJobID(), "", "", "", re.toString(), "");
        Toast.makeText(QLDSignages.this, "Changes have been saved!", Toast.LENGTH_SHORT).show();
    }

    public String getSignageAudit() {
        String temp = "";
        temp = "{\"mac\":" + db.getSignageAudt(mJobObject.getJobID()) + "}";
        return temp;
    }

    public String getcontetn(String gets, int position) {
        String getz = "";
        String[] splitters = gets.split("~");

        String abc = splitters[position].replace("^", "#");
        String[] splitter = abc.split("#");

        if (splitter.length >= 2) {
            getz = splitter[1];
        } else {
            getz = "";
        }
        return getz.replace("Metres:", "");
    }

    public void populateData() {
        JSONArray data = null;
        Log.e("haha", getSignageAudit());

        if (getSignageAudit().equals("{\"mac\":null}")) {

        } else {
            try {
                JSONObject jsonObj = new JSONObject(getSignageAudit());

                // Getting JSON Array node
                data = jsonObj.getJSONArray("mac");

                JSONObject c = data.getJSONObject(0);
                JSONObject x = data.getJSONObject(1);

                Log.e("JOB ID", c.getString("JobID"));
                cwevolution4.setText(c.getString("RegoNo"));
                cwlocallandmark4.setText(c.getString("Landmark"));

                spinner.setSelection(Integer.parseInt(c.getString("Direction")), true);

                if (!c.getString("TimeErected").equals("null")) {
                    TimeErected.setText(date.miliSecondConverter(c.getString("TimeErected"), "HH:mm"));
                    TimeErected2 = c.getString("TimeErected");
                    Time1 = c.getString("TimeErected");
                }

                if (!c.getString("TimeCollected").equals("null")) {
                    TimeCollected.setText(date.miliSecondConverter(c.getString("TimeCollected"), "HH:mm"));
                    TimeCollected2 = c.getString("TimeCollected");
                    Time6 = c.getString("TimeErected");
                }
                if (!c.getString("TimeChecked1").equals("null")) {
                    TimeChecked1.setText(date.miliSecondConverter(c.getString("TimeChecked1"), "HH:mm"));
                    TimeChecked12 = c.getString("TimeChecked1");
                    Time2 = c.getString("TimeChecked1");
                }
                if (!c.getString("TimeChecked2").equals("null")) {
                    TimeChecked2.setText(date.miliSecondConverter(c.getString("TimeChecked2"), "HH:mm"));
                    TimeChecked22 = c.getString("TimeChecked2");
                    Time3 = c.getString("TimeChecked2");
                }
                if (!c.getString("TimeChecked3").equals("null")) {
                    TimeChecked3.setText(date.miliSecondConverter(c.getString("TimeChecked3"), "HH:mm"));
                    TimeChecked32 = c.getString("TimeChecked3");
                    Time4 = c.getString("TimeChecked3");
                }
                if (!c.getString("TimeChecked4").equals("null")) {
                    TimeChecked4.setText(date.miliSecondConverter(c.getString("TimeChecked4"), "HH:mm"));
                    TimeChecked42 = c.getString("TimeChecked4");
                    Time5 = c.getString("TimeChecked3");
                }

                if (!c.getString("aftercare").equals("null")) {
                    ((RadioButton) aftercare.getChildAt(Integer.parseInt(c.getString("aftercare")))).setChecked(true);
                }

                qld1.setText(getcontetn(c.getString("AuditSigns"), 0));
                qld2.setText(getcontetn(c.getString("AuditSigns"), 1));
                qld3.setText(getcontetn(c.getString("AuditSigns"), 2));
                qld4.setText(getcontetn(c.getString("AuditSigns"), 3));
                qld5.setText(getcontetn(c.getString("AuditSigns"), 4));
                qld6.setText(getcontetn(c.getString("AuditSigns"), 5));
                qld7.setText(getcontetn(c.getString("AuditSigns"), 6));
                qld8.setText(getcontetn(c.getString("AuditSigns"), 7));
                qld9.setText(getcontetn(c.getString("AuditSigns"), 8));
                qld10.setText(getcontetn(c.getString("AuditSigns"), 9));
                qld11.setText(getcontetn(c.getString("AuditSigns"), 10));
                qld13.setText(getcontetn(c.getString("AuditSigns"), 11));
                qld14.setText(getcontetn(c.getString("AuditSigns"), 12));
                qld15.setText(getcontetn(c.getString("AuditSigns"), 13));
                qld16.setText(getcontetn(c.getString("AuditSigns"), 14));
                qld17.setText(getcontetn(c.getString("AuditSigns"), 15));
                qld18.setText(getcontetn(c.getString("AuditSigns"), 16));
                qld19.setText(getcontetn(c.getString("AuditSigns"), 17));
                qld20.setText(getcontetn(c.getString("AuditSigns"), 18));
                qld21.setText(getcontetn(c.getString("AuditSigns"), 19));
                qld22.setText(getcontetn(c.getString("AuditSigns"), 20));
                qld23.setText(getcontetn(c.getString("AuditSigns"), 21));
                qld24.setText(getcontetn(c.getString("AuditSigns"), 22));
                qld25.setText(getcontetn(c.getString("AuditSigns"), 23));
                qld26.setText(getcontetn(c.getString("AuditSigns"), 24));
                qld27.setText(getcontetn(c.getString("AuditSigns"), 25));
                qld28.setText(getcontetn(c.getString("AuditSigns"), 26));
                qld29.setText(getcontetn(c.getString("AuditSigns"), 27));
                qld30.setText(getcontetn(c.getString("AuditSigns"), 28));
                qld31.setText(getcontetn(c.getString("AuditSigns"), 29));
                qld32.setText(getcontetn(c.getString("AuditSigns"), 30));
                qld33.setText(getcontetn(c.getString("AuditSigns"), 31));
                qld34.setText(getcontetn(c.getString("AuditSigns"), 32));
                qld35.setText(getcontetn(c.getString("AuditSigns"), 33));
                qld36.setText(getcontetn(c.getString("AuditSigns"), 34));
                qld37.setText(getcontetn(c.getString("AuditSigns"), 35));
                qld38.setText(getcontetn(c.getString("AuditSigns"), 36));
                qld39.setText(getcontetn(c.getString("AuditSigns"), 37));
                qld40.setText(getcontetn(c.getString("AuditSigns"), 38));
                qld41.setText(getcontetn(c.getString("AuditSigns"), 39));
                qld42.setText(getcontetn(c.getString("AuditSigns"), 40));
                qld43.setText(getcontetn(c.getString("AuditSigns"), 41));
                qld44.setText(getcontetn(c.getString("AuditSigns"), 42));
                qld45.setText(getcontetn(c.getString("AuditSigns"), 43));
                qld46.setText(getcontetn(c.getString("AuditSigns"), 44));
                qld47.setText(getcontetn(c.getString("AuditSigns"), 45));
                qld48.setText(getcontetn(c.getString("AuditSigns"), 46));
                qld49.setText(getcontetn(c.getString("AuditSigns"), 47));
                qld50.setText(getcontetn(c.getString("AuditSigns"), 48));
                qld51.setText(getcontetn(c.getString("AuditSigns"), 59));
                qld52.setText(getcontetn(c.getString("AuditSigns"), 50));
                qld53.setText(getcontetn(c.getString("AuditSigns"), 51));
                qld54.setText(getcontetn(c.getString("AuditSigns"), 52));
                qld55.setText(getcontetn(c.getString("AuditSigns"), 53));
                qld56.setText(getcontetn(c.getString("AuditSigns"), 54));
                qld57.setText(getcontetn(c.getString("AuditSigns"), 55));
                qld58.setText(getcontetn(c.getString("AuditSigns"), 56));
                qld59.setText(getcontetn(c.getString("AuditSigns"), 57));
                qld60.setText(getcontetn(c.getString("AuditSigns"), 58));
                qld61.setText(getcontetn(c.getString("AuditSigns"), 59));
                qld62.setText(getcontetn(c.getString("AuditSigns"), 60));
                qld63.setText(getcontetn(c.getString("AuditSigns"), 61));
                qld64.setText(getcontetn(c.getString("AuditSigns"), 62));
                qld65.setText(getcontetn(c.getString("AuditSigns"), 63));
                qld66.setText(getcontetn(c.getString("AuditSigns"), 64));
                qld67.setText(getcontetn(c.getString("AuditSigns"), 65));
                qld68.setText(getcontetn(c.getString("AuditSigns"), 66));
                qld69.setText(getcontetn(c.getString("AuditSigns"), 67));
                qld70.setText(getcontetn(c.getString("AuditSigns"), 68));
                qld71.setText(getcontetn(c.getString("AuditSigns"), 69));
                qld72.setText(getcontetn(c.getString("AuditSigns"), 70));
                qld73.setText(getcontetn(c.getString("AuditSigns"), 71));
                qld74.setText(getcontetn(c.getString("AuditSigns"), 72));
                qld75.setText(getcontetn(c.getString("AuditSigns"), 73));
                qld76.setText(getcontetn(c.getString("AuditSigns"), 74));
                qld77.setText(getcontetn(c.getString("AuditSigns"), 75));

                qlda1.setText(getcontetn(x.getString("AuditSigns"), 0));
                qlda2.setText(getcontetn(x.getString("AuditSigns"), 1));
                qlda3.setText(getcontetn(x.getString("AuditSigns"), 2));
                qlda4.setText(getcontetn(x.getString("AuditSigns"), 3));
                qlda5.setText(getcontetn(x.getString("AuditSigns"), 4));
                qlda6.setText(getcontetn(x.getString("AuditSigns"), 5));
                qlda7.setText(getcontetn(x.getString("AuditSigns"), 6));
                qlda8.setText(getcontetn(x.getString("AuditSigns"), 7));
                qlda9.setText(getcontetn(x.getString("AuditSigns"), 8));
                qlda10.setText(getcontetn(x.getString("AuditSigns"), 9));
                qlda11.setText(getcontetn(x.getString("AuditSigns"), 10));
                qlda13.setText(getcontetn(x.getString("AuditSigns"), 11));
                qlda14.setText(getcontetn(x.getString("AuditSigns"), 12));
                qlda15.setText(getcontetn(x.getString("AuditSigns"), 13));
                qlda16.setText(getcontetn(x.getString("AuditSigns"), 14));
                qlda17.setText(getcontetn(x.getString("AuditSigns"), 15));
                qlda18.setText(getcontetn(x.getString("AuditSigns"), 16));
                qlda19.setText(getcontetn(x.getString("AuditSigns"), 17));
                qlda20.setText(getcontetn(x.getString("AuditSigns"), 18));
                qlda21.setText(getcontetn(x.getString("AuditSigns"), 19));
                qlda22.setText(getcontetn(x.getString("AuditSigns"), 20));
                qlda23.setText(getcontetn(x.getString("AuditSigns"), 21));
                qlda24.setText(getcontetn(x.getString("AuditSigns"), 22));
                qlda25.setText(getcontetn(x.getString("AuditSigns"), 23));
                qlda26.setText(getcontetn(x.getString("AuditSigns"), 24));
                qlda27.setText(getcontetn(x.getString("AuditSigns"), 25));
                qlda28.setText(getcontetn(x.getString("AuditSigns"), 26));
                qlda29.setText(getcontetn(x.getString("AuditSigns"), 27));
                qlda30.setText(getcontetn(x.getString("AuditSigns"), 28));
                qlda31.setText(getcontetn(x.getString("AuditSigns"), 29));
                qlda32.setText(getcontetn(x.getString("AuditSigns"), 30));
                qlda33.setText(getcontetn(x.getString("AuditSigns"), 31));
                qlda34.setText(getcontetn(x.getString("AuditSigns"), 32));
                qlda35.setText(getcontetn(x.getString("AuditSigns"), 33));
                qlda36.setText(getcontetn(x.getString("AuditSigns"), 34));
                qlda37.setText(getcontetn(x.getString("AuditSigns"), 35));
                qlda38.setText(getcontetn(x.getString("AuditSigns"), 36));
                qlda39.setText(getcontetn(x.getString("AuditSigns"), 37));
                qlda40.setText(getcontetn(x.getString("AuditSigns"), 38));
                qlda41.setText(getcontetn(x.getString("AuditSigns"), 39));
                qlda42.setText(getcontetn(x.getString("AuditSigns"), 40));
                qlda43.setText(getcontetn(x.getString("AuditSigns"), 41));
                qlda44.setText(getcontetn(x.getString("AuditSigns"), 42));
                qlda45.setText(getcontetn(x.getString("AuditSigns"), 43));
                qlda46.setText(getcontetn(x.getString("AuditSigns"), 44));
                qlda47.setText(getcontetn(x.getString("AuditSigns"), 45));
                qlda48.setText(getcontetn(x.getString("AuditSigns"), 46));
                qlda49.setText(getcontetn(x.getString("AuditSigns"), 47));
                qlda50.setText(getcontetn(x.getString("AuditSigns"), 48));
                qlda51.setText(getcontetn(x.getString("AuditSigns"), 59));
                qlda52.setText(getcontetn(x.getString("AuditSigns"), 50));
                qlda53.setText(getcontetn(x.getString("AuditSigns"), 51));
                qlda54.setText(getcontetn(x.getString("AuditSigns"), 52));
                qlda55.setText(getcontetn(x.getString("AuditSigns"), 53));
                qlda56.setText(getcontetn(x.getString("AuditSigns"), 54));
                qlda57.setText(getcontetn(x.getString("AuditSigns"), 55));
                qlda58.setText(getcontetn(x.getString("AuditSigns"), 56));
                qlda59.setText(getcontetn(x.getString("AuditSigns"), 57));
                qlda60.setText(getcontetn(x.getString("AuditSigns"), 58));
                qlda61.setText(getcontetn(x.getString("AuditSigns"), 59));
                qlda62.setText(getcontetn(x.getString("AuditSigns"), 60));
                qlda63.setText(getcontetn(x.getString("AuditSigns"), 61));
                qlda64.setText(getcontetn(x.getString("AuditSigns"), 62));
                qlda65.setText(getcontetn(x.getString("AuditSigns"), 63));
                qlda66.setText(getcontetn(x.getString("AuditSigns"), 64));
                qlda67.setText(getcontetn(x.getString("AuditSigns"), 65));
                qlda68.setText(getcontetn(x.getString("AuditSigns"), 66));
                qlda69.setText(getcontetn(x.getString("AuditSigns"), 67));
                qlda70.setText(getcontetn(x.getString("AuditSigns"), 68));
                qlda71.setText(getcontetn(x.getString("AuditSigns"), 69));
                qlda72.setText(getcontetn(x.getString("AuditSigns"), 70));
                qlda73.setText(getcontetn(x.getString("AuditSigns"), 71));
                qlda74.setText(getcontetn(x.getString("AuditSigns"), 72));
                qlda75.setText(getcontetn(x.getString("AuditSigns"), 73));
                qlda76.setText(getcontetn(x.getString("AuditSigns"), 74));
                qlda77.setText(getcontetn(x.getString("AuditSigns"), 75));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }


}
