package com.evolution.egm.Activities.signs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.evolution.egm.Utils.CreateJobSharedPreference;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Utils.DateHelper;
import com.evolution.egm.Models.Job;
import com.evolution.egm.R;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WASignagev2 extends AppCompatActivity implements View.OnTouchListener, View.OnClickListener {

    Toolbar toolbar;
    RadioButton slowlane, fastlane;
    LinearLayout slowlanel, fastlanel;
    MaterialEditText cwevolution4, cwlocallandmark4, TimeErected, TimeCollected, TimeChecked1, TimeChecked2, TimeChecked3, TimeChecked4;
    Spinner spinner;
    String Time1, Time2, Time3, Time4, Time5, Time6;
    String TimeErected2, TimeCollected2, TimeChecked12, TimeChecked22, TimeChecked32, TimeChecked42;
    DateHelper date;
    DatabaseHelper db;
    Dialog dialog;
    DatePicker datepicker;
    TimePicker timepicker;
    List<String> categories;
    final CharSequence cs1 = "-";
    Dialog.Builder builder2 = null;
    Job mJobObject;
    CreateJobSharedPreference shared;

    EditText w1, w2, w3, w4, w5, w6, w7, w8, w9, w10, w11, w12, w13, w14, w15, w16, w17, w18, w19, w20, w21, w22, w23, w24, w25, w26, w27, w28, w29, w30, w31, w32, w33, w34, w35, w36, w37, w38, w39, w40, w41, w42, w43, w44, w45, w46, w47, w48, w49, w50, w51, w52, w53, w54, w55, w56, w57, w58, w59, w60, w61, w62, w63;
    EditText wa1, wa2, wa3, wa4, wa5, wa6, wa7, wa8, wa9, wa10, wa11, wa12, wa13, wa14, wa15, wa16, wa17, wa18, wa19, wa20, wa21, wa22, wa23, wa24, wa25, wa26, wa27, wa28, wa29, wa30, wa31, wa32, wa33, wa34, wa35, wa36, wa37, wa38, wa39, wa40, wa41, wa42, wa43, wa44, wa45, wa46, wa47, wa48, wa49, wa50, wa51, wa52, wa53, wa54, wa55, wa56, wa57, wa58, wa59, wa60, wa61, wa62, wa63;

    String aftercares = "null";

    CheckBox ishaveaftercare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wasignsv2);
        initToolBar();

        date = new DateHelper();
        db = new DatabaseHelper(this);
        dialog = new Dialog(this, R.style.FullHeightDialog);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));
        }
        shared = new CreateJobSharedPreference(this);

        initViews();
        initSpinner();
        populateData();
        setTagme();
    }


    public void setTagme() {
        LinearLayout layout = (LinearLayout) findViewById(R.id.slowlanelsecond);
        for (int dfd = 0; dfd < layout.getChildCount(); dfd++) {
            LinearLayout la = (LinearLayout) layout.getChildAt(dfd);
            for (int c = 0; c < la.getChildCount(); c++) {
                View v = (View) la.getChildAt(c);
                if (v instanceof CheckBox) {
                    CheckBox b = (CheckBox) v;
                    b.setOnCheckedChangeListener(ss);
                    b.setTag(dfd);
                }
            }
        }
    }

    CompoundButton.OnCheckedChangeListener ss = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if (b) {
                slowlaneString2(compoundButton.getTag().toString(), slowlaneString1(compoundButton.getTag().toString()));
            } else {
                slowlaneString5(compoundButton.getTag().toString());
            }
        }
    };


    public String slowlaneString1(String index) {
        int id = Integer.parseInt(index);
        String first = "";
        String second = "";
        LinearLayout layout = (LinearLayout) findViewById(R.id.slowlanelfirst);
        LinearLayout la = (LinearLayout) layout.getChildAt(id);
        for (int ds = 0; ds < la.getChildCount(); ds++) {
            View v = (View) la.getChildAt(ds);
            if (v instanceof EditText) {
                EditText b = (EditText) v;
                if (ds == 1) {
                    first = b.getText().toString();
                }

                if (ds == 2) {
                    second = b.getText().toString();
                }
            }
        }
        return first + "," + second;
    }

    public void slowlaneString5(String index) {
        int id = Integer.parseInt(index);

        String first = "";
        String second = "";
        LinearLayout layout = (LinearLayout) findViewById(R.id.slowlanelsecond);
        LinearLayout la = (LinearLayout) layout.getChildAt(id);

        for (int ds = 0; ds < la.getChildCount(); ds++) {
            View v = (View) la.getChildAt(ds);
            if (v instanceof EditText) {
                EditText b = (EditText) v;
                if (ds == 1) {
                    b.setText("");
                }
                if (ds == 2) {
                    b.setText("");
                }
            }
        }
    }


    public void slowlaneString2(String index, String topast) {
        int id = Integer.parseInt(index);
        Log.e("pulag", topast);

        if (!topast.equals(",")) {
            String[] toepx = topast.split(",");

            String first = "";
            String second = "";
            LinearLayout layout = (LinearLayout) findViewById(R.id.slowlanelsecond);
            LinearLayout la = (LinearLayout) layout.getChildAt(id);

            for (int ds = 0; ds < la.getChildCount(); ds++) {
                View v = (View) la.getChildAt(ds);
                if (v instanceof EditText) {
                    EditText b = (EditText) v;

                    if (ds == 1) {
                        if (!toepx[0].equals(null))
                            b.setText(toepx[0]);
                    }

                    if (ds == 2) {
                        if (!toepx[1].equals(null))
                            b.setText(toepx[1]);
                    }
                }
            }
        }
    }



    public void initSpinner() {
        // Spinner click listener
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // On selecting a spinner item
                String item = parent.getItemAtPosition(position).toString();

                // Showing selected spinner item
//                Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Spinner Drop down elements
        categories = new ArrayList<String>();
        categories.add("North");
        categories.add("South");
        categories.add("East");
        categories.add("West");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinnerlocation, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.spinnerlocation);

        spinner.setAdapter(dataAdapter);

    }

    public void initViews() {

        TimeErected = (MaterialEditText) findViewById(R.id.TimeErected);
        TimeCollected = (MaterialEditText) findViewById(R.id.TimeCollected);
        TimeChecked1 = (MaterialEditText) findViewById(R.id.TimeChecked1);
        TimeChecked2 = (MaterialEditText) findViewById(R.id.TimeChecked2);
        TimeChecked3 = (MaterialEditText) findViewById(R.id.TimeChecked3);
        TimeChecked4 = (MaterialEditText) findViewById(R.id.TimeChecked4);

        TimeErected.setOnTouchListener(this);
        TimeCollected.setOnTouchListener(this);
        TimeChecked1.setOnTouchListener(this);
        TimeChecked2.setOnTouchListener(this);
        TimeChecked3.setOnTouchListener(this);
        TimeChecked4.setOnTouchListener(this);

        spinner = (Spinner) findViewById(R.id.cwdirectionlist4);

        slowlane = (RadioButton) findViewById(R.id.slowlane);
        fastlane = (RadioButton) findViewById(R.id.fastlane);

        slowlanel = (LinearLayout) findViewById(R.id.slowlanel);
        fastlanel = (LinearLayout) findViewById(R.id.fastlanel);

        cwevolution4 = (MaterialEditText) findViewById(R.id.cwevolution4);
        cwlocallandmark4 = (MaterialEditText) findViewById(R.id.cwlocallandmark4);

        slowlane.setOnClickListener(this);
        fastlane.setOnClickListener(this);

        ishaveaftercare = (CheckBox) findViewById(R.id.ishaveaftercare);
        ishaveaftercare.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    setDisableEditText(false);
                } else {
                    setDisableEditText(true);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_qldsignages, menu);
        return true;
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Signage Audit");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.saveqld:
                saveSings();
                break;
            case android.R.id.home:
                finish();
                overridePendingTransition(0, 0);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean checkifEmpty(MaterialEditText isEmpty, String messageError) {
        boolean temp = false;
        if (isEmpty.getText().toString().equals("")) {
            temp = true;
            isEmpty.setError(messageError);
        }
        return temp;
    }

    public void timedate(final int to, String whoClicked) {

        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.activity_timesheetpicker);

        datepicker = (DatePicker) dialog.findViewById(R.id.datePicker);
        datepicker.setVisibility(datepicker.GONE);
        timepicker = (TimePicker) dialog.findViewById(R.id.timePicker);

        LinearLayout save = (LinearLayout) dialog.findViewById(R.id.save);
        LinearLayout cancel = (LinearLayout) dialog.findViewById(R.id.cancel);
        TextView title = (TextView) dialog.findViewById(R.id.title);
        title.setText(whoClicked);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (to) {
                    case 1: {
                        TimeErected.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeErected2 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                    case 2: {
                        TimeCollected.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeCollected2 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                    case 3: {
                        TimeChecked1.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeChecked12 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                    case 4: {
                        TimeChecked2.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeChecked22 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                    case 5: {
                        TimeChecked3.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeChecked32 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                    case 6: {
                        TimeChecked4.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeChecked42 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    public boolean checkifhave(String toValidate) {
        boolean temp = false;
        boolean ishave = toValidate.contains(cs1);
        if (ishave) {
            temp = true;
        }
        return temp;
    }


    public String getTimeDifference(String first, String second) {
        long diffMinutes = 0;
        long diffHours = 0;
        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(first);
            d2 = format.parse(second);
            long diff = d2.getTime() - d1.getTime();
            diffMinutes = diff / (60 * 1000) % 60;
            diffHours = diff / (60 * 60 * 1000);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return diffHours + "h  " + diffMinutes + "m";
    }


    public Long generateMili(DatePicker datem, TimePicker timem) {
        int day = datem.getDayOfMonth();
        int month = datem.getMonth() + 1;
        int year = datem.getYear();
        String returns = day + " " + month + " " + year + " " + timem.getCurrentHour() + ":" + timem.getCurrentMinute() + "";
        return Long.parseLong(convertDate(returns));
    }

    public String convertDate(String date) {
        String temp = "";
        long timeInMilliseconds = 0;
        String givenDateString = date;
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
        try {
            Date mDate = sdf.parse(givenDateString);
            timeInMilliseconds = mDate.getTime();
            System.out.println("Date in milli :: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds + "";
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.TimeErected: {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    initTimepicker(1);
                }
                return true;
            }
            case R.id.TimeCollected:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeChecked4.getText().toString().equals("")) {
                        Toast.makeText(WASignagev2.this, "Please add Time Checked.", Toast.LENGTH_SHORT).show();
                    } else {
                        initTimepicker(2);
                    }
                }
                return true;
            case R.id.TimeChecked1:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeErected.getText().toString().equals("")) {
                        Toast.makeText(WASignagev2.this, "Please add Time Erected.", Toast.LENGTH_SHORT).show();
                    } else {
                        initTimepicker(3);
                    }
                }
                return true;
            case R.id.TimeChecked2:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeChecked1.getText().toString().equals("")) {
                        Toast.makeText(WASignagev2.this, "Please add Time Checked.", Toast.LENGTH_SHORT).show();
                    } else {
                        initTimepicker(4);
                    }
                }
                return true;
            case R.id.TimeChecked3:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeChecked2.getText().toString().equals("")) {
                        Toast.makeText(WASignagev2.this, "Please add Time Checked.", Toast.LENGTH_SHORT).show();
                    } else {
                        initTimepicker(5);
                    }
                }
                return true;
            case R.id.TimeChecked4:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeChecked3.getText().toString().equals("")) {
                        Toast.makeText(WASignagev2.this, "Please add Time Checked.", Toast.LENGTH_SHORT).show();
                    } else {
                        initTimepicker(6);
                    }
                }
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.slowlane:
                slowlanel.setVisibility(slowlanel.VISIBLE);
                fastlanel.setVisibility(slowlanel.GONE);
                break;
            case R.id.fastlane:
                slowlanel.setVisibility(slowlanel.GONE);
                fastlanel.setVisibility(slowlanel.VISIBLE);
                break;
        }
    }


    public static String hourConverter(String dime) {
        Log.e("tsetses", dime);

        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm:ss a");
        Date date = null;
        try {
            date = parseFormat.parse(dime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return displayFormat.format(date) + "";
    }

    public void initTimepicker(final int to) {
        builder2 = new TimePickerDialog.Builder(6, 00) {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                final TimePickerDialog dialogr = (TimePickerDialog) fragment.getDialog();
                switch (to) {
                    case 1:
                        if (!TimeChecked1.getText().toString().equals("")) {
                            if (checkifhave(getTimeDifference(dialogr.getFormattedTime(DateFormat.getDateTimeInstance()), TimeErected2))) {
                                Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                            } else {
                                TimeErected.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                                TimeErected2 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());
                                Time1 = convertDate(TimeErected2);
                            }
                        } else {
                            TimeErected.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                            TimeErected2 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());
                            Time1 = convertDate(TimeErected2);
                        }
                        break;
                    case 3:
                        TimeChecked12 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());

                        if (checkifhave(getTimeDifference(TimeErected2, TimeChecked12))) {
                            TimeChecked12 = "";
                            Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                        } else {
                            Time2 = convertDate(TimeChecked12);
                            TimeChecked1.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                        }
                        break;
                    case 4:
                        TimeChecked22 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());
                        if (checkifhave(getTimeDifference(TimeChecked12, TimeChecked22))) {
                            TimeChecked22 = "";
                            Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                        } else {
                            Time3 = convertDate(TimeChecked22);
                            TimeChecked2.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                        }
                        break;
                    case 5:
                        TimeChecked32 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());
                        if (checkifhave(getTimeDifference(TimeChecked22, TimeChecked32))) {
                            TimeChecked32 = "";
                            Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                        } else {
                            Time4 = convertDate(TimeChecked32);
                            TimeChecked3.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                        }
                        break;
                    case 6:
                        TimeChecked42 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());

                        if (checkifhave(getTimeDifference(TimeChecked32, TimeChecked42))) {
                            TimeChecked42 = "";
                            Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                        } else {
                            Time5 = convertDate(TimeChecked42);
                            TimeChecked4.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                        }
                        break;
                    case 2:
                        TimeCollected2 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());

                        if (checkifhave(getTimeDifference(TimeChecked42, TimeCollected2))) {
                            TimeCollected2 = "";
                            Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                        } else {
                            Time6 = convertDate(TimeCollected2);
                            TimeCollected.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                        }
                        break;
                }

                super.onPositiveActionClicked(fragment);
                super.onNegativeActionClicked(fragment);

            }
        };

        builder2.positiveAction("OK")
                .negativeAction("CANCEL");

        DialogFragment fragment2 = DialogFragment.newInstance(builder2);
        fragment2.show(getSupportFragmentManager(), null);

    }

    public String getcontetn(String gets, int position) {
        String getz = "";
        String[] splitters = gets.split("~");

        String abc = splitters[position].replace("^", "#");
        String[] splitter = abc.split("#");

        if (splitter.length >= 2) {
            getz = splitter[1];
        } else {
            getz = "";
        }

        return getz.replace("Metres:", "");
    }


    public void saveSings() {

        if (ishaveaftercare.isChecked() == true) {
            aftercares = "1";
        } else {
            aftercares = "0";
        }

        String returnsslow = "";
        LinearLayout layout = (LinearLayout) findViewById(R.id.slowlanelfirst);
        for (int dfd = 0; dfd < layout.getChildCount(); dfd++) {
            LinearLayout la = (LinearLayout) layout.getChildAt(dfd);
            for (int c = 0; c < la.getChildCount(); c++) {
                View v = (View) la.getChildAt(c);
                if (v instanceof EditText) {
                    EditText b = (EditText) v;
                    if (c == 1) {
                        returnsslow = returnsslow + b.getText().toString() + "~";
                    }
                    if (c == 2) {
                        returnsslow = returnsslow + b.getText().toString() + "~";
                    }
                }
            }
            returnsslow = returnsslow + "^^";
        }
        Log.e("gawa ", returnsslow);

//
        String returnsfast = "";

        LinearLayout layout2 = (LinearLayout) findViewById(R.id.slowlanelsecond);
        for (int dfd = 0; dfd < layout2.getChildCount(); dfd++) {
            LinearLayout la = (LinearLayout) layout2.getChildAt(dfd);

            for (int c = 0; c < la.getChildCount(); c++) {
                View v = (View) la.getChildAt(c);
                if (v instanceof CheckBox) {
                    CheckBox b = (CheckBox) v;
                    if (c == 0) {
                        if (b.isChecked() == true) {
                            returnsfast = returnsfast + "1~";
                        } else {
                            returnsfast = returnsfast + "0~";
                        }
                    }
                }

                if (v instanceof EditText) {
                    EditText b = (EditText) v;
                    if (c == 1) {
                        returnsfast = returnsfast + b.getText().toString() + "~";
                    }
                    if (c == 2) {
                        returnsfast = returnsfast + b.getText().toString() + "~";
                    }
                }
            }
            returnsfast = returnsfast + "^^";
        }

        String returnsslow2 = "";
        LinearLayout layouta = (LinearLayout) findViewById(R.id.slowlanelfirst2);
        for (int dfd = 0; dfd < layouta.getChildCount(); dfd++) {
            LinearLayout la = (LinearLayout) layouta.getChildAt(dfd);
            for (int c = 0; c < la.getChildCount(); c++) {
                View v = (View) la.getChildAt(c);
                if (v instanceof EditText) {
                    EditText b = (EditText) v;
                    if (c == 1) {
                        returnsslow2 = returnsslow2 + b.getText().toString() + "~";
                    }
                    if (c == 2) {
                        returnsslow2 = returnsslow2 + b.getText().toString() + "~";
                    }
                }
            }
            returnsslow2 = returnsslow2 + "^^";
        }
        Log.e("gawa ", returnsslow2);

//
        String returnsfast2 = "";

        LinearLayout layout2a = (LinearLayout) findViewById(R.id.slowlanelsecond2);
        for (int dfd = 0; dfd < layout2a.getChildCount(); dfd++) {
            LinearLayout la = (LinearLayout) layout2a.getChildAt(dfd);

            for (int c = 0; c < la.getChildCount(); c++) {
                View v = (View) la.getChildAt(c);
                if (v instanceof CheckBox) {
                    CheckBox b = (CheckBox) v;
                    if (c == 0) {
                        if (b.isChecked() == true) {
                            returnsfast2 = returnsfast2 + "1~";
                        } else {
                            returnsfast2 = returnsfast2 + "0~";
                        }
                    }
                }

                if (v instanceof EditText) {
                    EditText b = (EditText) v;
                    if (c == 1) {
                        returnsfast2 = returnsfast2 + b.getText().toString() + "~";
                    }
                    if (c == 2) {
                        returnsfast2 = returnsfast2 + b.getText().toString() + "~";
                    }
                }

            }
            returnsfast2 = returnsfast2 + "^^";
        }
        String mjobid = "";
        if(cwevolution4.getText().toString().equals("")){
            mjobid = "null";
        }else{
            mjobid = cwevolution4.getText().toString();
        }


        String landmarks = "";


        if(cwlocallandmark4.getText().toString().equals("")){
            landmarks = "null";
        }else{
            landmarks = cwlocallandmark4.getText().toString();
        }


        String re = "[" +
                "{" +
                "\"JobID\": \"" + mJobObject.getJobID() + "\"," +
                "\"RegoNo\": \"" + mjobid + "\"," +
                "\"Landmark\": \"" + landmarks + "\"," +
                "\"CarriageWay\": \"1\"," +
                "\"Direction\": \"" + spinner.getSelectedItemPosition() + "\"," +
                "    \"TimeErected\": \"" + Time1 + "\",\n" +
                "    \"TimeCollected\": \"" + Time6 + "\",\n" +
                "    \"TimeChecked1\": \"" + Time2 + "\",\n" +
                "    \"TimeChecked2\": \"" + Time3 + "\",\n" +
                "    \"TimeChecked3\": \"" + Time4 + "\",\n" +
                "    \"TimeChecked4\": \"" + Time5 + "\",\n" +
                "    \"aftercare\": \"" + aftercares + "\",\n" +
                "    \"AuditSigns\": \"" + returnsslow + "-----" + returnsfast + "\"}," + "{" +
                "\"JobID\": \"" + mJobObject.getJobID() + "\",\n" +
                "\"RegoNo\": \"" + cwevolution4.getText().toString() + "\",\n" +
                "\"Landmark\": \"" + cwlocallandmark4.getText().toString() + "\",\n" +
                "\"CarriageWay\":\"2\"," +
                "\"Direction\": \"" + spinner.getSelectedItemPosition() + "\",\n" +
                "    \"TimeErected\": \"" + Time1 + "\",\n" +
                "    \"TimeCollected\": \"" + Time6 + "\",\n" +
                "    \"TimeChecked1\": \"" + Time2 + "\",\n" +
                "    \"TimeChecked2\": \"" + Time3 + "\",\n" +
                "    \"TimeChecked3\": \"" + Time4 + "\",\n" +
                "    \"TimeChecked4\": \"" + Time5 + "\",\n" +
                "    \"aftercare\": \"" + aftercares + "\",\n" +
                "    \"AuditSigns\": \"" + returnsslow2 + "-----" + returnsfast2 + "\"}" +
                "]";

        Log.e("this is a test", re);
        if (db.checkExistCreateJObs(mJobObject.getJobID()) == null) {
            db.addCreateJobs(mJobObject.getJobID());
        }
        db.updateContact(mJobObject.getJobID(), "", "", "", re.toString(), "");
        Toast.makeText(WASignagev2.this, "Changes have been saved!", Toast.LENGTH_SHORT).show();

    }

    public String getSignageAudit() {
        String temp = "";
//        temp = "{\"mac\":}";
        temp = "{\"mac\":" + db.getSignageAudt(mJobObject.getJobID()) + "}";
        return temp;
    }


    public void setDisableEditText(boolean f) {

        LinearLayout layout2 = (LinearLayout) findViewById(R.id.slowlanelsecond);
        for (int dfd = 0; dfd < layout2.getChildCount(); dfd++) {
            LinearLayout la = (LinearLayout) layout2.getChildAt(dfd);

            for (int c = 0; c < la.getChildCount(); c++) {
                View v = (View) la.getChildAt(c);

                if (v instanceof CheckBox) {
                    CheckBox b = (CheckBox) v;
                    if (f) {
                        b.setEnabled(false);
                    } else {
                        b.setEnabled(true);
                    }
                }

                if (f) {
                    if (v instanceof EditText) {
                        EditText b = (EditText) v;
                        if (c == 1) {
                            b.setEnabled(false);
                        }
                        if (c == 2) {
                            b.setEnabled(false);
                        }
                    }
                } else {
                    if (v instanceof EditText) {
                        EditText b = (EditText) v;
                        if (c == 1) {
                            b.setEnabled(true);
                        }
                        if (c == 2) {
                            b.setEnabled(true);
                        }
                    }
                }
            }
        }

        LinearLayout layout3 = (LinearLayout) findViewById(R.id.slowlanelsecond2);
        for (int dfd = 0; dfd < layout3.getChildCount(); dfd++) {
            LinearLayout la = (LinearLayout) layout3.getChildAt(dfd);

            for (int c = 0; c < la.getChildCount(); c++) {
                View v = (View) la.getChildAt(c);

                if (v instanceof CheckBox) {
                    CheckBox b = (CheckBox) v;
                    if (f) {
                        b.setEnabled(false);
                    } else {
                        b.setEnabled(true);
                    }
                }

                if (f) {
                    if (v instanceof EditText) {
                        EditText b = (EditText) v;
                        if (c == 1) {
                            b.setEnabled(false);
                        }
                        if (c == 2) {
                            b.setEnabled(false);
                        }
                    }
                } else {
                    if (v instanceof EditText) {
                        EditText b = (EditText) v;
                        if (c == 1) {
                            b.setEnabled(true);
                        }
                        if (c == 2) {
                            b.setEnabled(true);
                        }
                    }
                }
            }
        }
    }


    public void populateData() {
        JSONArray data = null;
        Log.e("haha", getSignageAudit());
        if (getSignageAudit().equals("{\"mac\":null}")) {
            setDisableEditText(true);
        } else {
            try {
                JSONObject jsonObj = new JSONObject(getSignageAudit());

                data = jsonObj.getJSONArray("mac");

                JSONObject c = data.getJSONObject(0);
                JSONObject x = data.getJSONObject(1);

                Log.e("JOB ID", c.getString("JobID"));

                if (!c.getString("RegoNo").equals("null")) {
                    cwevolution4.setText(c.getString("RegoNo"));
                }

                if (!c.getString("Landmark").equals("null")) {
                    cwlocallandmark4.setText(c.getString("Landmark"));
                }

                spinner.setSelection(Integer.parseInt(c.getString("Direction")), true);

                if (!c.getString("TimeErected").equals("null")) {
                    TimeErected.setText(date.miliSecondConverter(c.getString("TimeErected"), "HH:mm:ss a"));
                    TimeErected2 = c.getString("TimeErected");
                    Time1 = c.getString("TimeErected");
                }
                if (!c.getString("TimeCollected").equals("null")) {
                    TimeCollected.setText(date.miliSecondConverter(c.getString("TimeCollected"), "HH:mm:ss a"));
                    TimeCollected2 = c.getString("TimeCollected");
                    Time6 = c.getString("TimeErected");
                }
                if (!c.getString("TimeChecked1").equals("null")) {
                    TimeChecked1.setText(date.miliSecondConverter(c.getString("TimeChecked1"), "HH:mm:ss a"));
                    TimeChecked12 = c.getString("TimeChecked1");
                    Time2 = c.getString("TimeChecked1");
                }

                if (!c.getString("TimeChecked2").equals("null")) {
                    TimeChecked2.setText(date.miliSecondConverter(c.getString("TimeChecked2"), "HH:mm:ss a"));
                    TimeChecked22 = c.getString("TimeChecked2");
                    Time3 = c.getString("TimeChecked2");
                }

                if (!c.getString("TimeChecked3").equals("null")) {
                    TimeChecked3.setText(date.miliSecondConverter(c.getString("TimeChecked3"), "HH:mm:ss a"));
                    TimeChecked32 = c.getString("TimeChecked3");
                    Time4 = c.getString("TimeChecked3");
                }

                if (!c.getString("TimeChecked4").equals("null")) {
                    TimeChecked4.setText(date.miliSecondConverter(c.getString("TimeChecked4"), "HH:mm:ss a"));
                    TimeChecked42 = c.getString("TimeChecked4");
                    Time5 = c.getString("TimeChecked3");
                }

                if (!c.getString("aftercare").equals("null")) {
                    if (c.getString("aftercare").equals("1")) {
                        Log.e("testsetetse", "1");
                        setDisableEditText(false);
                        ishaveaftercare.setChecked(true);
                    } else {
                        Log.e("testsetetse", "0");
                        setDisableEditText(true);
                        ishaveaftercare.setChecked(false);
                    }

                }

                Log.e("test output", c.getString("AuditSigns"));
                String[] signs = c.getString("AuditSigns").replace("^^", "###").split("-----");
                String[] signsleft = signs[0].replace("~", "-").split("###");

                LinearLayout layout = (LinearLayout) findViewById(R.id.slowlanelfirst);

                for (int dfd = 0; dfd < layout.getChildCount(); dfd++) {
                    LinearLayout la = (LinearLayout) layout.getChildAt(dfd);

                    String lane = signsleft[dfd].replace("-", " = ");
                    String[] mmm = lane.split("=");

                    Log.e("tatatat", mmm[0]);

                    for (int ds = 0; ds < la.getChildCount(); ds++) {
                        View v = (View) la.getChildAt(ds);
                        if (v instanceof EditText) {
                            EditText b = (EditText) v;

                            if (ds == 1) {
                                if (!mmm[0].equals("")) {
                                    b.setText(mmm[0].replace(" ", ""));
                                }
                            }

                            if (ds == 2) {
                                if (!mmm[1].equals("")) {
                                    b.setText(mmm[1].replace(" ", ""));
                                }
                            }
                        }
                    }
                }


                String[] signsright = signs[1].replace("~", "-").split("###");
                Log.e("blabla", signs[1]);
                LinearLayout layout2 = (LinearLayout) findViewById(R.id.slowlanelsecond);
                for (int dfd = 0; dfd < layout2.getChildCount(); dfd++) {
                    LinearLayout la = (LinearLayout) layout2.getChildAt(dfd);

                    String lane = signsright[dfd].replace("-", " = ");
                    String[] mmm = lane.split("=");

                    for (int rr = 0; rr < la.getChildCount(); rr++) {
                        View v = (View) la.getChildAt(rr);

                        if (v instanceof CheckBox) {
                            CheckBox b = (CheckBox) v;
                            Log.e("s", mmm[0]);
                            b.setChecked(true);
                            if (rr == 0) {
                                if (mmm[0].replace(" ", "").equals("1")) {
                                    b.setChecked(true);
                                } else {
                                    b.setChecked(false);
                                }
                            }
                        }


                        if (v instanceof EditText) {
                            EditText b = (EditText) v;

                            if (rr == 1) {
                                if (!mmm[1].equals("")) {
                                    b.setText(mmm[1].replace(" ", ""));
                                }
                            }

                            if (rr == 2) {
                                if (!mmm[2].equals("")) {
                                    b.setText(mmm[2].replace(" ", ""));
                                }
                            }
                        }
                    }
                }


                String[] signstv = x.getString("AuditSigns").replace("^^", "###").split("-----");
                String[] signslefttv = signstv[0].replace("~", "-").split("###");

                LinearLayout layouttv = (LinearLayout) findViewById(R.id.slowlanelfirst2);

                for (int dfdtv = 0; dfdtv < layouttv.getChildCount(); dfdtv++) {
                    LinearLayout la = (LinearLayout) layouttv.getChildAt(dfdtv);

                    String lane = signslefttv[dfdtv].replace("-", " = ");
                    String[] mmm = lane.split("=");

                    Log.e("tatatat", mmm[0]);

                    for (int ds = 0; ds < la.getChildCount(); ds++) {
                        View v = (View) la.getChildAt(ds);
                        if (v instanceof EditText) {
                            EditText b = (EditText) v;

                            if (ds == 1) {
                                if (!mmm[0].equals("")) {
                                    b.setText(mmm[0].replace(" ", ""));
                                }
                            }

                            if (ds == 2) {
                                if (!mmm[1].equals("")) {
                                    b.setText(mmm[1].replace(" ", ""));
                                }
                            }
                        }
                    }
                }

                String[] signsrighttv = signstv[1].replace("~", "-").split("###");
                Log.e("blabla", signstv[1]);
                LinearLayout layout2tv = (LinearLayout) findViewById(R.id.slowlanelsecond2);
                for (int dfd = 0; dfd < layout2tv.getChildCount(); dfd++) {
                    LinearLayout la = (LinearLayout) layout2tv.getChildAt(dfd);

                    String lane = signsrighttv[dfd].replace("-", " = ");
                    String[] mmm = lane.split("=");

                    for (int rr = 0; rr < la.getChildCount(); rr++) {
                        View v = (View) la.getChildAt(rr);

                        if (v instanceof CheckBox) {
                            CheckBox b = (CheckBox) v;
                            Log.e("s", mmm[0]);
                            b.setChecked(true);
                            if (rr == 0) {
                                if (mmm[0].replace(" ", "").equals("1")) {
                                    b.setChecked(true);
                                } else {
                                    b.setChecked(false);
                                }
                            }
                        }

                        if (v instanceof EditText) {
                            EditText b = (EditText) v;

                            if (rr == 1) {
                                if (!mmm[1].equals("")) {
                                    b.setText(mmm[1].replace(" ", ""));
                                }
                            }

                            if (rr == 2) {
                                if (!mmm[2].equals("")) {
                                    b.setText(mmm[2].replace(" ", ""));
                                }
                            }
                        }
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


}
