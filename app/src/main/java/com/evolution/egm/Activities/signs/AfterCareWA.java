package com.evolution.egm.Activities.signs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Utils.DateHelper;
import com.evolution.egm.Models.Job;
import com.evolution.egm.R;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class AfterCareWA extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    RadioButton slowlane, fastlane;
    LinearLayout slowlanel, fastlanel;
    DatabaseHelper db;
    Job mJobObject;
    EditText w1, w2, w3, w4, w5, w6, w7, w8, w9, w10, w11, w12, w13, w14, w15, w16, w17, w18, w19, w20, w21, w22, w23, w24, w25, w26, w27, w28, w29, w30, w31, w32, w33, w34, w35, w36, w37, w38, w39, w40, w41, w42, w43, w44, w45, w46, w47, w48, w49, w50, w51, w52, w53, w54, w55, w56, w57, w58, w59, w60, w61, w62, w63;
    EditText wa1, wa2, wa3, wa4, wa5, wa6, wa7, wa8, wa9, wa10, wa11, wa12, wa13, wa14, wa15, wa16, wa17, wa18, wa19, wa20, wa21, wa22, wa23, wa24, wa25, wa26, wa27, wa28, wa29, wa30, wa31, wa32, wa33, wa34, wa35, wa36, wa37, wa38, wa39, wa40, wa41, wa42, wa43, wa44, wa45, wa46, wa47, wa48, wa49, wa50, wa51, wa52, wa53, wa54, wa55, wa56, wa57, wa58, wa59, wa60, wa61, wa62, wa63;
    List<String> categories;
    DateHelper date;

    MaterialEditText cwevolution4, cwlocallandmark4, TimeErected, TimeCollected, TimeChecked1, TimeChecked2, TimeChecked3, TimeChecked4;
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_care_w);
        initToolBar();
        initViews();

        db = new DatabaseHelper(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));
        }
        date = new DateHelper();

    }

    public void initSpinner() {
        // Spinner click listener
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // On selecting a spinner item
                String item = parent.getItemAtPosition(position).toString();

                // Showing selected spinner item
//                Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner.setEnabled(false);

        // Spinner Drop down elements
        categories = new ArrayList<String>();
        categories.add("North");
        categories.add("South");
        categories.add("East");
        categories.add("West");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinnerlocation, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.spinnerlocation);

        spinner.setAdapter(dataAdapter);

    }


    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("After Care WA");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    public void hideItem(EditText ed, JSONObject c, int position) throws JSONException {
        if (getcontetn(c.getString("AuditSigns"), position).equals("")) {
//            ((ViewGroup) ed.getParent()).setVisibility(((ViewGroup) ed.getParent()).GONE);
            ed.setEnabled(false);
        } else {
            ed.setEnabled(false);
        }
    }

    public void initViews() {
        slowlane = (RadioButton) findViewById(R.id.slowlane);
        fastlane = (RadioButton) findViewById(R.id.fastlane);

        slowlanel = (LinearLayout) findViewById(R.id.slowlanel);
        fastlanel = (LinearLayout) findViewById(R.id.fastlanel);

        slowlane.setOnClickListener(this);
        fastlane.setOnClickListener(this);


        TimeErected = (MaterialEditText) findViewById(R.id.TimeErected);
        TimeCollected = (MaterialEditText) findViewById(R.id.TimeCollected);
        TimeChecked1 = (MaterialEditText) findViewById(R.id.TimeChecked1);
        TimeChecked2 = (MaterialEditText) findViewById(R.id.TimeChecked2);
        TimeChecked3 = (MaterialEditText) findViewById(R.id.TimeChecked3);
        TimeChecked4 = (MaterialEditText) findViewById(R.id.TimeChecked4);


        spinner = (Spinner) findViewById(R.id.cwdirectionlist4);

        cwevolution4 = (MaterialEditText) findViewById(R.id.cwevolution4);
        cwlocallandmark4 = (MaterialEditText) findViewById(R.id.cwlocallandmark4);


        w1 = (EditText) findViewById(R.id.w1);
        w2 = (EditText) findViewById(R.id.w2);
        w3 = (EditText) findViewById(R.id.w3);
        w4 = (EditText) findViewById(R.id.w4);
        w5 = (EditText) findViewById(R.id.w5);
        w6 = (EditText) findViewById(R.id.w6);
        w7 = (EditText) findViewById(R.id.w7);
        w8 = (EditText) findViewById(R.id.w8);
        w9 = (EditText) findViewById(R.id.w9);
        w10 = (EditText) findViewById(R.id.w10);
        w11 = (EditText) findViewById(R.id.w11);
        w12 = (EditText) findViewById(R.id.w12);
        w13 = (EditText) findViewById(R.id.w13);
        w14 = (EditText) findViewById(R.id.w14);
        w15 = (EditText) findViewById(R.id.w15);
        w16 = (EditText) findViewById(R.id.w16);
        w17 = (EditText) findViewById(R.id.w17);
        w18 = (EditText) findViewById(R.id.w18);
        w19 = (EditText) findViewById(R.id.w19);
        w20 = (EditText) findViewById(R.id.w20);
        w21 = (EditText) findViewById(R.id.w21);
        w22 = (EditText) findViewById(R.id.w22);
        w23 = (EditText) findViewById(R.id.w23);
        w24 = (EditText) findViewById(R.id.w24);
        w25 = (EditText) findViewById(R.id.w25);
        w26 = (EditText) findViewById(R.id.w26);
        w27 = (EditText) findViewById(R.id.w27);
        w28 = (EditText) findViewById(R.id.w28);
        w29 = (EditText) findViewById(R.id.w29);
        w30 = (EditText) findViewById(R.id.w30);
        w31 = (EditText) findViewById(R.id.w31);
        w32 = (EditText) findViewById(R.id.w32);
        w33 = (EditText) findViewById(R.id.w33);
        w34 = (EditText) findViewById(R.id.w34);
        w35 = (EditText) findViewById(R.id.w35);
        w36 = (EditText) findViewById(R.id.w36);
        w37 = (EditText) findViewById(R.id.w37);
        w38 = (EditText) findViewById(R.id.w38);
        w39 = (EditText) findViewById(R.id.w39);
        w40 = (EditText) findViewById(R.id.w40);
        w41 = (EditText) findViewById(R.id.w41);
        w42 = (EditText) findViewById(R.id.w42);
        w43 = (EditText) findViewById(R.id.w43);
        w44 = (EditText) findViewById(R.id.w44);
        w45 = (EditText) findViewById(R.id.w45);
        w46 = (EditText) findViewById(R.id.w46);
        w47 = (EditText) findViewById(R.id.w47);
        w48 = (EditText) findViewById(R.id.w48);
        w49 = (EditText) findViewById(R.id.w49);
        w50 = (EditText) findViewById(R.id.w50);
        w51 = (EditText) findViewById(R.id.w51);
        w52 = (EditText) findViewById(R.id.w52);
        w53 = (EditText) findViewById(R.id.w53);
        w54 = (EditText) findViewById(R.id.w54);
        w55 = (EditText) findViewById(R.id.w55);
        w56 = (EditText) findViewById(R.id.w56);
        w57 = (EditText) findViewById(R.id.w57);
        w58 = (EditText) findViewById(R.id.w58);
        w59 = (EditText) findViewById(R.id.w59);
        w60 = (EditText) findViewById(R.id.w60);
        w61 = (EditText) findViewById(R.id.w61);
        w62 = (EditText) findViewById(R.id.w62);
        w63 = (EditText) findViewById(R.id.w63);

        wa1 = (EditText) findViewById(R.id.wa1);
        wa2 = (EditText) findViewById(R.id.wa2);
        wa3 = (EditText) findViewById(R.id.wa3);
        wa4 = (EditText) findViewById(R.id.wa4);
        wa5 = (EditText) findViewById(R.id.wa5);
        wa6 = (EditText) findViewById(R.id.wa6);
        wa7 = (EditText) findViewById(R.id.wa7);
        wa8 = (EditText) findViewById(R.id.wa8);
        wa9 = (EditText) findViewById(R.id.wa9);
        wa10 = (EditText) findViewById(R.id.wa10);
        wa11 = (EditText) findViewById(R.id.wa11);
        wa12 = (EditText) findViewById(R.id.wa12);
        wa13 = (EditText) findViewById(R.id.wa13);
        wa14 = (EditText) findViewById(R.id.wa14);
        wa15 = (EditText) findViewById(R.id.wa15);
        wa16 = (EditText) findViewById(R.id.wa16);
        wa17 = (EditText) findViewById(R.id.wa17);
        wa18 = (EditText) findViewById(R.id.wa18);
        wa19 = (EditText) findViewById(R.id.wa19);
        wa20 = (EditText) findViewById(R.id.wa20);
        wa21 = (EditText) findViewById(R.id.wa21);
        wa22 = (EditText) findViewById(R.id.wa22);
        wa23 = (EditText) findViewById(R.id.wa23);
        wa24 = (EditText) findViewById(R.id.wa24);
        wa25 = (EditText) findViewById(R.id.wa25);
        wa26 = (EditText) findViewById(R.id.wa26);
        wa27 = (EditText) findViewById(R.id.wa27);
        wa28 = (EditText) findViewById(R.id.wa28);
        wa29 = (EditText) findViewById(R.id.wa29);
        wa30 = (EditText) findViewById(R.id.wa30);
        wa31 = (EditText) findViewById(R.id.wa31);
        wa32 = (EditText) findViewById(R.id.wa32);
        wa33 = (EditText) findViewById(R.id.wa33);
        wa34 = (EditText) findViewById(R.id.wa34);
        wa35 = (EditText) findViewById(R.id.wa35);
        wa36 = (EditText) findViewById(R.id.wa36);
        wa37 = (EditText) findViewById(R.id.wa37);
        wa38 = (EditText) findViewById(R.id.wa38);
        wa39 = (EditText) findViewById(R.id.wa39);
        wa40 = (EditText) findViewById(R.id.wa40);
        wa41 = (EditText) findViewById(R.id.wa41);
        wa42 = (EditText) findViewById(R.id.wa42);
        wa43 = (EditText) findViewById(R.id.wa43);
        wa44 = (EditText) findViewById(R.id.wa44);
        wa45 = (EditText) findViewById(R.id.wa45);
        wa46 = (EditText) findViewById(R.id.wa46);
        wa47 = (EditText) findViewById(R.id.wa47);
        wa48 = (EditText) findViewById(R.id.wa48);
        wa49 = (EditText) findViewById(R.id.wa49);
        wa50 = (EditText) findViewById(R.id.wa50);
        wa51 = (EditText) findViewById(R.id.wa51);
        wa52 = (EditText) findViewById(R.id.wa52);
        wa53 = (EditText) findViewById(R.id.wa53);
        wa54 = (EditText) findViewById(R.id.wa54);
        wa55 = (EditText) findViewById(R.id.wa55);
        wa56 = (EditText) findViewById(R.id.wa56);
        wa57 = (EditText) findViewById(R.id.wa57);
        wa58 = (EditText) findViewById(R.id.wa58);
        wa59 = (EditText) findViewById(R.id.wa59);
        wa60 = (EditText) findViewById(R.id.wa60);
        wa61 = (EditText) findViewById(R.id.wa61);
        wa62 = (EditText) findViewById(R.id.wa62);
        wa63 = (EditText) findViewById(R.id.wa63);

    }

    @Override
    protected void onResume() {
        super.onResume();
        populateData();
        initSpinner();
        ;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.slowlane:
                slowlanel.setVisibility(slowlanel.VISIBLE);
                fastlanel.setVisibility(slowlanel.GONE);
                break;
            case R.id.fastlane:
                slowlanel.setVisibility(slowlanel.GONE);
                fastlanel.setVisibility(slowlanel.VISIBLE);
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_after_care_qld, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.saveaftercare:
                checksaveCheckboxes();
                break;
            case android.R.id.home:
                finish();
                overridePendingTransition(0, 0);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public String getSignageAudit() {
        String temp = "";
        temp = "{\"mac\":" + db.getSignageAudt(mJobObject.getJobID()) + "}";
        return temp;
    }

    public void checksaveCheckboxes() {
        String returnsslow = "";
        String returnsfast = "";
        LinearLayout layout = (LinearLayout) findViewById(R.id.slowlanel);
        for (int dfd = 0; dfd < layout.getChildCount(); dfd++) {
            LinearLayout la = (LinearLayout) layout.getChildAt(dfd);
            for (int c = 0; c < la.getChildCount(); c++) {
                View v = (View) la.getChildAt(c);
                if (v instanceof CheckBox) {
                    CheckBox b = (CheckBox) v;
                    if (b.isChecked() == true) {
                        returnsslow = returnsslow + "1-";
                    } else {
                        returnsslow = returnsslow + "0-";
                    }
                }


            }
        }

        LinearLayout layout2 = (LinearLayout) findViewById(R.id.fastlanel);

        for (int fa = 0; fa < layout2.getChildCount(); fa++) {
            LinearLayout la = (LinearLayout) layout2.getChildAt(fa);
            for (int c = 0; c < la.getChildCount(); c++) {
                View v = (View) la.getChildAt(c);
                if (v instanceof CheckBox) {
                    CheckBox b = (CheckBox) v;
                    if (b.isChecked() == true) {
                        returnsfast = returnsfast + "1-";
                    } else {
                        returnsfast = returnsfast + "0-";
                    }
                }
            }
        }

        Log.e("before save", returnsslow + "~" + returnsfast);

        db.updateAfterCare(mJobObject.getId(), returnsslow + "~" + returnsfast);
    }


    public String getcontetn(String gets, int position) {
        String getz = "";
        String[] splitters = gets.split("~");

        String abc = splitters[position].replace("^", "#");
        String[] splitter = abc.split("#");

        if (splitter.length >= 2) {
            getz = splitter[1];
        } else {
            getz = "";
        }

        return getz.replace("Metres:", "");
    }


    public void populateData() {
        JSONArray data = null;
        Log.e("haha", getSignageAudit());
        if (getSignageAudit().equals("{\"mac\":null}")) {

        } else {
            try {
                JSONObject jsonObj = new JSONObject(getSignageAudit());

                data = jsonObj.getJSONArray("mac");

                JSONObject c = data.getJSONObject(0);
                JSONObject x = data.getJSONObject(1);


                cwevolution4.setText(c.getString("RegoNo"));
                cwlocallandmark4.setText(c.getString("Landmark"));

                spinner.setSelection(Integer.parseInt(c.getString("Direction")), true);

                if (!c.getString("TimeErected").equals("null")) {
                    TimeErected.setText(date.miliSecondConverter(c.getString("TimeErected"), "HH:mm:ss a"));
                }
                if (!c.getString("TimeCollected").equals("null")) {
                    TimeCollected.setText(date.miliSecondConverter(c.getString("TimeCollected"), "HH:mm:ss a"));
                }
                if (!c.getString("TimeChecked1").equals("null")) {
                    TimeChecked1.setText(date.miliSecondConverter(c.getString("TimeChecked1"), "HH:mm:ss a"));
                }
                if (!c.getString("TimeChecked2").equals("null")) {
                    TimeChecked2.setText(date.miliSecondConverter(c.getString("TimeChecked2"), "HH:mm:ss a"));
                }
                if (!c.getString("TimeChecked3").equals("null")) {
                    TimeChecked3.setText(date.miliSecondConverter(c.getString("TimeChecked3"), "HH:mm:ss a"));
                }
                if (!c.getString("TimeChecked4").equals("null")) {
                    TimeChecked4.setText(date.miliSecondConverter(c.getString("TimeChecked4"), "HH:mm:ss a"));
                }


                w1.setText(getcontetn(c.getString("AuditSigns"), 0));

                w2.setText(getcontetn(c.getString("AuditSigns"), 1));
                w3.setText(getcontetn(c.getString("AuditSigns"), 2));
                w4.setText(getcontetn(c.getString("AuditSigns"), 3));
                w5.setText(getcontetn(c.getString("AuditSigns"), 4));
                w6.setText(getcontetn(c.getString("AuditSigns"), 5));
                w7.setText(getcontetn(c.getString("AuditSigns"), 6));
                w8.setText(getcontetn(c.getString("AuditSigns"), 7));
                w9.setText(getcontetn(c.getString("AuditSigns"), 8));
                w10.setText(getcontetn(c.getString("AuditSigns"), 9));
                w11.setText(getcontetn(c.getString("AuditSigns"), 10));
                w12.setText(getcontetn(c.getString("AuditSigns"), 11));
                w13.setText(getcontetn(c.getString("AuditSigns"), 12));
                w14.setText(getcontetn(c.getString("AuditSigns"), 13));
                w15.setText(getcontetn(c.getString("AuditSigns"), 14));
                w16.setText(getcontetn(c.getString("AuditSigns"), 15));
                w17.setText(getcontetn(c.getString("AuditSigns"), 16));
                w18.setText(getcontetn(c.getString("AuditSigns"), 17));
                w19.setText(getcontetn(c.getString("AuditSigns"), 18));
                w20.setText(getcontetn(c.getString("AuditSigns"), 19));
                w21.setText(getcontetn(c.getString("AuditSigns"), 20));
                w22.setText(getcontetn(c.getString("AuditSigns"), 21));
                w23.setText(getcontetn(c.getString("AuditSigns"), 22));
                w24.setText(getcontetn(c.getString("AuditSigns"), 23));
                w25.setText(getcontetn(c.getString("AuditSigns"), 24));
                w26.setText(getcontetn(c.getString("AuditSigns"), 25));
                w27.setText(getcontetn(c.getString("AuditSigns"), 26));
                w28.setText(getcontetn(c.getString("AuditSigns"), 27));
                w29.setText(getcontetn(c.getString("AuditSigns"), 28));
                w30.setText(getcontetn(c.getString("AuditSigns"), 29));
                w31.setText(getcontetn(c.getString("AuditSigns"), 30));
                w32.setText(getcontetn(c.getString("AuditSigns"), 31));
                w33.setText(getcontetn(c.getString("AuditSigns"), 32));
                w34.setText(getcontetn(c.getString("AuditSigns"), 33));
                w35.setText(getcontetn(c.getString("AuditSigns"), 34));
                w36.setText(getcontetn(c.getString("AuditSigns"), 35));
                w37.setText(getcontetn(c.getString("AuditSigns"), 36));
                w38.setText(getcontetn(c.getString("AuditSigns"), 37));
                w39.setText(getcontetn(c.getString("AuditSigns"), 38));
                w40.setText(getcontetn(c.getString("AuditSigns"), 39));
                w41.setText(getcontetn(c.getString("AuditSigns"), 40));
                w42.setText(getcontetn(c.getString("AuditSigns"), 41));
                w43.setText(getcontetn(c.getString("AuditSigns"), 42));
                w44.setText(getcontetn(c.getString("AuditSigns"), 43));
                w45.setText(getcontetn(c.getString("AuditSigns"), 44));
                w46.setText(getcontetn(c.getString("AuditSigns"), 45));
                w47.setText(getcontetn(c.getString("AuditSigns"), 46));
                w48.setText(getcontetn(c.getString("AuditSigns"), 47));
                w49.setText(getcontetn(c.getString("AuditSigns"), 48));
                w50.setText(getcontetn(c.getString("AuditSigns"), 49));
                w51.setText(getcontetn(c.getString("AuditSigns"), 50));
                w52.setText(getcontetn(c.getString("AuditSigns"), 51));
                w53.setText(getcontetn(c.getString("AuditSigns"), 52));
                w54.setText(getcontetn(c.getString("AuditSigns"), 53));
                w55.setText(getcontetn(c.getString("AuditSigns"), 54));
                w56.setText(getcontetn(c.getString("AuditSigns"), 55));
                w57.setText(getcontetn(c.getString("AuditSigns"), 56));
                w58.setText(getcontetn(c.getString("AuditSigns"), 57));
                w59.setText(getcontetn(c.getString("AuditSigns"), 58));
                w60.setText(getcontetn(c.getString("AuditSigns"), 59));
                w61.setText(getcontetn(c.getString("AuditSigns"), 60));
                w62.setText(getcontetn(c.getString("AuditSigns"), 61));
                w63.setText(getcontetn(c.getString("AuditSigns"), 62));


                wa1.setText(getcontetn(x.getString("AuditSigns"), 0));
                wa2.setText(getcontetn(x.getString("AuditSigns"), 1));
                wa3.setText(getcontetn(x.getString("AuditSigns"), 2));
                wa4.setText(getcontetn(x.getString("AuditSigns"), 3));
                wa5.setText(getcontetn(x.getString("AuditSigns"), 4));
                wa6.setText(getcontetn(x.getString("AuditSigns"), 5));
                wa7.setText(getcontetn(x.getString("AuditSigns"), 6));
                wa8.setText(getcontetn(x.getString("AuditSigns"), 7));
                wa9.setText(getcontetn(x.getString("AuditSigns"), 8));
                wa10.setText(getcontetn(x.getString("AuditSigns"), 9));
                wa11.setText(getcontetn(x.getString("AuditSigns"), 10));
                wa12.setText(getcontetn(x.getString("AuditSigns"), 11));
                wa13.setText(getcontetn(x.getString("AuditSigns"), 12));
                wa14.setText(getcontetn(x.getString("AuditSigns"), 13));
                wa15.setText(getcontetn(x.getString("AuditSigns"), 14));
                wa16.setText(getcontetn(x.getString("AuditSigns"), 15));
                wa17.setText(getcontetn(x.getString("AuditSigns"), 16));
                wa18.setText(getcontetn(x.getString("AuditSigns"), 17));
                wa19.setText(getcontetn(x.getString("AuditSigns"), 18));
                wa20.setText(getcontetn(x.getString("AuditSigns"), 19));
                wa21.setText(getcontetn(x.getString("AuditSigns"), 20));
                wa22.setText(getcontetn(x.getString("AuditSigns"), 21));
                wa23.setText(getcontetn(x.getString("AuditSigns"), 22));
                wa24.setText(getcontetn(x.getString("AuditSigns"), 23));
                wa25.setText(getcontetn(x.getString("AuditSigns"), 24));
                wa26.setText(getcontetn(x.getString("AuditSigns"), 25));
                wa27.setText(getcontetn(x.getString("AuditSigns"), 26));
                wa28.setText(getcontetn(x.getString("AuditSigns"), 27));
                wa29.setText(getcontetn(x.getString("AuditSigns"), 28));
                wa30.setText(getcontetn(x.getString("AuditSigns"), 29));
                wa31.setText(getcontetn(x.getString("AuditSigns"), 30));
                wa32.setText(getcontetn(x.getString("AuditSigns"), 31));
                wa33.setText(getcontetn(x.getString("AuditSigns"), 32));
                wa34.setText(getcontetn(x.getString("AuditSigns"), 33));
                wa35.setText(getcontetn(x.getString("AuditSigns"), 34));
                wa36.setText(getcontetn(x.getString("AuditSigns"), 35));
                wa37.setText(getcontetn(x.getString("AuditSigns"), 36));
                wa38.setText(getcontetn(x.getString("AuditSigns"), 37));
                wa39.setText(getcontetn(x.getString("AuditSigns"), 38));
                wa40.setText(getcontetn(x.getString("AuditSigns"), 39));
                wa41.setText(getcontetn(x.getString("AuditSigns"), 40));
                wa42.setText(getcontetn(x.getString("AuditSigns"), 41));
                wa43.setText(getcontetn(x.getString("AuditSigns"), 42));
                wa44.setText(getcontetn(x.getString("AuditSigns"), 43));
                wa45.setText(getcontetn(x.getString("AuditSigns"), 44));
                wa46.setText(getcontetn(x.getString("AuditSigns"), 45));
                wa47.setText(getcontetn(x.getString("AuditSigns"), 46));
                wa48.setText(getcontetn(x.getString("AuditSigns"), 47));
                wa49.setText(getcontetn(x.getString("AuditSigns"), 48));
                wa50.setText(getcontetn(x.getString("AuditSigns"), 49));
                wa51.setText(getcontetn(x.getString("AuditSigns"), 50));
                wa52.setText(getcontetn(x.getString("AuditSigns"), 51));
                wa53.setText(getcontetn(x.getString("AuditSigns"), 52));
                wa54.setText(getcontetn(x.getString("AuditSigns"), 53));
                wa55.setText(getcontetn(x.getString("AuditSigns"), 54));
                wa56.setText(getcontetn(x.getString("AuditSigns"), 55));
                wa57.setText(getcontetn(x.getString("AuditSigns"), 56));
                wa58.setText(getcontetn(x.getString("AuditSigns"), 57));
                wa59.setText(getcontetn(x.getString("AuditSigns"), 58));
                wa60.setText(getcontetn(x.getString("AuditSigns"), 59));
                wa61.setText(getcontetn(x.getString("AuditSigns"), 60));
                wa62.setText(getcontetn(x.getString("AuditSigns"), 61));
                wa63.setText(getcontetn(x.getString("AuditSigns"), 62));


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (!db.getaftercare(mJobObject.getJobID()).equals("null")) {
            String[] slowfast = db.getaftercare(mJobObject.getJobID()).split("~");
            Log.e("slow", slowfast[0]);
            String[] aftercare = slowfast[0].split("-");
            LinearLayout layout = (LinearLayout) findViewById(R.id.slowlanel);

            for (int d = 0; d < layout.getChildCount(); d++) {
                LinearLayout la = (LinearLayout) layout.getChildAt(d);
                Log.e("dapat ", " to" + d);
                for (int c = 0; c < la.getChildCount(); c++) {
                    View v = (View) la.getChildAt(c);
                    if (v instanceof CheckBox) {
                        CheckBox b = (CheckBox) v;
                        if (aftercare[d].equals("1")) {
                            b.setChecked(true);
                        } else {
                            b.setChecked(false);
                        }
                    }

                    if (v instanceof EditText) {
                        EditText s = (EditText) v;
                        s.setEnabled(false);
                    }
                }
            }
            Log.e("fast", slowfast[1]);
            String[] aftercare2 = slowfast[1].split("-");
            LinearLayout layout2 = (LinearLayout) findViewById(R.id.fastlanel);

            for (int d = 0; d < layout2.getChildCount(); d++) {
                LinearLayout la = (LinearLayout) layout2.getChildAt(d);
                Log.e("dapat ", " to" + d);
                for (int c = 0; c < la.getChildCount(); c++) {
                    View v = (View) la.getChildAt(c);
                    if (v instanceof CheckBox) {
                        CheckBox b = (CheckBox) v;
                        if (aftercare2[d].equals("1")) {
                            b.setChecked(true);
                        } else {
                            b.setChecked(false);
                        }
                    }
                    if (v instanceof EditText) {
                        EditText s = (EditText) v;
                        s.setEnabled(false);
                    }
                }
            }
        }

        disableEditText();

    }

    public void disableEditText() {
        LinearLayout layout = (LinearLayout) findViewById(R.id.slowlanel);
        for (int d = 0; d < layout.getChildCount(); d++) {
            LinearLayout la = (LinearLayout) layout.getChildAt(d);
            for (int c = 0; c < la.getChildCount(); c++) {
                View v = (View) la.getChildAt(c);
                if (v instanceof EditText) {
                    EditText s = (EditText) v;
                    s.setEnabled(false);
                }
            }
        }

        LinearLayout layout2 = (LinearLayout) findViewById(R.id.fastlanel);

        for (int d = 0; d < layout2.getChildCount(); d++) {
            LinearLayout la = (LinearLayout) layout2.getChildAt(d);
            Log.e("dapat ", " to" + d);
            for (int c = 0; c < la.getChildCount(); c++) {
                View v = (View) la.getChildAt(c);
                if (v instanceof EditText) {
                    EditText s = (EditText) v;
                    s.setEnabled(false);
                }
            }
        }
    }


}
