package com.evolution.egm.Activities;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.evolution.egm.Adapters.AdapterJobMenu;
import com.evolution.egm.Models.Stocklist_Kit_Model;
import com.evolution.egm.Utils.ConnectionDetector;
import com.evolution.egm.Utils.Constants;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Utils.DateHelper;
import com.evolution.egm.Utils.GPSTracker;
import com.evolution.egm.Utils.RecyclerItemClickListener;
import com.evolution.egm.Utils.SessionManager;
import com.evolution.egm.Interface.APIInterface;
import com.evolution.egm.Models.CheckManstatStatus;
import com.evolution.egm.Models.JobStatuses;
import com.evolution.egm.Models.SignatureModelz;
import com.evolution.egm.Models.SingageAudit;
import com.evolution.egm.Models.Job;
import com.evolution.egm.Models.JobObjects;
import com.evolution.egm.Models.ModelRequirementsObjects;
import com.evolution.egm.Models.Notes;
import com.evolution.egm.Models.PhotoItem;
import com.evolution.egm.Models.SubJobCounter;
import com.evolution.egm.Models.Timesheet_Upload;
import com.evolution.egm.R;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;
import com.rey.material.widget.FloatingActionButton;
import com.squareup.okhttp.OkHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class JobView extends AppCompatActivity implements View.OnClickListener {

    public static final String TAG = JobView.class.getSimpleName();

    private Toolbar toolbar;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private String token;
    public SessionManager session;
    public FloatingActionButton createNewjob;
    boolean erroruploadImage = false;
    ArrayList<PhotoItem> photo = new ArrayList<PhotoItem>();
    final CharSequence cs1 = "^ ~";
    final CharSequence nn = "null";
    final CharSequence ifsubjob = ".";
    GPSTracker gps;
    ProgressBar progresssaving;
    TextView statusbaritem;
    TextView statuspercentage;
    Job mJobObject;
    DatabaseHelper db;

    boolean status2 = false;

    public String idSelected;
    final ArrayList<PhotoItem> finalPhoto = photo;
    EditText editText4;
    private ConnectionDetector connection;
    boolean stat = false;
    LinearLayout jobdiscription, loadingarea, ordernoedittext;
    JobObjects mj;
    TextView jobidtxt, CompanyNametxt, datetxt, Addresstxt, depottxt, authorisedby, sitecontact, ordernumbertext;
    Dialog dialog;
    Dialog dialog4;

    int counterphoto = 0;
    int progressing = 0;
    Dialog.Builder builder = null;
    Dialog.Builder builder2 = null;
    String gsp = "";
    boolean isFailSafe = false;
    boolean isFailSafe2 = false;
    String JobID;
    int REQUEST_CODE = 1, RESULT_OK = 1;

    String myDataset2[] = {
            "REQUIREMENTS", "SITE CHECKLIST", "STOCKLIST", "TIMESHEET", "EXTERNAL LINKS", "PHOTOS"
    };

    boolean checkerh;
    DateHelper date;
    int[] icons2 = {R.mipmap.jdrequirements, R.mipmap.jdchecklist, R.mipmap.jdaudit, R.mipmap.jdtimesheet, R.mipmap.jdexternallink, R.mipmap.jdphoto};

    RelativeLayout operatorSignature;
    RelativeLayout clientSignature;
    TextView subjobdots;
    TextView editSub;
    String jobclickedposition;
    ScrollView scc;
    OkHttpClient okHttpClient;
    int statusCounter = 1;
    JobStatuses ss3;
    Switch away;
    ArrayList<SignatureModelz> mSignatureModelz;
    Gson kk = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_view);
        String myDeviceModel = android.os.Build.MODEL;
        myDeviceModel = myDeviceModel + " -  " + android.os.Build.DEVICE;
        Log.e("phone information", myDeviceModel);
        initToolBar();

        session = new SessionManager(this);
        token = session.getToken();
        db = new DatabaseHelper(this);
        jobidtxt = (TextView) findViewById(R.id.jobid);
        CompanyNametxt = (TextView) findViewById(R.id.CompanyName);
        datetxt = (TextView) findViewById(R.id.date);
        Addresstxt = (TextView) findViewById(R.id.Address);
        connection = new ConnectionDetector(this);
        depottxt = (TextView) findViewById(R.id.depottxt);
        authorisedby = (TextView) findViewById(R.id.authorisedby);
        ordernumbertext = (TextView) findViewById(R.id.ordernumbertext);
        sitecontact = (TextView) findViewById(R.id.sitecontact);
        jobdiscription = (LinearLayout) findViewById(R.id.jobdiscription);
        loadingarea = (LinearLayout) findViewById(R.id.loadingarea);
        ordernoedittext = (LinearLayout) findViewById(R.id.ordernoedittext);
        editSub = (TextView) findViewById(R.id.editSub);
        subjobdots = (TextView) findViewById(R.id.subjobdots);
        scc = (ScrollView) findViewById(R.id.scc);
        editText4 = (EditText) findViewById(R.id.editText4);
        away = (Switch) findViewById(R.id.away);
        date = new DateHelper();

        away.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ss3 = new JobStatuses();
                if (b) {
                    Log.e("loge", "1");
                    ss3.setJobId(mJobObject.getJobID());
                    ss3.setAwayjob("1");
                    db.addJobStatus2(ss3);
                } else {
                    Log.e("loge", "0");
                    ss3.setJobId(mJobObject.getJobID());
                    ss3.setAwayjob("0");
                    db.addJobStatus2(ss3);
                }
            }
        });

        Bundle extras = getIntent().getExtras();

        if (extras != null) {

            mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));
            jobidtxt.setText(mJobObject.getJobID());
            CompanyNametxt.setText(mJobObject.getCompanyName());
            datetxt.setText(mJobObject.getJobShiftStartDateTimeFormat().replace("-", " ").substring(0, mJobObject.getJobShiftStartDateTimeFormat().length() - 8));
            Addresstxt.setText(mJobObject.getAddress());
            depottxt.setText(mJobObject.getDepot());
            authorisedby.setText(mJobObject.getAuthorizedPerson());
            siteContact(sitecontact, mJobObject.getContactMobilePhone(), mJobObject.getContactPhone());
            idSelected = mJobObject.getJobID();
            jobclickedposition = extras.getString("position");

            if (extras.getString("SubJob").equals("k")) {
                editSub.setVisibility(editSub.VISIBLE);
                subjobdots.setVisibility(subjobdots.VISIBLE);
            } else {
                editSub.setVisibility(editSub.GONE);
                subjobdots.setVisibility(subjobdots.GONE);
            }

        }

        getNotes();

        okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(120, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(120, TimeUnit.SECONDS);

        ordernumbertext.setText(mJobObject.getOrderNumber());
        operatorSignature = (RelativeLayout) findViewById(R.id.operatorSignature);
        clientSignature = (RelativeLayout) findViewById(R.id.clientSignature);

        operatorSignature.setOnClickListener(this);
        clientSignature.setOnClickListener(this);

        mRecyclerView = (RecyclerView) findViewById(R.id.menujob);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new AdapterJobMenu(myDataset2, icons2, idSelected, getApplicationContext(), mJobObject.getJobID());
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        switch (position) {
                            case 0:
                                Intent requiments = new Intent(JobView.this, Requirements.class);
                                requiments.putExtra("data", Parcels.wrap(mJobObject));
                                requiments.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(requiments);
                                break;
                            case 1:
                                db = new DatabaseHelper(getApplicationContext());
                                Log.e("tsetse", mJobObject.getJobID());
                                if (db.checkExistCreateJObs(mJobObject.getJobID()) != null) {
                                    Intent clickMe2 = new Intent(JobView.this, SiteChecklist.class);
                                    clickMe2.putExtra("data", Parcels.wrap(mJobObject));
                                    clickMe2.putExtra("position", position + "");
                                    clickMe2.putExtra("SubJob", "k");
                                    clickMe2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                    startActivity(clickMe2);
                                } else {
                                    Intent clickMe2 = new Intent(JobView.this, SiteCheckListNew.class);
                                    clickMe2.putExtra("data", Parcels.wrap(mJobObject));
                                    clickMe2.putExtra("position", position + "");
                                    clickMe2.putExtra("SubJob", "k");
                                    clickMe2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                    startActivity(clickMe2);
                                }
                                break;
                            case 2:
                                Intent s = new Intent(JobView.this, StocklistActivity.class);
                                s.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                s.putExtra("data", Parcels.wrap(mJobObject));
                                startActivity(s);

//                                Intent s = new Intent(JobView.this, SignageAudit.class);
//                                s.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                                s.putExtra("data", Parcels.wrap(mJobObject));
//                                startActivity(s);
                                break;
                            case 3:
                                Intent timesheet = new Intent(JobView.this, TimeSheets.class);
                                timesheet.putExtra("data", Parcels.wrap(mJobObject));
                                timesheet.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(timesheet);
                                break;
                            case 4:
                                Intent external = new Intent(JobView.this, ExternalLinks.class);
                                external.putExtra("data", Parcels.wrap(mJobObject));
                                external.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(external);
                                break;
                            case 5:
                                Intent photo = new Intent(JobView.this, Photo.class);
                                photo.putExtra("data", Parcels.wrap(mJobObject));
                                photo.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(photo);
                                break;
                        }
                    }
                })
        );

        createNewjob = (FloatingActionButton) findViewById(R.id.createNewjob);
        createNewjob.setOnClickListener(this);
        boolean cc = mJobObject.getJobID().contains(ifsubjob);
        if (cc) {
            createNewjob.setVisibility(createNewjob.GONE);
        }
        ordernoedittext.setOnClickListener(this);
        dialog = new Dialog(this, R.style.FullHeightDialog);
        dialog4 = new Dialog(this, R.style.FullHeightDialog);
        scc.fullScroll(ScrollView.FOCUS_UP);
        scc.smoothScrollTo(0, 0);
        addStatus();
        Log.e("test", db.getJobStatusStats(mJobObject.getJobID()).getAwayjob());
        String tocheck = db.getJobStatusStats(mJobObject.getJobID()).getAwayjob().toString();
        if (tocheck.equals("-1")) {
            awayJob();
        } else {
            if (tocheck.equals("1")) {
                away.setChecked(true);
            } else {
                away.setChecked(false);
            }
        }


        redSTocks("sd");
    }

    public void addStatus() {
        if (!db.checkJobstatus(mJobObject.getJobID())) {
            JobStatuses ss = new JobStatuses();
            ss.setJobId(mJobObject.getJobID());
            ss.setSingageAudit("1");
            db.addJobStatus0(ss);
        }
    }


    public void awayJob() {
        for (int i = 0; i < db.getallRequirements(mJobObject.getJobID()).getmRequirements().size(); i++) {
            if (db.getallRequirements(mJobObject.getJobID()).getmRequirements().get(i).getRequirementID().equals("701")) {
//                if(Integer.parseInt(db.getallRequirements(mJobObject.getJobID()).getmRequirements().get(i).getQuantity().toString()) > 0){
                status2 = true;
//                }
            }
        }

        if (status2) {
            away.setChecked(true);
//            away.setClickable(false);
        } else {
            away.setChecked(false);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void getNotes() {
        if (connection.isConnectedToInternet()) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Constants.API_URL)
                    .build();
            APIInterface interfacem = restAdapter.create(APIInterface.class);
            interfacem.getNotes(session.getToken(), Constants.REQUEST_JOBINFO, new Callback<Notes>() {
                @Override
                public void success(Notes notes, Response response) {
                    String jsonObjectResponse = new Gson().toJson(notes);
                    db.addNotes(notes);
                    if (db.getNotes().getStatus().equals("false")) {

                    } else {
                        generateDialog(db.getNotes().getNote());
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        } else {
            if (db.getNotes().getStatus().equals("false")) {

            } else {
                generateDialog(db.getNotes().getNote());
            }
        }
    }

    public void generateDialog(String message) {
        Dialog.Builder builder = null;

        builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                super.onPositiveActionClicked(fragment);
            }
        };

        builder.build(this).setCanceledOnTouchOutside(false);
        ((SimpleDialog.Builder) builder).message(message)
                .title("Safety Alert")
                .positiveAction("Ok");
        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(getSupportFragmentManager(), null);
        fragment.setCancelable(false);
    }

    public boolean opensignatureValidation(String id) {
        boolean checker = true;
        final CharSequence cs1 = "null";
        boolean ishave2;
        String[] firstm = db.getAlllocalTimesheetMul2(id).replace("^^^^", "####").split("####");
        if (firstm.length != db.getTimesheetSignature(id)) {
            checker = false;
        }
        if (db.checkOktoupload(id)) {
            checker = false;
        }
        if (db.checkExistCreateJObs(id) == null) {
            checker = false;
        }
        return checker;
    }

    public String getSignageAuditd(String ids) {
        String temp = "";
        temp = "{\"mac\":" + db.getSignageAudt(ids) + "}";
        return temp;
    }

    public boolean ifAfterisYES() {
        boolean isyes;
        JSONArray data = null;
        if (getSignageAudit().equals("{\"mac\":null}")) {
            isyes = false;
            Log.e("stage s", "true");
        } else {
            Log.e("stage s", "false" + getSignageAudit() + "sdfsd");
            if (getSignageAudit().isEmpty()) {
                isyes = false;
                Log.e("stage s", "walang laman");
            } else {
                try {
                    JSONObject jsonObj = new JSONObject("{\"mac\":" + getSignageAudit() + "}");
                    data = jsonObj.getJSONArray("mac");
                    JSONObject c = data.getJSONObject(0);
                    JSONObject x = data.getJSONObject(1);
                    if (!c.getString("aftercare").equals("null")) {
                        if (c.getString("aftercare").equals("0")) {
                            isyes = true;
                            Log.e("stage", "notnull -- false 0" + c.getString("aftercare"));
                        } else {
                            isyes = false;
                            Log.e("stage", "notnull -- true 1" + c.getString("aftercare"));
                        }
                    } else {
                        isyes = false;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("stage", "notnull2");
                    isyes = false;
                }
                Log.e("stage s", "mayroon");
            }
        }
        Log.e("test test test ", getSignageAudit());
        return isyes;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("resume me", db.getClientStatus(idSelected) + "");
        if (ifAfterisYES()) {
            mAdapter = new AdapterJobMenu(myDataset2, icons2, idSelected, getApplicationContext(), mJobObject.getJobID());
        } else {
            mAdapter = new AdapterJobMenu(myDataset2, icons2, idSelected, getApplicationContext(), mJobObject.getJobID());
        }
        mRecyclerView.setAdapter(mAdapter);
        String timeshet = db.getJobDockets(mJobObject.getJobID());
        deleteIfJobUploaded(mJobObject.getJobID());
        String abc = timeshet.replace("^^^^^^^^", "###");
        String[] splitter = abc.split("###");
        for (int c = 0; c < splitter.length; c++) {
            String abcd = splitter[c].replace("|~~~~|", ">>>>");
            String[] splitters = abcd.split(">>>>");
            if (splitters[2].equals("1")) {
                if (!(splitters[4].equals("null"))) {
                    if (db.getClientStatus(idSelected).equals("0")) {
                        clientSignature.setBackgroundColor(getResources().getColor(R.color.colorSecondaryText));
                        db.timesheetsign(idSelected);
                        db.updateAttachedToRemove(idSelected);
                        Log.e("resuming", "Activity0");
                    } else {
                        clientSignature.setBackgroundColor(getResources().getColor(R.color.greencolor));
                        Log.e("resuming", "Activity1");
                    }
                } else {
                    if (db.getClientStatus(idSelected).equals("0")) {
                        clientSignature.setBackgroundColor(getResources().getColor(R.color.colorSecondaryText));
                        db.timesheetsign(idSelected);
                        db.updateAttachedToRemove(idSelected);
                        Log.e("resuming", "Activity0");
                    } else {
                        clientSignature.setBackgroundColor(getResources().getColor(R.color.skyblue));
                        Log.e("resuming", "Activity1");
                    }
                }
            }
            if (splitters[2].equals("2")) {
                if (!(splitters[4].equals("null"))) {
                    operatorSignature.setBackgroundColor(getResources().getColor(R.color.greencolor));
                } else {
                    operatorSignature.setBackgroundColor(getResources().getColor(R.color.skyblue));
                }
            }
        }
        checkifExpired();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data.getExtras().containsKey("save_data")) {
            Toast.makeText(getApplicationContext(), data.getStringExtra("save_data"), Toast.LENGTH_SHORT);
        }
    }

    public void gpsUploadPhoto() {
        gps = new GPSTracker(getApplicationContext());
        if (gps.canGetLocation()) {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            gsp = latitude + ", " + longitude;
            if (connection.isConnectedToInternet()) {
                Dialog.Builder builder = null;
                builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                    @Override
                    public void onPositiveActionClicked(DialogFragment fragment) {
                        if (statusCounter < 5) {
                            Log.e("sdfsdfdsf", "testing < ");
                            dialog4.show();
                            checkManstat();
                        } else {
                            Log.e("sdfsdfdsf", "testing >");
                            builder2 = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                                @Override
                                public void onPositiveActionClicked(DialogFragment fragment) {
                                    isFailSafe = true;
                                    isFailSafe2 = true;
                                    uploadPhotoWidthLoading();
                                    super.onPositiveActionClicked(fragment);
                                }

                                @Override
                                public void onNegativeActionClicked(DialogFragment fragment) {
                                    statusCounter = 1;
                                    isFailSafe = false;
                                    isFailSafe2 = false;
                                    super.onNegativeActionClicked(fragment);
                                }
                            };
                            ((SimpleDialog.Builder) builder2).message("Submission has failed 5 times. Do you want to activate fail-safe submission?\n")
                                    .title("Unable to connect to ManStat").positiveAction("YES")
                                    .negativeAction("NO");
                            DialogFragment fragment2 = DialogFragment.newInstance(builder2);
                            fragment2.show(getSupportFragmentManager(), null);
                            fragment2.setCancelable(false);
                        }
                        super.onPositiveActionClicked(fragment);
                    }

                    @Override
                    public void onNegativeActionClicked(DialogFragment fragment) {
                        super.onNegativeActionClicked(fragment);
                    }
                };
                ((SimpleDialog.Builder) builder).message("Do you want to proceed?")
                        .title("Submit Job Docket")
                        .positiveAction("YES")
                        .negativeAction("NO");

                DialogFragment fragment = DialogFragment.newInstance(builder);
                fragment.show(getSupportFragmentManager(), null);

            } else {
                Toast.makeText(getApplicationContext(), "Please Check your Internet Connection", Toast.LENGTH_SHORT).show();
            }

        } else {

            com.rey.material.app.Dialog.Builder builder = null;
            builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {

                @Override
                public void onPositiveActionClicked(DialogFragment fragment) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                    super.onPositiveActionClicked(fragment);
                }

                @Override
                public void onNegativeActionClicked(DialogFragment fragment) {
                    super.onNegativeActionClicked(fragment);
                }
            };
            ((SimpleDialog.Builder) builder).message("Please turn on location service to get your current location.")
                    .title("Location")
                    .positiveAction("OK");

            DialogFragment fragment = DialogFragment.newInstance(builder);
            fragment.show(getSupportFragmentManager(), null);
        }
    }

    public void submitJob() {

        dialog4.setCanceledOnTouchOutside(false);
        dialog4.setContentView(R.layout.loading);
        dialog4.contentMargin(0, 0, 0, -25);

        boolean cc = getCheckDocket2().contains(nn);
        if (!cc) {
            boolean dd = getCheckDocket().contains(nn);
            if (((db.getClientStatus(idSelected).equals("1")) || db.getClientStatus(idSelected).equals("null")) && dd) {
                Toast.makeText(getApplicationContext(), "Please Complete the client signature.", Toast.LENGTH_SHORT).show();
            } else {
                RestAdapter restAdapter = new RestAdapter.Builder()
                        .setEndpoint(Constants.API_URL)
                        .build();
                APIInterface interfacem = restAdapter.create(APIInterface.class);
                interfacem.checkJob(session.getToken(), Constants.REQUEST_JOBINFO, mJobObject.getJobID(), new Callback<Object>() {
                    @Override
                    public void success(Object o, Response response) {
                        String jsonObjectResponse = new Gson().toJson(o);
                        try {
                            JSONObject jsonObj = new JSONObject(jsonObjectResponse);
                            if (jsonObj.getString("status").equals("true")) {
                                Log.e("Status of job", " true");
                                builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                                    @Override
                                    public void onPositiveActionClicked(DialogFragment fragment) {
                                        deleteimageData();
                                        db.deleteifSuccess(mJobObject.getJobID());
                                        finish();
                                        super.onPositiveActionClicked(fragment);
                                    }
                                };

                                ((SimpleDialog.Builder) builder).message("It will deleted automatically")
                                        .title(mJobObject.getJobID() + " already submited")
                                        .positiveAction("Ok");
                                DialogFragment fragment = DialogFragment.newInstance(builder);
                                fragment.setCancelable(false);
                                fragment.show(getSupportFragmentManager(), null);
                                //
                            } else {
                                Log.e("Status of job", "false");
                                counterphoto = 0;
                                gpsUploadPhoto();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

//                        String jsonObjectResponse = new Gson().toJson(o);
//                        Log.d("tae", jsonObjectResponse.toString());
//                        try {
//                            JSONObject jsonObject = new JSONObject(jsonObjectResponse.toString());
//                            if (jsonObject.get("status").toString().equals("false")) {
//                                if (connection.isConnectedToInternet()) {
//                                    gpsUploadPhoto();
//                                } else {
//                                    Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
//                                }
//                            } else {
//                                Dialog.Builder builder = null;
//                                builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
//                                    @Override
//                                    public void onPositiveActionClicked(DialogFragment fragment) {
//                                        if (connection.isConnectedToInternet()) {
//                                            gpsUploadPhoto();
//                                        } else {
//                                            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
//                                        }
//                                        super.onPositiveActionClicked(fragment);
//                                    }
//
//                                    @Override
//                                    public void onNegativeActionClicked(DialogFragment fragment) {
//                                        Dialog.Builder builder = null;
//                                        builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
//                                            @Override
//                                            public void onPositiveActionClicked(DialogFragment fragment) {
//                                                Toast.makeText(JobView.this, "Job successfully deleted.", Toast.LENGTH_SHORT).show();
//                                                finish();
//                                                super.onPositiveActionClicked(fragment);
//                                            }
//
//                                            @Override
//                                            public void onNegativeActionClicked(DialogFragment fragment) {
//                                                super.onNegativeActionClicked(fragment);
//                                            }
//                                        };
//
//                                        ((SimpleDialog.Builder) builder).message("Would you like to delete it?")
//                                                .title("")
//                                                .positiveAction("YES")
//                                                .negativeAction("NO");
//
//                                        DialogFragment fragmentd = DialogFragment.newInstance(builder);
//                                        fragmentd.show(getSupportFragmentManager(), null);
//
//                                        super.onNegativeActionClicked(fragment);
//                                    }
//                                };
//
//                                ((SimpleDialog.Builder) builder).message("Job has been already submitted. Would you like to override?")
//                                        .title("")
//                                        .positiveAction("YES")
//                                        .negativeAction("NO");
//
//                                DialogFragment fragment = DialogFragment.newInstance(builder);
//                                fragment.show(getSupportFragmentManager(), null);
//                            }
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            Log.e("Error", "Error while connecting to server" + e.toString());
//                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("sd", error.toString());
                    }
                });
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please Complete the operator signature.", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_settings:
                if (connection.isConnectedToInternet()) {
                    submitJob();
                } else {
                    Toast.makeText(getApplicationContext(), "No internet connection.", Toast.LENGTH_SHORT).show();
                }
                return true;
            case android.R.id.home:
//                boolean cc = mJobObject.getJobID().contains(nn);
//                if (!cc) {
//                    Log.e("mo tiot", getDockets());
//                    if (getDockets().equals("[]")) {
//                        Toast.makeText(getApplicationContext(), "Please have your client signed before sending.", Toast.LENGTH_SHORT).show();
//                    } else {
////                        progress = ProgressDialog.show(this, "",
////                                "Uploading..", true);
////                        if (connection.isConnectedToInternet()) {
////                            uploadData();
////                        } else {
////                            Toast.makeText(getApplicationContext(), "Please Check your Internet Connection", Toast.LENGTH_SHORT).show();
////                            progress.dismiss();
////                        }
//                    }
//                } else {
//                    Toast.makeText(getApplicationContext(), "Please Complete the operator and client signature.", Toast.LENGTH_SHORT).show();
//                }

                finish();
                overridePendingTransition(0, 0);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public String signAge() {
        return "[   {   \"JobID\":\"" + mJobObject.getJobID() + "\", \"RegoNo\":\"1233\", \"Landmark\":\"123\", \"CarriageWay\":\"1\", \"Direction\":\"0\", \"TimeErected\":\"1800\", \"TimeCollected\":\"1800\", \"TimeChecked1\":\"1800\", \"TimeChecked2\":\"1800\", \"TimeChecked3\":\"1800\", \"TimeChecked4\":\"1800\", \"AuditSigns\":\"SignID:1^Metres:2^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:2^Metres:23^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:3^Metres:322^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:4^Metres:23^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:5^Metres:23^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:6^Metres:23^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:7^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:8^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:9^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:10^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:11^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:12^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:13^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:14^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:15^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:16^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:17^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:18^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:19^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:20^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:21^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:22^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:23^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:24^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:25^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:26^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:27^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:28^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:29^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:30^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:31^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:32^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:33^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:34^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:35^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:36^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:37^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:38^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:39^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:40^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:41^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:42^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:43^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2\" }, {   \"JobID\":\"" + mJobObject.getJobID() + "\", \"RegoNo\":\"2\", \"Landmark\":\"2\", \"CarriageWay\":\"2\", \"Direction\":\"0\", \"TimeErected\":\"1800\", \"TimeCollected\":\"1800\", \"TimeChecked1\":\"1800\", \"TimeChecked2\":\"1800\", \"TimeChecked3\":\"1800\", \"TimeChecked4\":\"1800\", \"AuditSigns\":\"SignID:1^Metres:2^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:2^Metres:2^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:3^Metres:2^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:4^Metres:22^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:5^Metres:2^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:6^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:7^Metres:22^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:8^Metres:2^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:9^Metres:2^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:10^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:11^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:12^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:13^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:14^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:15^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:16^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:17^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:18^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:19^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:20^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:21^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:22^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:23^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:24^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:25^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:26^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:27^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:28^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:29^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:30^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:31^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:32^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:33^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:34^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:35^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:36^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:37^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:38^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:39^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:40^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:41^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:42^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2~SignID:43^Metres:^Qty:1^AfterCare:1^AfterCareMetres:20^AfterCareQty:2\" } ]";
    }

    public String getCheckDocket2() {
        String temp = "";
        String timeshet = db.getDcoketsClient(mJobObject.getJobID());
        Log.e("to heck", timeshet);
        String abc = timeshet.replace("^^^", "###");
        String[] splitter = abc.split("###");

        for (int c = 0; c < splitter.length; c++) {
            String abcd = splitter[1].replace("|~~~~|", ">>>>");
            String[] splitters = abcd.split(">>>>");
            if (splitters.length >= 4) {
                if (splitter.length - 1 == c) {
                } else {
                    temp = temp + "{\"JobID\":\"" + splitters[0] + "\",\"OperatorID\":\"" + splitters[1] + "\",\"Attachment\":\"" + splitters[4] + "\",\"AttachmentTypeID\":\"2\"," + "\"AttachedOn\":\"" + splitters[3] + "\"}";
                }
            } else {
                temp = "";
            }
        }
        return "[" + temp + "]";
    }

    public String getCheckDocket() {
        String temp = "";
        String timeshet = db.getDcoketsClient(mJobObject.getJobID());

        String abc = timeshet.replace("^^^^^^^^", "###");
        String[] splitter = abc.split("###");

        for (int c = 0; c < splitter.length; c++) {
            String abcd = splitter[c].replace("|~~~~|", ">>>>");
            String[] splitters = abcd.split(">>>>");
            if (splitters.length >= 4) {
                if (db.getClientStatus(idSelected).equals("0")) {
                    if (splitter.length - 1 == c) {
                        temp = temp + "{\"JobID\":\"" + splitters[0] + "\",\"OperatorID\":\"" + splitters[1] + "\",\"Attachment\":\"" + splitters[4] + "\",\"AttachmentTypeID\":\"2\"," + "\"AttachedOn\":\"" + splitters[3] + "\"}";
                    } else {

                    }
                } else {
                    if (splitter.length - 1 == c) {
                        temp = temp + "{\"JobID\":\"" + splitters[0] + "\",\"OperatorID\":\"" + splitters[1] + "\",\"Attachment\":\"" + splitters[4] + "\",\"AttachmentTypeID\":\"1\"," + "\"AttachedOn\":\"" + splitters[3] + "\"}";
                    } else {
                        temp = temp + "{\"JobID\":\"" + splitters[0] + "\",\"OperatorID\":\"" + splitters[1] + "\",\"Attachment\":\"" + splitters[4] + "\",\"AttachmentTypeID\":\"2\"," + "\"AttachedOn\":\"" + splitters[3] + "\"},";
                    }
                }
            } else {
                temp = "";
            }
        }
        return "[" + temp + "]";
    }

    public String signsAge() {
        String re = "[";
        ArrayList<SingageAudit> signage = db.getAllSignage(mJobObject.getJobID());
        if (db.getAllSelectedSignstoUpload(mJobObject.getJobID(), "1").equals("")) {
            Log.e("log", "0 false" + db.getAllSelectedSignstoUpload(mJobObject.getJobID(), "0"));
        } else {
            int da = Integer.parseInt(signage.get(0).getDirection()) + 1;
            re = re + "\n" +
                    "{\n" +
                    "\"JobID\": \"" + mJobObject.getJobID() + "\",\n" +
                    "\"Landmark\": \"" + signage.get(0).getLandmark() + "\",\n" +
                    "\"CarriageWay\": \"1\",\n" +
                    "\"Direction\": \"" + da + "\",\n" +
                    "\"TimeErected\": \"" + signage.get(0).getTimeErected() + "\",\n" +
                    "\"TimeCollected\": \"" + signage.get(0).getTimeCollected() + "\",\n" +
                    "\"TimeChecked1\": \"" + signage.get(0).getTimeChecked1() + "\",\n" +
                    "\"TimeChecked2\": \"" + signage.get(0).getTimeChecked2() + "\",\n" +
                    "\"TimeChecked3\": \"" + signage.get(0).getTimeChecked3() + "\",\n" +
                    "\"TimeChecked4\": \"" + signage.get(0).getTimeChecked4() + "\",\n" +
                    "\"ErectedBy\": \"" + signage.get(0).getErectedBy() + "\",\n" +
                    "\"CollectedBy\": \"" + signage.get(0).getCollectedBy() + "\",\n" +
                    "\"AuditSigns\": \"" + db.getAllSelectedSignstoUpload(mJobObject.getJobID(), "1").substring(0, db.getAllSelectedSignstoUpload(mJobObject.getJobID(), "1").length() - 1).replace("null", "") + "\"\n" +
                    "}";
        }

        if (!(db.getAllSelectedSignstoUpload(mJobObject.getJobID(), "1").equals("")) && !(db.getAllSelectedSignstoUpload(mJobObject.getJobID(), "0").equals(""))) {
            re = re + ",";
        }

        if ((db.getAllSelectedSignstoUpload(mJobObject.getJobID(), "1").equals("")) && (db.getAllSelectedSignstoUpload(mJobObject.getJobID(), "0").equals(""))) {
            re = re + "\n" +
                    "{\n" +
                    "\"JobID\": \"" + mJobObject.getJobID() + "\",\n" +
                    "\"Landmark\": \"" + signage.get(0).getLandmark() + "\",\n" +
                    "\"CarriageWay\": \"\",\n" +
                    "\"Direction\": \"" + signage.get(0).getDirection() + "\",\n" +
                    "\"TimeErected\": \"\",\n" +
                    "\"TimeCollected\": \"\",\n" +
                    "\"TimeChecked1\": \"\",\n" +
                    "\"TimeChecked2\": \"\",\n" +
                    "\"TimeChecked3\": \"\",\n" +
                    "\"TimeChecked4\": \"\",\n" +
                    "\"ErectedBy\": \"\",\n" +
                    "\"CollectedBy\": \"\",\n" +
                    "\"AuditSigns\": \"\"\n" +
                    "}";
        }

        if (db.getAllSelectedSignstoUpload(mJobObject.getJobID(), "0").equals("")) {
            Log.e("log", "0 false" + db.getAllSelectedSignstoUpload(mJobObject.getJobID(), "0"));
        } else {
            re = re + "{" +
                    "\"JobID\": \"" + mJobObject.getJobID() + "\",\n" +
                    "\"Landmark\": \"" + signage.get(0).getLandmark() + "\",\n" +
                    "\"CarriageWay\": \"2\",\n" +
                    "\"Direction\": \"" + signage.get(0).getDirection() + "\",\n" +
                    "\"TimeErected\": \"" + signage.get(0).getTimeErected() + "\",\n" +
                    "\"TimeCollected\": \"" + signage.get(0).getTimeCollected() + "\",\n" +
                    "\"TimeChecked1\": \"" + signage.get(0).getTimeChecked1() + "\",\n" +
                    "\"TimeChecked2\": \"" + signage.get(0).getTimeChecked2() + "\",\n" +
                    "\"TimeChecked3\": \"" + signage.get(0).getTimeChecked3() + "\",\n" +
                    "\"TimeChecked4\": \"" + signage.get(0).getTimeChecked4() + "\",\n" +
                    "\"ErectedBy\": \"" + signage.get(0).getErectedBy() + "\",\n" +
                    "\"CollectedBy\": \"" + signage.get(0).getCollectedBy() + "\",\n" +
                    "\"AuditSigns\": \"" + db.getAllSelectedSignstoUpload(mJobObject.getJobID(), "0").substring(0, db.getAllSelectedSignstoUpload(mJobObject.getJobID(), "0").length() - 1).replace("null", "") + "\"\n" +
                    "}";
        }
        return re + "]";
    }

    public String getAllAdditionalHazard() {
        Gson mac = new Gson();
        String re = "";
        String de = db.checkhazard(mJobObject.getJobID()).replace("]", "");
        if (db.checkhazard(mJobObject.getJobID()).contains("\"AddHazard\":\"\"")) {
            re = "[]";
        } else {
            if (db.getAdditionalHazard(mJobObject.getJobID()).size() != 0) {
                de = de + ",";
            }
            re = de + mac.toJson(db.getAdditionalHazard(mJobObject.getJobID())).replace("[", "");
        }
        return re;
    }

    public void uploadData() {

        long time = System.currentTimeMillis();

        mSignatureModelz = new ArrayList<>();
        JsonArray o = (JsonArray) new JsonParser().parse(getSignatures().replace("\n", "").replace("]]", "]"));

        for (int i = 0; i < o.size(); i++) {
            SignatureModelz sss = new SignatureModelz();
            sss.setAttachedOn(time + "");
            sss.setJobID(o.get(i).getAsJsonObject().get("JobID").getAsString());
            sss.setTCName(o.get(i).getAsJsonObject().get("TCName").getAsString());
            sss.setAttachment(o.get(i).getAsJsonObject().get("Attachment").getAsString());
            mSignatureModelz.add(sss);
        }

        Log.e("new return", kk.toJson(mSignatureModelz));

        String finalSiageaudit = "";
        Log.e("reqz", getRequriements());
        if (!checkSignageTOUpload()) {
            finalSiageaudit = "[\n" +
                    "{\n" +
                    "\"JobID\": \"" + mJobObject.getJobID() + "\",\n" +
                    "\"Landmark\": \"n/a\",\n" +
                    "\"CarriageWay\": \"\",\n" +
                    "\"Direction\": \"\",\n" +
                    "\"TimeErected\": \"\",\n" +
                    "\"TimeCollected\": \"\",\n" +
                    "\"TimeChecked1\": \"\",\n" +
                    "\"TimeChecked2\": \"\",\n" +
                    "\"TimeChecked3\": \"\",\n" +
                    "\"TimeChecked4\": \"\",\n" +
                    "\"ErectedBy\": \"\",\n" +
                    "\"CollectedBy\": \"\",\n" +
                    "\"AuditSigns\": \"\"\n" +
                    "}]";
        } else {
            finalSiageaudit = signsAge().replace("AfterCareQty\"", "AfterCareQty:\"");
        }

        Log.e(TAG, checkSignageTOUpload() + " -uploadData: " + finalSiageaudit);

        progresssaving.setProgress(70);
        statusbaritem.setText("Uploading Job Docket ");
        statuspercentage.setText("70%");
        String Checklist2 = getSiteChecklist().replace("|~", "~");
        String Checklist = Checklist2.replace("^x~", "^n/a~");
        String d = getDockets().replace("\n", "");
        String requiremnents = getRequriements().replace("\n", "");
        String signs = getSignageAudit().replace("\n", "");
        String additionalhazzad = db.checkhazard(mJobObject.getJobID()).replace("\n", "");

        boolean cc = mJobObject.getJobID().contains(ifsubjob);

        Map<String, String> params = new HashMap<>();
        params.put("operatorID", mJobObject.getOperatorID().toString());
        params.put("jobID", mJobObject.getJobID());
        params.put("assets", "[]");
        params.put("tcAttachment", kk.toJson(mSignatureModelz));//s
        params.put("timesheets", getAllTimesheet());
        params.put("requirements", requiremnents);
        params.put("dockets", d);
        params.put("siteCheckList", Checklist);
        params.put("signageAudit", finalSiageaudit);
//        params.put("signageAudit", signsAge().replace("AfterCareQty\"", "AfterCareQty:\""));
        params.put("orderNumber", mJobObject.getOrderNumber());
        params.put("additionalHazards", getAllAdditionalHazard().replace("\n", ""));
        Log.e(TAG, "uploadData: " + gps);
        params.put("geotag", session.getlocation());

        if (ifAfterisYES()) {
            params.put("afterCare", "1");
        } else {
            params.put("afterCare", "0");
        }
        if (cc) {
            params.put("parentJob", mJobObject.getId());
        } else {
            params.put("parentJob", "");
        }

        if (!db.getotherfield(mJobObject.getJobID()).equals("wala")) {
            String data = db.getotherfield(mJobObject.getJobID());
            String[] arr = data.split("~");
            params.put("clientEmail", arr[2].toString());
            params.put("clientName", arr[1].toString());
        } else {
            params.put("clientEmail", "");
            params.put("clientName", "");
        }

        String hashmap = "operatorID :" + params.get("operatorID").toString() + "\n\n" +
                "jobID :" + params.get("jobID").toString() + "\n\n" +
                "assets :" + params.get("assets").toString() + "\n\n" +
                "tcAttachment :" + params.get("tcAttachment").toString() + "\n\n" +
                "timesheets :" + params.get("timesheets").toString() + "\n\n" +
                "requirements :" + params.get("requirements").toString() + "\n\n" +
                "dockets :" + params.get("dockets").toString() + "\n\n" +
                "siteCheckList :" + params.get("siteCheckList").toString() + "\n\n" +
                "signageAudit :" + finalSiageaudit + "\n\n" +
                "orderNumber :" + params.get("orderNumber").toString() + "\n\n" +
                "additionalHazards :" + params.get("additionalHazards").toString() + "\n\n" +
                "afterCare :" + params.get("afterCare").toString() + "\n\n" +
                "parentJob :" + params.get("parentJob").toString() + "\n\n" +
                "clientEmail :" + params.get("clientEmail").toString() + "\n\n" +
                "geotag :" + session.getlocation() + "\n\n" +
                "clientName :" + params.get("clientName").toString() + "\n\n";

        try {
            File myFile = new File("/sdcard/zzz-" + mJobObject.getJobID() + ".txt");
            myFile.createNewFile();
            FileOutputStream fOut = new FileOutputStream(myFile);
            OutputStreamWriter myOutWriter =
                    new OutputStreamWriter(fOut);
            myOutWriter.append(hashmap);
            myOutWriter.close();
            fOut.close();
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setClient(new OkClient(okHttpClient))
                .setEndpoint(Constants.API_URL)
                .build();

        APIInterface upload = restAdapter.create(APIInterface.class);
//        isFailSafe2 = true;
        if (!isFailSafe2) {
            Log.e(TAG, "uploadData: is failsafe false");
            upload.uploads(session.getToken(), Constants.REQUEST_JOBINFO, params, new Callback<Object>() {
                @Override
                public void success(Object o, Response response) {

                    String jsonObjectResponse = new Gson().toJson(o);
                    Log.e(TAG + " Return Data: ", response.getStatus() + "");
                    Log.e(TAG + " Return Data: ", jsonObjectResponse.toString());
                    Log.e(TAG + " delete gago: ", "sdfsdfsdf");

                    try {
                        JSONObject jsonObject = new JSONObject(jsonObjectResponse.toString());
                        if (jsonObject.has("status")) {
                            String status = jsonObject.getString("status");
                            if (status.equals("false")) {
                                Toast.makeText(getApplicationContext(), jsonObject.getString("message") + " \nPlease retry again.", Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            } else {
                                uploadImage();
                                Dialog.Builder builder = null;
                                builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                                    @Override
                                    public void onPositiveActionClicked(DialogFragment fragment) {
                                        deleteimageData();
                                        db.deleteifSuccess(mJobObject.getJobID());
                                        Intent returnIntent = new Intent();
                                        returnIntent.putExtra("reload", "Yes");
                                        setResult(1);
                                        finish();
                                        // TODO: 05/08/2016 mac
                                        super.onPositiveActionClicked(fragment);
                                    }
                                };
                                ((SimpleDialog.Builder) builder).message("Job Docket " + mJobObject.getJobID() + " has been successfully sent.")
                                        .positiveAction("Ok");
                                DialogFragment fragment = DialogFragment.newInstance(builder);
                                fragment.show(getSupportFragmentManager(), null);
                                fragment.setCancelable(false);
                            }
                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("Error", "Error while connecting to server" + e.toString());
                        finish();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Response r = error.getResponse();
                    switch (error.getKind()) {
                        case NETWORK:
                            Log.e("error", "error while connectiong to NETWORK");
                            break;
                        case UNEXPECTED:
                            Log.e("error", "error while connectiong to server");
                            throw error;
                        case HTTP:
                            switch (r.getStatus()) {
                                case 401:
                                    stat = false;
                                    statusCounter++;
                                    Toast.makeText(JobView.this, "Unable to connect to ManStat. Please try again.", Toast.LENGTH_SHORT).show();
                                    Log.e("check manstat", "false-" + statusCounter);
                                    dialog4.hide();
                                    break;
                                case 404:
                                    stat = false;
                                    statusCounter++;
                                    Toast.makeText(JobView.this, "Unable to connect to ManStat. Please try again.", Toast.LENGTH_SHORT).show();
                                    Log.e("check manstat", "false-" + statusCounter);
                                    dialog4.hide();
                                    break;
                                case 422:
                                    stat = false;
                                    statusCounter++;
                                    Toast.makeText(JobView.this, "Unable to connect to ManStat. Please try again.", Toast.LENGTH_SHORT).show();
                                    Log.e("check manstat", "false-" + statusCounter);
                                    dialog4.hide();
                                    break;
                                case 500:
                                    stat = false;
                                    statusCounter++;
                                    Toast.makeText(JobView.this, "Unable to connect to ManStat. Please try again.", Toast.LENGTH_SHORT).show();
                                    Log.e("check manstat", "false-" + statusCounter);
                                    dialog4.hide();
                                    break;
                            }
                            Log.e("error ", "error while connectiong to HTTP --- " + r.getStatus() + " --0000 " + error.getMessage().toString());
                            break;
                        default:
                            Log.e("error ", "error while connectiong to HTTP" + r.getStatus());
                            break;
                    }
                    dialog.dismiss();
                }
            });
        } else {
            Log.e(TAG, "uploadData: is failsafe true");
            upload.uploadsFailFE(session.getToken(), Constants.REQUEST_JOBINFO, params, new Callback<Object>() {
                @Override
                public void success(Object o, Response response) {
                    String jsonObjectResponse = new Gson().toJson(o);

                    Log.e(TAG + " Return Data: ", response.getStatus() + "");
                    Log.e(TAG + " Return Data: ", jsonObjectResponse.toString());

                    try {
                        JSONObject jsonObject = new JSONObject(jsonObjectResponse.toString());
                        if (jsonObject.has("status")) {
                            String status = jsonObject.getString("status");
                            if (status.equals("false")) {
                                Toast.makeText(getApplicationContext(), jsonObject.getString("message") + " \nPlease retry again.", Toast.LENGTH_SHORT).show();
                                dialog4.dismiss();
                            } else {
                                uploadImage();
                                Dialog.Builder builder = null;
                                builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                                    @Override
                                    public void onPositiveActionClicked(DialogFragment fragment) {
                                        deleteimageData();
                                        db.deleteifSuccess(mJobObject.getJobID());
                                        Intent returnIntent = new Intent();
                                        returnIntent.putExtra("reload", "Yes");
                                        setResult(1);
                                        finish();
                                        // TODO: 05/08/2016 mac
                                        super.onPositiveActionClicked(fragment);
                                    }
                                };

                                ((SimpleDialog.Builder) builder).message("Job Docket " + mJobObject.getJobID() + " has been successfully sent through fail-safe.")
                                        .positiveAction("Ok");
                                DialogFragment fragment = DialogFragment.newInstance(builder);
                                fragment.show(getSupportFragmentManager(), null);
                                fragment.setCancelable(false);
                            }
                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("Error", "Error while connecting to server" + e.toString());
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Response r = error.getResponse();
                    Toast.makeText(JobView.this, "Job Docket " + mJobObject.getJobID() + " has been successfully sent through fail-safe.", Toast.LENGTH_SHORT).show();
                    Log.e("error", "error while connectiong to NETWORK" + error.toString());
                    dialog4.dismiss();
                    dialog.dismiss();
//                    switch (error.getKind()) {
//                        case NETWORK:
//                            Log.e("error", "error while connectiong to NETWORK");
//                            break;
//                        case UNEXPECTED:
//                            Log.e("error", "error while connectiong to server");
//                            throw error;
//                        case HTTP:
//                            switch (r.getStatus()) {
//                                case 401:
//                                    stat = false;
//                                    statusCounter++;
//                                    Toast.makeText(JobView.this, "Unable to connect to ManStat. Please try again.", Toast.LENGTH_SHORT).show();
//                                    Log.e("check manstat", "false-" + statusCounter);
//                                    dialog4.hide();
//                                    break;
//                                case 404:
//                                    stat = false;
//                                    statusCounter++;
//                                    Toast.makeText(JobView.this, "Unable to connect to ManStat. Please try again.", Toast.LENGTH_SHORT).show();
//                                    Log.e("check manstat", "false-" + statusCounter);
//                                    dialog4.hide();
//                                    break;
//                                case 422:
//                                    stat = false;
//                                    statusCounter++;
//                                    Toast.makeText(JobView.this, "Unable to connect to ManStat. Please try again.", Toast.LENGTH_SHORT).show();
//                                    Log.e("check manstat", "false-" + statusCounter);
//                                    dialog4.hide();
//                                    break;
//                                case 500:
//                                    stat = false;
//                                    statusCounter++;
//                                    Toast.makeText(JobView.this, "Unable to connect to ManStat. Please try again.", Toast.LENGTH_SHORT).show();
//                                    Log.e("check manstat", "false-" + statusCounter);
//                                    dialog4.hide();
//                                    break;
//                            }
//                            Log.e("error ", "error while connectiong to HTTP" + r.getStatus() + " 0000 " + error.getMessage().toString());
//                            break;
//                        default:
//                            Log.e("error ", "error while connectiong to HTTP" + r.getStatus());
//                            break;
//                    }
//                    dialog4.dismiss();
//                    dialog.dismiss();
                }
            });
        }
    }

    public void uploadImage() {
        Map<String, String> params = new HashMap<>();
        params.put("jobID", mJobObject.getJobID());

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setClient(new OkClient(okHttpClient))
                .setEndpoint(Constants.API_URL)
                .build();

        APIInterface upload = restAdapter.create(APIInterface.class);
        upload.jobimageReportGenerator(session.getToken(), Constants.REQUEST_JOBINFO, params, new Callback<Object>() {

            @Override
            public void success(Object o, Response response) {
                String jsonObjectResponse = new Gson().toJson(o);
                Log.e(TAG + " yrdy ", response.getStatus() + "");
            }

            @Override
            public void failure(RetrofitError e) {
                e.printStackTrace();
                Log.e("Error", "Error while connecting to server" + e.toString());
            }
        });

    }

    private void galleryAddPic(String dir) {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(dir);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    //// TODO: 05/08/2016 deleteimage
    public void deleteimageData() {
        Log.e("datatocheck", "true");
        photo = db.getAllPhotos(mJobObject.getJobID());
        for (int i = 0; i < photo.size(); i++) {
            photo.get(i).getImage_url();

            Log.e(TAG, "Deleted: " + photo.get(i).getImage_url());

            String[] separated = photo.get(i).getImage_url().split("/");
            String finame = separated[separated.length - 1];

            galleryAddPic(photo.get(i).getImage_url().replace(finame,""));

            File file = new File(Uri.fromFile(new File(photo.get(i).getImage_url())).getPath());
            file.delete();

            try {
                file.getCanonicalFile().delete();
                getApplicationContext().deleteFile(file.getName());
                Log.e(TAG, "Deleted: ");

            } catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG, "Error while deleting: ");
            }

            String[] projection = {MediaStore.Images.Media._ID};

// Match on the file path
            String selection = MediaStore.Images.Media.DATA + " = ?";
            String[] selectionArgs = new String[]{file.getAbsolutePath()};

// Query for the ID of the media matching the file path
            Uri queryUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            ContentResolver contentResolver = getContentResolver();
            Cursor c = contentResolver.query(queryUri, projection, selection, selectionArgs, null);
            if (c.moveToFirst()) {
                // We found the ID. Deleting the item via the content provider will also remove the file
                long id = c.getLong(c.getColumnIndexOrThrow(MediaStore.Images.Media._ID));
                Uri deleteUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);
                contentResolver.delete(deleteUri, null, null);
            } else {
                // File not found in media store DB
            }
            c.close();


//            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
//                getApplicationContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
//                        Uri.parse("file://" + Environment.getExternalStorageDirectory())));
//            } else {
//
//                MediaScannerConnection.scanFile(getApplicationContext(), separated, null, new MediaScannerConnection.OnScanCompletedListener() {
//                    /*
//                     *   (non-Javadoc)
//                     * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
//                     */
//                    public void onScanCompleted(String path, Uri uri) {
//                        Log.i("ExternalStorage", "Scanned " + path + ":");
//                        Log.i("ExternalStorage", "-> uri=" + uri);
//                    }
//                });
//
//            }


//            File file = new File(photo.get(i).getImage_url().replace(finame, ""), finame);
//            boolean deleted = file.delete();
//
//            if (deleted) {
//                Log.e(TAG, "Deleted: ");
//            } else {
//                Log.e(TAG, "Error while deleting: ");
//            }
        }


    }

    public String getRequriements() {
        String rrequimrents = "";
        ModelRequirementsObjects mrequirements;
        mrequirements = db.getallRequirements(mJobObject.getJobID());

        for (int c = 0; c < mrequirements.getmRequirements().size(); c++) {
            if (mrequirements.getmRequirements().size() - 1 == c) {

                Log.e("zzzzz", "-1=c" + (mrequirements.getmRequirements().size() - 1) + " --" + c);
                rrequimrents = rrequimrents + "{\n" +
                        "\"JobID\":\"" + db.getallRequirements(mJobObject.getJobID()).getmRequirements().get(c).getJobID() + "\",\n" +
                        "\"RequirementID\":\"" + db.getallRequirements(mJobObject.getJobID()).getmRequirements().get(c).getRequirementID() + "\",\n" +
                        "\"Description\":\"" + db.getallRequirements(mJobObject.getJobID()).getmRequirements().get(c).getDescription() + "\",\n" +
                        "\"Quantity\":\"" + db.getallRequirements(mJobObject.getJobID()).getmRequirements().get(c).getQuantity() + "\"\n" +
                        "}";

//                if (away.isChecked()) {
//                    rrequimrents = rrequimrents + ",{\n" +
//                            "\"JobID\":\"" + mJobObject.getJobID() + "\",\n" +
//                            "\"RequirementID\":\"701\",\n" +
//                            "\"Description\":\"Away Job\",\n" +
//                            "\"Quantity\":\"1\"\n" +
//                            "}";
//                }
//
//                rrequimrents = rrequimrents + "{\n" +
//                        "\"JobID\":\"" + mJobObject.getJobID() + "\",\n" +
//                        "\"RequirementID\":\"701\",\n" +
//                        "\"Description\":\"Away Job\",\n" +
//                        "\"Quantity\":\"1\"\n" +
//                        "}";

//                if (!status2) {
//                    if (away.isChecked()) {
//
//                    }
//                }
                Log.e("test", rrequimrents);
            } else {
                Log.e("zzzzz", "-1=c false" + (mrequirements.getmRequirements().size() - 1) + " --" + c);
                rrequimrents = rrequimrents + "{\n" +
                        "\"JobID\":\"" + db.getallRequirements(mJobObject.getJobID()).getmRequirements().get(c).getJobID() + "\",\n" +
                        "\"RequirementID\":\"" + db.getallRequirements(mJobObject.getJobID()).getmRequirements().get(c).getRequirementID() + "\",\n" +
                        "\"Description\":\"" + db.getallRequirements(mJobObject.getJobID()).getmRequirements().get(c).getDescription() + "\",\n" +
                        "\"Quantity\":\"" + db.getallRequirements(mJobObject.getJobID()).getmRequirements().get(c).getQuantity() + "\"\n" +
                        "},";
            }
        }
        return "[" + rrequimrents + "]";
    }

//<3 then, <3 still, I always have and I always will

    public String getSiteChecklist() {
        String temp = "";
        if (db.getSiteChecklist(mJobObject.getJobID()) == null) {

        } else {
            temp = db.getSiteChecklist(mJobObject.getJobID());
        }
        return temp;
    }

    public String getSignageAudit() {
        String temp = "";
        if (db.getSignageAudt(mJobObject.getJobID()) == null) {

        } else {
            temp = db.getSignageAudt(mJobObject.getJobID());
        }
        return temp;
    }

    public String getSignatures() {
        String temp = "";
        if (db.getTCAttachment(mJobObject.getJobID()) == null) {
        } else {
            temp = db.getTCAttachment(mJobObject.getJobID());
        }
        return temp;
    }

    public String gelSingleDocket(String jobid, String attachedID) {
        String temp = "";

        String timeshet = db.gelSingleDocket(mJobObject.getJobID(), "1");

        String abc = timeshet.replace("^^^^^^^^", "###");
        String[] splitter = abc.split("###");

        return temp;
    }

    public boolean checkDockets() {
        String temp = "";
        String timeshet = db.getJobDockets(mJobObject.getJobID());
        String abc = timeshet.replace("^^^", "###");
        String[] splitter = abc.split("###");
        for (int c = 0; c < splitter.length; c++) {
            if (c == 1) {
                String abcd = splitter[c].replace("|~~~~|", ">>>>");
                String[] splitters = abcd.split(">>>>");
                temp = temp + "{\"JobID\":\"" + splitters[0] + "\",\"OperatorID\":\"" + splitters[1] + "\",\"Attachment\":\"" + splitters[4] + "\",\"AttachmentTypeID\":\"2\"," + "\"AttachedOn\":\"" + splitters[3] + "\"}";
            }
        }
        return true;
    }

    public String getDockets() {
        String temp = "";
        String timeshet = db.getJobDockets(mJobObject.getJobID());
        String abc = timeshet.replace("^^^^^^^^", "###");
        String[] splitter = abc.split("###");
        for (int c = 0; c < splitter.length; c++) {
            String abcd = splitter[c].replace("|~~~~|", ">>>>");
            String[] splitters = abcd.split(">>>>");
            if (splitters.length >= 4) {
                if (splitter.length - 1 == c) {
                    temp = temp + "{\"JobID\":\"" + splitters[0] + "\",\"OperatorID\":\"" + splitters[1] + "\",\"Attachment\":\"" + splitters[4] + "\",\"AttachmentTypeID\":\"2\"," + "\"AttachedOn\":\"" + splitters[3] + "\"}";
                } else {
                    if (!splitters[4].toString().equals("null")) {
                        temp = temp + "{\"JobID\":\"" + splitters[0] + "\",\"OperatorID\":\"" + splitters[1] + "\",\"Attachment\":\"" + splitters[4] + "\",\"AttachmentTypeID\":\"1\"," + "\"AttachedOn\":\"" + splitters[3] + "\"},";
                    }
                }
            } else {
                temp = "";
            }
        }
        return "[" + temp + "]";

    }

    public String getAllTimesheet() {
        ArrayList<Timesheet_Upload> sdF = db.getAllTime(mJobObject.getJobID());
        Gson gson = new Gson();
        return gson.toJson(sdF).replace("\"BreakFinishDateTime\":\"\"", "\"BreakFinishDateTime\":\"0\"").replace("\"BreakStartDateTime\":\"\"", "\"BreakStartDateTime\":\"0\"");
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Job Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onClick(View v) {
        Dialog.Builder builder = null;
        switch (v.getId()) {
            case R.id.clientSignature: {
                int sizeofTao = db.getAllTimeSigBy(mJobObject.getJobID()).size();
                int mdd = db.counttimeSheetTao(mJobObject.getJobID());
                if (sizeofTao == mdd) {
                    Intent clint = new Intent(JobView.this, TimesheetExpandable.class);
                    clint.putExtra("type", "1");
                    clint.putExtra("data", Parcels.wrap(mJobObject));
                    startActivity(clint);
                } else {
                    Toast.makeText(getApplicationContext(), "Please complete the form to proceed.", Toast.LENGTH_SHORT).show();
                }
            }
            break;
            case R.id.operatorSignature:
                int sizeofTao = db.getAllTimeSigBy(mJobObject.getJobID()).size();
                int mdd = db.counttimeSheetTao(mJobObject.getJobID());
                if (sizeofTao == mdd) {
                    if (db.checkJobstatus(mJobObject.getJobID())) {
                        if (db.getJobStatusStats(mJobObject.getJobID()).getSingageAudit().equals("1")) {
                            if (checkSignageTOUpload()) {
                                Intent clint2 = new Intent(JobView.this, SignatureActivity.class);
                                clint2.putExtra("type", "2");
                                clint2.putExtra("data", Parcels.wrap(mJobObject));
                                startActivity(clint2);
                            } else {
                                Toast.makeText(getApplicationContext(), "Please complete the form to proceed.", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent clint2 = new Intent(JobView.this, SignatureActivity.class);
                            clint2.putExtra("type", "2");
                            clint2.putExtra("data", Parcels.wrap(mJobObject));
                            startActivity(clint2);
                        }
                    } else {
                        Intent clint2 = new Intent(JobView.this, SignatureActivity.class);
                        clint2.putExtra("type", "2");
                        clint2.putExtra("data", Parcels.wrap(mJobObject));
                        startActivity(clint2);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please complete the form to proceed.", Toast.LENGTH_SHORT).show();
                }

//                if (opensignatureValidation(mJobObject.getJobID())) {
//                    Intent clint2 = new Intent(JobView.this, SignatureActivity.class);
//                    clint2.putExtra("type", "2");
//                    clint2.putExtra("data", Parcels.wrap(mJobObject));
//                    startActivity(clint2);
//                } else {

//                }

                break;
            case R.id.createNewjob:
                builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                    @Override
                    public void onPositiveActionClicked(DialogFragment fragment) {
                        mj = db.getAllJobsID(mJobObject.getJobID());

                        int count;
                        if (db.getAllSubJobCoutner(mJobObject.getJobID()).size() == 0) {
                            count = 1;
                        } else {
                            count = Integer.parseInt(db.getAllSubJobCoutner(mJobObject.getJobID()).get(0).getCounter()) + 1;
                        }

                        SubJobCounter subJobCounter = new SubJobCounter();
                        subJobCounter.setJobId(mJobObject.getJobID());
                        subJobCounter.setCounter(count + "");
                        db.addUpdateSUbjobCounter(subJobCounter);


                        for (int s = 0; s < db.getAllDocketAttachments(mJobObject.getJobID()).getDocketAttachments().size(); s++) {
                            db.addDocketAttachments2(db.getAllDocketAttachments(mJobObject.getJobID()).getDocketAttachments().get(s), mJobObject.getJobID() + "." + count);
                        }

                        db.addContact2(mj.getmJob().get(0), mJobObject.getJobID() + "." + count);
                        db.updatejob(mJobObject.getJobID() + "." + count, mJobObject.getJobID() + "." + count);
                        if (db.getallRequirements(mJobObject.getJobID()).getmRequirements().size() != 0) {
                            db.addRequirements(db.getallRequirements(mJobObject.getJobID()).getmRequirements().get(0), mJobObject.getJobID() + "." + count);
                        }
                        Log.e("timesheet return", db.getallTimesheets(mJobObject.getJobID()).getTimesheets().size() + "");

                        for (int c = 0; c < db.getallTimesheets(mJobObject.getJobID()).getTimesheets().size(); c++) {
                            db.addTimesheets2(db.getallTimesheets(mJobObject.getJobID()).getTimesheets().get(c), mJobObject.getJobID() + "." + count);
//                            String d = db.checkifExistLocalTime(db.getallTimesheets(mJobObject.getJobID()).getTimesheets().get(c).getJobID() + "." + counter, db.getallTimesheets(mJobObject.getJobID()).getTimesheets().get(c).getContactID());
//                            if (d == null) {
                            db.addTimeSheetLocal(mJobObject.getJobID() + "." + count, db.getallTimesheets(mJobObject.getJobID()).getTimesheets().get(c).getContactID(), db.getallTimesheets(mJobObject.getJobID()).getTimesheets().get(c).getId(), db.getallTimesheets(mJobObject.getJobID()).getTimesheets().get(c).getStartDateTime(), db.getallTimesheets(mJobObject.getJobID()).getTimesheets().get(c).getEndDateTime());
//                            }
                        }

//                        if (ad == null) {
                        db.addCreateDockets(mJobObject.getJobID() + "." + count, mJobObject.getOperatorID(), "1", "null", "null");
//                        }

//                        if (bd == null) {
                        db.addCreateDockets(mJobObject.getJobID() + "." + count, mJobObject.getOperatorID(), "2", "null", "null");
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("reload", "Yes");
                        setResult(1);
                        finish();

                        super.onPositiveActionClicked(fragment);
                    }

                    @Override
                    public void onNegativeActionClicked(DialogFragment fragment) {
                        super.onNegativeActionClicked(fragment);
                    }
                };

                ((SimpleDialog.Builder) builder).message("Do you want to proceed?")
                        .title("Add Sub Job")
                        .positiveAction("CREATE")
                        .negativeAction("CANCEL");
                DialogFragment fragment = DialogFragment.newInstance(builder);
                fragment.show(getSupportFragmentManager(), null);

                break;
            case R.id.ordernoedittext:

                dialog.setCanceledOnTouchOutside(false);
                dialog.setContentView(R.layout.activity_requirement_item_view);
                dialog.contentMargin(0, 0, 0, -25);
                final EditText quan = (EditText) dialog.findViewById(R.id.quantity);
                quan.setText(mJobObject.getOrderNumber());

                LinearLayout save = (LinearLayout) dialog.findViewById(R.id.save);
                LinearLayout cancel = (LinearLayout) dialog.findViewById(R.id.cancel);

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        db.updateordernumber(mJobObject.getJobID(), quan.getText().toString());
                        mJobObject.setOrderNumber(quan.getText().toString());
                        ordernumbertext.setText(quan.getText().toString());
                        Toast.makeText(getApplicationContext(), "Changes have been saved.", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();

                    }
                });
                dialog.show();
                break;
        }
    }

    public String returrID() {
        return JobID;
    }

    public void siteContact(TextView sitecontact, String mobilephone, String phone) {
        if ((mobilephone.equals("")) && (phone.equals(""))) {
            sitecontact.setText("Not Applicable");
        } else if ((mobilephone.equals("")) && (!phone.equals(""))) {
            sitecontact.setText(phone);
        } else if ((!mobilephone.equals("")) && (phone.equals(""))) {
            sitecontact.setText(mobilephone);
        } else if ((!mobilephone.equals("")) && (!phone.equals(""))) {
            sitecontact.setText(mobilephone + " / " + phone);
        }
    }

    public long compareDates(long miliseconds1, long miliseconds2) {
        return miliseconds1 - miliseconds2;
    }

    public String dateReadable(long miliseconds) {
        long time = miliseconds;
        return (new SimpleDateFormat("dd MMM yyyy HH:mm")).format(new Date(time));
    }

    public boolean checkBelow(String toValidate) {
        boolean temp = false;
        boolean ishave = toValidate.contains("-");
        if (ishave) {
            temp = true;
        }
        return temp;
    }

    public void checkifExpired() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_URL)
                .build();
        APIInterface interfacem = restAdapter.create(APIInterface.class);
        interfacem.checkExist(session.getToken(), Constants.REQUEST_JOBINFO, mJobObject.getJobID(), new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                String jsonObjectResponse = new Gson().toJson(o);
                Log.e("return ofcheckifExpired", jsonObjectResponse);
                try {
                    JSONObject jsonObj = new JSONObject(jsonObjectResponse);
                    if (jsonObj.getString("status").equals("true")) {
                        builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                            @Override
                            public void onPositiveActionClicked(DialogFragment fragment) {
                                deleteimageData();
                                db.deleteifSuccess(mJobObject.getJobID());
                                finish();
                                super.onPositiveActionClicked(fragment);
                            }
                        };
                        ((SimpleDialog.Builder) builder).message(mJobObject.getJobID() + " is already expired. It will now be deleted automatically.")
                                .title("Expired job")
                                .positiveAction("Ok");
                        DialogFragment fragment = DialogFragment.newInstance(builder);
                        fragment.setCancelable(false);
                        fragment.show(getSupportFragmentManager(), null);
                    } else {
                        Log.e("Status of job expired", " false");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Response r = error.getResponse();
                switch (error.getKind()) {
                    case NETWORK:
                        // TODO something
                        Log.e("error", "error while connectiong to NETWORK");
                        break;
                    case UNEXPECTED:
                        Log.e("error", "error while connectiong to server");
                        throw error;
                    case HTTP:
                        switch (r.getStatus()) {
                            case 401:

                                break;
                            case 404:

                                break;
                            case 500:

                                break;
                        }
                        Log.e("error ", "error while connectiong to HTTP" + r.getStatus() + " 0000 " + r.getStatus() + error.getMessage().toString());
                        break;
                    default:
                        Log.e("error ", "error while connectiong to HTTP" + r.getStatus());
                        break;
                }
            }
        });
    }

    public String deleteIfJobUploaded(final String jobid) {
        final String[] ids = {""};

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_URL)
                .build();

        APIInterface interfacem = restAdapter.create(APIInterface.class);
        interfacem.checkJob(session.getToken(), Constants.REQUEST_JOBINFO, jobid, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                String jsonObjectResponse = new Gson().toJson(o);
                try {
                    JSONObject jsonObj = new JSONObject(jsonObjectResponse);
                    if (jsonObj.getString("status").equals("true")) {
                        Log.e("Status of job", " true");
                        builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                            @Override
                            public void onPositiveActionClicked(DialogFragment fragment) {
                                deleteimageData();
                                db.deleteifSuccess(jobid);
                                finish();
                                super.onPositiveActionClicked(fragment);
                            }
                        };
                        ((SimpleDialog.Builder) builder).message("It will deleted automatically")
                                .title(mJobObject.getJobID() + " already submited")
                                .positiveAction("Ok");
                        DialogFragment fragment = DialogFragment.newInstance(builder);
                        fragment.setCancelable(false);
                        fragment.show(getSupportFragmentManager(), null);
                        //
                    } else {
                        Log.e("Status of job", "false");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e(TAG, "failure:sdfsdf " + error.toString());
            }
        });
        return ids[0];
    }

    public void uploadPhotoWidthLoading() {
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.loadingtoupload);

        statusbaritem = (TextView) dialog.findViewById(R.id.statusbaritem);
        statuspercentage = (TextView) dialog.findViewById(R.id.statuspercentage);
        progresssaving = (ProgressBar) dialog.findViewById(R.id.progressBar7);

        progresssaving.getProgressDrawable().setColorFilter(Color.LTGRAY, PorterDuff.Mode.SRC_IN);
        progresssaving.setProgress(5);
        dialog.contentMargin(0, 0, 0, -25);
        uploadPhoto();
        dialog.show();
    }

    public void uploadPhoto() {
        progressing = 0;
        progresssaving.setProgress(20);
        statusbaritem.setText("Uploading Photos");
        Log.d("uploadphotos", "stage one");
        final ArrayList<PhotoItem> finalPhoto = photo;
        photo = db.getAllPhotos(mJobObject.getJobID());
        if (photo.size() <= 0) {
            uploadData();
        } else {
            for (final int[] i = {0}; i[0] < photo.size(); i[0]++) {
                Map<String, String> params = new HashMap<>();
                RestAdapter restAdapter = new RestAdapter.Builder()
                        .setClient(new OkClient(okHttpClient))
                        .setEndpoint(Constants.API_URL)
                        .build();
                TypedFile typedFile = new TypedFile("multipart/form-data", new File(photo.get(i[0]).getImage_url()));
                APIInterface interfacem = restAdapter.create(APIInterface.class);
                final int finalI = i[0];
                interfacem.uploadPhotos(session.getToken(), Constants.REQUEST_JOBINFO, mJobObject.getJobID(), "14.584619, 121.076066", typedFile, new Callback<Object>() {
                    @Override
                    public void success(Object o, Response response) {
                        String jsonObjectResponse = new Gson().toJson(o);
                        Log.d(TAG, jsonObjectResponse.toString());
                        Log.d("uploadphotos", jsonObjectResponse.toString());
                        counterphoto++;
                        float hpercent = 100 / photo.size();
                        progressing = (int) (20 + hpercent + counterphoto);
                        db.updatePhotoStatz(photo.get(finalI).getImage_url());
                        progresssaving.setProgress(50);
                        statusbaritem.setText("Uploading Photos");
                        statuspercentage.setText(50 + "%");
                        if (counterphoto == photo.size()) {
                            uploadData();
                        }
                    }

                    @Override
                    public void failure(RetrofitError e) {
                        Log.e("Errord", e.toString());
                        e.printStackTrace();
                        erroruploadImage = true;
                        i[0] = photo.size();

                        Response r = e.getResponse();

                        switch (e.getKind()) {
                            case NETWORK:
                                Log.e("error", "error while connectiong to NETWORK");

                                if (connection.isConnectedToInternet()) {
                                    counterphoto++;
                                    float hpercent = 100 / photo.size();
                                    progressing = (int) (20 + hpercent + counterphoto);
                                    db.updatePhotoStatz(photo.get(finalI).getImage_url());
                                    progresssaving.setProgress(50);
                                    statusbaritem.setText("Uploading Photos");
                                    statuspercentage.setText(50 + "%");
                                    if (counterphoto == photo.size()) {
                                        uploadData();
                                    }
                                } else {
                                    Toast.makeText(JobView.this, "Error While Uploading Photos \nNo internet connection.", Toast.LENGTH_SHORT).show();

                                    dialog.dismiss();
                                }


                                break;
                            case UNEXPECTED:
                                Log.e("error", "error while connectiong to server");
                                Toast.makeText(JobView.this, "Error While Uploading Photos \nConnection Error.", Toast.LENGTH_SHORT).show();

                                throw e;
                            case HTTP:
                                switch (r.getStatus()) {
                                    case 401:
                                        stat = false;
                                        dialog4.hide();
                                        Log.e("error", "error while connectiong to " + r.getStatus());
                                        dialog.dismiss();
                                        break;
                                    case 404:
                                        stat = false;
                                        Log.e("error", "error while connectiong to " + r.getStatus());
                                        dialog.dismiss();
                                        dialog4.hide();
                                        break;
                                    case 422:
                                        stat = false;
                                        Log.e("error", "error while connectiong to " + r.getStatus());
                                        dialog.dismiss();
                                        break;
                                    case 500:
                                        stat = false;
                                        Log.e("error", "error while connectiong to " + r.getStatus());
                                        dialog.dismiss();
                                        break;
                                }
                                break;
                            default:
                                Log.e("error", "error while connectiong to " + r.getStatus());
                                dialog.dismiss();
                                break;
                        }
                    }
                });
            }
        }
    }

    public void checkManstat() {
        if (connection.isConnectedToInternet()) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Constants.API_URL)
                    .build();
            APIInterface interfacem = restAdapter.create(APIInterface.class);
            interfacem.getCheckManstat(session.getToken(), Constants.REQUEST_JOBINFO, new Callback<CheckManstatStatus>() {
                @Override
                public void success(CheckManstatStatus mans, Response response) {
                    String jsonObjectResponse = new Gson().toJson(mans);
                    Log.e("sdf", jsonObjectResponse);
                    if (mans.getStatus().equals("false")) {
                        stat = false;
                        statusCounter++;
                        dialog4.hide();
                        Toast.makeText(JobView.this, "Unable to connect to ManStat. Please try again.", Toast.LENGTH_SHORT).show();
                        Log.e("check manstat", "false-" + statusCounter);
                    } else {
                        uploadPhotoWidthLoading();
                        Log.e("check manstat", "true" + statusCounter);
                        stat = true;
                        dialog4.hide();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Response r = error.getResponse();
                    switch (error.getKind()) {
                        case NETWORK:
                            Log.e("error", "error while connectiong to NETWORK");
                            break;
                        case UNEXPECTED:
                            Log.e("error", "error while connectiong to server");
                            throw error;
                        case HTTP:
                            switch (r.getStatus()) {
                                case 401:
                                    stat = false;
                                    statusCounter++;
                                    Toast.makeText(JobView.this, "Unable to connect to ManStat. Please try again.", Toast.LENGTH_SHORT).show();
                                    Log.e("check manstat", "false-" + statusCounter);
                                    dialog4.hide();
                                    break;
                                case 404:
                                    stat = false;
                                    statusCounter++;
                                    Toast.makeText(JobView.this, "Unable to connect to ManStat. Please try again.", Toast.LENGTH_SHORT).show();
                                    Log.e("check manstat", "false-" + statusCounter);
                                    dialog4.hide();
                                    break;
                                case 422:
                                    stat = false;
                                    statusCounter++;
                                    Toast.makeText(JobView.this, "Unable to connect to ManStat. Please try again.", Toast.LENGTH_SHORT).show();
                                    Log.e("check manstat", "false-" + statusCounter);
                                    dialog4.hide();
                                    break;
                                case 500:
                                    stat = false;
                                    statusCounter++;
                                    Toast.makeText(JobView.this, "Unable to connect to ManStat. Please try again.", Toast.LENGTH_SHORT).show();
                                    Log.e("check manstat", "false-" + statusCounter);
                                    dialog4.hide();
                                    break;
                            }
                            Log.e("error ", "error while connectiong to HTTP-" + r.getStatus() + "- 0000 -" + error.getMessage().toString());
                            break;
                        default:
                            Log.e("error ", "error while connectiong to HTTP" + r.getStatus());
                            break;
                    }
                }
            });
        } else {
            Toast.makeText(JobView.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean checkSignageTOUpload() {
        boolean g = false;
        if (!db.checkOktoupload(mJobObject.getJobID())) {
            if (!metresQtyChecker(mJobObject.getJobID())) {
                g = true;
            } else {
                g = false;
            }

        } else {
            g = false;
        }
        return g;
    }

    public boolean metresQtyChecker(String ids) {
        boolean s = false;

        if ((db.getAllSelectedSignstoUpload(ids, "0").contains("^Metres:null")) || (db.getAllSelectedSignstoUpload(ids, "0").contains("^Metres:^")) || (db.getAllSelectedSignstoUpload(ids, "0").contains("^Metres:0^"))) {
            s = true;
        }
        if ((db.getAllSelectedSignstoUpload(ids, "1").contains("^Metres:null")) || (db.getAllSelectedSignstoUpload(ids, "1").contains("^Metres:^")) || (db.getAllSelectedSignstoUpload(ids, "1").contains("^Metres:0^"))) {
            s = true;
        }

        if (db.getAllSelectedSignstoUpload(ids, "1").contains("^Qty:null") || (db.getAllSelectedSignstoUpload(ids, "1").contains("^Qty:^")) || (db.getAllSelectedSignstoUpload(ids, "1").contains("^Qty:0^"))) {
            s = true;
        }

        if (db.getAllSelectedSignstoUpload(ids, "0").contains("^Qty:null") || (db.getAllSelectedSignstoUpload(ids, "0").contains("^Qty:^")) || (db.getAllSelectedSignstoUpload(ids, "0").contains("^Qty:0^"))) {
            s = true;
        }
        if (checkifAftercareisOK(ids)) {
            s = true;
        }
        return s;
    }


    public boolean checkifAftercareisOK(String ids) {

        boolean s = false;
        String[] signages = db.getAllSelectedSignstoUpload(ids, "0").split("~");
        for (int i = 0; i < signages.length; i++) {
            if (signages[i].contains("AfterCare:1")) {
                String d = signages[i] + "s";
                if (d.contains("^AfterCareQty:nulls") || (d.contains("^AfterCareQty:s")) || (d.contains("^AfterCareQty:0s"))) {
                    s = true;
                }
                if ((signages[i].contains("^AfterCareMetres:null")) || (signages[i].contains("^AfterCareMetres:^")) || (signages[i].contains("^AfterCareMetres:0^"))) {
                    s = true;
                }
            }
        }

        String[] signages2 = db.getAllSelectedSignstoUpload(ids, "1").split("~");
        for (int i = 0; i < signages2.length; i++) {
            if (signages2[i].contains("AfterCare:1")) {
                Log.e("loveis", signages2[i]);
                String d = signages2[i] + "s";
                if (d.contains("^AfterCareQty:nulls") || (d.contains("^AfterCareQty:s")) || (d.contains("^AfterCareQty:0s"))) {
                    s = true;
                }
                if ((signages2[i].contains("^AfterCareMetres:null")) || (signages2[i].contains("^AfterCareMetres:^")) || (signages2[i].contains("^AfterCareMetres:0^"))) {
                    s = true;
                }
            }
        }
        return s;
    }

public void redSTocks(String type){

    db.deleteStocks2(mJobObject.getJobID());
    String kits =  "{\n" +
            "\"kits\": [\n" +
            "  {\"type\": \"W-Beam Guardrail\", \"brand\": \"Public Domain\", \"lenght\":\"4m\", \"size\":\"N/A\", \"description\":\"TL3, Semi-Rigid Steel Barrier - Rail, Posts, Blocks, Stiffener, Bolts\", \"category\":\"0\",\"stocklist_type\":\"0\", \"isChecked\":\"0\"},\n" +
            "   {\"type\": \"Thrie-Beam Guardrail\", \"brand\": \"Public Domain\", \"lenght\":\"4m\", \"size\":\"4m\", \"description\":\"TL3, Semi-Rigid Steel Barrier - Rail, Posts, Blocks, Stiffener, Bolts\", \"category\":\"\",\"stocklist_type\":\"0\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \"Modified Thrie-Beam Guardrail\", \"brand\": \"Public Domain\", \"lenght\":\"4m\", \"size\":\"N/A\", \"description\":\"TL4, Semi-Rigid Steel Barrier\", \"category\":\"0\",\"stocklist_type\":\"0\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \"Ezy-Guard Smart Barrier\", \"brand\": \"Ingal Civil\", \"lenght\":\"4m\", \"size\":\"N/A\", \"description\":\"TL3, Semi-Rigid Steel Barrier - Rail, Posts, Cartridge, Bolts\", \"category\":\"0\",\"stocklist_type\":\"0\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \"Ramshield Barrier\", \"brand\": \"Safe Directions\", \"lenght\":\"4m\", \"size\":\"N/A\", \"description\":\"TL3, Semi-Rigid Steel Barrier - Rail, Posts, Cartridge, Bolts\", \"category\":\"0\",\"stocklist_type\":\"0\", \"isChecked\":\"0\"},\n" +
            "\n" +
            "  {\"type\": \"Brifen 4 rope Barrier\", \"brand\": \"Hill and Smith\", \"lenght\":\"N/A\", \"size\":\"N/A\", \"description\":\"TL4, Wire Rope Barrier - Post, Bobbins, Dust Cap, Post Cap, Reflector\", \"category\":\"1\",\"stocklist_type\":\"0\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \"Flexfence  Barrier\", \"brand\": \"Ingal Civil\", \"lenght\":\"N/A\", \"size\":\"N/A\", \"description\":\"TL3 and TL4 Wire Rope Barrier - Post, Spacers, Collar, Dust Cap, Post Cap, Reflector\", \"category\":\"1\",\"stocklist_type\":\"0\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \"Sentryline II 4 Rope Barrier\", \"brand\": \"Australian Construction Supplies\", \"lenght\":\"N/A\", \"size\":\"N/A\", \"description\":\"TL4, Wire Rope Barrier - Post, O-Ring, Post Cap, Reflector\", \"category\":\"1\",\"stocklist_type\":\"0\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \"MELT Terminal\", \"brand\": \"Public Domain\", \"lenght\":\"8m\", \"size\":\"N/A\", \"description\":\"TL3 Guardrail Terminal\", \"category\":\"2\",\"stocklist_type\":\"0\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \"MELT Terminal\", \"brand\": \"Ingal Civil\", \"lenght\":\"8m\", \"size\":\"N/A\", \"description\":\"\", \"category\":\"2\",\"stocklist_type\":\"0\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \"MELT Terminal\", \"brand\": \"Australian Construction Supplies\", \"lenght\":\"8m\", \"size\":\"N/A\", \"description\":\"TL3 Guardrail Terminal\", \"category\":\"2\",\"stocklist_type\":\"0\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \"E2000 Plus Terminal TL3\", \"brand\": \"Ingal Civil\", \"lenght\":\"16m\", \"size\":\"N/A\", \"description\":\"TL3 Guardrail Terminal\", \"category\":\"2\",\"stocklist_type\":\"0\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \"E2000 Plus Terminal TL3\", \"brand\": \"Ingal Civil\", \"lenght\":\"8m\", \"size\":\"N/A\", \"description\":\"TL3 Guardrail Terminal\", \"category\":\"2\",\"stocklist_type\":\"0\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \"X350 Terminal\", \"brand\": \"Australian Construction Supplies\", \"lenght\":\"8m\", \"size\":\"N/A\", \"description\":\"TL3 Guardrail Terminal\", \"category\":\"2\",\"stocklist_type\":\"0\", \"isChecked\":\"0\"},\n" +
            "\n" +
            "  {\"type\": \"Quadguard\", \"brand\": \"Ingal Civil\", \"lenght\":\"various\", \"size\":\"N/A\", \"description\":\"Crash Cushion - various sizes and speed zones available\", \"category\":\"3\",\"stocklist_type\":\"0\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \"Tracc Unit\", \"brand\": \"Ingal Civil\", \"lenght\":\"various\", \"size\":\"N/A\", \"description\":\"Crash Cushion - various sizes and speed zones available\", \"category\":\"3\",\"stocklist_type\":\"0\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \"TAU II\", \"brand\": \"\", \"lenght\":\"various\", \"size\":\"N/A\", \"description\":\"Crash Cushion - various sizes and speed zones available\",\"category\":\"3\",\"stocklist_type\":\"0\", \"isChecked\":\"0\"}\n" +
            "\n" +
            "]\n" +
            "}";

    JSONObject jsonObj = null;
    try {
        jsonObj = new JSONObject(kits);
        JSONArray kitslist = jsonObj.getJSONArray("kits");

        for (int i = 0; i < kitslist.length(); i++) {
            Stocklist_Kit_Model stock = new Stocklist_Kit_Model();
            JSONObject c = kitslist.getJSONObject(i);

            stock.setType(c.getString("type"));
            stock.setBrand(c.getString("brand"));
            stock.setLenght(c.getString("lenght"));
            stock.setSize(c.getString("size"));
            stock.setDescription(c.getString("description"));
            stock.setCategory(c.getString("category"));
            stock.setStocklist_type(c.getString("stocklist_type"));
            Log.e(TAG, "redSTocks: " + kitslist.length());
            db.addStocklist2(stock, "0", mJobObject.getJobID());
        }

    } catch (JSONException e) {
        e.printStackTrace();
    }

    String components = "{\n" +
            "\"kits\": [\n" +
            "  {\"type\": \"W-Beam Guardrail\", \"brand\": \"Public Domain\", \"lenght\":\"4m\", \"size\":\"312x81x4300\", \"description\":\" \", \"category\":\"0\",\"stocklist_type\":\"1\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \"W-Beam Post - Option 1\", \"brand\": \"Public Domain\", \"lenght\":\"1850mm\", \"size\":\" \", \"description\":\"110x150x1850\", \"category\":\"0\",\"stocklist_type\":\"1\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \"W-Beam Post - Option 2\", \"brand\": \"Public Domain\", \"lenght\":\"2150m\", \"size\":\"110x150x2150\", \"description\":\" \", \"category\":\"0\",\"stocklist_type\":\"1\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \"W-Beam Post - Option 2\", \"brand\": \"Public Domain\", \"lenght\":\"2150m\", \"size\":\"110x150x2150\", \"description\":\" \", \"category\":\"0\",\"stocklist_type\":\"1\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \" W-Beam Block Out\", \"brand\": \"Public Domain\", \"lenght\":\" \", \"size\":\"110x150x360\", \"description\":\" \", \"category\":\"0\",\"stocklist_type\":\"1\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \" W-Beam Stiffener\", \"brand\": \"Public Domain\", \"lenght\":\" \", \"size\":\"312x300x81\", \"description\":\" \", \"category\":\"0\",\"stocklist_type\":\"1\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \" Bolt Kit (4m panel)\", \"brand\": \"Public Domain\", \"lenght\":\" \", \"size\":\"N/A\", \"description\":\" \", \"category\":\"0\",\"stocklist_type\":\"1\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \"Thrie-Beam Guardrail\", \"brand\": \"Public Domain\", \"lenght\":\" \", \"size\":\" \", \"description\":\" \", \"category\":\"0\",\"stocklist_type\":\"1\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \"Thrie-Beam Post - Option 1\", \"brand\": \"Public Domain\", \"lenght\":\" \", \"size\":\" \", \"description\":\" \", \"category\":\"0\",\"stocklist_type\":\"1\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \"Thrie-Beam Post - Option 2\", \"brand\": \"Public Domain\", \"lenght\":\" \", \"size\":\" \", \"description\":\" \", \"category\":\"0\",\"stocklist_type\":\"1\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \" Thrie-Beam Block Out\", \"brand\": \"Public Domain\", \"lenght\":\" \", \"size\":\" \", \"description\":\" \", \"category\":\"0\",\"stocklist_type\":\"1\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \" Thrie-Beam Stiffener\", \"brand\": \"Public Domain\", \"lenght\":\" \", \"size\":\" \", \"description\":\" \", \"category\":\"0\",\"stocklist_type\":\"1\", \"isChecked\":\"0\"},\n" +
            "  {\"type\": \" Thrie-Beam Bolt Kit\", \"brand\": \"Public Domain\", \"lenght\":\" \", \"size\":\" \", \"description\":\" \", \"category\":\"0\",\"stocklist_type\":\"1\", \"isChecked\":\"0\"}\n" +
            "]\n" +
            "}";

    JSONObject jsonObj2 = null;
    try {
        jsonObj2 = new JSONObject(components);
        JSONArray kitslist = jsonObj2.getJSONArray("kits");

        for (int i = 0; i < kitslist.length(); i++) {
            Stocklist_Kit_Model stock = new Stocklist_Kit_Model();
            JSONObject c = kitslist.getJSONObject(i);

            stock.setType(c.getString("type"));
            stock.setBrand(c.getString("brand"));
            stock.setLenght(c.getString("lenght"));
            stock.setSize(c.getString("size"));
            stock.setDescription(c.getString("description"));
            stock.setCategory(c.getString("category"));
            stock.setStocklist_type("1");
            Log.e(TAG, "redSTocks: " + kitslist.length());
            db.addStocklist2(stock, "1", mJobObject.getJobID());

        }

    } catch (JSONException e) {
        e.printStackTrace();
    }


}
}


