package com.evolution.egm.Activities;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.evolution.egm.Adapters.AdapterPhotos;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Interface.PhotoInterface;
import com.evolution.egm.Models.Job;
import com.evolution.egm.Models.PhotoItem;
import com.evolution.egm.R;
import com.rey.material.app.Dialog;

import org.parceler.Parcels;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class Photo extends AppCompatActivity implements View.OnClickListener, PhotoInterface {
    Toolbar toolbar;

    private Uri fileUri;
    private RecyclerView mRecyclerView;
    private AdapterPhotos mAdapter;

    private StaggeredGridLayoutManager gaggeredGridLayoutManager;

    private RecyclerView.LayoutManager mLayoutManager;

    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final String IMAGE_DIRECTORY_NAME = "EvolutionImage";
    private static final int SELECT_PHOTO = 120;
    LinearLayout takeapicture, selectgalery;
    PhotoItem items;
    DatabaseHelper db;
    Job mJobObject;
    public ArrayList<PhotoItem> photo = new ArrayList<PhotoItem>();
    TextView textView15;
    String selectedImagePath;
    //ADDED
    String filemanagerstring, imagePath;
    Dialog dialog;
    int column_index;
    Menu deleteshow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        initToolBar();

        takeapicture = (LinearLayout) findViewById(R.id.takeapicture);
        selectgalery = (LinearLayout) findViewById(R.id.selectgalery);

        db = new DatabaseHelper(this);
        mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));

        takeapicture.setOnClickListener(this);
        selectgalery.setOnClickListener(this);

        mRecyclerView = (RecyclerView) findViewById(R.id.photos);
        mLayoutManager = new GridLayoutManager(getApplicationContext(), 3);
        mRecyclerView.setLayoutManager(mLayoutManager);
        dialog = new Dialog(this, R.style.FullHeightDialog);
        textView15 = (TextView) findViewById(R.id.textView15);

        this.invalidateOptionsMenu();
    }

    @Override
    protected void onResume() {
        super.onResume();
        photo = db.getAllPhotos(mJobObject.getJobID());
        if (photo.size() != 0) {
            textView15.setVisibility(textView15.GONE);
            for (int i = 0; i < photo.size(); i++) {
                Log.e("images", photo.get(i).getImage_url() + "");
            }
        }
        mAdapter = new AdapterPhotos(photo, this);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Photos");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case R.id.canceldelete:

                mAdapter.RestArrayList();
                deleteshow.findItem(R.id.deletephoto).setVisible(false);
                deleteshow.findItem(R.id.canceldelete).setVisible(false);

                photo = db.getAllPhotos(mJobObject.getJobID());
                mAdapter = new AdapterPhotos(photo, this);
                mRecyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();

                return true;
            case R.id.deletephoto:

                dialog.show();
                dialog.setCanceledOnTouchOutside(false);
                dialog.setContentView(R.layout.activity_deletephoto);
                dialog.contentMargin(0, 0, 0, -20);

                LinearLayout save = (LinearLayout) dialog.findViewById(R.id.save);
                LinearLayout cancel = (LinearLayout) dialog.findViewById(R.id.cancel);

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        mAdapter.notifyDataSetChanged();
                        mAdapter.RestArrayList();
                        deleteshow.findItem(R.id.deletephoto).setVisible(false);
                        deleteshow.findItem(R.id.canceldelete).setVisible(false);

                        dialog.dismiss();
                    }
                });

                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (int i = 0; i <= mAdapter.getAllSlectedITem().size() - 1; i++) {
                            db.removeImageById(mAdapter.getAllSlectedITem().get(i).toString());
                        }
                        dialogOnDismiss();
                    }
                });

                return true;
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void galleryAddPic() {

        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(mediaStorageDir.getPath());
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    public void dialogOnDismiss() {

        mAdapter.RestArrayList();
        deleteshow.findItem(R.id.deletephoto).setVisible(false);
        deleteshow.findItem(R.id.canceldelete).setVisible(false);

        photo = db.getAllPhotos(mJobObject.getJobID());
        mAdapter = new AdapterPhotos(photo, this);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        dialog.dismiss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // if the result is capturing Image
//        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // successfully captured the image
                // display it in image view
                galleryAddPic();
                previewCapturedImage();
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_PHOTO) {
                Uri selectedImageUri = data.getData();
                filemanagerstring = selectedImageUri.getPath();
                selectedImagePath = getPath(selectedImageUri);
                Log.e("Imagepaty", selectedImagePath);
                String imageurl = imagePath + "";
                Log.e("test", imagePath + "");
                if (!imageurl.equals("null")) {
                    Bitmap bm = BitmapFactory.decodeFile(imagePath);
                    db.addPhoto(mJobObject.getJobID(), imagePath);
                } else {
                    Toast.makeText(Photo.this, " Please Select Photos from the gallery.", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (resultCode == RESULT_CANCELED) {
            // user cancelled Image capture
            Toast.makeText(getApplicationContext(),
                    "User cancelled image select", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    //UPDATED!
    public String getPath(Uri uri) {
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        column_index = cursor
                .getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        imagePath = cursor.getString(column_index);
        return cursor.getString(column_index);
    }

    private void previewCapturedImage() {

        Log.e("tsst", "" + fileUri);

        try {
            db.addPhoto(mJobObject.getJobID(), fileUri.getPath());
            mAdapter.notifyDataSetChanged();
        } catch (NullPointerException e) {
            e.printStackTrace();
            Log.e("tsst", "" + fileUri);
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.takeapicture:

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                // start the image capture Intent
                startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

                break;
            case R.id.selectgalery:
//                Intent photoPickerIntent = new Intent();
//                photoPickerIntent.setType("image/*");
//                photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
//                startActivityForResult(Intent.createChooser(photoPickerIntent,
//                        "Select Picture"), SELECT_PHOTO);
                galleryAddPic();
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, SELECT_PHOTO);

//                Intent intentd = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
//                startActivityForResult(intentd, SELECT_PHOTO);

                break;
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_deletephoto, menu);
        this.deleteshow = menu;

        menu.findItem(R.id.deletephoto).setVisible(false);
        menu.findItem(R.id.canceldelete).setVisible(false);

        return true;
    }

    private static File getOutputMediaFile(int type) {

        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());

        File mediaFile;

        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    private boolean deleteLastPhotoTaken() {

        boolean success = false;
        try {
            File[] images = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "DCIM/Camera").listFiles();
            File latestSavedImage = images[0];
            for (int i = 1; i < images.length; ++i) {
                if (images[i].lastModified() > latestSavedImage.lastModified()) {
                    latestSavedImage = images[i];
                }
            }

            // OR JUST Use  success = latestSavedImage.delete();
            success = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "DCIM/Camera/"
                    + latestSavedImage.getAbsoluteFile()).delete();
            return success;
        } catch (Exception e) {
            e.printStackTrace();
            return success;
        }
    }

    @Override
    public void deletePhoto(String idtoDelete) {
        db.removeImageById(idtoDelete);
        photo = db.getAllPhotos(mJobObject.getJobID());
        mAdapter = new AdapterPhotos(photo, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void viewPhoto(String idtoView) {
        Intent mainIntent;
        mainIntent = new Intent(getApplicationContext(), Photoview.class);
        mainIntent.putExtra("position", idtoView);
        mainIntent.putExtra("data", Parcels.wrap(mJobObject));
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(mainIntent);
    }

    @Override
    public void showHideMenu(boolean isvisible) {
        if (isvisible) {
            deleteshow.findItem(R.id.deletephoto).setVisible(true);
            deleteshow.findItem(R.id.canceldelete).setVisible(true);
        } else {
            deleteshow.findItem(R.id.deletephoto).setVisible(false);
            deleteshow.findItem(R.id.canceldelete).setVisible(false);
        }
    }
}
