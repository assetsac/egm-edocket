package com.evolution.egm.Activities;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.evolution.egm.Adapters.AdapterStocklistComponents;
import com.evolution.egm.Adapters.AdapterStocklistKits;
import com.evolution.egm.Adapters.AdapterStocklistMisc;
import com.evolution.egm.Interface.StocklistViewInterface;
import com.evolution.egm.Models.Job;
import com.evolution.egm.Models.Stocklist_Kit_Model;
import com.evolution.egm.R;
import com.evolution.egm.Utils.DatabaseHelper;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;


public class StocklistActivity extends AppCompatActivity implements View.OnClickListener, StocklistViewInterface {
    Toolbar toolbar;
    LinearLayout kitswrapper, componentswrapper, miscwrapper;
    RadioButton kits, components, misc;
    DatabaseHelper db;

    //Dialog
    public AlertDialog dialog;
    public AlertDialog.Builder builder;
    Job mJobObject;

    //Recycler
    private RecyclerView mRecyclerView_kits;
    private RecyclerView.Adapter mAdapter_kits;
    private RecyclerView.LayoutManager mLayoutManager_kits;

    private RecyclerView mRecyclerView_components;
    private RecyclerView.Adapter mAdapter_components;
    private RecyclerView.LayoutManager mLayoutManager_components;

    private RecyclerView mRecyclerView_misc;
    private RecyclerView.Adapter mAdapter_misc;
    private RecyclerView.LayoutManager mLayoutManager_misc;
    List<String> categories;

    Menu stock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stocklist);
        initToolBar();
        db = new DatabaseHelper(this);
        kitswrapper = (LinearLayout) findViewById(R.id.kitswrapper);
        componentswrapper = (LinearLayout) findViewById(R.id.componentswrapper);
        miscwrapper = (LinearLayout) findViewById(R.id.miscwrapper);

        kits = (RadioButton) findViewById(R.id.kits);
        components = (RadioButton) findViewById(R.id.components);
        misc = (RadioButton) findViewById(R.id.misc);
        mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));
        kits.setOnClickListener(this);
        components.setOnClickListener(this);
        misc.setOnClickListener(this);

        mRecyclerView_kits = (RecyclerView) findViewById(R.id.kits_recycler);
        mLayoutManager_kits = new LinearLayoutManager(this);
        mRecyclerView_kits.setLayoutManager(mLayoutManager_kits);
        mRecyclerView_kits.setHasFixedSize(true);

        mRecyclerView_components = (RecyclerView) findViewById(R.id.components_recycler);
        mLayoutManager_components = new LinearLayoutManager(this);
        mRecyclerView_components.setLayoutManager(mLayoutManager_components);
        mRecyclerView_components.setHasFixedSize(true);

        mRecyclerView_misc = (RecyclerView) findViewById(R.id.misc_recycler);
        mLayoutManager_misc = new LinearLayoutManager(this);
        mRecyclerView_misc.setLayoutManager(mLayoutManager_misc);
        mRecyclerView_misc.setHasFixedSize(true);

        mAdapter_kits = new AdapterStocklistKits(db.getAllKitsStocks(mJobObject.getJobID()), this);
        mRecyclerView_kits.setAdapter(mAdapter_kits);
        iniArray();
        this.invalidateOptionsMenu();

    }

    public void iniArray() {

        categories = new ArrayList<String>();
        categories.add("Guardrail Barriers Kits");
        categories.add("Wire Rope Barriers Kits");
        categories.add("Guardrial Terminal Ends Kits");
        categories.add("Crash Cushions");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_stocklist, menu);
        this.stock = menu;
        menu.findItem(R.id.addstock).setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.addstock:
                LayoutInflater inflater2 = getLayoutInflater();
                dialog_addstokclist(R.layout.daiglog_addstocklist, inflater2);
                break;
            case android.R.id.home:
                finish();
                overridePendingTransition(0, 0);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Stocklist");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.kits:
                stock.findItem(R.id.addstock).setVisible(false);
                kitswrapper.setVisibility(kitswrapper.VISIBLE);
                componentswrapper.setVisibility(componentswrapper.GONE);
                miscwrapper.setVisibility(miscwrapper.GONE);
                mAdapter_kits = new AdapterStocklistKits(db.getAllKitsStocks(mJobObject.getJobID()), this);
                mRecyclerView_kits.setAdapter(mAdapter_kits);
                break;
            case R.id.components:
                stock.findItem(R.id.addstock).setVisible(false);
                kitswrapper.setVisibility(kitswrapper.GONE);
                componentswrapper.setVisibility(componentswrapper.VISIBLE);
                miscwrapper.setVisibility(miscwrapper.GONE);
                mAdapter_components = new AdapterStocklistComponents(db.getAllComponentsStocks(mJobObject.getJobID()), this);
                mRecyclerView_components.setAdapter(mAdapter_components);

                break;
            case R.id.misc:
                stock.findItem(R.id.addstock).setVisible(true);
                kitswrapper.setVisibility(kitswrapper.GONE);
                componentswrapper.setVisibility(componentswrapper.GONE);
                miscwrapper.setVisibility(miscwrapper.VISIBLE);
                miscdata();
                break;
        }
    }

public void miscdata(){
    mAdapter_misc = new AdapterStocklistMisc(db.getAllMiscStocks(mJobObject.getJobID()), this);
    mRecyclerView_misc.setAdapter(mAdapter_misc);

}
    public void dialog_viewstokclist(int layout, LayoutInflater inflater, Stocklist_Kit_Model stock) {
        builder = new AlertDialog.Builder(this);
        dialog = builder.create();
        View dialogView = inflater.inflate(layout, null);
        dialog.setView(dialogView);
        TextView vcategory = (TextView) dialogView.findViewById(R.id.vcategory);
        TextView vtype = (TextView) dialogView.findViewById(R.id.vtype);
        TextView vbrand = (TextView) dialogView.findViewById(R.id.vbrand);
        TextView vlenght = (TextView) dialogView.findViewById(R.id.vlenght);
        TextView vsize = (TextView) dialogView.findViewById(R.id.vsize);
        TextView vdescription = (TextView) dialogView.findViewById(R.id.vdescription);

        vtype.setText(stock.getType());
        vbrand.setText(stock.getBrand());
        vlenght.setText(stock.getLenght());
        vsize.setText(stock.getSize());
        vdescription.setText(stock.getDescription());

        vcategory.setText(categories.get(Integer.parseInt(stock.getCategory())));
        Log.e("datass", "dialog_viewstokclist: " + stock.getCategory());
//        vcategory.setText(stock.getCategory());

        Button close = (Button) dialogView.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void dialog_addstokclist(int layout, LayoutInflater inflater) {
        builder = new AlertDialog.Builder(this);
        dialog = builder.create();
        View dialogView = inflater.inflate(layout, null);
        dialog.setView(dialogView);

        final EditText s_type = (EditText) dialogView.findViewById(R.id.s_type);
        final EditText s_brand = (EditText) dialogView.findViewById(R.id.s_brand);
        final EditText s_lenght = (EditText) dialogView.findViewById(R.id.s_lenght);
        final EditText s_size = (EditText) dialogView.findViewById(R.id.s_size);
        final EditText s_descritiption = (EditText) dialogView.findViewById(R.id.s_descritiption);
        final Spinner categorys = (Spinner) dialogView.findViewById(R.id.selectCategory);
        initSpinner(categorys);

        Button close = (Button) dialogView.findViewById(R.id.close);
        Button save = (Button) dialogView.findViewById(R.id.save);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (s_type.getText().toString().isEmpty() || s_brand.getText().toString().isEmpty() || s_lenght.getText().toString().isEmpty() || s_size.getText().toString().isEmpty() || s_descritiption.getText().toString().isEmpty()) {
                    Toast.makeText(StocklistActivity.this, "Please Complete all fields.", Toast.LENGTH_SHORT).show();
                } else {
                    Stocklist_Kit_Model stock = new Stocklist_Kit_Model();
                    stock.setType(s_type.getText().toString());
                    stock.setBrand(s_brand.getText().toString());
                    stock.setLenght(s_lenght.getText().toString());
                    stock.setSize(s_size.getText().toString());
                    stock.setCategory(categorys.getSelectedItemPosition() + "");
                    stock.setDescription(s_descritiption.getText().toString());
                    db.addStocklist(stock, "2", mJobObject.getJobID());
                    miscdata();
                    dialog.dismiss();
                }
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void viewstocks(Stocklist_Kit_Model stock) {
        LayoutInflater inflater2 = getLayoutInflater();
        dialog_viewstokclist(R.layout.daiglog_addstockview, inflater2, stock);
    }

    @Override
    public void deleteStocks(final Stocklist_Kit_Model stock) {

        Dialog.Builder builder2 = null;
        builder2 = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                db.deleteStocks(stock.get_id());
                miscdata();
                super.onPositiveActionClicked(fragment);
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                super.onNegativeActionClicked(fragment);
            }
        };

        ((SimpleDialog.Builder) builder2).message("Are you sure?")
                .title("Deleting Stock " + stock.getType())
                .positiveAction("YES")
                .negativeAction("CANCEL");
        DialogFragment fragment = DialogFragment.newInstance(builder2);
        fragment.show(getSupportFragmentManager(), null);
    }

    @Override
    public void editStocks(Stocklist_Kit_Model stock) {
        LayoutInflater inflater2 = getLayoutInflater();
        editStocks(R.layout.daiglog_addstocklist, inflater2, stock);
    }

    public void editStocks(int layout, LayoutInflater inflater, final Stocklist_Kit_Model stocksd ) {
        builder = new AlertDialog.Builder(this);
        dialog = builder.create();
        View dialogView = inflater.inflate(layout, null);
        dialog.setView(dialogView);

        final EditText s_type = (EditText) dialogView.findViewById(R.id.s_type);
        final EditText s_brand = (EditText) dialogView.findViewById(R.id.s_brand);
        final EditText s_lenght = (EditText) dialogView.findViewById(R.id.s_lenght);
        final EditText s_size = (EditText) dialogView.findViewById(R.id.s_size);
        final EditText s_descritiption = (EditText) dialogView.findViewById(R.id.s_descritiption);
        final Spinner categorys = (Spinner) dialogView.findViewById(R.id.selectCategory);

        initSpinner(categorys);

        s_type.setText(stocksd.getType());
        s_brand.setText(stocksd.getBrand());
        s_lenght.setText(stocksd.getLenght());
        s_size.setText(stocksd.getSize());
        s_descritiption.setText(stocksd.getDescription());

        Button close = (Button) dialogView.findViewById(R.id.close);
        Button save = (Button) dialogView.findViewById(R.id.save);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (s_type.getText().toString().isEmpty() || s_brand.getText().toString().isEmpty() || s_lenght.getText().toString().isEmpty() || s_size.getText().toString().isEmpty() || s_descritiption.getText().toString().isEmpty()) {
                    Toast.makeText(StocklistActivity.this, "Please Complete all fields.", Toast.LENGTH_SHORT).show();
                } else {
                    Stocklist_Kit_Model stock = new Stocklist_Kit_Model();
                    stock.setType(s_type.getText().toString());
                    stock.setBrand(s_brand.getText().toString());
                    stock.setLenght(s_lenght.getText().toString());
                    stock.setSize(s_size.getText().toString());
                    stock.setCategory(categorys.getSelectedItemPosition() + "");
                    stock.setDescription(s_descritiption.getText().toString());
                    db.updateStocks(stock, stocksd.get_id());
                    miscdata();
                    dialog.dismiss();
                }
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    public void initSpinner(Spinner spinner) {


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // On selecting a spinner item
                String item = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinnerlocation, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.spinnerlocation);
        spinner.setAdapter(dataAdapter);

    }

}
