package com.evolution.egm.Activities.signs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.evolution.egm.Utils.CreateJobSharedPreference;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Utils.DateHelper;
import com.evolution.egm.Models.Job;
import com.evolution.egm.R;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WASignage extends AppCompatActivity implements View.OnTouchListener, View.OnClickListener {

    Toolbar toolbar;
    RadioButton slowlane, fastlane;
    LinearLayout slowlanel, fastlanel;
    MaterialEditText cwevolution4, cwlocallandmark4, TimeErected, TimeCollected, TimeChecked1, TimeChecked2, TimeChecked3, TimeChecked4;
    Spinner spinner;
    String Time1, Time2, Time3, Time4, Time5, Time6;
    String TimeErected2, TimeCollected2, TimeChecked12, TimeChecked22, TimeChecked32, TimeChecked42;
    DateHelper date;
    DatabaseHelper db;
    Dialog dialog;
    DatePicker datepicker;
    TimePicker timepicker;
    List<String> categories;
    final CharSequence cs1 = "-";
    Dialog.Builder builder2 = null;
    Job mJobObject;
    CreateJobSharedPreference shared;

    EditText w1, w2, w3, w4, w5, w6, w7, w8, w9, w10, w11, w12, w13, w14, w15, w16, w17, w18, w19, w20, w21, w22, w23, w24, w25, w26, w27, w28, w29, w30, w31, w32, w33, w34, w35, w36, w37, w38, w39, w40, w41, w42, w43, w44, w45, w46, w47, w48, w49, w50, w51, w52, w53, w54, w55, w56, w57, w58, w59, w60, w61, w62, w63;
    EditText wa1, wa2, wa3, wa4, wa5, wa6, wa7, wa8, wa9, wa10, wa11, wa12, wa13, wa14, wa15, wa16, wa17, wa18, wa19, wa20, wa21, wa22, wa23, wa24, wa25, wa26, wa27, wa28, wa29, wa30, wa31, wa32, wa33, wa34, wa35, wa36, wa37, wa38, wa39, wa40, wa41, wa42, wa43, wa44, wa45, wa46, wa47, wa48, wa49, wa50, wa51, wa52, wa53, wa54, wa55, wa56, wa57, wa58, wa59, wa60, wa61, wa62, wa63;

    String aftercares = "null";
    RadioButton afteryes, afterno;
    RadioGroup aftercare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wasignage);
        initToolBar();

        date = new DateHelper();
        db = new DatabaseHelper(this);
        dialog = new Dialog(this, R.style.FullHeightDialog);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            mJobObject = Parcels.unwrap(getIntent().getExtras().getParcelable("data"));
        }

        shared = new CreateJobSharedPreference(this);

        initViews();
        initSpinner();
        populateData();
    }

    public void initSpinner() {
        // Spinner click listener
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // On selecting a spinner item
                String item = parent.getItemAtPosition(position).toString();

                // Showing selected spinner item
//                Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Spinner Drop down elements
        categories = new ArrayList<String>();
        categories.add("North");
        categories.add("South");
        categories.add("East");
        categories.add("West");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinnerlocation, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.spinnerlocation);

        spinner.setAdapter(dataAdapter);

        w1 = (EditText) findViewById(R.id.w1);
        w2 = (EditText) findViewById(R.id.w2);
        w3 = (EditText) findViewById(R.id.w3);
        w4 = (EditText) findViewById(R.id.w4);
        w5 = (EditText) findViewById(R.id.w5);
        w6 = (EditText) findViewById(R.id.w6);
        w7 = (EditText) findViewById(R.id.w7);
        w8 = (EditText) findViewById(R.id.w8);
        w9 = (EditText) findViewById(R.id.w9);
        w10 = (EditText) findViewById(R.id.w10);
        w11 = (EditText) findViewById(R.id.w11);
        w12 = (EditText) findViewById(R.id.w12);
        w13 = (EditText) findViewById(R.id.w13);
        w14 = (EditText) findViewById(R.id.w14);
        w15 = (EditText) findViewById(R.id.w15);
        w16 = (EditText) findViewById(R.id.w16);
        w17 = (EditText) findViewById(R.id.w17);
        w18 = (EditText) findViewById(R.id.w18);
        w19 = (EditText) findViewById(R.id.w19);
        w20 = (EditText) findViewById(R.id.w20);
        w21 = (EditText) findViewById(R.id.w21);
        w22 = (EditText) findViewById(R.id.w22);
        w23 = (EditText) findViewById(R.id.w23);
        w24 = (EditText) findViewById(R.id.w24);
        w25 = (EditText) findViewById(R.id.w25);
        w26 = (EditText) findViewById(R.id.w26);
        w27 = (EditText) findViewById(R.id.w27);
        w28 = (EditText) findViewById(R.id.w28);
        w29 = (EditText) findViewById(R.id.w29);
        w30 = (EditText) findViewById(R.id.w30);
        w31 = (EditText) findViewById(R.id.w31);
        w32 = (EditText) findViewById(R.id.w32);
        w33 = (EditText) findViewById(R.id.w33);
        w34 = (EditText) findViewById(R.id.w34);
        w35 = (EditText) findViewById(R.id.w35);
        w36 = (EditText) findViewById(R.id.w36);
        w37 = (EditText) findViewById(R.id.w37);
        w38 = (EditText) findViewById(R.id.w38);
        w39 = (EditText) findViewById(R.id.w39);
        w40 = (EditText) findViewById(R.id.w40);
        w41 = (EditText) findViewById(R.id.w41);
        w42 = (EditText) findViewById(R.id.w42);
        w43 = (EditText) findViewById(R.id.w43);
        w44 = (EditText) findViewById(R.id.w44);
        w45 = (EditText) findViewById(R.id.w45);
        w46 = (EditText) findViewById(R.id.w46);
        w47 = (EditText) findViewById(R.id.w47);
        w48 = (EditText) findViewById(R.id.w48);
        w49 = (EditText) findViewById(R.id.w49);
        w50 = (EditText) findViewById(R.id.w50);
        w51 = (EditText) findViewById(R.id.w51);
        w52 = (EditText) findViewById(R.id.w52);
        w53 = (EditText) findViewById(R.id.w53);
        w54 = (EditText) findViewById(R.id.w54);
        w55 = (EditText) findViewById(R.id.w55);
        w56 = (EditText) findViewById(R.id.w56);
        w57 = (EditText) findViewById(R.id.w57);
        w58 = (EditText) findViewById(R.id.w58);
        w59 = (EditText) findViewById(R.id.w59);
        w60 = (EditText) findViewById(R.id.w60);
        w61 = (EditText) findViewById(R.id.w61);
        w62 = (EditText) findViewById(R.id.w62);
        w63 = (EditText) findViewById(R.id.w63);

        wa1 = (EditText) findViewById(R.id.wa1);
        wa2 = (EditText) findViewById(R.id.wa2);
        wa3 = (EditText) findViewById(R.id.wa3);
        wa4 = (EditText) findViewById(R.id.wa4);
        wa5 = (EditText) findViewById(R.id.wa5);
        wa6 = (EditText) findViewById(R.id.wa6);
        wa7 = (EditText) findViewById(R.id.wa7);
        wa8 = (EditText) findViewById(R.id.wa8);
        wa9 = (EditText) findViewById(R.id.wa9);
        wa10 = (EditText) findViewById(R.id.wa10);
        wa11 = (EditText) findViewById(R.id.wa11);
        wa12 = (EditText) findViewById(R.id.wa12);
        wa13 = (EditText) findViewById(R.id.wa13);
        wa14 = (EditText) findViewById(R.id.wa14);
        wa15 = (EditText) findViewById(R.id.wa15);
        wa16 = (EditText) findViewById(R.id.wa16);
        wa17 = (EditText) findViewById(R.id.wa17);
        wa18 = (EditText) findViewById(R.id.wa18);
        wa19 = (EditText) findViewById(R.id.wa19);
        wa20 = (EditText) findViewById(R.id.wa20);
        wa21 = (EditText) findViewById(R.id.wa21);
        wa22 = (EditText) findViewById(R.id.wa22);
        wa23 = (EditText) findViewById(R.id.wa23);
        wa24 = (EditText) findViewById(R.id.wa24);
        wa25 = (EditText) findViewById(R.id.wa25);
        wa26 = (EditText) findViewById(R.id.wa26);
        wa27 = (EditText) findViewById(R.id.wa27);
        wa28 = (EditText) findViewById(R.id.wa28);
        wa29 = (EditText) findViewById(R.id.wa29);
        wa30 = (EditText) findViewById(R.id.wa30);
        wa31 = (EditText) findViewById(R.id.wa31);
        wa32 = (EditText) findViewById(R.id.wa32);
        wa33 = (EditText) findViewById(R.id.wa33);
        wa34 = (EditText) findViewById(R.id.wa34);
        wa35 = (EditText) findViewById(R.id.wa35);
        wa36 = (EditText) findViewById(R.id.wa36);
        wa37 = (EditText) findViewById(R.id.wa37);
        wa38 = (EditText) findViewById(R.id.wa38);
        wa39 = (EditText) findViewById(R.id.wa39);
        wa40 = (EditText) findViewById(R.id.wa40);
        wa41 = (EditText) findViewById(R.id.wa41);
        wa42 = (EditText) findViewById(R.id.wa42);
        wa43 = (EditText) findViewById(R.id.wa43);
        wa44 = (EditText) findViewById(R.id.wa44);
        wa45 = (EditText) findViewById(R.id.wa45);
        wa46 = (EditText) findViewById(R.id.wa46);
        wa47 = (EditText) findViewById(R.id.wa47);
        wa48 = (EditText) findViewById(R.id.wa48);
        wa49 = (EditText) findViewById(R.id.wa49);
        wa50 = (EditText) findViewById(R.id.wa50);
        wa51 = (EditText) findViewById(R.id.wa51);
        wa52 = (EditText) findViewById(R.id.wa52);
        wa53 = (EditText) findViewById(R.id.wa53);
        wa54 = (EditText) findViewById(R.id.wa54);
        wa55 = (EditText) findViewById(R.id.wa55);
        wa56 = (EditText) findViewById(R.id.wa56);
        wa57 = (EditText) findViewById(R.id.wa57);
        wa58 = (EditText) findViewById(R.id.wa58);
        wa59 = (EditText) findViewById(R.id.wa59);
        wa60 = (EditText) findViewById(R.id.wa60);
        wa61 = (EditText) findViewById(R.id.wa61);
        wa62 = (EditText) findViewById(R.id.wa62);
        wa63 = (EditText) findViewById(R.id.wa63);

    }

    public void initViews() {

        TimeErected = (MaterialEditText) findViewById(R.id.TimeErected);
        TimeCollected = (MaterialEditText) findViewById(R.id.TimeCollected);
        TimeChecked1 = (MaterialEditText) findViewById(R.id.TimeChecked1);
        TimeChecked2 = (MaterialEditText) findViewById(R.id.TimeChecked2);
        TimeChecked3 = (MaterialEditText) findViewById(R.id.TimeChecked3);
        TimeChecked4 = (MaterialEditText) findViewById(R.id.TimeChecked4);

        TimeErected.setOnTouchListener(this);
        TimeCollected.setOnTouchListener(this);
        TimeChecked1.setOnTouchListener(this);
        TimeChecked2.setOnTouchListener(this);
        TimeChecked3.setOnTouchListener(this);
        TimeChecked4.setOnTouchListener(this);

        spinner = (Spinner) findViewById(R.id.cwdirectionlist4);

        slowlane = (RadioButton) findViewById(R.id.slowlane);
        fastlane = (RadioButton) findViewById(R.id.fastlane);

        slowlanel = (LinearLayout) findViewById(R.id.slowlanel);
        fastlanel = (LinearLayout) findViewById(R.id.fastlanel);

        cwevolution4 = (MaterialEditText) findViewById(R.id.cwevolution4);
        cwlocallandmark4 = (MaterialEditText) findViewById(R.id.cwlocallandmark4);

        slowlane.setOnClickListener(this);
        fastlane.setOnClickListener(this);



        afteryes = (RadioButton) findViewById(R.id.afteryes);
        afterno = (RadioButton) findViewById(R.id.afterno);
        aftercare = (RadioGroup) findViewById(R.id.aftercarer);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_qldsignages, menu);
        return true;
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Signage Audit");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.saveqld:
//                if (checkifEmpty(cwevolution4, "Rego No. is required.")) {
//                } else {
//                    if (checkifEmpty(cwlocallandmark4, "Local Landmark is required.")) {
//                    } else {
//                        if (checkifEmpty(TimeErected, "Time Erected is required.")) {
//                        } else {
//                            if (checkifEmpty(TimeChecked1, "Time Collected is required.")) {
//                            } else {
//                                if (checkifEmpty(TimeChecked2, "Time Checked is required.")) {
//                                } else {
//                                    if (checkifEmpty(TimeChecked3, "Time Checked is required.")) {
//                                    } else {
//                                        if (checkifEmpty(TimeChecked4, "Time Checked is required.")) {
//                                        } else {
//                                            if (checkifEmpty(TimeCollected, "Time Checked is required.")) {
//                                            } else {
                saveSings();
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
                break;
            case android.R.id.home:
                finish();
                overridePendingTransition(0, 0);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean checkifEmpty(MaterialEditText isEmpty, String messageError) {
        boolean temp = false;
        if (isEmpty.getText().toString().equals("")) {
            temp = true;
            isEmpty.setError(messageError);
        }
        return temp;
    }

    public void timedate(final int to, String whoClicked) {

        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.activity_timesheetpicker);

        datepicker = (DatePicker) dialog.findViewById(R.id.datePicker);
        datepicker.setVisibility(datepicker.GONE);
        timepicker = (TimePicker) dialog.findViewById(R.id.timePicker);

        LinearLayout save = (LinearLayout) dialog.findViewById(R.id.save);
        LinearLayout cancel = (LinearLayout) dialog.findViewById(R.id.cancel);
        TextView title = (TextView) dialog.findViewById(R.id.title);
        title.setText(whoClicked);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (to) {
                    case 1: {
                        TimeErected.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeErected2 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                    case 2: {
                        TimeCollected.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeCollected2 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                    case 3: {
                        TimeChecked1.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeChecked12 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                    case 4: {
                        TimeChecked2.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeChecked22 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                    case 5: {
                        TimeChecked3.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeChecked32 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                    case 6: {
                        TimeChecked4.setText(date.miliSecondConverter(generateMili(datepicker, timepicker) + "", "HH:mm a"));
                        TimeChecked42 = generateMili(datepicker, timepicker) + "";
                    }
                    break;
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    public boolean checkifhave(String toValidate) {
        boolean temp = false;
        boolean ishave = toValidate.contains(cs1);
        if (ishave) {
            temp = true;
        }
        return temp;
    }


    public String getTimeDifference(String first, String second) {
        long diffMinutes = 0;
        long diffHours = 0;
        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(first);
            d2 = format.parse(second);
            long diff = d2.getTime() - d1.getTime();
            diffMinutes = diff / (60 * 1000) % 60;
            diffHours = diff / (60 * 60 * 1000);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return diffHours + "h  " + diffMinutes + "m";
    }


    public Long generateMili(DatePicker datem, TimePicker timem) {
        int day = datem.getDayOfMonth();
        int month = datem.getMonth() + 1;
        int year = datem.getYear();
        String returns = day + " " + month + " " + year + " " + timem.getCurrentHour() + ":" + timem.getCurrentMinute() + "";
        return Long.parseLong(convertDate(returns));
    }

    public String convertDate(String date) {
        String temp = "";
        long timeInMilliseconds = 0;
        String givenDateString = date;
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
        try {
            Date mDate = sdf.parse(givenDateString);
            timeInMilliseconds = mDate.getTime();
            System.out.println("Date in milli :: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds + "";
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.TimeErected: {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    initTimepicker(1);
                }
                return true;
            }
            case R.id.TimeCollected:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeChecked4.getText().toString().equals("")) {
                        Toast.makeText(WASignage.this, "Please add Time Checked.", Toast.LENGTH_SHORT).show();
                    } else {
                        initTimepicker(2);
                    }
                }
                return true;
            case R.id.TimeChecked1:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeErected.getText().toString().equals("")) {
                        Toast.makeText(WASignage.this, "Please add Time Collected.", Toast.LENGTH_SHORT).show();
                    } else {
                        initTimepicker(3);
                    }
                }
                return true;
            case R.id.TimeChecked2:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeChecked1.getText().toString().equals("")) {
                        Toast.makeText(WASignage.this, "Please add Time Checked.", Toast.LENGTH_SHORT).show();
                    } else {
                        initTimepicker(4);
                    }
                }
                return true;
            case R.id.TimeChecked3:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeChecked2.getText().toString().equals("")) {
                        Toast.makeText(WASignage.this, "Please add Time Checked.", Toast.LENGTH_SHORT).show();
                    } else {
                        initTimepicker(5);
                    }
                }
                return true;
            case R.id.TimeChecked4:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (TimeChecked3.getText().toString().equals("")) {
                        Toast.makeText(WASignage.this, "Please add Time Checked.", Toast.LENGTH_SHORT).show();
                    } else {
                        initTimepicker(6);
                    }
                }
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.slowlane:
                slowlanel.setVisibility(slowlanel.VISIBLE);
                fastlanel.setVisibility(slowlanel.GONE);
                break;
            case R.id.fastlane:
                slowlanel.setVisibility(slowlanel.GONE);
                fastlanel.setVisibility(slowlanel.VISIBLE);
                break;
        }
    }


    public static String hourConverter(String dime) {
        Log.e("tsetses", dime);

        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm:ss a");
        Date date = null;
        try {
            date = parseFormat.parse(dime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return displayFormat.format(date) + "";
    }

    public void initTimepicker(final int to) {
        builder2 = new TimePickerDialog.Builder(6, 00) {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                final TimePickerDialog dialogr = (TimePickerDialog) fragment.getDialog();
                switch (to) {
                    case 1:
                        if (!TimeChecked1.getText().toString().equals("")) {
                            if (checkifhave(getTimeDifference(dialogr.getFormattedTime(DateFormat.getDateTimeInstance()), TimeErected2))) {
                                Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                            } else {
                                TimeErected.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                                TimeErected2 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());
                                Time1 = convertDate(TimeErected2);
                            }
                        } else {
                            TimeErected.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                            TimeErected2 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());
                            Time1 = convertDate(TimeErected2);
                        }
                        break;
                    case 3:
                        TimeChecked12 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());

                        if (checkifhave(getTimeDifference(TimeErected2, TimeChecked12))) {
                            TimeChecked12 = "";
                            Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                        } else {
                            Time2 = convertDate(TimeChecked12);
                            TimeChecked1.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                        }
                        break;
                    case 4:
                        TimeChecked22 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());
                        if (checkifhave(getTimeDifference(TimeChecked12, TimeChecked22))) {
                            TimeChecked22 = "";
                            Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                        } else {
                            Time3 = convertDate(TimeChecked22);
                            TimeChecked2.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                        }
                        break;
                    case 5:
                        TimeChecked32 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());
                        if (checkifhave(getTimeDifference(TimeChecked22, TimeChecked32))) {
                            TimeChecked32 = "";
                            Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                        } else {
                            Time4 = convertDate(TimeChecked32);
                            TimeChecked3.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                        }
                        break;
                    case 6:
                        TimeChecked42 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());

                        if (checkifhave(getTimeDifference(TimeChecked32, TimeChecked42))) {
                            TimeChecked42 = "";
                            Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                        } else {
                            Time5 = convertDate(TimeChecked42);
                            TimeChecked4.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                        }
                        break;
                    case 2:
                        TimeCollected2 = dialogr.getFormattedTime(DateFormat.getDateTimeInstance());

                        if (checkifhave(getTimeDifference(TimeChecked42, TimeCollected2))) {
                            TimeCollected2 = "";
                            Toast.makeText(getApplicationContext(), "Invalid Time.", Toast.LENGTH_SHORT).show();
                        } else {
                            Time6 = convertDate(TimeCollected2);
                            TimeCollected.setText(hourConverter(dialogr.getFormattedTime(DateFormat.getTimeInstance())));
                        }
                        break;
                }

                super.onPositiveActionClicked(fragment);
                super.onNegativeActionClicked(fragment);

            }
        };

        builder2.positiveAction("OK")
                .negativeAction("CANCEL");

        DialogFragment fragment2 = DialogFragment.newInstance(builder2);
        fragment2.show(getSupportFragmentManager(), null);

    }
    public String getcontetn(String gets, int position) {
        String getz = "";
        String[] splitters = gets.split("~");

        String abc = splitters[position].replace("^", "#");
        String[] splitter = abc.split("#");

        if (splitter.length >= 2) {
            getz = splitter[1];
        } else {
            getz = "";
        }

        return getz.replace("Metres:", "");
    }

    public void saveSings() {





        int radioButtonID2 = aftercare.getCheckedRadioButtonId();
        RadioButton rb2 = (RadioButton) aftercare.findViewById(radioButtonID2);

        if (aftercare.getCheckedRadioButtonId() != -1) {
            aftercares = aftercare.indexOfChild(rb2) + "";
        }



        String re = "[" +
                "{" +
                "\"JobID\": \"" + mJobObject.getJobID() + "\"," +
                "\"RegoNo\": \"" + cwevolution4.getText().toString() + "\"," +
                "\"Landmark\": \"" + cwlocallandmark4.getText().toString() + "\"," +
                "\"CarriageWay\": \"1\"," +
                "\"Direction\": \"" + spinner.getSelectedItemPosition() + "\"," +
                "    \"TimeErected\": \"" + Time1 + "\",\n" +
                "    \"TimeCollected\": \"" + Time6 + "\",\n" +
                "    \"TimeChecked1\": \"" + Time2 + "\",\n" +
                "    \"TimeChecked2\": \"" + Time3 + "\",\n" +
                "    \"TimeChecked3\": \"" + Time4 + "\",\n" +
                "    \"TimeChecked4\": \"" + Time5 + "\",\n" +
                "    \"aftercare\": \"" + aftercares + "\",\n" +
                "    \"AuditSigns\": \"SignID:1^Metres:" + w1.getText() + "" +
                "~SignID:2^Metres:" + w2.getText().toString() + "~SignID:3^Metres:" + w3.getText() + "~SignID:4^Metres:" + w4.getText() + "~SignID:5^Metres:" + w5.getText() + "~" +
                "SignID:6^Metres:" + w6.getText() + "~SignID:7^Metres:" + w7.getText() + "~SignID:8^Metres:" + w8.getText() + "~SignID:9^Metres:" + w9.getText() + "~" +
                "SignID:10^Metres:" + w10.getText() + "~SignID:11^Metres:" + w11.getText() + "~SignID:12^Metres:" + w12.getText() + "~SignID:13^Metres:" + w13.getText() + "~" +
                "SignID:14^Metres:" + w14.getText() + "~SignID:15^Metres:" + w15.getText() + "~SignID:16^Metres:" + w16.getText() + "~SignID:17^Metres:" + w17.getText() + "~" +
                "SignID:18^Metres:" + w18.getText() + "~SignID:19^Metres:" + w19.getText() + "~SignID:20^Metres:" + w20.getText() + "~SignID:21^Metres:" + w21.getText() + "~" +
                "SignID:22^Metres:" + w22.getText() + "~SignID:23^Metres:" + w23.getText() + "~SignID:24^Metres:" + w24.getText() + "~SignID:25^Metres:" + w25.getText() + "~" +
                "SignID:26^Metres:" + w26.getText() + "~SignID:27^Metres:" + w27.getText() + "~SignID:28^Metres:" + w28.getText() + "~SignID:29^Metres:" + w29.getText() + "~" +
                "SignID:30^Metres:" + w30.getText() + "~SignID:31^Metres:" + w31.getText() + "~SignID:32^Metres:" + w32.getText() + "~SignID:33^Metres:" + w33.getText() + "~" +
                "SignID:34^Metres:" + w34.getText() + "~SignID:35^Metres:" + w35.getText() + "~SignID:36^Metres:" + w36.getText() + "~SignID:37^Metres:" + w37.getText() + "~" +
                "SignID:38^Metres:" + w38.getText() + "~SignID:39^Metres:" + w39.getText() + "~SignID:40^Metres:" + w40.getText() + "~SignID:41^Metres:" + w41.getText() + "~" +
                "SignID:42^Metres:" + w42.getText() + "~SignID:43^Metres:" + w43.getText() + "~SignID:43^Metres:" + w44.getText() + "~SignID:43^Metres:" + w45.getText() +
                "~SignID:43^Metres:" + w46.getText() + "~SignID:43^Metres:" + w47.getText() + "~SignID:43^Metres:" + w48.getText() + "~SignID:43^Metres:" + w49.getText() +
                "~SignID:43^Metres:" + w50.getText() + "~SignID:43^Metres:" + w51.getText() + "~SignID:43^Metres:" + w52.getText() + "~SignID:43^Metres:" + w53.getText() +
                "~SignID:43^Metres:" + w54.getText() + "~SignID:43^Metres:" + w55.getText() + "~SignID:43^Metres:" + w56.getText() + "~SignID:43^Metres:" + w57.getText() +
                "~SignID:43^Metres:" + w58.getText() + "~SignID:43^Metres:" + w59.getText() + "~SignID:43^Metres:" + w60.getText() + "~SignID:43^Metres:" + w61.getText() +
                "~SignID:43^Metres:" + w62.getText() + "~SignID:43^Metres:" + w63.getText() + "~SignID:43^Metres:" +
                "\"}," + "{" +
                "\"JobID\": \"" + mJobObject.getJobID() + "\",\n" +
                "\"RegoNo\": \"" + cwevolution4.getText().toString() + "\",\n" +
                "\"Landmark\": \"" + cwlocallandmark4.getText().toString() + "\",\n" +
                "\"CarriageWay\":\"2\"," +
                "\"Direction\": \"" + spinner.getSelectedItemPosition() + "\",\n" +
                "    \"TimeErected\": \"" + Time1 + "\",\n" +
                "    \"TimeCollected\": \"" + Time6 + "\",\n" +
                "    \"TimeChecked1\": \"" + Time2 + "\",\n" +
                "    \"TimeChecked2\": \"" + Time3 + "\",\n" +
                "    \"TimeChecked3\": \"" + Time4 + "\",\n" +
                "    \"TimeChecked4\": \"" + Time5 + "\",\n" +
                "    \"aftercare\": \"" + aftercares + "\",\n" +
                "    \"AuditSigns\": \"SignID:1^Metres:" + wa1.getText() + "" +
                "~SignID:2^Metres:" + wa2.getText().toString() + "~SignID:3^Metres:" + wa3.getText() + "~SignID:4^Metres:" + wa4.getText() + "~SignID:5^Metres:" + wa5.getText() + "~" +
                "SignID:6^Metres:" + wa6.getText() + "~SignID:7^Metres:" + wa7.getText() + "~SignID:8^Metres:" + wa8.getText() + "~SignID:9^Metres:" + wa9.getText() + "~" +
                "SignID:10^Metres:" + wa10.getText() + "~SignID:11^Metres:" + wa11.getText() + "~SignID:12^Metres:" + wa12.getText() + "~SignID:13^Metres:" + wa13.getText() + "~" +
                "SignID:14^Metres:" + wa14.getText() + "~SignID:15^Metres:" + wa15.getText() + "~SignID:16^Metres:" + wa16.getText() + "~SignID:17^Metres:" + wa17.getText() + "~" +
                "SignID:18^Metres:" + wa18.getText() + "~SignID:19^Metres:" + wa19.getText() + "~SignID:20^Metres:" + wa20.getText() + "~SignID:21^Metres:" + wa21.getText() + "~" +
                "SignID:22^Metres:" + wa22.getText() + "~SignID:23^Metres:" + wa23.getText() + "~SignID:24^Metres:" + wa24.getText() + "~SignID:25^Metres:" + wa25.getText() + "~" +
                "SignID:26^Metres:" + wa26.getText() + "~SignID:27^Metres:" + wa27.getText() + "~SignID:28^Metres:" + wa28.getText() + "~SignID:29^Metres:" + wa29.getText() + "~" +
                "SignID:30^Metres:" + wa30.getText() + "~SignID:31^Metres:" + wa31.getText() + "~SignID:32^Metres:" + wa32.getText() + "~SignID:33^Metres:" + wa33.getText() + "~" +
                "SignID:34^Metres:" + wa34.getText() + "~SignID:35^Metres:" + wa35.getText() + "~SignID:36^Metres:" + wa36.getText() + "~SignID:37^Metres:" + wa37.getText() + "~" +
                "SignID:38^Metres:" + wa38.getText() + "~SignID:39^Metres:" + wa39.getText() + "~SignID:40^Metres:" + wa40.getText() + "~SignID:41^Metres:" + wa41.getText() + "~" +
                "SignID:42^Metres:" + wa42.getText() + "~SignID:43^Metres:" + wa43.getText() + "~SignID:43^Metres:" + wa44.getText() + "~SignID:43^Metres:" + wa45.getText() +
                "~SignID:43^Metres:" + wa46.getText() + "~SignID:43^Metres:" + wa47.getText() + "~SignID:43^Metres:" + wa48.getText() + "~SignID:43^Metres:" + wa49.getText() +
                "~SignID:43^Metres:" + wa50.getText() + "~SignID:43^Metres:" + wa51.getText() + "~SignID:43^Metres:" + wa52.getText() + "~SignID:43^Metres:" + wa53.getText() +
                "~SignID:43^Metres:" + wa54.getText() + "~SignID:43^Metres:" + wa55.getText() + "~SignID:43^Metres:" + wa56.getText() + "~SignID:43^Metres:" + wa57.getText() +
                "~SignID:43^Metres:" + wa58.getText() + "~SignID:43^Metres:" + wa59.getText() + "~SignID:43^Metres:" + wa60.getText() + "~SignID:43^Metres:" + wa61.getText() +
                "~SignID:43^Metres:" + wa62.getText() + "~SignID:43^Metres:" + wa63.getText() + "~SignID:43^Metres:" +
                "\"}\n" +
                "]";
        if (db.checkExistCreateJObs(mJobObject.getJobID()) == null) {
            db.addCreateJobs(mJobObject.getJobID());
        }

        db.updateContact(mJobObject.getJobID(), "", "", "", re.toString(), "");
        Toast.makeText(WASignage.this, "Changes have been saved!", Toast.LENGTH_SHORT).show();
    }

    public String getSignageAudit() {
        String temp = "";
        temp = "{\"mac\":" + db.getSignageAudt(mJobObject.getJobID()) + "}";
        return temp;
    }


    public void populateData() {
        JSONArray data = null;
        Log.e("haha", getSignageAudit());
        if (getSignageAudit().equals("{\"mac\":null}")) {

        } else {
            try {
                JSONObject jsonObj = new JSONObject(getSignageAudit());

                // Getting JSON Array node
                data = jsonObj.getJSONArray("mac");

                JSONObject c = data.getJSONObject(0);
                JSONObject x = data.getJSONObject(1);

                Log.e("JOB ID", c.getString("JobID"));
                cwevolution4.setText(c.getString("RegoNo"));
                cwlocallandmark4.setText(c.getString("Landmark"));

                spinner.setSelection(Integer.parseInt(c.getString("Direction")), true);

                if (!c.getString("TimeErected").equals("null")) {
                    TimeErected.setText(date.miliSecondConverter(c.getString("TimeErected"), "HH:mm:ss a"));
                    TimeErected2 = c.getString("TimeErected");
                    Time1 = c.getString("TimeErected");
                }
                if (!c.getString("TimeCollected").equals("null")) {
                    TimeCollected.setText(date.miliSecondConverter(c.getString("TimeCollected"), "HH:mm:ss a"));
                    TimeCollected2 = c.getString("TimeCollected");
                    Time6 = c.getString("TimeErected");
                }
                if (!c.getString("TimeChecked1").equals("null")) {
                    TimeChecked1.setText(date.miliSecondConverter(c.getString("TimeChecked1"), "HH:mm:ss a"));
                    TimeChecked12 = c.getString("TimeChecked1");
                    Time2 = c.getString("TimeChecked1");
                }

                if (!c.getString("TimeChecked2").equals("null")) {
                    TimeChecked2.setText(date.miliSecondConverter(c.getString("TimeChecked2"), "HH:mm:ss a"));
                    TimeChecked22 = c.getString("TimeChecked2");
                    Time3 = c.getString("TimeChecked2");
                }

                if (!c.getString("TimeChecked3").equals("null")) {
                    TimeChecked3.setText(date.miliSecondConverter(c.getString("TimeChecked3"), "HH:mm:ss a"));
                    TimeChecked32 = c.getString("TimeChecked3");
                    Time4 = c.getString("TimeChecked3");
                }

                if (!c.getString("TimeChecked4").equals("null")) {
                    TimeChecked4.setText(date.miliSecondConverter(c.getString("TimeChecked4"), "HH:mm:ss a"));
                    TimeChecked42 = c.getString("TimeChecked4");
                    Time5 = c.getString("TimeChecked3");
                }

                if (!c.getString("aftercare").equals("null")) {
                    ((RadioButton) aftercare.getChildAt(Integer.parseInt(c.getString("aftercare")))).setChecked(true);
                }

                w1.setText(getcontetn(c.getString("AuditSigns"), 0));
                w2.setText(getcontetn(c.getString("AuditSigns"), 1));
                w3.setText(getcontetn(c.getString("AuditSigns"), 2));
                w4.setText(getcontetn(c.getString("AuditSigns"), 3));
                w5.setText(getcontetn(c.getString("AuditSigns"), 4));
                w6.setText(getcontetn(c.getString("AuditSigns"), 5));
                w7.setText(getcontetn(c.getString("AuditSigns"), 6));
                w8.setText(getcontetn(c.getString("AuditSigns"), 7));
                w9.setText(getcontetn(c.getString("AuditSigns"), 8));
                w10.setText(getcontetn(c.getString("AuditSigns"), 9));
                w11.setText(getcontetn(c.getString("AuditSigns"), 10));
                w12.setText(getcontetn(c.getString("AuditSigns"), 11));
                w13.setText(getcontetn(c.getString("AuditSigns"), 12));
                w14.setText(getcontetn(c.getString("AuditSigns"), 13));
                w15.setText(getcontetn(c.getString("AuditSigns"), 14));
                w16.setText(getcontetn(c.getString("AuditSigns"), 15));
                w17.setText(getcontetn(c.getString("AuditSigns"), 16));
                w18.setText(getcontetn(c.getString("AuditSigns"), 17));
                w19.setText(getcontetn(c.getString("AuditSigns"), 18));
                w20.setText(getcontetn(c.getString("AuditSigns"), 19));
                w21.setText(getcontetn(c.getString("AuditSigns"), 20));
                w22.setText(getcontetn(c.getString("AuditSigns"), 21));
                w23.setText(getcontetn(c.getString("AuditSigns"), 22));
                w24.setText(getcontetn(c.getString("AuditSigns"), 23));
                w25.setText(getcontetn(c.getString("AuditSigns"), 24));
                w26.setText(getcontetn(c.getString("AuditSigns"), 25));
                w27.setText(getcontetn(c.getString("AuditSigns"), 26));
                w28.setText(getcontetn(c.getString("AuditSigns"), 27));
                w29.setText(getcontetn(c.getString("AuditSigns"), 28));
                w30.setText(getcontetn(c.getString("AuditSigns"), 29));
                w31.setText(getcontetn(c.getString("AuditSigns"), 30));
                w32.setText(getcontetn(c.getString("AuditSigns"), 31));
                w33.setText(getcontetn(c.getString("AuditSigns"), 32));
                w34.setText(getcontetn(c.getString("AuditSigns"), 33));
                w35.setText(getcontetn(c.getString("AuditSigns"), 34));
                w36.setText(getcontetn(c.getString("AuditSigns"), 35));
                w37.setText(getcontetn(c.getString("AuditSigns"), 36));
                w38.setText(getcontetn(c.getString("AuditSigns"), 37));
                w39.setText(getcontetn(c.getString("AuditSigns"), 38));
                w40.setText(getcontetn(c.getString("AuditSigns"), 39));
                w41.setText(getcontetn(c.getString("AuditSigns"), 40));
                w42.setText(getcontetn(c.getString("AuditSigns"), 41));
                w43.setText(getcontetn(c.getString("AuditSigns"), 42));
                w44.setText(getcontetn(c.getString("AuditSigns"), 43));
                w45.setText(getcontetn(c.getString("AuditSigns"), 44));
                w46.setText(getcontetn(c.getString("AuditSigns"), 45));
                w47.setText(getcontetn(c.getString("AuditSigns"), 46));
                w48.setText(getcontetn(c.getString("AuditSigns"), 47));
                w49.setText(getcontetn(c.getString("AuditSigns"), 48));
                w50.setText(getcontetn(c.getString("AuditSigns"), 49));
                w51.setText(getcontetn(c.getString("AuditSigns"), 50));
                w52.setText(getcontetn(c.getString("AuditSigns"), 51));
                w53.setText(getcontetn(c.getString("AuditSigns"), 52));
                w54.setText(getcontetn(c.getString("AuditSigns"), 53));
                w55.setText(getcontetn(c.getString("AuditSigns"), 54));
                w56.setText(getcontetn(c.getString("AuditSigns"), 55));
                w57.setText(getcontetn(c.getString("AuditSigns"), 56));
                w58.setText(getcontetn(c.getString("AuditSigns"), 57));
                w59.setText(getcontetn(c.getString("AuditSigns"), 58));
                w60.setText(getcontetn(c.getString("AuditSigns"), 59));
                w61.setText(getcontetn(c.getString("AuditSigns"), 60));
                w62.setText(getcontetn(c.getString("AuditSigns"), 61));
                w63.setText(getcontetn(c.getString("AuditSigns"), 62));

                wa1.setText(getcontetn(x.getString("AuditSigns"), 0));
                wa2.setText(getcontetn(x.getString("AuditSigns"), 1));
                wa3.setText(getcontetn(x.getString("AuditSigns"), 2));
                wa4.setText(getcontetn(x.getString("AuditSigns"), 3));
                wa5.setText(getcontetn(x.getString("AuditSigns"), 4));
                wa6.setText(getcontetn(x.getString("AuditSigns"), 5));
                wa7.setText(getcontetn(x.getString("AuditSigns"), 6));
                wa8.setText(getcontetn(x.getString("AuditSigns"), 7));
                wa9.setText(getcontetn(x.getString("AuditSigns"), 8));
                wa10.setText(getcontetn(x.getString("AuditSigns"), 9));
                wa11.setText(getcontetn(x.getString("AuditSigns"), 10));
                wa12.setText(getcontetn(x.getString("AuditSigns"), 11));
                wa13.setText(getcontetn(x.getString("AuditSigns"), 12));
                wa14.setText(getcontetn(x.getString("AuditSigns"), 13));
                wa15.setText(getcontetn(x.getString("AuditSigns"), 14));
                wa16.setText(getcontetn(x.getString("AuditSigns"), 15));
                wa17.setText(getcontetn(x.getString("AuditSigns"), 16));
                wa18.setText(getcontetn(x.getString("AuditSigns"), 17));
                wa19.setText(getcontetn(x.getString("AuditSigns"), 18));
                wa20.setText(getcontetn(x.getString("AuditSigns"), 19));
                wa21.setText(getcontetn(x.getString("AuditSigns"), 20));
                wa22.setText(getcontetn(x.getString("AuditSigns"), 21));
                wa23.setText(getcontetn(x.getString("AuditSigns"), 22));
                wa24.setText(getcontetn(x.getString("AuditSigns"), 23));
                wa25.setText(getcontetn(x.getString("AuditSigns"), 24));
                wa26.setText(getcontetn(x.getString("AuditSigns"), 25));
                wa27.setText(getcontetn(x.getString("AuditSigns"), 26));
                wa28.setText(getcontetn(x.getString("AuditSigns"), 27));
                wa29.setText(getcontetn(x.getString("AuditSigns"), 28));
                wa30.setText(getcontetn(x.getString("AuditSigns"), 29));
                wa31.setText(getcontetn(x.getString("AuditSigns"), 30));
                wa32.setText(getcontetn(x.getString("AuditSigns"), 31));
                wa33.setText(getcontetn(x.getString("AuditSigns"), 32));
                wa34.setText(getcontetn(x.getString("AuditSigns"), 33));
                wa35.setText(getcontetn(x.getString("AuditSigns"), 34));
                wa36.setText(getcontetn(x.getString("AuditSigns"), 35));
                wa37.setText(getcontetn(x.getString("AuditSigns"), 36));
                wa38.setText(getcontetn(x.getString("AuditSigns"), 37));
                wa39.setText(getcontetn(x.getString("AuditSigns"), 38));
                wa40.setText(getcontetn(x.getString("AuditSigns"), 39));
                wa41.setText(getcontetn(x.getString("AuditSigns"), 40));
                wa42.setText(getcontetn(x.getString("AuditSigns"), 41));
                wa43.setText(getcontetn(x.getString("AuditSigns"), 42));
                wa44.setText(getcontetn(x.getString("AuditSigns"), 43));
                wa45.setText(getcontetn(x.getString("AuditSigns"), 44));
                wa46.setText(getcontetn(x.getString("AuditSigns"), 45));
                wa47.setText(getcontetn(x.getString("AuditSigns"), 46));
                wa48.setText(getcontetn(x.getString("AuditSigns"), 47));
                wa49.setText(getcontetn(x.getString("AuditSigns"), 48));
                wa50.setText(getcontetn(x.getString("AuditSigns"), 49));
                wa51.setText(getcontetn(x.getString("AuditSigns"), 50));
                wa52.setText(getcontetn(x.getString("AuditSigns"), 51));
                wa53.setText(getcontetn(x.getString("AuditSigns"), 52));
                wa54.setText(getcontetn(x.getString("AuditSigns"), 53));
                wa55.setText(getcontetn(x.getString("AuditSigns"), 54));
                wa56.setText(getcontetn(x.getString("AuditSigns"), 55));
                wa57.setText(getcontetn(x.getString("AuditSigns"), 56));
                wa58.setText(getcontetn(x.getString("AuditSigns"), 57));
                wa59.setText(getcontetn(x.getString("AuditSigns"), 58));
                wa60.setText(getcontetn(x.getString("AuditSigns"), 59));
                wa61.setText(getcontetn(x.getString("AuditSigns"), 60));
                wa62.setText(getcontetn(x.getString("AuditSigns"), 61));
                wa63.setText(getcontetn(x.getString("AuditSigns"), 62));

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


}
