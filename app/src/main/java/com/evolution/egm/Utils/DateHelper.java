package com.evolution.egm.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by mcclynreyarboleda on 5/29/15.
 */
public class DateHelper {
    public String miliSecondConverter(String date, String pattern) {
        DateFormat formatter = new SimpleDateFormat(pattern);
        long milliSeconds = Long.parseLong(date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }
}
