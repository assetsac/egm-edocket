package com.evolution.egm.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

/**
 * Created by mcclynreyarboleda on 5/25/15.
 */
public class CreateJobSharedPreference {

    private Context context;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private static final int PRIVATE_MODE = 0;

    public CreateJobSharedPreference(Context context) {
        this.context = context;

        sharedPreferences = context.getSharedPreferences(
                Constants.SHAREDPREFERENCE, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public void createFirstItem(HashMap<String, String> data) {
        editor.putString("first", data.get("first"));
        editor.commit();
    }

    public void createSecondItem(HashMap<String, String> data) {
        editor.putString("second", data.get("second"));
        editor.commit();
    }

    public void createSignatureArea(HashMap<String, String> data) {
        editor.putString("signatures", data.get("signatures"));
        editor.commit();
    }

    public void createSignageAudit(HashMap<String, String> data) {
        editor.putString("signs", data.get("signs"));
        editor.commit();
    }


    public void createsitechecklist1(HashMap<String, String> data) {
        editor.putString("sitechecklist1", data.get("sitechecklist1"));
        editor.commit();
    }


    public String getCreateJobFirst() {
        return sharedPreferences.getString("first", "");
    }

    public String getCreateJobSecond2() {
        return sharedPreferences.getString("second", "");
    }
    public String getSignatures() {
        return sharedPreferences.getString("signatures", "");
    }

    public String getSignage() {
        return sharedPreferences.getString("signs", "");
    }
    public String getsitechecklist1() {
        return sharedPreferences.getString("sitechecklist1", "");
    }

}
