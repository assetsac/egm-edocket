package com.evolution.egm.Utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Compressor {
    public static byte[] compress(byte[] content){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try{
            GZIPOutputStream gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gzipOutputStream.write(content);
            gzipOutputStream.close();
        } catch(IOException e){
            throw new RuntimeException(e);
        }
        return byteArrayOutputStream.toByteArray();
    }

    public static byte[] decompress(byte[] contentBytes){
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try
        {
            ByteArrayInputStream in = new ByteArrayInputStream(contentBytes);
        	GZIPInputStream compressedInput = new GZIPInputStream(in);
        } 
        catch(IOException e)
        {
            throw new RuntimeException(e);
        }catch (Exception e)
        {
        	e.printStackTrace();
        }
        return out.toByteArray();
    }
}
