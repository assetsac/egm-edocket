package com.evolution.egm.Utils;

/**
 * Created by mcclynreyarboleda on 3/27/15.
 */

public class Constants {
//    public static final String API_URL = "http://app.ermg.com.au:9998/EvolutionTrafficApp/public/api/v2";
    public static final String API_URL = "http://testevoapp.manstat.com:9997/EvolutionTrafficApp/public/api/v2";

    public static final String REQUEST_JOBS = "/job_all";
    public static final String REQUEST_LOGIN = "/auth/login";
    public static final String REQUEST_UPLOAD = "/job/update";
    public static final String FAIL_SAFE = "/job/generate_fail_safe_report";

    public static final String REQUEST_JOBSB = "/job";
    public static final String REQUEST_JOBINFO = "Basic TWFuc3RhdElTOlBhc3M0TUlTIQ==";
    public static final String REQUEST_REQUIREMENTS = "/job/requirements/";
    public static final String notes = "/job/notes";
    public static final String CHECKMANSTAT = "/job/check_connectivity";
    public static final String JOBSYNC = "/job/job_sync/";
    public static final String JOBEXIST = "/job/job_expire/";
    public static final String UPLOADPHOTO = "/job/upload_job_image";
    public static final String UPLOADVIDEO = "/upload_video";
    public static final String SiteCheckListSync = "/job/generate_app_safety_report";
    public static final String jobImageReport = "/job/generate_job_image_report";
    public static final String checkReports = "/job/job_report/safety/";
    public static final String SHAREDPREFERENCE = "EVOLUTION";
    public static final String SHAREDPREFERENCE2 = "EVOLUTION2";
    public static final String SHAREDPREFERENCE3 = "EVOLUTION3";

    public static final String TOKEN_ID = "X-Auth-Token";
    public static final String AUT = "Authorization";
    public static final String KEY_LOGGED_IN = "logged_in";
    public static final String KEY_USERNAME = "etccode";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_ERROR = "errors";

}
