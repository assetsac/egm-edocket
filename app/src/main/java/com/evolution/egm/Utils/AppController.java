package com.evolution.egm.Utils;

import android.app.Application;

import com.evolution.egm.Interface.APIInterface;
import com.squareup.okhttp.OkHttpClient;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

@ReportsCrashes(
        formUri = "http://straightarrowasset.com/android_crashreport/acra.php",
        formUriBasicAuthLogin = "your username",
        formUriBasicAuthPassword = "your password",
        reportType = org.acra.sender.HttpSender.Type.JSON,
        sendReportsAtShutdown = false
)

/**
 * Created by mcgwapo on 2/9/16.
 */
public class AppController extends Application {
    public static final String TAG = AppController.class.getSimpleName();
    private static AppController mInstance;
    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public APIInterface sacInterface;
    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        ACRA.init(this);
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(100, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(100, TimeUnit.SECONDS);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_URL)
                .setClient(new OkClient(okHttpClient))
                .build();
        sacInterface = restAdapter.create(APIInterface.class);
    }

    public APIInterface getBoardsAPI() {
        return sacInterface;
    }
}
