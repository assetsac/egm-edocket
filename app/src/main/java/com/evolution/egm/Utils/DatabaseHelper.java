package com.evolution.egm.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.evolution.egm.Models.JobStatuses;
import com.evolution.egm.Models.SingageAudit;
import com.evolution.egm.Models.DocketAttachments;
import com.evolution.egm.Models.DocketAttachmentsObject;
import com.evolution.egm.Models.Job;
import com.evolution.egm.Models.JobObjects;
import com.evolution.egm.Models.ModelRequirements;
import com.evolution.egm.Models.ModelRequirementsObjects;
import com.evolution.egm.Models.Notes;
import com.evolution.egm.Models.PhotoItem;
import com.evolution.egm.Models.SignagesMeterModel;
import com.evolution.egm.Models.SignagesObject;
import com.evolution.egm.Models.Stocklist_Kit_Model;
import com.evolution.egm.Models.SubJobCounter;
import com.evolution.egm.Models.TimeSheetObjects;
import com.evolution.egm.Models.TimeSheetSignature;
import com.evolution.egm.Models.Timesheet;
import com.evolution.egm.Models.Timesheet_Upload;
import com.evolution.egm.Models.VideoModel;
import com.evolution.egm.Models.siteChecklistAddmodel;
import com.google.gson.Gson;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "EvolutionApp";

    // Contacts table name

    // Table User login informations
    private static final String TB_ASSETS = "Assets";
    private static final String TB_APPLICATION_INFO = "ApplicationInfos";
    private static final String TB_CONTACT_DETAILS = "ContactDetails";
    private static final String TB_CONTACTS = "Contacts";
    private static final String TB_DEPOT = "Depots";
    private static final String TB_JOBASSETS = "JobAssets";
    private static final String TB_CREATE_JOB = "CreateJobs";
    private static final String TB_JOBREQUIREMENTS = "JobRequirements";
    private static final String TB_JOBS = "Jobs";
    private static final String TB_SUBJOBS = "SubJobs";
    private static final String TB_LOCATIONS = "Locations";
    private static final String TB_DOCKETS = "Dockets";
    private static final String TB_CHECKLIST = "checklist";
    private static final String TB_TIMESHEETS = "Timesheets";
    private static final String TB_TIMESHEETLOCAL = "LocalTimesheets";
    private static final String DOCKETOTHERFIELD = "timeSheetOtherField";
    private static final String Photo = "Gallery";
    private static final String VideoGallery = "VideoGallery";
    private static final String Signs = "Signs";
    private static final String Signage_Audit = "Signage_Audit";
    private static final String Notesz = "Notes";
    private static final String reUploadJob = "SiteCheckReUpload";
    private static final String jobStatuses = "Statuses";

    //abc
    private static final String TB_StockList = "StockList";

    // Fields for Jobs
    private static final String tb_id = "_id";
    private static final String id = "Id";
    private static final String code = "code";
    private static final String jobId = "jobId";
    private static final String additionalNotes = "additionalNotes";
    private static final String authorizerID = "authorizerID";
    private static final String contactID = "contactID";
    private static final String depotID = "depotID";
    private static final String jobCode = "jobCode";
    private static final String endDateTime = "endDateTime";
    private static final String foremanID = "foremanID";
    private static final String operatorID = "operatorID";
    private static final String orderNumber = "orderNumber";
    private static final String parentJobID = "parentJobID";
    private static final String startDateTime = "startDateTime";
    private static final String startType = "startType";
    private static final String contactPerson = "contactPerson";
    private static final String authorizedPerson = "authorizedPerson";
    private static final String address = "address";
    private static final String depot = "depot";
    private static final String companyName = "companyName";
    private static final String startDateTimeFormat = "startDateTimeFormat";
    private static final String endDateTimeFormat = "endDateTimeFormat";
    private static final String State = "State";
    private static final String Status = "status";
    private static final String SubJob = "SubJob";
    private static final String External = "DocketAttachments";

    //Fields for Requirements
    private static final String RowID = "RowID";
    private static final String Description_r = "Description";
    private static final String JobID_r = "JobID";
    private static final String Quantity_r = "Quantity";
    private static final String RequirementID_r = "RequirementID";

    //Fields for employees
    private static final String ContactID_t = "ContactID";
    private static final String EndDateTime_t = "EndDateTime";
    private static final String Id_t = "Id";
    private static final String JobID_t = "JobID";
    private static final String ShiftLocation_t = "ShiftLocation";
    private static final String StartDateTime_t = "StartDateTime";
    private static final String StartType_t = "StartType";
    private static final String StartDateTimeFormat_t = "StartDateTimeFormat";
    private static final String EndDateTimeFormat_t = "EndDateTimeFormat";
    private static final String Name_t = "Name";
    private static final String Description_t = "Description";

    //Fields for Checklist
    private static final String Id_c = "Id";
    private static final String SectionID_c = "SectionID";
    private static final String SequenceNo_c = "SequenceNo";
    private static final String ValueType_c = "ValueType";
    private static final String Description_c = "Description";
    private static final String options = "options";

    //Fields for Checklist
    private static final String JobID_d = "JobID";
    private static final String OperatorID_d = "OperatorID";
    private static final String AttachmentTypeID_d = "AttachmentType";
    private static final String AttachedOn_d = "AttachedOn";
    private static final String Attachment = "Attachment";

    public static final String AdditionalHazard = "AdditionalHazard";

    private static final String TB_TIMESHEETS_UPLOAD = "Timesheet_Upload";
    private static final String TB_TIMESHEETS_UPLOAD_SIG = "Timesheet_Upload_Sig";
    private static final String SubjobCounter = "SubjobCounter";

    //abc
    private static final String StockListIni = "CREATE TABLE "
            + TB_StockList + " ( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + "type TEXT,"
            + "brand TEXT,"
            + "lenght TEXT,"
            + "size TEXT,"
            + "description TEXT,"
            + "category TEXT," +
            "stocklist_type TEXT, " +
            "isChecked TEXT," +
            "jobID TEXT)";

    private static final String SUBJOBCOUNTER = "CREATE TABLE "
            + SubjobCounter + " ( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + "JobId TEXT,"
            + "Counter TEXT)";

    private static final String JOBSTATUSTB = "CREATE TABLE "
            + jobStatuses + " ( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + "JobId TEXT,"
            + "singageAudit TEXT,"
            + "awayjob TEXT)";


    private static final String TIMESHEET_UPLOAD_SIG_INI = "CREATE TABLE "
            + TB_TIMESHEETS_UPLOAD_SIG + " ( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + "contactID TEXT,"
            + "jobID TEXT,"
            + "name TEXT,"
            + "signature TEXT"
            + ")";

    public static final String TB_TIMESHEETS_UPLOAD_INI = "CREATE TABLE "
            + TB_TIMESHEETS_UPLOAD + " ( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + "JobID TEXT,"
            + "WorkerID TEXT,"
            + "ShiftID TEXT,"
            + "TravelStartDateTime TEXT,"
            + "TravelStartDateTimeEnd TEXT,"
            + "TravelStartDistance TEXT,"
            + "JobStartDateTime TEXT,"
            + "JobFinishDateTime TEXT,"
            + "BreakStartDateTime TEXT,"
            + "BreakFinishDateTime TEXT,"
            + "TravelFinishDateTimeStart TEXT,"
            + "TravelFinishDateTime TEXT,"
            + "TravelFinishDistance TEXT,"
            + "FatigueCompliance TEXT,"
            + "AttendDepot TEXT,"
            + "RegoNo TEXT,"
            + "TraveledKilometers TEXT,"
            + "isHaveMeal TEXT"
            + ")";


    public static final String AdditionalHazardDB = "CREATE TABLE "
            + AdditionalHazard + " ( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + "JobID TEXT,"
            + "additionalHazzard TEXT,"
            + "initialrisk TEXT,"
            + "measures TEXT,"
            + "residual TEXT,"
            + "position TEXT"
            + ")";

    public static final String reUploadJobTable = "CREATE TABLE "
            + reUploadJob + " ( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + "JobID TEXT"
            + ")";

    public static final String Signage_Audit_INI = "CREATE TABLE "
            + Signage_Audit + " ( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + "JobID TEXT,"
            + "RegoNo TEXT,"
            + "Landmark TEXT,"
            + "CarriageWay TEXT,"
            + "Direction TEXT,"
            + "TimeErected TEXT,"
            + "TimeCollected TEXT,"
            + "TimeChecked1 TEXT,"
            + "TimeChecked2 TEXT,"
            + "TimeChecked3 TEXT,"
            + "TimeChecked4 TEXT,"
            + "AuditSigns TEXT,"
            + "IsAfterCare TEXT,"
            + "ErectedBy TEXT,"
            + "CollectedBy TEXT"
            + ")";

    private static final String CREATE_TABLE_TB_DOCKETS = "CREATE TABLE "
            + TB_DOCKETS + " ( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + JobID_d + " TEXT,"
            + OperatorID_d + " TEXT,"
            + AttachmentTypeID_d + " TEXT,"
            + AttachedOn_d + " TEXT,"
            + Attachment + " BLOB"
            + ")";

    public static final String signscreate = "CREATE TABLE "
            + "Signs ( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + "jobid TEXT,"
            + "image TEXT,"
            + "metress TEXT,"
            + "quantity TEXT,"
            + "after_care TEXT,"
            + "metress2 TEXT,"
            + "quantity2 TEXT,"
            + "isSelected TEXT,"
            + "typemode TEXT"
            + ")";

    public static final String hazard = "CREATE TABLE "
            + "hazard ( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + "jobID TEXT,"
            + "hazard TEXT"
            + ")";
    public static final String initNotes = "CREATE TABLE "
            + Notesz + " ( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + "status TEXT,"
            + "note TEXT"
            + ")";

    public static final String externallinks = "CREATE TABLE "
            + External + " ( _id INTEGER PRIMARY KEY, " + " "
            + "Caption TEXT,"
            + "ExternalLink TEXT,"
            + "ItemId TEXT,"
            + "JobId TEXT,"
            + "RowID TEXT,"
            + "AppLink TEXT"
            + ")";

    public static final String galleryq = "CREATE TABLE "
            + Photo + " ( _id INTEGER PRIMARY KEY, " + " "
            + "jobid TEXT,"
            + "image_url TEXT,"
            + "IsUploaded TEXT"
            + ")";

    public static final String VideoGalleryIni = "CREATE TABLE "
            + VideoGallery + " ( _id INTEGER PRIMARY KEY, " + " "
            + "jobid TEXT,"
            + "video_url TEXT,"
            + "isSeleted TEXT,"
            + "dateUploaded TEXT,"
            + "geotag TEXT,"
            + "subject TEXT,"
            + "note TEXT,"
            + "startCapture TEXT,"
            + "endCapture TEXT,"
            + "startLocation TEXT,"
            + "endLocation TEXT"
            + ")";

    public static final String otherdocket = "CREATE TABLE "
            + DOCKETOTHERFIELD + "( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + "jobid TEXT,"
            + "name TEXT,"
            + "email TEXT"
            + ")";

    public static final String sitecheclistDummy = "CREATE TABLE "
            + "ChecklistStore ( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + JobID_d + " TEXT,"
            + "firstsection TEXT,"
            + "secondsection TEXT,"
            + "thirdsection TEXT"
            + ")";

    private static final String CREATE_TABLE_TB_TIMESHEETLOCAL = "CREATE TABLE "
            + TB_TIMESHEETLOCAL + " ( " + tb_id + " INTEGER PRIMARY KEY, "
            + "JobID TEXT,"
            + " WorkerID TEXT,"
            + " ShiftID TEXT,"
            + " TravelStartDateTime TEXT,"
            + " TravelStartDateTimeEnd TEXT,"
            + " TravelStartDistance TEXT,"
            + " JobStartDateTime TEXT,"
            + " BreakStartDateTime TEXT,"
            + " BreakFinishDateTime TEXT,"
            + " JobFinishDateTime TEXT,"
            + " TravelFinishDateTime TEXT,"
            + " TravelFinishDateTimeEnd TEXT,"
            + "TravelFinishDistance TEXT,"
            + "FatigueCompliance TEXT,"
            + "Odometer TEXT,"
            + "mealbreak TEXT,"
            + "regoNo TEXT,"
            + "attenddepot TEXT"
            + ")";

    private static final String CREATE_TABLE_TB_CREATE_JOB = "CREATE TABLE "
            + TB_CREATE_JOB + " ( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + "jobID TEXT,"
            + "Section1 TEXT,"
            + "Section2 TEXT,"
            + "Signatures TEXT,"
            + "Sections TEXT,"
            + "Status TEXT,"
            + "aftercare TEXT"
            + ")";

    private static final String CREATESITECHECKDUMMY = "CREATE TABLE "
            + " DUMMYSITE ( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + "jobID TEXT,"
            + "idselected TEXT)";

    private static final String isHaveClientSignature = "CREATE TABLE "
            + " isHaveClientSignature ( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + "jobid TEXT,"
            + "status TEXT)";

    private static final String CREATE_TABLE_TB_CHECKLIST = "CREATE TABLE "
            + TB_CHECKLIST + " ( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + Description_c + " TEXT,"
            + Id_c + " TEXT,"
            + SectionID_c + " TEXT,"
            + SequenceNo_c + " TEXT,"
            + ValueType_c + " TEXT,"
            + options + " TEXT"
            + ")";

    private static final String TIMESHEET_SIGNATURE = "CREATE TABLE "
            + "timesheet_signature" + " ( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + "contactID TEXT,"
            + "jobID TEXT,"
            + "name TEXT,"
            + "signature TEXT"
            + ")";

    private static final String CREATE_TABLE_ASSETS = "CREATE TABLE "
            + TB_JOBS + " ( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + id + " TEXT," +
            code + " TEXT," +
            jobId + " TEXT," +
            additionalNotes + " TEXT," +
            authorizerID + " TEXT," +
            contactID + " TEXT," +
            depotID + " TEXT," +
            jobCode + " TEXT," +
            endDateTime + " TEXT," +
            foremanID + " TEXT," +
            operatorID + " TEXT," +
            orderNumber + " TEXT," +
            parentJobID + " TEXT," +
            startDateTime + " TEXT," +
            startType + " TEXT," +
            contactPerson + " TEXT," +
            authorizedPerson + " TEXT," +
            address + " TEXT," +
            depot + " TEXT," +
            companyName + " TEXT," +
            startDateTimeFormat + " TEXT," +
            endDateTimeFormat + " TEXT," +
            State + " TEXT," +
            Status + " TEXT," +
            "ContactMobilePhone TEXT," +
            "ContactPhone TEXT," +
            "ContactEmail TEXT," +
            "JobShiftStartDateTime TEXT," +
            "JobShiftStartDateTimeFormat TEXT," +
            "JobShiftEndDateTime TEXT," +
            "JobShiftEndDateTimeFormat TEXT," +
            "CreateDateTime TEXT," +
            "CreateDateTimeFormat TEXT," +
            SubJob + " TEXT" +
            ")";

    private static final String CREATE_TABLE_SUBJOBS = "CREATE TABLE "
            + TB_SUBJOBS + " ( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + id + " TEXT," +
            code + " TEXT," +
            jobId + " TEXT," +
            additionalNotes + " TEXT," +
            authorizerID + " TEXT," +
            contactID + " TEXT," +
            depotID + " TEXT," +
            jobCode + " TEXT," +
            endDateTime + " TEXT," +
            foremanID + " TEXT," +
            operatorID + " TEXT," +
            orderNumber + " TEXT," +
            parentJobID + " TEXT," +
            startDateTime + " TEXT," +
            startType + " TEXT," +
            contactPerson + " TEXT," +
            authorizedPerson + " TEXT," +
            address + " TEXT," +
            depot + " TEXT," +
            companyName + " TEXT," +
            startDateTimeFormat + " TEXT," +
            endDateTimeFormat + " TEXT," +
            State + " TEXT," +
            Status + " TEXT," +
            "ContactMobilePhone TEXT," +
            "ContactPhone TEXT," +
            "ContactEmail TEXT," +
            "JobShiftStartDateTime TEXT," +
            "JobShiftStartDateTimeFormat TEXT," +
            "JobShiftEndDateTime TEXT," +
            "JobShiftEndDateTimeFormat TEXT," +
            "CreateDateTime TEXT," +
            "CreateDateTimeFormat TEXT," +
            SubJob + " TEXT" +
            ")";

    private static final String CREATE_TABLE_TB_JOBREQUIREMENTS = "CREATE TABLE "
            + TB_JOBREQUIREMENTS + " ( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + RowID + " TEXT,"
            + Description_r + " TEXT,"
            + JobID_r + " TEXT,"
            + Quantity_r + " TEXT,"
            + RequirementID_r + " TEXT"
            + ")";

    private static final String CREATE_TABLE_TB_TB_TIMESHEETS = "CREATE TABLE "
            + TB_TIMESHEETS + " ( " + tb_id + " INTEGER PRIMARY KEY, " + " "
            + ContactID_t + " TEXT,"
            + EndDateTime_t + " TEXT,"
            + Id_t + " TEXT,"
            + JobID_t + " TEXT,"
            + ShiftLocation_t + " TEXT,"
            + StartDateTime_t + " TEXT,"
            + StartType_t + " TEXT,"
            + StartDateTimeFormat_t + " TEXT,"
            + EndDateTimeFormat_t + " TEXT,"
            + Name_t + " TEXT,"
            + Description_t + " TEXT,"
            + "WorkerID TEXT"
            + ")";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_ASSETS);
        db.execSQL(CREATE_TABLE_TB_JOBREQUIREMENTS);
        db.execSQL(CREATE_TABLE_TB_TB_TIMESHEETS);
        db.execSQL(CREATE_TABLE_TB_CHECKLIST);
        db.execSQL(CREATE_TABLE_TB_CREATE_JOB);
        db.execSQL(CREATE_TABLE_SUBJOBS);
        db.execSQL(CREATE_TABLE_TB_DOCKETS);
        db.execSQL(CREATE_TABLE_TB_TIMESHEETLOCAL);
        db.execSQL(CREATESITECHECKDUMMY);
        db.execSQL(sitecheclistDummy);
        db.execSQL(otherdocket);
        db.execSQL(TIMESHEET_SIGNATURE);
        db.execSQL(hazard);
        db.execSQL(galleryq);
        db.execSQL(externallinks);
        db.execSQL(signscreate);
        db.execSQL(isHaveClientSignature);
        db.execSQL(Signage_Audit_INI);
        db.execSQL(AdditionalHazardDB);
        db.execSQL(initNotes);
        db.execSQL(reUploadJobTable);
        db.execSQL(TB_TIMESHEETS_UPLOAD_INI);
        db.execSQL(SUBJOBCOUNTER);
        db.execSQL(TIMESHEET_UPLOAD_SIG_INI);
        db.execSQL(JOBSTATUSTB);
        db.execSQL(VideoGalleryIni);
        db.execSQL(StockListIni);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TB_JOBS);
        db.execSQL("DROP TABLE IF EXISTS " + TB_JOBREQUIREMENTS);
        db.execSQL("DROP TABLE IF EXISTS " + TB_TIMESHEETS);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_TB_CHECKLIST);
        db.execSQL("DROP TABLE IF EXISTS " + TB_CREATE_JOB);
        db.execSQL("DROP TABLE IF EXISTS " + TB_SUBJOBS);
        db.execSQL("DROP TABLE IF EXISTS " + TB_DOCKETS);
        db.execSQL("DROP TABLE IF EXISTS " + TB_TIMESHEETLOCAL);
        db.execSQL("DROP TABLE IF EXISTS " + DOCKETOTHERFIELD);
        db.execSQL("DROP TABLE IF EXISTS " + VideoGallery);
        db.execSQL("DROP TABLE IF EXISTS " + Photo);
        db.execSQL("DROP TABLE IF EXISTS " + Notesz);
        db.execSQL("DROP TABLE IF EXISTS " + Signs);
        db.execSQL("DROP TABLE IF EXISTS " + AdditionalHazard);
        db.execSQL("DROP TABLE IF EXISTS " + External);
        db.execSQL("DROP TABLE IF EXISTS " + Signage_Audit);
        db.execSQL("DROP TABLE IF EXISTS " + reUploadJob);
        db.execSQL("DROP TABLE IF EXISTS " + TB_TIMESHEETS_UPLOAD);
        db.execSQL("DROP TABLE IF EXISTS " + TB_TIMESHEETS_UPLOAD_SIG);
        db.execSQL("DROP TABLE IF EXISTS " + SubjobCounter);
        db.execSQL("DROP TABLE IF EXISTS " + jobStatuses);
        db.execSQL("DROP TABLE IF EXISTS " + TB_StockList);
        db.execSQL("DROP TABLE IF EXISTS DUMMYSITE");
        db.execSQL("DROP TABLE IF EXISTS ChecklistStore");
        db.execSQL("DROP TABLE IF EXISTS hazard");
        db.execSQL("DROP TABLE IF EXISTS timesheet_signature");
        db.execSQL("DROP TABLE IF EXISTS isHaveClientSignature");
        // Create tables again
        onCreate(db);
    }

    public void addJobStatus0(JobStatuses jobstatus) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("JobId", jobstatus.getJobId());
        values.put("singageAudit", jobstatus.getSingageAudit());
        values.put("awayjob", "-1");
        if (checkJobstatus(jobstatus.getJobId())) {
            db.update(jobStatuses, values, "JobId = ?",
                    new String[]{String.valueOf(jobstatus.getJobId())});
        } else {
            db.insert(jobStatuses, null, values);
        }
        db.close();
    }

    public int setCheckStocks(String id, boolean f) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("isChecked", f);
        return db.update(TB_StockList, values, " " + " _id = ?",
                new String[]{String.valueOf(id)});
    }

    public int updateStocks(Stocklist_Kit_Model stocks, String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("type", stocks.getType());
        values.put("brand", stocks.getBrand());
        values.put("lenght", stocks.getLenght());
        values.put("size", stocks.getSize());
        values.put("description", stocks.getDescription());
        values.put("category", stocks.getCategory());
        return db.update(TB_StockList, values, " " + " _id = ?",
                new String[]{String.valueOf(id)});
    }

    public void deleteStocks2(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TB_StockList + " WHERE stocklist_type = 0 AND jobID = '"+id+"'");
        db.close();

        SQLiteDatabase db2 = this.getWritableDatabase();
        db2.execSQL("delete from " + TB_StockList + " WHERE stocklist_type = 1 AND jobID = '"+id+"'");
        db2.close();

    }
    public void deleteStocks(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TB_StockList + " WHERE _id = '" + id + "'");
        db.close();
    }

    public void addStocklist2(Stocklist_Kit_Model stocks, String stocklist_type, String jobID) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("type", stocks.getType());
        values.put("brand", stocks.getBrand());
        values.put("lenght", stocks.getLenght());
        values.put("size", stocks.getSize());
        values.put("description", stocks.getDescription());
        values.put("category", stocks.getCategory());
        values.put("stocklist_type", stocklist_type);
        values.put("isChecked", "0");
        values.put("jobID", jobID);
        db.insert(TB_StockList, null, values);
        db.close();
    }

    public String checkifExist(String id) {

//        String selectQuery = "SELECT  * FROM " + TB_StockList + " WHERE stocklist_type = 2 AND jobID = " + jobid;
        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
        return "";
    }


    //abcsdfs
    public void addStocklist(Stocklist_Kit_Model stocks, String stocklist_type, String jobID) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("type", stocks.getType());
        values.put("brand", stocks.getBrand());
        values.put("lenght", stocks.getLenght());
        values.put("size", stocks.getSize());
        values.put("description", stocks.getDescription());
        values.put("category", stocks.getCategory());
        values.put("stocklist_type", stocklist_type);
        values.put("isChecked", "1");
        values.put("jobID", jobID);
        db.insert(TB_StockList, null, values);
        db.close();
    }

    public ArrayList<Stocklist_Kit_Model> getAllMiscStocks(String jobid) {
        ArrayList<Stocklist_Kit_Model> stocks = new ArrayList<Stocklist_Kit_Model>();
        String selectQuery = "SELECT  * FROM " + TB_StockList + " WHERE stocklist_type = 2 AND jobID = " + jobid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Stocklist_Kit_Model stocklist = new Stocklist_Kit_Model();
                stocklist.set_id(cursor.getString(0));
                stocklist.setType(cursor.getString(1));
                stocklist.setBrand(cursor.getString(2));
                stocklist.setLenght(cursor.getString(3));
                stocklist.setSize(cursor.getString(4));
                stocklist.setDescription(cursor.getString(5));
                stocklist.setCategory(cursor.getString(6));
                if (cursor.getString(8).toString().equals("1")) {
                    stocklist.setChecked(true);
                } else {
                    stocklist.setChecked(false);
                }
                stocklist.setJobID(cursor.getString(9));
                stocks.add(stocklist);
            } while (cursor.moveToNext());
        }
        return stocks;
    }


    public ArrayList<Stocklist_Kit_Model> getAllKitsStocks(String jobid) {
        ArrayList<Stocklist_Kit_Model> stocks = new ArrayList<Stocklist_Kit_Model>();
        String selectQuery = "SELECT  * FROM " + TB_StockList + " WHERE stocklist_type = 0 AND jobID = " + jobid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Stocklist_Kit_Model stocklist = new Stocklist_Kit_Model();
                stocklist.set_id(cursor.getString(0));
                stocklist.setType(cursor.getString(1));
                stocklist.setBrand(cursor.getString(2));
                stocklist.setLenght(cursor.getString(3));
                stocklist.setSize(cursor.getString(4));
                stocklist.setDescription(cursor.getString(5));
                stocklist.setCategory(cursor.getString(6));
                if (cursor.getString(8).toString().equals("1")) {
                    stocklist.setChecked(true);
                } else {
                    stocklist.setChecked(false);
                }
                stocklist.setJobID(cursor.getString(9));
                stocks.add(stocklist);
            } while (cursor.moveToNext());
        }
        return stocks;
    }

    public ArrayList<Stocklist_Kit_Model> getAllComponentsStocks(String jobid) {
        ArrayList<Stocklist_Kit_Model> stocks = new ArrayList<Stocklist_Kit_Model>();
        String selectQuery = "SELECT  * FROM " + TB_StockList + " WHERE stocklist_type = 1 AND jobID = " + jobid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Stocklist_Kit_Model stocklist = new Stocklist_Kit_Model();
                stocklist.set_id(cursor.getString(0));
                stocklist.setType(cursor.getString(1));
                stocklist.setBrand(cursor.getString(2));
                stocklist.setLenght(cursor.getString(3));
                stocklist.setSize(cursor.getString(4));
                stocklist.setDescription(cursor.getString(5));
                stocklist.setCategory(cursor.getString(6));
                if (cursor.getString(8).toString().equals("1")) {
                    stocklist.setChecked(true);
                } else {
                    stocklist.setChecked(false);
                }
                stocklist.setJobID(cursor.getString(9));
                stocks.add(stocklist);
            } while (cursor.moveToNext());
        }
        return stocks;
    }


    public void addJobStatus(JobStatuses jobstatus) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("JobId", jobstatus.getJobId());
        values.put("singageAudit", jobstatus.getSingageAudit());
        if (checkJobstatus(jobstatus.getJobId())) {
            db.update(jobStatuses, values, "JobId = ?",
                    new String[]{String.valueOf(jobstatus.getJobId())});
        } else {
            db.insert(jobStatuses, null, values);
        }
        db.close();
    }

    public void addJobStatus2(JobStatuses jobstatus) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("awayjob", jobstatus.getAwayjob());
        db.update(jobStatuses, values, "JobId = ?",
                new String[]{String.valueOf(jobstatus.getJobId())});
        db.close();
    }

    public boolean checkJobstatus(String JobID) {
        boolean d;
        String selectQuery = "SELECT  1 FROM " + jobStatuses + " where JobId = " + JobID;
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                d = true;
            } while (cursor.moveToNext());
        } else {
            d = false;
        }
        return d;
    }

    public JobStatuses getJobStatusStats(String JobId) {
        JobStatuses ss = new JobStatuses();
        String selectQuery = "SELECT  * FROM " + jobStatuses + " where JobId = " + JobId;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                ss.setJobId(cursor.getString(1));
                ss.setSingageAudit(cursor.getString(2));
                ss.setAwayjob(cursor.getString(3));
            } while (cursor.moveToNext());
        }
        return ss;
    }

    public void addUpdateSUbjobCounter(SubJobCounter subJobCounter) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("JobId", subJobCounter.getJobId());
        values.put("Counter", subJobCounter.getCounter());
        if (checksubJobCounterChecker(subJobCounter.getJobId())) {
            db.update(SubjobCounter, values, "JobId = ?",
                    new String[]{String.valueOf(subJobCounter.getJobId())});
        } else {
            db.insert(SubjobCounter, null, values);
        }
        db.close();
    }

    public boolean checksubJobCounterChecker(String JobID) {
        boolean d;
        String selectQuery = "SELECT  1 FROM " + SubjobCounter + " where JobId = " + JobID;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                d = true;
            } while (cursor.moveToNext());
        } else {
            d = false;
        }
        return d;
    }

    public ArrayList<SubJobCounter> getAllSubJobCoutner(String JobId) {
        ArrayList<SubJobCounter> timeSelected = new ArrayList<SubJobCounter>();
        String selectQuery = "SELECT  * FROM " + SubjobCounter + " where JobId = " + JobId;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                SubJobCounter subJobCounter = new SubJobCounter();
                subJobCounter.setJobId(cursor.getString(1));
                subJobCounter.setCounter(cursor.getString(2));
                timeSelected.add(subJobCounter);
            } while (cursor.moveToNext());
        }
        return timeSelected;
    }


    public void addUpdateToUploadTimeSheet(Timesheet_Upload timesheet) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("JobID", timesheet.getJobID());
        values.put("WorkerID", timesheet.getWorkerID());
        values.put("ShiftID", timesheet.getShiftID());
        values.put("TravelStartDateTime", timesheet.getTravelStartDateTime());
        values.put("TravelStartDateTimeEnd", timesheet.getTravelStartDateTimeEnd());
        values.put("TravelStartDistance", timesheet.getTravelStartDistance());
        values.put("JobStartDateTime", timesheet.getJobStartDateTime());
        values.put("JobFinishDateTime", timesheet.getJobFinishDateTime());
        values.put("BreakStartDateTime", timesheet.getBreakStartDateTime());
        values.put("BreakFinishDateTime", timesheet.getBreakFinishDateTime());
        values.put("TravelFinishDateTimeStart", timesheet.getTravelFinishDateTimeStart());
        values.put("TravelFinishDateTime", timesheet.getTravelFinishDateTime());
        values.put("TravelFinishDistance", timesheet.getTravelFinishDistance());
        values.put("FatigueCompliance", timesheet.getFatigueCompliance());
        values.put("AttendDepot", timesheet.getAttendDepot());
        values.put("RegoNo", timesheet.getRegoNo());
        values.put("TraveledKilometers", timesheet.getTraveledKilometers());
        values.put("isHaveMeal", timesheet.getIsHaveMeal());
        if (checkTimesheetUploadExisting(timesheet.getJobID(), timesheet.getWorkerID())) {
            db.update(TB_TIMESHEETS_UPLOAD, values, "JobID = " + timesheet.getJobID() + " AND WorkerID = ?",
                    new String[]{String.valueOf(timesheet.getWorkerID())});
        } else {
            db.insert(TB_TIMESHEETS_UPLOAD, null, values);
        }
        db.close();
    }

    public void addUpdateToUploadTimeSheet_SIG(TimeSheetSignature timesheet) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("JobID", timesheet.getJobID());
        values.put("contactID", timesheet.getContactID());
        values.put("name", timesheet.getName());
        values.put("signature", timesheet.getSignature());
        if (checkTimesheetUploadSigExisting(timesheet.getJobID(), timesheet.getContactID())) {
            db.update(TB_TIMESHEETS_UPLOAD_SIG, values, "JobID = " + timesheet.getJobID() + " AND contactID = ?",
                    new String[]{String.valueOf(timesheet.getContactID())});
        } else {
            db.insert(TB_TIMESHEETS_UPLOAD_SIG, null, values);
        }
        db.close();
    }

    public void removeSigTIme(String JobID, String ContactID) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TB_TIMESHEETS_UPLOAD_SIG + " WHERE JobID = " + JobID + " AND contactID = " + ContactID);
        db.close();
    }

    public boolean checkTimesheetUploadSigExisting(String JobID, String WorkerID) {
        boolean d;
        String selectQuery = "SELECT  1 FROM " + TB_TIMESHEETS_UPLOAD_SIG + " where JobID = " + JobID + " AND  contactID = " + WorkerID;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                d = true;
            } while (cursor.moveToNext());
        } else {
            d = false;
        }
        return d;
    }

    public ArrayList<TimeSheetSignature> getAllTimeSigByID(String jobid, String contactID) {
        ArrayList<TimeSheetSignature> timeSelected = new ArrayList<TimeSheetSignature>();
        String selectQuery = "SELECT  * FROM " + TB_TIMESHEETS_UPLOAD_SIG + " where JobID = " + jobid + " AND contactID = " + contactID;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                TimeSheetSignature time = new TimeSheetSignature();
                time.setJobID(cursor.getString(2));
                time.setContactID(cursor.getString(1));
                time.setName(cursor.getString(3));
                time.setSignature(cursor.getString(4));
                timeSelected.add(time);
            } while (cursor.moveToNext());
        }
        return timeSelected;
    }

    public ArrayList<TimeSheetSignature> getAllTimeSigBy(String jobid) {
        ArrayList<TimeSheetSignature> timeSelected = new ArrayList<TimeSheetSignature>();
        String selectQuery = "SELECT  * FROM " + TB_TIMESHEETS_UPLOAD_SIG + " where JobID = " + jobid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                TimeSheetSignature time = new TimeSheetSignature();
                time.setJobID(cursor.getString(2));
                time.setContactID(cursor.getString(1));
                time.setName(cursor.getString(3));
                time.setSignature(cursor.getString(4));
                timeSelected.add(time);
            } while (cursor.moveToNext());
        }
        return timeSelected;
    }

    public int counttimeSheetTao(String jobid) {
        int m = 0;
        String selectQuery = "SELECT  * FROM " + TB_TIMESHEETLOCAL + " where JobID = " + jobid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                m++;
            } while (cursor.moveToNext());
        }
        return m;
    }

    public ArrayList<Timesheet_Upload> getAllTimeByID(String jobid, String contactID) {
        ArrayList<Timesheet_Upload> timeSelected = new ArrayList<Timesheet_Upload>();
        String selectQuery = "SELECT  * FROM " + TB_TIMESHEETS_UPLOAD + " where JobID = " + jobid + " AND WorkerID = " + contactID;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Timesheet_Upload time = new Timesheet_Upload();
                time.setJobID(cursor.getString(1));
                time.setWorkerID(cursor.getString(2));
                time.setShiftID(cursor.getString(3));
                time.setTravelStartDateTime(cursor.getString(4));
                time.setTravelStartDateTimeEnd(cursor.getString(5));
                time.setTravelStartDistance(cursor.getString(6));
                time.setJobStartDateTime(cursor.getString(7));
                time.setJobFinishDateTime(cursor.getString(8));
                time.setBreakStartDateTime(cursor.getString(9));
                time.setBreakFinishDateTime(cursor.getString(10));
                time.setTravelFinishDateTimeStart(cursor.getString(11));
                time.setTravelFinishDateTime(cursor.getString(12));
                time.setTravelFinishDistance(cursor.getString(13));
                time.setFatigueCompliance(cursor.getString(14));
                time.setAttendDepot(cursor.getString(15));
                time.setRegoNo(cursor.getString(16));
                time.setTraveledKilometers(cursor.getString(17));
                time.setOdometer(cursor.getString(17));
                time.setIsHaveMeal(cursor.getString(18));
                timeSelected.add(time);
            } while (cursor.moveToNext());
        }
        return timeSelected;
    }

    public ArrayList<Timesheet_Upload> getAllTime(String jobid) {
        ArrayList<Timesheet_Upload> timeSelected = new ArrayList<Timesheet_Upload>();
        String selectQuery = "SELECT  * FROM " + TB_TIMESHEETS_UPLOAD + " where JobID = " + jobid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Timesheet_Upload time = new Timesheet_Upload();

                time.setJobID(cursor.getString(1));
                time.setWorkerID(cursor.getString(2));
                time.setShiftID(cursor.getString(3));
                time.setTravelStartDateTime(cursor.getString(4));
                time.setTravelStartDateTimeEnd(cursor.getString(5));
                time.setTravelStartDistance(cursor.getString(6));
                time.setJobStartDateTime(cursor.getString(7));
                time.setJobFinishDateTime(cursor.getString(8));
                time.setBreakStartDateTime(cursor.getString(9));
                time.setBreakFinishDateTime(cursor.getString(10));
                time.setTravelFinishDateTimeStart(cursor.getString(11));
                time.setTravelFinishDateTime(cursor.getString(12));
                time.setTravelFinishDistance(cursor.getString(13));
                time.setFatigueCompliance(cursor.getString(14));
                time.setAttendDepot(cursor.getString(15));
                time.setRegoNo(cursor.getString(16));
                time.setTraveledKilometers(cursor.getString(17));
                time.setOdometer(cursor.getString(17));
                time.setIsHaveMeal(cursor.getString(18));

                timeSelected.add(time);

            } while (cursor.moveToNext());
        }
        return timeSelected;
    }

    public boolean checkTimesheetUploadExisting(String JobID, String WorkerID) {
        boolean d;
        String selectQuery = "SELECT  1 FROM " + TB_TIMESHEETS_UPLOAD + " where JobID = " + JobID + " AND  WorkerID = " + WorkerID;
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                d = true;
            } while (cursor.moveToNext());
        } else {
            d = false;
        }
        return d;
    }

    public void addreUploadJob(String jobid) {
        if (ishavereUploadJob(jobid)) {

        } else {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("JobID", jobid);
            db.insert(reUploadJob, null, values);
            db.close();
        }
    }

    public boolean ishavereUploadJob(String JobID) {
        boolean d;
        String selectQuery = "SELECT  1 FROM " + reUploadJob + " where JobID = " + JobID;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                d = true;
            } while (cursor.moveToNext());
        } else {
            d = false;
        }
        return d;
    }

    public void addAdditionalHazard(siteChecklistAddmodel site, String ID, String position) {
        if (!isAdditionalHazardExist(ID, position)) {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("JobID", ID);
            values.put("additionalHazzard", site.getAddHazard());
            values.put("measures", site.getControlMeasures());
            values.put("position", position);
            String te = "null";

            String c = site.getInitialRisk() + "i";
            String d = site.getResidualRisk() + "i";

            if (!c.equals("nulli")) {
                if (site.getInitialRisk().equals("0")) {
                    te = "High";
                } else if (site.getInitialRisk().equals("1")) {
                    te = "Med";
                } else if (site.getInitialRisk().equals("2")) {
                    te = "Low";
                } else {
                    te = site.getInitialRisk();
                }
            } else {
                te = site.getInitialRisk();
            }

            String te2 = "null";
            if (!d.equals("nulli")) {
                if (site.getResidualRisk().equals("0")) {
                    te2 = "High";
                } else if (site.getResidualRisk().equals("1")) {
                    te2 = "Med";
                } else if (site.getResidualRisk().equals("2")) {
                    te2 = "Low";
                } else {
                    te2 = site.getResidualRisk();
                }
            } else {
                te2 = site.getResidualRisk();
            }

            values.put("initialrisk", te);
            values.put("residual", te2);

            Log.e("add aditional i", te + "sdfd" + position);
            Log.e("add aditional re", te2 + "sdfd" + position);

            db.insert(AdditionalHazard, null, values);
            db.close();

        } else {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("additionalHazzard", site.getAddHazard());
            values.put("initialrisk", site.getInitialRisk());
            values.put("measures", site.getControlMeasures());
            values.put("residual", site.getResidualRisk());
            Log.e("add aditional i ---wer", site.getInitialRisk());
            Log.e("add aditional re ---wer", site.getResidualRisk());
            db.update(AdditionalHazard, values, "position = " + position + " AND JobID = ?",
                    new String[]{String.valueOf(ID)});
            db.close();
        }
    }

    public void removeAdditionalHazard2(String JobID) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + AdditionalHazard + " WHERE JobID = " + JobID);
        db.close();
    }

    public void removeAdditionalHazard(String JobID, String position) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + AdditionalHazard + " WHERE JobID = " + JobID + " AND position = " + position);
        db.close();
    }

    public ArrayList<siteChecklistAddmodel> getAdditionalHazard(String ID) {
        ArrayList<siteChecklistAddmodel> sites = new ArrayList<siteChecklistAddmodel>();
        String selectQuery = "SELECT  * FROM " + AdditionalHazard + " where JobID = " + ID;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                siteChecklistAddmodel si = new siteChecklistAddmodel();
                si.setJobID(cursor.getString(1));
                si.setAddHazard(cursor.getString(2));

                if (cursor.getString(3) != null) {
                    Log.e("checker-mo", cursor.getString(3));
                    if (cursor.getString(3).equals("0")) {
                        si.setInitialRisk("High");
                    } else if (cursor.getString(3).equals("1")) {
                        si.setInitialRisk("Med");
                    } else if (cursor.getString(3).equals("2")) {
                        si.setInitialRisk("Low");
                    } else {
                        si.setInitialRisk(cursor.getString(3));
                    }
                }

                if (cursor.getString(5) != null) {
                    Log.e("checker-mo", cursor.getString(5));
                    if (cursor.getString(5).equals("0")) {
                        si.setResidualRisk("High");
                    } else if (cursor.getString(5).equals("1")) {
                        si.setResidualRisk("Med");
                    } else if (cursor.getString(5).equals("2")) {
                        si.setResidualRisk("Low");
                    } else {
                        si.setResidualRisk(cursor.getString(5));
                    }
                }

                si.setControlMeasures(cursor.getString(4));

                si.setPosition(cursor.getString(6));
                sites.add(si);
            } while (cursor.moveToNext());
        }

        return sites;
    }

    public boolean isAdditionalHazardExist(String JobID, String position) {
        boolean d;
        String selectQuery = "SELECT  1 FROM " + AdditionalHazard + " where JobID = " + JobID + " AND  position = " + position;
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                d = true;
            } while (cursor.moveToNext());
        } else {
            d = false;
        }
        return d;
    }

    public void addSignage(SingageAudit signage) {
        if (isAlreadySignage(signage.getJobID(), signage.getCarriageWay())) {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("RegoNo", signage.getRegoNo());
            values.put("Landmark", signage.getLandmark());
            values.put("CarriageWay", signage.getCarriageWay());
            values.put("Direction", signage.getDirection());
            values.put("TimeErected", signage.getTimeErected());
            values.put("TimeCollected", signage.getTimeCollected());
            values.put("TimeChecked1", signage.getTimeChecked1());
            values.put("TimeChecked2", signage.getTimeChecked2());
            values.put("TimeChecked3", signage.getTimeChecked3());
            values.put("TimeChecked4", signage.getTimeChecked4());
            values.put("AuditSigns", signage.getAuditSigns());
            values.put("ErectedBy", signage.getErectedBy());
            values.put("CollectedBy", signage.getCollectedBy());
            db.update(Signage_Audit, values, "CarriageWay = " + signage.getCarriageWay() + " AND JobID = ?",
                    new String[]{String.valueOf(signage.getJobID())});
            db.close();
        } else {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("JobID", signage.getJobID());
            values.put("RegoNo", signage.getRegoNo());
            values.put("Landmark", signage.getLandmark());
            values.put("CarriageWay", signage.getCarriageWay());
            values.put("Direction", signage.getDirection());
            values.put("TimeErected", signage.getTimeErected());
            values.put("TimeCollected", signage.getTimeCollected());
            values.put("TimeChecked1", signage.getTimeChecked1());
            values.put("TimeChecked2", signage.getTimeChecked2());
            values.put("TimeChecked3", signage.getTimeChecked3());
            values.put("TimeChecked4", signage.getTimeChecked4());
            values.put("AuditSigns", signage.getAuditSigns());
            values.put("IsAfterCare", "0");
            values.put("ErectedBy", signage.getErectedBy());
            values.put("CollectedBy", signage.getCollectedBy());
            db.insert(Signage_Audit, null, values);
            db.close();
        }
    }

    public ArrayList<SingageAudit> getAllSignage(String jobid) {
        ArrayList<SingageAudit> signs = new ArrayList<SingageAudit>();
        String selectQuery = "SELECT  * FROM " + Signage_Audit + " where JobID = " + jobid + " AND CarriageWay = 1";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                SingageAudit signage = new SingageAudit();
                signage.set_id(cursor.getString(0));
                signage.setJobID(cursor.getString(1));
                signage.setRegoNo(cursor.getString(2));
                signage.setLandmark(cursor.getString(3));
                signage.setCarriageWay(cursor.getString(4));
                signage.setDirection(cursor.getString(5));
                signage.setTimeErected(cursor.getString(6));
                signage.setTimeCollected(cursor.getString(7));
                signage.setTimeChecked1(cursor.getString(8));
                signage.setTimeChecked2(cursor.getString(9));
                signage.setTimeChecked3(cursor.getString(10));
                signage.setTimeChecked4(cursor.getString(11));
                signage.setAuditSigns("sdfsdf");
                signage.setIsAfterCare(cursor.getString(13));
                signage.setErectedBy(cursor.getString(14));
                signage.setCollectedBy(cursor.getString(15));
                signs.add(signage);
            } while (cursor.moveToNext());
        }
        return signs;
    }

    public ArrayList<SingageAudit> getAllSignage2(String jobid) {
        ArrayList<SingageAudit> signs = new ArrayList<SingageAudit>();
        String selectQuery = "SELECT  * FROM " + Signage_Audit + " where JobID = " + jobid + " AND CarriageWay = 2";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                SingageAudit signage = new SingageAudit();
                signage.set_id(cursor.getString(0));
                signage.setJobID(cursor.getString(1));
                signage.setRegoNo(cursor.getString(2));
                signage.setLandmark(cursor.getString(3));
                signage.setCarriageWay(cursor.getString(4));
                signage.setDirection(cursor.getString(5));
                signage.setTimeErected(cursor.getString(6));
                signage.setTimeCollected(cursor.getString(7));
                signage.setTimeChecked1(cursor.getString(8));
                signage.setTimeChecked2(cursor.getString(9));
                signage.setTimeChecked3(cursor.getString(10));
                signage.setTimeChecked4(cursor.getString(11));
                signage.setAuditSigns(cursor.getString(12));
                signage.setIsAfterCare(cursor.getString(13));
                signage.setErectedBy(cursor.getString(14));
                signage.setCollectedBy(cursor.getString(15));
                signs.add(signage);
            } while (cursor.moveToNext());
        }
        return signs;
    }

    public int updateSignages(boolean isChecked, String Jobid, String CarriageWay) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        if (isChecked) {
            values.put("IsAfterCare", "1");
        } else {
            values.put("IsAfterCare", "0");
        }
        return db.update(Signage_Audit, values, " CarriageWay = " + CarriageWay + " AND JobID = ?",
                new String[]{String.valueOf(Jobid)});
    }

    public boolean isAlreadySignage(String JobID, String CarriageWay) {
        boolean d;
        String selectQuery = "SELECT  1 FROM " + Signage_Audit + " where JobID = " + JobID + " AND CarriageWay = " + CarriageWay;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                d = true;
            } while (cursor.moveToNext());
        } else {
            d = false;
        }
        return d;
    }

    public boolean isAlreadySignage2(String JobID, String CarriageWay) {
        boolean d;
        String selectQuery = "SELECT  1 FROM " + Signs + " where JobID = " + JobID + "  AND isSelected = 1";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                d = true;
            } while (cursor.moveToNext());
        } else {
            d = false;
        }
        return d;
    }

    public void addPhoto(String id, String url) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("jobid", id);
        values.put("image_url", url);
        values.put("IsUploaded", "0");
        db.insert(Photo, null, values);
        db.close(); // Closing database connection
    }

    public void addVideo(VideoModel video) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("jobid", "null");
        values.put("video_url", video.getVideo_url());
        values.put("isSeleted", "0");
        values.put("dateUploaded", video.getDateUploaded());
        values.put("geotag", video.getGeotag());
        values.put("subject", video.getSubject());
        values.put("note", video.getNote());

        values.put("startCapture", video.getStartCapture());
        values.put("endCapture", video.getEndCapture());
        values.put("startLocation", video.getStartLocation());
        values.put("endLocation", video.getEndLocation());

        db.insert(VideoGallery, null, values);
        db.close(); // Closing database connection
    }

    public int updatesubject(String video, String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("subject", video);
        return db.update(VideoGallery, values, " " + " _id = ?",
                new String[]{String.valueOf(id)});
    }

    public int updatenote(String video, String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("note", video);
        return db.update(VideoGallery, values, " " + " _id = ?",
                new String[]{String.valueOf(id)});
    }


    public ArrayList<VideoModel> getAllVideos() {
        ArrayList<VideoModel> videos = new ArrayList<VideoModel>();
        String selectQuery = "SELECT  * FROM " + VideoGallery + "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                VideoModel requirements = new VideoModel();
                requirements.set_id(cursor.getString(0));
                requirements.setJobid(cursor.getString(1));
                requirements.setVideo_url(cursor.getString(2));
                requirements.setIsSeleted(cursor.getString(3));
                requirements.setDateUploaded(cursor.getString(4));
                requirements.setNote(cursor.getString(7));
                requirements.setSubject(cursor.getString(6));

                requirements.setStartCapture(cursor.getString(8));
                requirements.setEndCapture(cursor.getString(9));
                requirements.setStartLocation(cursor.getString(10));
                requirements.setEndLocation(cursor.getString(11));
                videos.add(requirements);
            } while (cursor.moveToNext());
        }
        return videos;
    }

    public void removeVideo() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + VideoGallery);
        db.close();
    }


    public void addNotes(Notes notes) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + Notesz);
        ContentValues values = new ContentValues();
        values.put("status", notes.getStatus());
        values.put("note", notes.getNote());
        db.insert(Notesz, null, values);
        db.close(); // Closing database connection
    }

    public Notes getNotes() {
        Notes nos = new Notes();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + Notesz + "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                nos.setStatus(cursor.getString(1));
                nos.setNote(cursor.getString(2));
            } while (cursor.moveToNext());
        }
        return nos;
    }

    public void addJobStatusTable(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("jobid", id);
        values.put("status", "1");
        db.insert("isHaveClientSignature", null, values);
        db.close();
    }

    public int updateJobClientSignatureStatus(String jobid, boolean status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        if (status) {
            values.put("status", "1");
        } else {
            values.put("status", "0");
        }
        return db.update("isHaveClientSignature", values, " " + " jobid = ?",
                new String[]{String.valueOf(jobid)});
    }

    public String getClientStatus(String jobid) {
        String d = "";
        String selectQuery = "SELECT status FROM isHaveClientSignature where jobid = " + jobid + "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                d = cursor.getString(0);
            } while (cursor.moveToNext());
        } else {
            d = "null";
        }
        return d;
    }

    public int updateAttachedToRemove(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Attachment, "null");
        // updating row
        return db.update(TB_DOCKETS, values, "AttachmentType" + " = 1 AND JobID = ?",
                new String[]{String.valueOf(id)});
    }


    public ArrayList<PhotoItem> getAllPhotos(String id) {
        ArrayList<PhotoItem> poto = new ArrayList<PhotoItem>();
        String selectQuery = "SELECT  * FROM " + Photo + " where jobid = " + id + " AND IsUploaded = '0'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                PhotoItem requirements = new PhotoItem();
                requirements.setId(cursor.getString(1));
                requirements.setImage_url(cursor.getString(2));
                requirements.setIsUploaded(cursor.getString(3));
                poto.add(requirements);
            } while (cursor.moveToNext());
        }
        return poto;
    }

    public int updatePhotoStatz(String imgurl) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        if (!imgurl.equals("")) {
            values.put("IsUploaded", "0");
        }
        return db.update(Photo, values, " " + " image_url = ?",
                new String[]{String.valueOf(imgurl)});
    }

    public void removeImageById(String url) {
        Log.e("tes", " delete from " + Photo + " WHERE image_url = '" + url + "'");
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + Photo + " WHERE image_url = '" + url + "'");
        db.close();
    }

    public void addCreateDockets(String jobid, String operatorId, String AttachmentTypeID, String AttachedOn, String Attachmentd) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(JobID_d, jobid);
        values.put(OperatorID_d, operatorId);
        values.put(AttachmentTypeID_d, AttachmentTypeID);
        values.put(AttachedOn_d, "null");
        values.put(Attachment, "null");
        db.insert(TB_DOCKETS, null, values);
        db.close(); // Closing database connection

    }


    public void addotherfield(String jobid, String name, String email) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("jobid", jobid);
        values.put("name", name);
        values.put("email", email);
        db.insert(DOCKETOTHERFIELD, null, values);
        db.close(); // Closing database connection
    }

    public String getotherfield(String jobid) {
        String d = "";
        String selectQuery = "SELECT  * FROM  " + DOCKETOTHERFIELD + " where jobID = " + jobid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                d = cursor.getString(1) + "~" + cursor.getString(2) + "~" + cursor.getString(3);
            } while (cursor.moveToNext());
        } else {
            d = "wala";
        }
        return d;
    }

    public int updateotherfield(String jobid, String name, String email) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        if (!name.equals("")) {
            values.put("name", name);
        }
        if (!email.equals("")) {
            values.put("email", email);
        }
        return db.update(DOCKETOTHERFIELD, values, " " + " jobid = ?",
                new String[]{String.valueOf(jobid)});
    }

    public void hazardtb(String jobID, String hazard) {
        if (checkhazard(jobID) != null) {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            if (!hazard.equals("")) {
                values.put("hazard", hazard);
            }

            db.update("hazard", values, " jobID = ?",
                    new String[]{String.valueOf(jobID)});
            db.close();

        } else {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("jobID", jobID);
            values.put("hazard", hazard);
            db.insert("hazard", null, values);
            db.close(); // Closing database connection
        }
    }

    public String checkhazard(String jobid) {
        String d = "";
        String selectQuery = "SELECT  *  FROM hazard where JobID = " + jobid + "";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                d = cursor.getString(2);
            } while (cursor.moveToNext());

        } else {
            d = null;
        }
        return d;
    }


    public void addDummySiteChecklist(String jobID, String insertids) {
        if (getSiteChecklistDummy(jobID) != null) {

        } else {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("jobID", jobID);
            values.put("idselected", insertids);
            db.insert("DUMMYSITE", null, values);
            db.close(); // Closing database connection
        }
    }

    public int updateRequirementsDummy(String jobID, String val) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("idselected", val);
        // updating row
        return db.update("DUMMYSITE", values, " jobID = ?",
                new String[]{String.valueOf(jobID)});
    }

    public String getDummySiteChecklist(String jobID) {

        String temp = "";
        String selectQuery = "SELECT  idselected  FROM DUMMYSITE WHERE jobID = " + jobID;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                temp = cursor.getString(0);
            } while (cursor.moveToNext());
        } else {
            temp = null;
        }

        db.close();
        return temp;
    }


    public String getSiteChecklistDummy(String jobid) {
        String d = "";
        String selectQuery = "SELECT  *  FROM DUMMYSITE where JobID = " + jobid + "";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                d = cursor.getString(0);
            } while (cursor.moveToNext());

        } else {
            d = null;
        }
        db.close();
        return d;
    }

    public void addTimeSheetLocal(String jobid, String workerid, String shiftid, String start, String end) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("JobID", jobid);
        values.put("WorkerID", workerid);
        values.put("ShiftID", shiftid);
        values.put("JobStartDateTime", "null");
        values.put("JobFinishDateTime", "null");

        db.insert(TB_TIMESHEETLOCAL, null, values);
        db.close();
    }

    public void deleteifSuccess(String jobid) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from Dockets WHERE JobID = " + jobid);
        db.execSQL("delete from CreateJobs WHERE jobID = " + jobid);
        db.execSQL("delete from LocalTimesheets WHERE JobID = " + jobid);
        db.execSQL("delete from Timesheets WHERE JobID = " + jobid);
        db.execSQL("delete from DUMMYSITE WHERE jobID = " + jobid);
        db.execSQL("delete from hazard WHERE jobID = " + jobid);
        db.execSQL("delete from JobRequirements WHERE jobID = " + jobid);
        db.execSQL("delete from Jobs WHERE jobID = " + jobid);
        db.execSQL("delete from timesheet_signature WHERE jobID = " + jobid);
        db.execSQL("delete from " + Photo + " WHERE jobid = " + jobid);
        db.execSQL("delete from " + Signs + " WHERE jobid = " + jobid);
        db.execSQL("delete from " + AdditionalHazard + " WHERE jobid = " + jobid);
        db.execSQL("delete from " + Signage_Audit + " WHERE jobid = " + jobid);
        db.execSQL("delete from " + TB_TIMESHEETS_UPLOAD_SIG + " WHERE jobid = " + jobid);
        db.execSQL("delete from " + TB_TIMESHEETS_UPLOAD + " WHERE jobid = " + jobid);
        db.execSQL("delete from " + Signage_Audit + " WHERE jobid = " + jobid);
        db.close();
    }

    public void deleteifSuccess2() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from Dockets");
        db.execSQL("delete from CreateJobs");
        db.execSQL("delete from LocalTimesheets");
        db.execSQL("delete from Timesheets");
        db.execSQL("delete from DUMMYSITE");
        db.execSQL("delete from hazard");
        db.execSQL("delete from JobRequirements");
        db.execSQL("delete from Jobs");
        db.execSQL("delete from timesheet_signature");
        db.execSQL("delete from " + Photo);
        db.close();
    }


    public void addCreateJobs(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("jobID", id);
        values.put("Status", "0");
        values.put("aftercare", "null");
        db.insert(TB_CREATE_JOB, null, values);
        db.close(); // Closing database connection
    }

    // Adding new job
    public void addRequirements(ModelRequirements requirements, String newJobID) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Description_r, requirements.getDescription());
        values.put(RowID, requirements.getRowID());

        if (newJobID.equals("null")) {
            values.put(JobID_r, requirements.getJobID());
        } else {
            values.put(JobID_r, newJobID);
        }
        values.put(Quantity_r, requirements.getQuantity());
        values.put(RequirementID_r, requirements.getRequirementID());
        // Inserting Row
        db.insert(TB_JOBREQUIREMENTS, null, values);
        db.close(); // Closing database connection
    }

    // Adding new job
    public void addDocketAttachments(DocketAttachments docketAttachments) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("Caption", docketAttachments.getCaption());
        values.put("ExternalLink", docketAttachments.getExternalLink());
        values.put("ItemId", docketAttachments.getItemId());
        values.put("JobId", docketAttachments.getJobId());
        values.put("RowID", docketAttachments.getRowID());
        values.put("AppLink", docketAttachments.getAppLink());

        // Inserting Row
        db.insert(External, null, values);
        db.close(); // Closing database connection
    }


    // Adding new job
    public void addDocketAttachments2(DocketAttachments docketAttachments, String neid) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("Caption", docketAttachments.getCaption());
        values.put("ExternalLink", docketAttachments.getExternalLink());
        values.put("ItemId", docketAttachments.getItemId());
        values.put("JobId", neid);
        values.put("RowID", docketAttachments.getRowID());
        values.put("AppLink", docketAttachments.getAppLink());

        // Inserting Row
        db.insert(External, null, values);
        db.close(); // Closing database connection
    }

    // Adding new job
    public void addTimesheets(Timesheet Timesheet, String newJobID) {
        String idtoCheck;

        if (newJobID.equals("")) {
            idtoCheck = Timesheet.getJobID();
        } else {
            idtoCheck = newJobID;
        }

        if (checkifTimesheetExist(idtoCheck, Timesheet.getContactID()).equals("null") && newJobID != Timesheet.getJobID()) {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(ContactID_t, Timesheet.getContactID());
            values.put(EndDateTime_t, Timesheet.getEndDateTime());
            values.put(StartDateTime_t, Timesheet.getStartDateTime());
            values.put(Id_t, Timesheet.getId());

            if (newJobID.equals("")) {
                values.put(JobID_t, Timesheet.getJobID());
            } else {
                values.put(JobID_t, newJobID);
            }

            values.put(ShiftLocation_t, Timesheet.getShiftLocation());
            values.put(StartDateTimeFormat_t, Timesheet.getStartDateTimeFormat());
            values.put(EndDateTimeFormat_t, Timesheet.getEndDateTimeFormat());
            values.put(Name_t, Timesheet.getName());
            values.put(Description_t, Timesheet.getDescription());
            values.put("WorkerID", Timesheet.getWorkerID());

            // Inserting Row
            db.insert(TB_TIMESHEETS, null, values);
            db.close(); // Closing database connection

        } else {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();

            values.put(ContactID_t, Timesheet.getContactID());
            values.put(EndDateTime_t, Timesheet.getEndDateTime());
            values.put(StartDateTime_t, Timesheet.getStartDateTime());
            values.put(Id_t, Timesheet.getId());

            if (newJobID.equals("")) {
                values.put(JobID_t, Timesheet.getJobID());
            } else {
                values.put(JobID_t, newJobID);
            }

            values.put(ShiftLocation_t, Timesheet.getShiftLocation());
            values.put(StartDateTimeFormat_t, Timesheet.getStartDateTimeFormat());
            values.put(EndDateTimeFormat_t, Timesheet.getEndDateTimeFormat());
            values.put(Name_t, Timesheet.getName());
            values.put(Description_t, Timesheet.getDescription());
            values.put("WorkerID", Timesheet.getWorkerID());

            // updating row
            db.update(TB_TIMESHEETS, values, "jobID" + " = ?",
                    new String[]{String.valueOf(newJobID)});

        }
    }

    // Adding new job
    public void addTimesheets2(Timesheet Timesheet, String newJobID) {

        String idtoCheck;

        if (newJobID.equals("")) {
            idtoCheck = Timesheet.getJobID();
        } else {
            idtoCheck = newJobID;
        }

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ContactID_t, Timesheet.getContactID());
        values.put(EndDateTime_t, Timesheet.getEndDateTime());
        values.put(StartDateTime_t, Timesheet.getStartDateTime());
        values.put(Id_t, Timesheet.getId());

        if (newJobID.equals("")) {
            values.put(JobID_t, Timesheet.getJobID());
        } else {
            values.put(JobID_t, newJobID);
        }

        values.put(ShiftLocation_t, Timesheet.getShiftLocation());
        values.put(StartDateTimeFormat_t, Timesheet.getStartDateTimeFormat());
        values.put(EndDateTimeFormat_t, Timesheet.getEndDateTimeFormat());
        values.put(Name_t, Timesheet.getName());
        values.put(Description_t, Timesheet.getDescription());
        values.put("WorkerID", Timesheet.getWorkerID());

        // Inserting Row
        db.insert(TB_TIMESHEETS, null, values);
        db.close(); // Closing database connection
    }


    public String checkifTimesheetExist(String jobid, String contact) {
        String d = "";
        String selectQuery = "SELECT  *  FROM " + TB_TIMESHEETS + " where JobID = " + jobid + " AND  ContactID = " + contact;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                d = cursor.getString(0);
            } while (cursor.moveToNext());
        } else {
            d = "null";
        }
        return d;
    }

    public void addSings(SignagesObject signages, String id, String mode) {
        for (int i = 0; i < signages.getSignages().size(); i++) {
            if (checkifSignexist(id, signages.getSignages().get(i).getImage(), mode)) {

            } else {
                SQLiteDatabase db = this.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put("jobid", id);
                values.put("image", signages.getSignages().get(i).getImage());
                values.put("after_care", "0");
                values.put("isSelected", signages.getSignages().get(i).isSelected());
                values.put("typemode", mode);
                db.insert(Signs, null, values);
                db.close(); // Closing database connection8
            }
        }
    }

    public void updateSigns(SignagesObject signages, String id, String mode) {
        SignagesObject signs = new SignagesObject(getAllSignages(id));

        SQLiteDatabase db2 = this.getWritableDatabase();
        ContentValues values2 = new ContentValues();
        values2.put("isSelected", "0");
        db2.update(Signs, values2, " typemode =  " + mode + " AND jobid  = ?",
                new String[]{String.valueOf(id)});
        db2.close(); // Closing database connection

        for (int i = 0; i < signages.getSignages().size(); i++) {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("metress", signages.getSignages().get(i).getMetress());
            values.put("quantity", signages.getSignages().get(i).getQuantity());
            values.put("after_care", signages.getSignages().get(i).getAfter_care());
            values.put("metress2", signages.getSignages().get(i).getMetress2());
            values.put("quantity2", signages.getSignages().get(i).getQuantity2());
            if (signages.getSignages().get(i).isSelected()) {
                values.put("isSelected", "1");
            } else {
                values.put("isSelected", "0");
            }
            db.update(Signs, values, " image =  " + signages.getSignages().get(i).getImage() + " AND typemode =  " + mode + " AND jobid  = ?",
                    new String[]{String.valueOf(id)});
            db.close(); // Closing database connection
        }
    }

    public boolean checkifSignexist(String jobid, String Image, String mode) {
        boolean d;
        String selectQuery = "SELECT  *  FROM " + Signs + " where JobID = " + jobid + " AND image = " + Image + " AND typemode = " + mode;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                d = true;
            } while (cursor.moveToNext());
        } else {
            d = false;
        }
        return d;
    }

    // Adding new job
    public void addContact(Job job, String subJob) {
        Log.e("Equal ba sila?", subJob + " " + job.getJobID() + " -- " + checkifJobExist(job.getJobID()));
        String idtoCheck;

        if (subJob.equals("")) {
            idtoCheck = job.getJobID();
        } else {
            idtoCheck = subJob;
        }

        if (checkifJobExist(idtoCheck).equals("null") && subJob != job.getJobID()) {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(id, job.getId());
            values.put(code, job.getCode());

            if (!subJob.equals("")) {
                values.put(jobId, subJob);
            } else {
                values.put(jobId, job.getJobID());
            }

            values.put(additionalNotes, job.getAdditionalNotes());
            values.put(authorizerID, job.getAuthorizerID());
            values.put(contactID, job.getContactID());
            values.put(depotID, job.getDepotID());
            values.put(jobCode, job.getJobCode());
            values.put(endDateTime, job.getEndDateTime());
            values.put(foremanID, job.getForemanID());
            values.put(operatorID, job.getOperatorID());
            values.put(orderNumber, job.getOrderNumber());
            values.put(parentJobID, job.getJobID());
            values.put(startDateTime, job.getStartDateTime());
            values.put(startType, job.getStartDateTime());
            values.put(contactPerson, job.getContactPerson());
            values.put(authorizedPerson, job.getAuthorizedPerson());
            values.put(address, job.getAddress());
            values.put(depot, job.getDepot());
            values.put(companyName, job.getCompanyName());
            values.put(startDateTimeFormat, job.getStartDateTimeFormat());
            values.put(endDateTimeFormat, job.getEndDateTimeFormat());
            values.put(State, job.getState());
            values.put("ContactMobilePhone", job.getContactMobilePhone());
            values.put("ContactPhone", job.getContactPerson());
            values.put("ContactEmail", job.getContactEmail());
            values.put("JobShiftStartDateTime", job.getJobShiftStartDateTime());
            values.put("JobShiftStartDateTimeFormat", job.getJobShiftStartDateTimeFormat());
            values.put("JobShiftEndDateTime", job.getJobShiftEndDateTime());
            values.put("JobShiftEndDateTimeFormat", job.getJobShiftEndDateTimeFormat());
            values.put("CreateDateTimeFormat", job.getCreateDateTimeFormat());
            values.put("ContactMobilePhone", job.getContactMobilePhone());
            values.put(Status, "");

            // Inserting Row
            db.insert(TB_JOBS, null, values);
            db.close(); // Closing database connection

        } else {
            Log.e("mayroon", "mayroon");
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();

            values.put(id, job.getId());
            values.put(code, job.getCode());

            if (!subJob.equals("")) {
                values.put(jobId, subJob);
            } else {
                values.put(jobId, subJob);
            }

            values.put(additionalNotes, job.getAdditionalNotes());
            values.put(authorizerID, job.getAuthorizerID());
            values.put(contactID, job.getContactID());
            values.put(depotID, job.getDepotID());
            values.put(jobCode, job.getJobCode());
            values.put(endDateTime, job.getEndDateTime());
            values.put(foremanID, job.getForemanID());
            values.put(operatorID, job.getOperatorID());
            values.put(orderNumber, job.getOrderNumber());
            values.put(parentJobID, job.getJobID());
            values.put(startDateTime, job.getStartDateTime());
            values.put(startType, job.getStartDateTime());
            values.put(contactPerson, job.getContactPerson());
            values.put(authorizedPerson, job.getAuthorizedPerson());
            values.put(address, job.getAddress());
            values.put(depot, job.getDepot());
            values.put(companyName, job.getCompanyName());
            values.put(startDateTimeFormat, job.getStartDateTimeFormat());
            values.put(endDateTimeFormat, job.getEndDateTimeFormat());
            values.put(State, job.getState());
            values.put("ContactMobilePhone", job.getContactMobilePhone());
            values.put("ContactPhone", job.getContactPerson());
            values.put("ContactEmail", job.getContactEmail());
            values.put("JobShiftStartDateTime", job.getJobShiftStartDateTime());
            values.put("JobShiftStartDateTimeFormat", job.getJobShiftStartDateTimeFormat());
            values.put("JobShiftEndDateTime", job.getJobShiftEndDateTime());
            values.put("JobShiftEndDateTimeFormat", job.getJobShiftEndDateTimeFormat());
            values.put("CreateDateTimeFormat", job.getCreateDateTimeFormat());
            values.put("ContactMobilePhone", job.getContactMobilePhone());
            values.put(Status, "");

            if (!subJob.equals("")) {
                values.put(SubJob, subJob);
            }

            // updating row
            db.update(TB_JOBS, values, "jobID" + " = ?",
                    new String[]{String.valueOf(subJob)});
            db.close(); // Closing database connection
        }
    }

    // Adding new job
    public void addContact2(Job job, String subJob) {
        Log.e("Equal ba sila?", subJob + " " + job.getJobID() + " -- " + checkifJobExist(job.getJobID()));
        String idtoCheck;

        if (subJob.equals("")) {
            idtoCheck = job.getJobID();
        } else {
            idtoCheck = subJob;
        }

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(id, job.getId());
        values.put(code, job.getCode());

        if (!subJob.equals("")) {
            values.put(jobId, subJob);
        } else {
            values.put(jobId, job.getJobID());
        }

        values.put(additionalNotes, job.getAdditionalNotes());
        values.put(authorizerID, job.getAuthorizerID());
        values.put(contactID, job.getContactID());
        values.put(depotID, job.getDepotID());
        values.put(jobCode, job.getJobCode());
        values.put(endDateTime, job.getEndDateTime());
        values.put(foremanID, job.getForemanID());
        values.put(operatorID, job.getOperatorID());
        values.put(orderNumber, job.getOrderNumber());
        values.put(parentJobID, job.getJobID());
        values.put(startDateTime, job.getStartDateTime());
        values.put(startType, job.getStartDateTime());
        values.put(contactPerson, job.getContactPerson());
        values.put(authorizedPerson, job.getAuthorizedPerson());
        values.put(address, job.getAddress());
        values.put(depot, job.getDepot());
        values.put(companyName, job.getCompanyName());
        values.put(startDateTimeFormat, job.getStartDateTimeFormat());
        values.put(endDateTimeFormat, job.getEndDateTimeFormat());
        values.put(State, job.getState());
        values.put("ContactMobilePhone", job.getContactMobilePhone());
        values.put("ContactPhone", job.getContactPerson());
        values.put("ContactEmail", job.getContactEmail());
        values.put("JobShiftStartDateTime", job.getJobShiftStartDateTime());
        values.put("JobShiftStartDateTimeFormat", job.getJobShiftStartDateTimeFormat());
        values.put("JobShiftEndDateTime", job.getJobShiftEndDateTime());
        values.put("JobShiftEndDateTimeFormat", job.getJobShiftEndDateTimeFormat());
        values.put("CreateDateTimeFormat", job.getCreateDateTimeFormat());
        values.put("ContactMobilePhone", job.getContactMobilePhone());
        values.put(Status, "");

        // Inserting Row
        db.insert(TB_JOBS, null, values);
        db.close(); // Closing database connection
    }


    public String checkifJobExist(String jobid) {
        String d = "";
        String selectQuery = "SELECT  *  FROM " + TB_JOBS + " where JobID = " + jobid + "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                d = cursor.getString(0);
            } while (cursor.moveToNext());
        } else {
            d = "null";
        }
        return d;
    }


    public int updatejob(String newJobid, String oldJobID) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        Log.e("id", newJobid + " " + oldJobID);
        if (!newJobid.equals("")) {
            values.put("jobId", newJobid);
        }

        return db.update(TB_JOBS, values, "SubJob" + " = ?",
                new String[]{String.valueOf(oldJobID)});
    }

    public void deleteChecklist() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TB_CHECKLIST);
        db.close();
    }

    public String getJobStatus(String status) {
        String ret;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TB_CREATE_JOB, new String[]{"jobID"}, "jobID" + "=?",
                new String[]{status}, null, null, null, null);
        if (cursor.moveToFirst()) {
            ret = cursor.getString(0);
        } else {
            ret = "a";
        }
        return ret;
    }

    public void timeTcDelete(String jobid, String workerId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TB_TIMESHEETS + " WHERE JobID = '" + jobid + "' AND ContactID ='" + workerId + "'");
        db.execSQL("delete from " + TB_TIMESHEETS_UPLOAD_SIG + " WHERE jobID = '" + jobid + "' AND contactID ='" + workerId + "'");
        db.execSQL("delete from " + TB_TIMESHEETLOCAL + " WHERE JobID = '" + jobid + "' AND WorkerID ='" + workerId + "'");
        db.close();
    }


    public TimeSheetObjects getallTimesheets(String id) {
        TimeSheetObjects temp = new TimeSheetObjects();
        ArrayList<Timesheet> timesheets = new ArrayList<Timesheet>();
        String selectQuery = "SELECT  * FROM " + TB_TIMESHEETS + " where JobID = " + id;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Timesheet timesshet = new Timesheet();
                timesshet.setContactID(cursor.getString(1));
                timesshet.setEndDateTime(cursor.getString(2));
                timesshet.setId(cursor.getString(3));
                timesshet.setJobID(cursor.getString(4));
                timesshet.setShiftLocation(cursor.getString(5));
                timesshet.setStartDateTime(cursor.getString(6));
                timesshet.setStartType(cursor.getString(7));
                timesshet.setStartDateTimeFormat(cursor.getString(8));
                timesshet.setEndDateTimeFormat(cursor.getString(9));
                timesshet.setName(cursor.getString(10));
                timesshet.setDescription(cursor.getString(11));
                timesheets.add(timesshet);

            } while (cursor.moveToNext());
        }
        temp = new TimeSheetObjects(timesheets);
        return temp;
    }

    public ModelRequirementsObjects getallRequirements(String id) {
        ModelRequirementsObjects temp = new ModelRequirementsObjects();
        ArrayList<ModelRequirements> requirementlist = new ArrayList<ModelRequirements>();

        String selectQuery = "SELECT  * FROM " + TB_JOBREQUIREMENTS + " where JobID = " + id;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int b = 0;
        if (cursor.moveToFirst()) {

            do {
                ModelRequirements requirements = new ModelRequirements();
                b++;
                requirements.setRowID(cursor.getString(1));
                requirements.setDescription(cursor.getString(2));
                requirements.setJobID(cursor.getString(3));
                requirements.setQuantity(cursor.getString(4));
                requirements.setRequirementID(cursor.getString(5));
                requirementlist.add(requirements);
            } while (cursor.moveToNext());

        }
        temp = new ModelRequirementsObjects(requirementlist);
        return temp;
    }


    public DocketAttachmentsObject getAllDocketAttachments(String id) {
        DocketAttachmentsObject temp = new DocketAttachmentsObject();
        ArrayList<DocketAttachments> docketAttachments = new ArrayList<DocketAttachments>();

        String selectQuery = "SELECT  * FROM " + External + " where JobID = " + id;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int b = 0;
        if (cursor.moveToFirst()) {
            do {
                DocketAttachments docketAttachmentsitem = new DocketAttachments();
                b++;
                docketAttachmentsitem.setCaption(cursor.getString(1));
                docketAttachmentsitem.setExternalLink(cursor.getString(2));
                docketAttachmentsitem.setItemId(cursor.getString(3));
                docketAttachmentsitem.setJobId(cursor.getString(4));
                docketAttachmentsitem.setRowID(cursor.getString(5));
                docketAttachmentsitem.setAppLink(cursor.getString(6));
                docketAttachments.add(docketAttachmentsitem);
            } while (cursor.moveToNext());

        }
        temp = new DocketAttachmentsObject(docketAttachments);
        return temp;
    }


    public String getAlllocalTimesheet(String jobid, String workerid) {
        String d = "";
        String selectQuery = "SELECT  * FROM " + TB_TIMESHEETLOCAL + " where JobID = " + jobid + " " + " AND WorkerID = " + workerid +
                "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                d = cursor.getString(4) + "~" + cursor.getString(5) + "~" + cursor.getString(6) + "~" + cursor.getString(7) + "~" + cursor.getString(8) + "~" + cursor.getString(9) + "~" + cursor.getString(10) + "~" + cursor.getString(11) + "~" + cursor.getString(12) + "~" + cursor.getString(13) + "~" + cursor.getString(14) + "~" + cursor.getString(15) + "~" + cursor.getString(16) + "~" + cursor.getString(17) + "~" + cursor.getString(18);
            } while (cursor.moveToNext());
        } else {
            d = null;
        }
        return d;
    }

    public String getAlllocalTimesheetMul2(String jobid) {
        String d = "";
        String selectQuery = "SELECT  * FROM " + TB_TIMESHEETLOCAL + " where JobID = " + jobid + "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                d = d + cursor.getString(4) + "~" + cursor.getString(5) + "~" + cursor.getString(6) + "~" + cursor.getString(7) + "~" + cursor.getString(8) + "~" + cursor.getString(9) + "~" + cursor.getString(10) + "~" + cursor.getString(11) + "~" + cursor.getString(12) + "~" + cursor.getString(13) + "~" + cursor.getString(14) + "~" + cursor.getString(15) + "~" + cursor.getString(16) + "^^^^";
            } while (cursor.moveToNext());
        } else {
            d = null;
        }
        return d;
    }


    public int getTimesheetSignature(String jobid) {

        String selectQuery = "SELECT  * FROM timesheet_signature where JobID = " + jobid + "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int d = 0;
        if (cursor.moveToFirst()) {
            do {
                d++;
            } while (cursor.moveToNext());

        } else {
            d = 0;
        }
        return d;
    }

    public String getCountTime(String jobid) {
        String d = "";
        String selectQuery = "SELECT  * FROM " + TB_TIMESHEETLOCAL + " where JobID = " + jobid + " " +
                "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int counter = 0;
        if (cursor.moveToFirst()) {
            do {
                counter++;
            } while (cursor.moveToNext());

        } else {
            counter = 0;
        }
        return counter + "";
    }


    public String checkExistCreateJob(String jobID, String operatorId) {
        String d = "";

        String selectQuery = "SELECT  1 FROM " + TB_DOCKETS + " where JobID = " + jobID + " " +
                " AND AttachmentType = " + operatorId + "";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                d = "mayroon";
            } while (cursor.moveToNext());

        } else {
            d = null;
        }
        return d;
    }


    public String checkExistCreateJObs(String jobID) {
        String d = "";
        String selectQuery = "SELECT  1 FROM " + TB_CREATE_JOB + " where JobID = " + jobID + " AND Status = 1";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {

            do {
                d = "mayroon";
            } while (cursor.moveToNext());

        } else {
            d = null;
        }
        return d;
    }

    public String checkExistCreateJObs2(String jobID) {
        String d = "";
        String selectQuery = "SELECT  1 FROM " + TB_CREATE_JOB + " where JobID = " + jobID + " AND Status = 0";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                d = "mayroon";
            } while (cursor.moveToNext());
        } else {
            d = null;
        }
        return d;
    }


    public String checkifExist(String JobID, String rowID) {

        String d = "";
        String selectQuery = "SELECT  1 FROM " + TB_JOBREQUIREMENTS + " where JobID = " + JobID + " AND RowID = " + rowID;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                d = "mayroon";
            } while (cursor.moveToNext());

        } else {
            d = null;
        }
        return d;

    }

    public String checkDocketAttachments(String JobID, String rowID) {

        String d = "";
        String selectQuery = "SELECT  1 FROM " + External + " where JobID = " + JobID + " AND ItemId = " + rowID;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                d = "mayroon";
            } while (cursor.moveToNext());

        } else {
            d = null;
        }
        return d;
    }


    public String checkifExistLocalTime(String JobID, String WorkerID) {

        String d = "";
        String selectQuery = "SELECT  1 FROM " + TB_TIMESHEETLOCAL + " where JobID = " + JobID + " AND WorkerID = " + WorkerID;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                d = "mayroon";
            } while (cursor.moveToNext());
        } else {
            d = null;
        }

        return d;
    }

    public String gelSingleDocket(String jobid, String attachedID) {
        String d = "";
        String selectQuery = "SELECT  * FROM " + TB_DOCKETS + " where JobID = " + jobid + " AND AttachmentType = '" + attachedID + "'";
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                d = d + cursor.getString(1) + "|~~~~|" + cursor.getString(2) + "|~~~~|" + cursor.getString(3) + "|~~~~|" + cursor.getString(4) + "|~~~~|" + cursor.getString(5);
            } while (cursor.moveToNext());

        } else {
            d = null;
        }
        return d;
    }

    public String getJobDockets(String jobid) {
        String d = "";
        String selectQuery = "SELECT  * FROM " + TB_DOCKETS + " where JobID = " + jobid + "";
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                if (cursor.isAfterLast()) {
                    d = d + cursor.getString(1) + "|~~~~|" + cursor.getString(2) + "|~~~~|" + cursor.getString(3) + "|~~~~|" + cursor.getString(4) + "|~~~~|" + cursor.getString(5);
                } else {
                    d = d + cursor.getString(1) + "|~~~~|" + cursor.getString(2) + "|~~~~|" + cursor.getString(3) + "|~~~~|" + cursor.getString(4) + "|~~~~|" + cursor.getString(5) + "^^^^^^^^";
                }
            } while (cursor.moveToNext());

        } else {
            d = null;
        }
        return d;
    }


    public String getDcoketsClient(String jobid) {
        String d = "";
        String selectQuery = "SELECT  * FROM " + TB_DOCKETS + " where JobID = " + jobid + "";
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                d = d + cursor.getString(1) + "|~~~~|" + cursor.getString(2) + "|~~~~|" + cursor.getString(3) + "|~~~~|" + cursor.getString(4) + "|~~~~|" + cursor.getString(5) + "^^^";
            } while (cursor.moveToNext());
        } else {
            d = null;
        }
        return d;
    }


    public String getSiteChecklist(String jobid) {
        String d = "";
        String selectQuery = "SELECT  Section2  FROM " + TB_CREATE_JOB + " where JobID = " + jobid + "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                d = cursor.getString(0);
            } while (cursor.moveToNext());

        } else {
            d = null;
        }
        return d;
    }

    public String getTCAttachment(String jobid) {
        String temp = "";
        String selectQuery = "SELECT  Signatures  FROM " + TB_CREATE_JOB + " where JobID = " + jobid + " ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                temp = cursor.getString(0);
            } while (cursor.moveToNext());
        } else {
            temp = null;
        }
        return temp;
    }

    public String getSignageAudt(String jobid) {
        String temp = "";
        String selectQuery = "SELECT  Sections  FROM " + TB_CREATE_JOB + " where JobID = " + jobid + " ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                temp = cursor.getString(0);
            } while (cursor.moveToNext());
        } else {
            temp = null;
        }
        return temp;
    }

    public ArrayList<SignagesMeterModel> getAllSignages(String id) {
        ArrayList<SignagesMeterModel> sings = new ArrayList<SignagesMeterModel>();
        String selectQuery = "SELECT  * FROM " + Signs + " WHERE jobid = '" + id + "'  AND typemode = 1";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                SignagesMeterModel signitem = new SignagesMeterModel();
                signitem.set_id(cursor.getString(0));
                signitem.setJobid(cursor.getString(1));
                signitem.setImage(cursor.getString(2));
                signitem.setMetress(cursor.getString(3));
                signitem.setQuantity(cursor.getString(4));
                signitem.setAfter_care(cursor.getString(5));
                signitem.setMetress2(cursor.getString(6));
                signitem.setQuantity2(cursor.getString(7));

                if (cursor.getString(8).equals("1")) {
                    signitem.setIsSelected(true);
                } else {
                    signitem.setIsSelected(false);
                }
                signitem.setTypemode(cursor.getString(9));
                sings.add(signitem);
            } while (cursor.moveToNext());
        }
        return sings;
    }

    public boolean checkOktoupload(String jobid) {
        boolean isok = false;
        ArrayList<SingageAudit> signage = getAllSignage(jobid);
        if (!(signage.size() == 0)) {

            if ((signage.get(0).getLandmark() == null) || (signage.get(0).getLandmark().equals(""))) {
                isok = true;
                Log.e("getLandmark", "1");
            }
            if (isAlreadySignage2(jobid, signage.get(0).getCarriageWay())) {
                if ((signage.get(0).getTimeErected() == null) || (signage.get(0).getTimeErected().equals(""))) {
                    isok = true;
                    Log.e("getTimeErected", "1");
                }
                if ((signage.get(0).getTimeCollected() == null) || (signage.get(0).getTimeCollected().equals(""))) {
                    isok = true;
                    Log.e("getTimeCollected", "1");
                }
            }

            if (checkSignages(jobid)) {
                isok = true;
            }


        } else {
            isok = true;
        }
        return isok;
    }

    public boolean checkSignages(String id) {
        boolean isok = false;

        ArrayList<SignagesMeterModel> sings1 = getSelectedOnly(getAllSignages(id));
        ArrayList<SignagesMeterModel> sings2 = getSelectedOnly(getAllSignages2(id));
//        ArrayList<SingageAudit> signage = db.getAllSignage2(id);
        Gson s = new Gson();

        if (!(s.toJson(sings1).toString().equals("[]")) || !(s.toJson(sings2).toString().equals("[]"))) {
            isok = false;
        } else {
            isok = true;
        }

        Log.e("databases checks", s.toJson(sings2) + " =-=-= " + s.toJson(sings1));
        return isok;
    }

    public ArrayList<SignagesMeterModel> getSelectedOnly(ArrayList<SignagesMeterModel> mm) {
        ArrayList<SignagesMeterModel> mmd = new ArrayList<>();
        for (int c = 0; c < mm.size(); c++) {
            if (mm.get(c).isSelected()) {
                mmd.add(mm.get(c));
            } else {

            }
        }
        return mmd;
    }

    public String getAllSelectedSignstoUpload(String JobID, String typemode) {
        String d = "";
        String selectQuery = "SELECT  * FROM " + Signs + " where JobID = " + JobID + "  AND typemode = " + typemode + "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int mc = 0;
        if (cursor.moveToFirst()) {
            do {
                mc++;
                if (cursor.isLast()) {
                    if (cursor.getString(8).equals("1")) {
                        d = d + "SignID:" + mc + "^Metres:" + cursor.getString(3) + "^Qty:" + cursor.getString(4) + "^AfterCare:" + cursor.getString(5) + "^AfterCareMetres:" + cursor.getString(6) + "^AfterCareQty:" + cursor.getString(7);
                    }
                } else {
                    if (cursor.getString(8).equals("1")) {
                        d = d + "SignID:" + mc + "^Metres:" + cursor.getString(3) + "^Qty:" + cursor.getString(4) + "^AfterCare:" + cursor.getString(5) + "^AfterCareMetres:" + cursor.getString(6) + "^AfterCareQty:" + cursor.getString(7) + "~";
                    }
                }
            } while (cursor.moveToNext());
        } else {
            d = "null";
        }
        return d;
    }

    public ArrayList<SignagesMeterModel> getAllSignages2(String id) {
        ArrayList<SignagesMeterModel> sings = new ArrayList<SignagesMeterModel>();
        String selectQuery = "SELECT  * FROM " + Signs + " WHERE jobid = '" + id + "' AND typemode = 0";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                SignagesMeterModel signitem = new SignagesMeterModel();
                signitem.set_id(cursor.getString(0));
                signitem.setJobid(cursor.getString(1));
                signitem.setImage(cursor.getString(2));
                signitem.setMetress(cursor.getString(3));
                signitem.setQuantity(cursor.getString(4));
                signitem.setAfter_care(cursor.getString(5));
                signitem.setMetress2(cursor.getString(6));
                signitem.setQuantity2(cursor.getString(7));
                if (cursor.getString(8).equals("1")) {
                    signitem.setIsSelected(true);
                } else {
                    signitem.setIsSelected(false);
                }
                signitem.setTypemode(cursor.getString(9));
                sings.add(signitem);
            } while (cursor.moveToNext());
        }
        return sings;
    }

    public ArrayList<SignagesMeterModel> getAllSignagesSelected(String id) {
        ArrayList<SignagesMeterModel> sings = new ArrayList<SignagesMeterModel>();
        String selectQuery = "SELECT  * FROM " + Signs + " WHERE jobid = '" + id + "' AND isSelected = 1";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                SignagesMeterModel signitem = new SignagesMeterModel();
                signitem.setJobid(cursor.getString(1));
                signitem.setImage(cursor.getString(2));
                signitem.setMetress(cursor.getString(3));
                signitem.setQuantity(cursor.getString(4));
                signitem.setAfter_care(cursor.getString(5));
                signitem.setMetress2(cursor.getString(6));
                signitem.setQuantity2(cursor.getString(7));
                signitem.setIsSelected(Boolean.parseBoolean(cursor.getString(8)));
                sings.add(signitem);
            } while (cursor.moveToNext());
        }
        return sings;
    }

    // Getting All Contacts
    public JobObjects getAllJobs() {
        JobObjects temp = new JobObjects();
        ArrayList<Job> jobList = new ArrayList<Job>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TB_JOBS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Job job = new Job();
                job.setId(cursor.getString(1)); // Contact Name
                job.setCode(cursor.getString(2)); // Contact Name
                job.setJobID(cursor.getString(3)); // Contact Name
                job.setAdditionalNotes(cursor.getString(4)); // Contact Name
                job.setAuthorizerID(cursor.getString(5)); // Contact Name
                job.setContactID(cursor.getString(6)); // Contact Name
                job.setDepotID(cursor.getString(7)); // Contact Name
                job.setJobCode(cursor.getString(8)); // Contact Name
                job.setEndDateTime(cursor.getString(9)); // Contact Name
                job.setForemanID(cursor.getString(10)); // Contact Name
                job.setOperatorID(cursor.getString(11)); // Contact Name
                job.setOrderNumber(cursor.getString(12)); // Contact Name
                job.setParentJobID(cursor.getString(13)); // Contact Name
                job.setStartDateTime(cursor.getString(14)); // Contact Name
                job.setStartType(cursor.getString(15)); // Contact Namec
                job.setContactPerson(cursor.getString(16)); // Contact Name
                job.setAuthorizedPerson(cursor.getString(17)); // Contact Name
                job.setAddress(cursor.getString(18)); // Contact Name
                job.setDepot(cursor.getString(19)); // Contact Name
                job.setCompanyName(cursor.getString(20)); // Contact Name
                job.setStartDateTimeFormat(cursor.getString(21)); // Contact Name
                job.setEndDateTimeFormat(cursor.getString(22)); // Contact Name
                job.setState(cursor.getString(23)); // Contact Name
                job.setContactMobilePhone(cursor.getString(24));
                job.setContactPhone(cursor.getString(25));
                job.setContactEmail(cursor.getString(26));
                job.setJobShiftStartDateTime(cursor.getString(28));
                job.setJobShiftStartDateTimeFormat(cursor.getString(29));
                job.setJobShiftEndDateTime(cursor.getString(30));
                job.setJobShiftEndDateTimeFormat(cursor.getString(31));
                job.setCreateDateTime(cursor.getString(32));
                job.setCreateDateTimeFormat(cursor.getString(33));

                // Adding contact to list
                jobList.add(job);
            } while (cursor.moveToNext());
        }
        temp = new JobObjects(jobList);
        // return contact list
        return temp;
    }

    // Getting All Contacts
    public JobObjects getAllJobsID(String jobid) {
        JobObjects temp = new JobObjects();
        ArrayList<Job> jobList = new ArrayList<Job>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TB_JOBS + " WHERE jobId = '" + jobid + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                Job job = new Job();
                job.setId(cursor.getString(1)); // Contact Name
                job.setCode(cursor.getString(2)); // Contact Name
                job.setJobID(cursor.getString(3)); // Contact Name
                job.setAdditionalNotes(cursor.getString(4)); // Contact Name
                job.setAuthorizerID(cursor.getString(5)); // Contact Name
                job.setContactID(cursor.getString(6)); // Contact Name
                job.setDepotID(cursor.getString(7)); // Contact Name
                job.setJobCode(cursor.getString(8)); // Contact Name
                job.setEndDateTime(cursor.getString(9)); // Contact Name
                job.setForemanID(cursor.getString(10)); // Contact Name
                job.setOperatorID(cursor.getString(11)); // Contact Name
                job.setOrderNumber(cursor.getString(12)); // Contact Name
                job.setParentJobID(cursor.getString(13)); // Contact Name
                job.setStartDateTime(cursor.getString(14)); // Contact Name
                job.setStartType(cursor.getString(15)); // Contact Name
                job.setContactPerson(cursor.getString(16)); // Contact Name
                job.setAuthorizedPerson(cursor.getString(17)); // Contact Name
                job.setAddress(cursor.getString(18)); // Contact Name
                job.setDepot(cursor.getString(19));
                job.setCompanyName(cursor.getString(20));
                job.setStartDateTimeFormat(cursor.getString(21));
                job.setEndDateTimeFormat(cursor.getString(22));
                job.setState(cursor.getString(23));
                job.setContactMobilePhone(cursor.getString(24));
                job.setContactPhone(cursor.getString(25));
                job.setContactEmail(cursor.getString(26));
                job.setJobShiftStartDateTime(cursor.getString(28));
                job.setJobShiftStartDateTimeFormat(cursor.getString(29));
                job.setJobShiftEndDateTime(cursor.getString(30));
                job.setJobShiftEndDateTimeFormat(cursor.getString(31));
                job.setCreateDateTime(cursor.getString(32));
                job.setCreateDateTimeFormat(cursor.getString(33));

                // Adding contact to list
                jobList.add(job);
            } while (cursor.moveToNext());
        }
        temp = new JobObjects(jobList);

        // return contact list
        return temp;
    }


    //    // Updating single contact
    public int updateContact(String ID, String section1, String section2, String signature, String Sections, String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        if (!section1.equals("")) {
            values.put("Section1", section1);
        }
        if (!section2.equals("")) {
            values.put("Section2", section2);
        }
        if (!signature.equals("")) {
            values.put("Signatures", signature);
        }
        if (!Sections.equals("")) {
            values.put("Sections", Sections);
        }
        if (!status.equals("")) {
            values.put("Status", status);
        }

        // updating row
        return db.update(TB_CREATE_JOB, values, "jobID" + " = ?",
                new String[]{String.valueOf(ID)});
    }

    public int updateAfterCare(String ID, String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        if (!status.equals("")) {
            values.put("aftercare", status);
        }

        return db.update(TB_CREATE_JOB, values, "jobID" + " = ?",
                new String[]{String.valueOf(ID)});
    }


    public String getaftercare(String jobid) {
        String temp = "";
        String selectQuery = "SELECT  aftercare  FROM " + TB_CREATE_JOB + " where JobID = " + jobid + " ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                temp = cursor.getString(0);
            } while (cursor.moveToNext());
        } else {
            temp = "null";
        }
        return temp;
    }


    public int updateRequirements(String jobID, String quantity, String val) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Quantity_r, val);
        return db.update(TB_JOBREQUIREMENTS, values, "JobID" + " = " + jobID + " AND RowID = ?",
                new String[]{String.valueOf(quantity)});
    }


    public int updateordernumber(String jobID, String val) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("orderNumber", val);
        return db.update("Jobs", values, "JobID" + " = ?",
                new String[]{String.valueOf(jobID)});
    }


    public int updateDockets(String AttachementTypeID, String AttachedOn, String Attachmentx, String jobIDs) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        if (!AttachedOn.equals("")) {
            values.put(AttachedOn_d, AttachedOn);
        }

        if (!Attachmentx.equals(null)) {
            values.put(Attachment, Attachmentx);
        }

        return db.update(TB_DOCKETS, values, "AttachmentType" + " = " + AttachementTypeID + " AND JobID = ?",
                new String[]{String.valueOf(jobIDs)});

    }

    public void timesheetsign(String jobId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from timeSheetOtherField WHERE jobid = '" + jobId + "'");
        db.close();
    }

    public void deleteTimesheetInfoEdited(String jobid, String ContactID) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from timesheet_signature WHERE jobID = '" + jobid + "' AND contactID = '" + ContactID + "'");
        db.close();
    }

    public void timeDatepicker() {

    }
}

