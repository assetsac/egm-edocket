package com.evolution.egm.Utils;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.evolution.egm.Activities.LoginActivity;
import com.evolution.egm.Activities.MainPage;

import java.util.HashMap;


/**
 * Created by mcclynreyarboleda on 1/27/15.
 */

public class SessionManager {

    private Context context;
    private SharedPreferences sharedPreferences;
    private SharedPreferences sharedPreferences2;
    private SharedPreferences sharedPreferences3;
    private SharedPreferences tempVideoInfo;
    private Editor tempVideoInfoeditor;
    private Editor editor;
    private Editor recentlogin;
    private Editor locationEditor;
    DatabaseHelper db;
    private static final int PRIVATE_MODE = 0;

    public SessionManager(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(
                Constants.SHAREDPREFERENCE, PRIVATE_MODE);
        editor = sharedPreferences.edit();

        sharedPreferences2 = context.getSharedPreferences(
                Constants.SHAREDPREFERENCE2, PRIVATE_MODE);

        recentlogin = sharedPreferences2.edit();

        sharedPreferences3 = context.getSharedPreferences(
                Constants.SHAREDPREFERENCE2, PRIVATE_MODE);

        tempVideoInfo = context.getSharedPreferences(
                "tempVideoInfo", PRIVATE_MODE);
        tempVideoInfoeditor = tempVideoInfo.edit();

        locationEditor = sharedPreferences3.edit();
        db = new DatabaseHelper(context);
    }

    public void createLoginSession(HashMap<String, String> data) {
        editor.putString(Constants.KEY_USERNAME,
                data.get(Constants.KEY_USERNAME));
        editor.putString(Constants.KEY_PASSWORD,
                data.get(Constants.KEY_PASSWORD));
        editor.putString(Constants.KEY_TOKEN, data.get(Constants.KEY_TOKEN));
        editor.putString("docushareCookie", data.get("docushareCookie"));
        editor.putBoolean(Constants.KEY_LOGGED_IN, true);
        editor.commit();
    }


    public void createVideoInfo(HashMap<String, String> data) {
        tempVideoInfoeditor.putString("tempStarttime", data.get("tempStarttime"));
        tempVideoInfoeditor.putString("tempLocation", data.get("tempLocation"));
        tempVideoInfoeditor.commit();
    }

    public String getTemStartime() {
        return tempVideoInfo.getString("tempStarttime", "");
    }

    public String getTemStarloc() {
        return tempVideoInfo.getString("tempLocation", "");
    }

    public void createRecentID(HashMap<String, String> data) {
        recentlogin.putString("id", data.get("id"));
        recentlogin.commit();
    }

    public void createLocaltionData(String location) {
        locationEditor.putString("location", location);
        locationEditor.commit();
    }

    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(Constants.KEY_LOGGED_IN, false);
    }

    public String getlocation() {
        return sharedPreferences2.getString("location", "");
    }

    public String getRecentID() {
        return sharedPreferences2.getString("id", "");
    }

    public String getToken() {
        return sharedPreferences.getString(Constants.KEY_TOKEN, "");
    }

    public String getUsername() {
        return sharedPreferences.getString(Constants.KEY_USERNAME, "");
    }

    public String getPassword() {
        return sharedPreferences.getString(Constants.KEY_PASSWORD, "");
    }

    public String getDocushareCookie() {
        return sharedPreferences.getString("docushareCookie", "");
    }

    public void logout() {
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("id", getUsername());
        createRecentID(data);
        editor.clear().commit();
        MainPage activity = (MainPage) context;
        Intent i = new Intent(context, LoginActivity.class);
        // set the new task and clear flags
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        activity.finish();
        activity.overridePendingTransition(0, 0);
        context.startActivity(i);
    }

}