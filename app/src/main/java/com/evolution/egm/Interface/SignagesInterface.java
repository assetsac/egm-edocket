package com.evolution.egm.Interface;

import com.evolution.egm.Models.SignagesMeterModel;

import java.util.ArrayList;

/**
 * Created by mcgwapo on 11/18/15.
 */
public interface SignagesInterface {
    public void returnNewSign(ArrayList<SignagesMeterModel> mDataset, String mode);
}
