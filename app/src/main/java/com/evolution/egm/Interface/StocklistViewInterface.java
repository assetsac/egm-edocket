package com.evolution.egm.Interface;

import com.evolution.egm.Models.Stocklist_Kit_Model;

/**
 * Created by mcgwapo on 10/12/15.
 */
public interface StocklistViewInterface {
     void viewstocks(Stocklist_Kit_Model stock);
     void deleteStocks(Stocklist_Kit_Model stock);
     void editStocks(Stocklist_Kit_Model stock);
}
