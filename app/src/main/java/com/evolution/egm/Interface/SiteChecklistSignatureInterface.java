package com.evolution.egm.Interface;

import com.evolution.egm.Models.SiteCheckListSignatureModel;

import java.util.ArrayList;

/**
 * Created by McClynrey on 3/2/2016.
 */
public interface SiteChecklistSignatureInterface {

    void updateSignature(ArrayList<SiteCheckListSignatureModel> mDataset);
}
