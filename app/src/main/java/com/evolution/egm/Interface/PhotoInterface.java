package com.evolution.egm.Interface;

/**
 * Created by mcgwapo on 10/12/15.
 */
public interface PhotoInterface {
    public void deletePhoto(String idtoDelete);
    public void viewPhoto(String idtoView);
    public void showHideMenu(boolean isvisible);
}
