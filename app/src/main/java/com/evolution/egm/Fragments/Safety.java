package com.evolution.egm.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evolution.egm.Activities.IncidentReportForm;
import com.evolution.egm.Adapters.AdapterSafetyForms;
import com.evolution.egm.Utils.RecyclerItemClickListener;
import com.evolution.egm.R;
import com.rey.material.app.Dialog;

/**
 * Created by mcclynreyarboleda on 4/13/15.
 */
public class Safety extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";

    String[] myDataset = {"Motor Vehicle Accident Report", "Hazard Identification Notice Risk \n Assessment Worksheet", "Incident Report Form", "Corrective Action / Improvement Form"};
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public Safety() {

    }

    public static Safety newInstance(int sectionNumber) {
        Safety fragment = new Safety();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_safety, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.safetyforms);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);


        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Dialog.Builder builder = null;
                        switch (position) {
                            case 0:
                            break;
                            case 1:

                                break;
                            case 2:
                                Intent m = new Intent(getActivity(), IncidentReportForm.class);
                                m.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(m);

                                break;
                            case 3:
                                break;
                        }
                    }
                })
        );

        mAdapter = new AdapterSafetyForms(myDataset, getActivity());
        mRecyclerView.setAdapter(mAdapter);

        return view;

    }
}
