package com.evolution.egm.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.evolution.egm.Activities.SiteCheckListNew;
import com.evolution.egm.Activities.JobView;
import com.evolution.egm.Adapters.AdapterJObs;
import com.evolution.egm.Utils.ConnectionDetector;
import com.evolution.egm.Utils.Constants;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Utils.DateHelper;
import com.evolution.egm.Utils.RecyclerItemClickListener;
import com.evolution.egm.Utils.SessionManager;
import com.evolution.egm.Interface.APIInterface;
import com.evolution.egm.Models.Job;
import com.evolution.egm.Models.JobObjects;
import com.evolution.egm.R;
import com.google.gson.Gson;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;
import com.rey.material.widget.FloatingActionButton;
import com.rey.material.widget.RippleManager;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by mcclynreyarboleda on 4/13/15.
 */
public class Jobs extends Fragment {

    public static final String TAG = Jobs.class.getSimpleName();
    ProgressBar progressBar3;
    private RecyclerView mRecyclerView;
    private AdapterJObs mAdapter;
    private LinearLayoutManager mLayoutManager;
    LinearLayout loadingarea, loadingnointernet, loadingnodata;
    private FloatingActionButton createjob;
    SwipeRefreshLayout swipeRefreshLayout;
    ProgressBar progressBar2;
    private String token;
    Dialog.Builder builder = null;
    DatabaseHelper db;
    ConnectionDetector connection;
    ArrayList<String> listofIDSubmitted;
    ArrayList<String> listofIDSubmitted2;

    public SessionManager session;
    int REQUEST_CODE = 1, RESULT_OK = 1;
    private ArrayList<Job> joblist = new ArrayList<Job>();
    TextView alternatetext;
    private static final String ARG_SECTION_NUMBER = "section_number";
    DateHelper date;
    ArrayList<String> workeridJOb;
    ArrayList<String> workeridJOb2;

    public Jobs() {

    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("", "Start");
    }

    @Override
    public void onResume() {
        super.onResume();
        joblist = db.getAllJobs().getmJob();
        Log.e("", "Resume");
        for (int i = 0; i < db.getAllJobs().getmJob().size(); i++) {
            deleteIfJobUploaded(db.getAllJobs().getmJob().get(i).getJobID());
        }
        refreshItems();
    }

    public void refreDATA() {
        mAdapter = new AdapterJObs(db.getAllJobs().getmJob(), getActivity());
        mRecyclerView.setAdapter(mAdapter);
    }

    public static Jobs newInstance(int sectionNumber) {
        Jobs fragment = new Jobs();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        date = new DateHelper();
        RippleManager mRippleManager = new RippleManager();
        listofIDSubmitted = new ArrayList<String>();

    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        session = new SessionManager(getActivity());
        token = session.getToken();
        db = new DatabaseHelper(getActivity());
        connection = new ConnectionDetector(getActivity());
    }

    public String deleteIfJobUploaded(final String jobid) {

        return "";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_jobs, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        progressBar2 = (ProgressBar) view.findViewById(R.id.progressBar2);

        alternatetext = (TextView) view.findViewById(R.id.alternatetext);
        createjob = (FloatingActionButton) view.findViewById(R.id.createNewjob);
        progressBar3 = (ProgressBar) view.findViewById(R.id.progressBar3);
        loadingarea = (LinearLayout) view.findViewById(R.id.loadingarea);
        loadingnodata = (LinearLayout) view.findViewById(R.id.loadingnodata);
        loadingnointernet = (LinearLayout) view.findViewById(R.id.loadingnointernet);

        LinearLayout reloagnojobfound = (LinearLayout) view.findViewById(R.id.reloagnojobfound);

        reloagnojobfound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar2.setVisibility(progressBar2.VISIBLE);
                refreshItems();
            }
        });

        LinearLayout reloadpagenow = (LinearLayout) view.findViewById(R.id.reloadpagenow);
        reloadpagenow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar2.setVisibility(progressBar2.VISIBLE);
                refreshItems();
            }
        });

        createjob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent createJob = new Intent(getActivity(), SiteCheckListNew.class);
                startActivity(createJob);
            }
        });

        downloadFeed();

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Log.e("Checker", db.getJobStatus(joblist.get(position).getJobID()));
                        if (db.getJobStatus(joblist.get(position).getJobID()).equals(joblist.get(position).getJobID())) {
                            Intent clickMe = new Intent(getActivity(), JobView.class);
                            clickMe.putExtra("data", Parcels.wrap(joblist.get(position)));
                            clickMe.putExtra("position", position + "");
                            clickMe.putExtra("SubJob", "c");
                            clickMe.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivityForResult(clickMe, REQUEST_CODE);
                        } else {
                            Intent clickMe = new Intent(getActivity(), JobView.class);
                            clickMe.putExtra("data", Parcels.wrap(joblist.get(position)));
                            clickMe.putExtra("SubJob", "c");
                            clickMe.putExtra("position", position + "");
                            clickMe.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivityForResult(clickMe, REQUEST_CODE);
                        }
                    }
                })
        );

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setProgressViewOffset(false, 0, 100);
        swipeRefreshLayout.setRefreshing(true);

        swipetoRefreshinit();
        return view;
    }

    public void swipetoRefreshinit() {
        // Setup refresh listener which triggers new data loading
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                downloadFeed();
            }
        });
        // Configure the refreshing colors
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    void refreshItems() {
        downloadFeed();
        swipeRefreshLayout.setRefreshing(false);
    }


    public void loadofline() {
        mAdapter = new AdapterJObs(db.getAllJobs().getmJob(), getActivity());
        mRecyclerView.setAdapter(mAdapter);
        joblist = db.getAllJobs().getmJob();
        if (db.getAllJobs().getmJob() == null) {
            progressBar3.setVisibility(progressBar3.VISIBLE);
            mRecyclerView.setVisibility(mRecyclerView.GONE);
            loadingarea.setVisibility(loadingarea.GONE);
            loadingnodata.setVisibility(loadingnodata.VISIBLE);
            loadingnointernet.setVisibility(loadingnointernet.GONE);
        } else {
            mAdapter = new AdapterJObs(db.getAllJobs().getmJob(), getActivity());
            mRecyclerView.setAdapter(mAdapter);
            progressBar3.setVisibility(progressBar3.VISIBLE);
            mRecyclerView.setVisibility(mRecyclerView.VISIBLE);
            loadingarea.setVisibility(loadingarea.GONE);
            loadingnodata.setVisibility(loadingnodata.GONE);
            loadingnointernet.setVisibility(loadingnointernet.GONE);
            progressBar3.setVisibility(progressBar3.GONE);
        }
        if (db.getAllJobs().getmJob().size() == 0) {
            progressBar3.setVisibility(progressBar3.VISIBLE);
            mRecyclerView.setVisibility(mRecyclerView.GONE);
            loadingarea.setVisibility(loadingarea.GONE);
            loadingnodata.setVisibility(loadingnodata.VISIBLE);
            loadingnointernet.setVisibility(loadingnointernet.GONE);
        }
    }

    public void downloadFeed() {
        if (!connection.isConnectedToInternet()) {
            mAdapter = new AdapterJObs(db.getAllJobs().getmJob(), getActivity());
            mRecyclerView.setAdapter(mAdapter);
            joblist = db.getAllJobs().getmJob();
            Log.e("counter job", db.getAllJobs().getmJob().size() + "  ");
            if (db.getAllJobs().getmJob() == null) {
                progressBar3.setVisibility(progressBar3.VISIBLE);
                mRecyclerView.setVisibility(mRecyclerView.GONE);
                loadingarea.setVisibility(loadingarea.GONE);
                loadingnodata.setVisibility(loadingnodata.VISIBLE);
                loadingnointernet.setVisibility(loadingnointernet.GONE);
            } else {
                mAdapter = new AdapterJObs(db.getAllJobs().getmJob(), getActivity());
                mRecyclerView.setAdapter(mAdapter);
                progressBar3.setVisibility(progressBar3.VISIBLE);
                mRecyclerView.setVisibility(mRecyclerView.VISIBLE);
                loadingarea.setVisibility(loadingarea.GONE);
                loadingnodata.setVisibility(loadingnodata.GONE);
                loadingnointernet.setVisibility(loadingnointernet.GONE);
                progressBar3.setVisibility(progressBar3.GONE);
            }
            if (db.getAllJobs().getmJob().size() == 0) {
                progressBar3.setVisibility(progressBar3.VISIBLE);
                mRecyclerView.setVisibility(mRecyclerView.GONE);
                loadingarea.setVisibility(loadingarea.GONE);
                loadingnodata.setVisibility(loadingnodata.VISIBLE);
                loadingnointernet.setVisibility(loadingnointernet.GONE);
            }
        } else {

            Map<String, String> params = new HashMap<>();
            params.put("geotag", session.getlocation());
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Constants.API_URL)
                    .build();
            APIInterface interfacem = restAdapter.create(APIInterface.class);
            interfacem.downloadActivities(session.getToken(), Constants.REQUEST_JOBINFO, params, new Callback<JobObjects>() {

                @Override
                public void success(JobObjects jobObjects, Response response) {
                    String jsonObjectResponse = new Gson().toJson(jobObjects);
                    Log.e("jobs", jsonObjectResponse);
                    if (jobObjects.getmJob() == null) {
                        Log.e("checker", "item checker a");
                        mAdapter = new AdapterJObs(db.getAllJobs().getmJob(), getActivity());
                        mRecyclerView.setAdapter(mAdapter);
                        joblist = db.getAllJobs().getmJob();

                        if (db.getAllJobs() != null) {

                            progressBar3.setVisibility(progressBar3.VISIBLE);
                            mRecyclerView.setVisibility(mRecyclerView.VISIBLE);
                            loadingarea.setVisibility(loadingarea.GONE);
                            loadingnodata.setVisibility(loadingnodata.GONE);
                            loadingnointernet.setVisibility(loadingnointernet.GONE);
                            progressBar3.setVisibility(progressBar3.GONE);

                            if (db.getAllJobs().getmJob().size() == 0) {
                                progressBar3.setVisibility(progressBar3.VISIBLE);
                                mRecyclerView.setVisibility(mRecyclerView.GONE);
                                loadingarea.setVisibility(loadingarea.GONE);
                                loadingnodata.setVisibility(loadingnodata.VISIBLE);
                                loadingnointernet.setVisibility(loadingnointernet.GONE);
                            }
                        } else {

                            Log.e("checker", "item checker 1");
                            mAdapter = new AdapterJObs(db.getAllJobs().getmJob(), getActivity());
                            mRecyclerView.setAdapter(mAdapter);
                            joblist = db.getAllJobs().getmJob();
                            if (db.getAllJobs().getmJob() == null) {
                                Log.e("checker", "item checker 2");
                                progressBar3.setVisibility(progressBar3.VISIBLE);
                                mRecyclerView.setVisibility(mRecyclerView.GONE);
                                loadingarea.setVisibility(loadingarea.GONE);
                                loadingnodata.setVisibility(loadingnodata.VISIBLE);
                                loadingnointernet.setVisibility(loadingnointernet.GONE);
                            } else {
                                Log.e("checker", "item checker 3");
                                mAdapter = new AdapterJObs(db.getAllJobs().getmJob(), getActivity());
                                mRecyclerView.setAdapter(mAdapter);
                                progressBar3.setVisibility(progressBar3.VISIBLE);
                                mRecyclerView.setVisibility(mRecyclerView.VISIBLE);
                                loadingarea.setVisibility(loadingarea.GONE);
                                loadingnodata.setVisibility(loadingnodata.GONE);
                                loadingnointernet.setVisibility(loadingnointernet.GONE);
                                progressBar3.setVisibility(progressBar3.GONE);
                            }

                            progressBar3.setVisibility(progressBar3.VISIBLE);
                            mRecyclerView.setVisibility(mRecyclerView.GONE);
                            loadingarea.setVisibility(loadingarea.GONE);
                            loadingnodata.setVisibility(loadingnodata.VISIBLE);
                            loadingnointernet.setVisibility(loadingnointernet.GONE);

                        }
                    } else {
                        mAdapter = new AdapterJObs(jobObjects.getmJob(), getActivity());
                        mRecyclerView.setAdapter(mAdapter);
                        joblist = jobObjects.getmJob();

                        deteteJob();

                        for (int i = 0; i < joblist.size(); i++) {
                            db.addContact(jobObjects.getmJob().get(i), "");
                            String ad = db.checkExistCreateJob(jobObjects.getmJob().get(i).getJobID(), "1");
                            String bd = db.checkExistCreateJob(jobObjects.getmJob().get(i).getJobID(), "2");

                            if (ad == null) {
                                db.addCreateDockets(jobObjects.getmJob().get(i).getJobID(), jobObjects.getmJob().get(i).getOperatorID(), "1", "", "");
                            }

                            if (bd == null) {
                                db.addCreateDockets(jobObjects.getmJob().get(i).getJobID(), jobObjects.getmJob().get(i).getOperatorID(), "2", "", "");
                            }

                            for (int b = 0; b < jobObjects.getmJob().get(i).getmRequirements().size(); b++) {
                                String a = db.checkifExist(jobObjects.getmJob().get(i).getmRequirements().get(b).getJobID(), jobObjects.getmJob().get(i).getmRequirements().get(b).getRowID());
                                if (a == null) {
                                    db.addRequirements(jobObjects.getmJob().get(i).getmRequirements().get(b), "null");
                                } else {
                                }
                            }

                            for (int c = 0; c < jobObjects.getmJob().get(i).getTimesheets().size(); c++) {
                                db.addTimesheets(jobObjects.getmJob().get(i).getTimesheets().get(c), "");
                                String d = db.checkifExistLocalTime(jobObjects.getmJob().get(i).getTimesheets().get(c).getJobID(), jobObjects.getmJob().get(i).getTimesheets().get(c).getContactID());
                                if (d == null) {
                                    db.addTimeSheetLocal(jobObjects.getmJob().get(i).getTimesheets().get(c).getJobID(), jobObjects.getmJob().get(i).getTimesheets().get(c).getContactID(), jobObjects.getmJob().get(i).getTimesheets().get(c).getId(), jobObjects.getmJob().get(i).getTimesheets().get(c).getStartDateTime(), jobObjects.getmJob().get(i).getTimesheets().get(c).getEndDateTime());
                                }
                            }

                            for (int c = 0; c < jobObjects.getmJob().get(i).getDocketAttachments().size(); c++) {
                                String a = db.checkDocketAttachments(jobObjects.getmJob().get(i).getDocketAttachments().get(c).getJobId(), jobObjects.getmJob().get(i).getDocketAttachments().get(c).getItemId());
                                if (a == null) {
                                    db.addDocketAttachments(jobObjects.getmJob().get(i).getDocketAttachments().get(c));
                                } else {

                                }
                            }

                            workeridJOb = new ArrayList<String>();
                            workeridJOb2 = new ArrayList<String>();

                            for (int xx = 0; xx < db.getallTimesheets(jobObjects.getmJob().get(i).getJobID()).getTimesheets().size(); xx++) {
                                workeridJOb.add(db.getallTimesheets(jobObjects.getmJob().get(i).getJobID()).getTimesheets().get(xx).getContactID());
                            }

                            for (int xx = 0; xx < jobObjects.getmJob().get(i).getTimesheets().size(); xx++) {
                                workeridJOb2.add(jobObjects.getmJob().get(i).getTimesheets().get(xx).getContactID());
                            }

                            if (db.checkExistCreateJObs(jobObjects.getmJob().get(i).getJobID()) == null) {
                                for (int rt = 0; rt < workeridJOb.size(); rt++) {
                                    if (workeridJOb2.contains(workeridJOb.get(rt))) {
                                        Log.e("may kapariha", "true-" + jobObjects.getmJob().get(i).getJobID());
                                    } else {
                                        Log.e("may kapariha", "false-" + jobObjects.getmJob().get(i).getJobID());
                                        db.timeTcDelete(jobObjects.getmJob().get(i).getJobID(), workeridJOb.get(rt));
                                    }
                                }
                            } else if (db.checkExistCreateJObs2(jobObjects.getmJob().get(i).getJobID()) != null) {

                            } else {

                            }
                        }

                        progressBar3.setVisibility(progressBar3.VISIBLE);
                        mRecyclerView.setVisibility(mRecyclerView.VISIBLE);
                        loadingarea.setVisibility(loadingarea.GONE);
                        loadingnodata.setVisibility(loadingnodata.GONE);
                        loadingnointernet.setVisibility(loadingnointernet.GONE);
                        progressBar3.setVisibility(progressBar3.GONE);

                        mAdapter = new AdapterJObs(db.getAllJobs().getmJob(), getActivity());
                        mRecyclerView.setAdapter(mAdapter);
                        joblist = db.getAllJobs().getmJob();
                        progressBar2.setVisibility(progressBar2.VISIBLE);

                        if (joblist.size() == 0) {
                            progressBar3.setVisibility(progressBar3.VISIBLE);
                            mRecyclerView.setVisibility(mRecyclerView.GONE);
                            loadingarea.setVisibility(loadingarea.GONE);
                            loadingnodata.setVisibility(loadingnodata.GONE);
                            alternatetext.setText("No Job Found");
                            loadingnointernet.setVisibility(loadingnointernet.VISIBLE);
                            Log.e("error", "No job found.");
                        }
                    }

                    swipeRefreshLayout.setRefreshing(false);
                }

                @Override
                public void failure(RetrofitError error) {
                    Response r = error.getResponse();
                    switch (error.getKind()) {
                        case NETWORK:
                            // TODO something
                            progressBar3.setVisibility(progressBar3.VISIBLE);
                            mRecyclerView.setVisibility(mRecyclerView.GONE);
                            loadingarea.setVisibility(loadingarea.GONE);
                            loadingnodata.setVisibility(loadingnodata.GONE);
                            alternatetext.setText("No Internet Connection");
                            loadingnointernet.setVisibility(loadingnointernet.VISIBLE);
                            Log.e("error", "error while connectiong to NETWORK");

                            break;
                        case UNEXPECTED:
                            Log.e("error", "error while connectiong to server");
                            throw error;
                        case HTTP:
                            switch (r.getStatus()) {
                                case 401:
                                    builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                                        @Override
                                        public void onPositiveActionClicked(DialogFragment fragment) {
                                            session.logout();
                                            super.onPositiveActionClicked(fragment);
                                        }
                                    };
                                    ((SimpleDialog.Builder) builder).message("Session Expired.\n" +
                                            "Please login again.")
                                            .positiveAction("Ok");
                                    DialogFragment fragment = DialogFragment.newInstance(builder);
                                    fragment.show(getFragmentManager(), null);
                                    fragment.setCancelable(false);
                                    break;
                                case 404:
                                    // TODO something
                                    progressBar3.setVisibility(progressBar3.VISIBLE);
                                    mRecyclerView.setVisibility(mRecyclerView.GONE);
                                    loadingarea.setVisibility(loadingarea.GONE);
                                    alternatetext.setText("Unable to connect to ManStat. \nPlease try again.");
                                    loadingnodata.setVisibility(loadingnodata.GONE);
                                    loadingnointernet.setVisibility(loadingnointernet.VISIBLE);

                                    loadofline();
                                    break;
                                case 500:
                                    // TODO something
                                    progressBar3.setVisibility(progressBar3.VISIBLE);
                                    mRecyclerView.setVisibility(mRecyclerView.GONE);
                                    loadingarea.setVisibility(loadingarea.GONE);
                                    alternatetext.setText("Unable to connect to ManStat. \nPlease try again.");
                                    loadingnodata.setVisibility(loadingnodata.GONE);
                                    loadingnointernet.setVisibility(loadingnointernet.VISIBLE);
                                    loadofline();
                                    break;
                            }

                            Log.e("error ", "error while connectiong to HTTP" + r.getStatus());
                            break;
                        default:

                            Log.e("error ", "error while connectiong to HTTP" + r.getStatus());
                            break;
                    }
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
        }
    }

    public void deteteJob() {
        db.deleteChecklist();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {

            mAdapter = new AdapterJObs(db.getAllJobs().getmJob(), getActivity());
            mRecyclerView.setAdapter(mAdapter);
            joblist = db.getAllJobs().getmJob();
        }
    }
}