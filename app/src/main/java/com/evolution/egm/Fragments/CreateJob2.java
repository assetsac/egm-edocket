package com.evolution.egm.Fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.evolution.egm.Adapters.AdapterSiteChecklist;
import com.evolution.egm.Utils.CreateJobSharedPreference;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Utils.SessionManager;
import com.evolution.egm.Interface.SiteChecklistInterface;
import com.evolution.egm.Models.CreateJobFisrt;
import com.evolution.egm.Models.Job;
import com.evolution.egm.Models.siteChecklistAddmodel;
import com.evolution.egm.R;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by mcclynreyarboleda on 4/13/15.
 */
public class CreateJob2 extends Fragment implements View.OnClickListener, RadioGroup.OnCheckedChangeListener, SiteChecklistInterface {

    RadioButton firstsegmented, secondsegmented, thirdsegmented;
    LinearLayout firstlayout, secondlayout, thirdlayout, ds, roadconfiguration, hazads;
    RelativeLayout nextfirst, nextButton, previos, save;
    private static final String ARG_SECTION_NUMBER = "section_number";
    CreateJobFisrt createJobFisrt;
    public OnnextandPre mListener;
    private static final ScheduledExecutorService worker =
            Executors.newSingleThreadScheduledExecutor();
    private String token;
    boolean dc = false;
    RadioGroup rgroup1, rgroup2, rgroup3m, rgroup4, rgroup5, rgroup6deleted, rgroup7, rgroup8, rgroup9, rgroup10, rgroup11, rgroup12, highlow, residual;
    RadioButton rb1, rb2, rb3, rb4, rb5, rb6, rb7, rb8, rb9, rb10, rb11, rb12;
    RadioButton rb1s, rb2s, rb3s, rb4s, rb5s, rb6s, rb7s, rb8s, rb9s, rb10s, rb11s, rb12s, rb13s;
    ScrollView sitecheclistscroll;
    RadioGroup layout3_1, layout3_2, layout3_3, layout3_4, layout3_5, layout3_6, layout3_7, layout3_8, layout3_9, layout3_10, layout3_11, layout3_12, layout3_13;
    public SessionManager session;
    CreateJobSharedPreference createjob;
    Job mJobObject;
    Bundle extras;
    DatabaseHelper db;
    EditText ifyesz, tcp, anyadd, texare;
    String returnDatam, returnDatam2, returnDatam3;
    RadioButton job2R1, job2R2, job2R3, last1, last2, swmsid1, swmsid2;
    boolean required1 = false, required2 = false, required3 = false;
    MenuItem update, savemen;
    RadioButton normal1, normal2, normal3, normal4, normal5, normal6, normal7, normal8, yesnote4, nonote4, yesnote3, nonote3, yesnote5, nonote5, yesnote6, nonote6, yesnote7, nonote7, yesnote8, nonote8, yesnote9, nonote9, yesnote10, nonote10, yesnote11, nonote11, yesnote12, nonote12;
    RadioGroup test1, test2, test3;
    String speedLimit = "";
    String speedlimitid = "";
    com.rengwuxian.materialedittext.MaterialEditText firstnote, note2, note3, note4, note5, note6, note7, note8, note9, note10, note11, note12;
    LinearLayout addhazard;
    boolean isAdditionaYes = false;
    boolean bnote1 = false, bnote2 = false, bnote3 = false, bnote4 = false, bnote5 = false, bnote6 = false, bnote7 = false, bnote8 = false, bnote9 = false, bnote10 = false, bnote11 = false, bnote12 = false, bnote13 = true;
    LinearLayout layouttoadd;
    public ArrayList<siteChecklistAddmodel> sites;
    LinearLayout.LayoutParams params;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    int iniHeight = 0;
    Display d;
    int width;

    public CreateJob2() {

    }

    public static CreateJob2 newInstance(int sectionNumber) {
        CreateJob2 fragment = new CreateJob2();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mJobObject = new Job();
        setHasOptionsMenu(true);
        extras = getActivity().getIntent().getExtras();

        if (extras != null) {
            mJobObject = Parcels.unwrap(getActivity().getIntent().getExtras().getParcelable("data"));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        session = new SessionManager(getActivity());
        token = session.getToken();
        createjob = new CreateJobSharedPreference(getActivity());

        db = new DatabaseHelper(getActivity());

        try {
            mListener = (OnnextandPre) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        }
        return file;
    }

    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        stream.close();
    }

    public boolean addSignatureToGallery(Bitmap signature, String filename) {
        boolean result = false;
        try {
            File photo = new File(getAlbumStorageDir("Evolution"), String.format(filename + ".jpg", System.currentTimeMillis()));
            saveBitmapToJPG(signature, photo);
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(photo);
            mediaScanIntent.setData(contentUri);
            getActivity().sendBroadcast(mediaScanIntent);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_createjob2, container, false);

        sitecheclistscroll = (ScrollView) view.findViewById(R.id.sitecheclistscroll);

        mLayoutManager = new LinearLayoutManager(getActivity());

        d = getActivity().getWindowManager().getDefaultDisplay();
        width = d.getWidth();

        sites = new ArrayList<siteChecklistAddmodel>();

        params = new
                LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.height = iniHeight;

        mRecyclerView = (RecyclerView) view.findViewById(R.id.addhazardlist);
        mRecyclerView.setLayoutParams(params);
        mLayoutManager = new LinearLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new AdapterSiteChecklist(db.getAdditionalHazard(mJobObject.getJobID()), getActivity(), this);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        for (int i = 0; i < db.getAdditionalHazard(mJobObject.getJobID()).size(); i++) {
            sites.add(db.getAdditionalHazard(mJobObject.getJobID()).get(i));
            int screenSize = getResources().getConfiguration().screenLayout &
                    Configuration.SCREENLAYOUT_SIZE_MASK;
            String toastMsg;
            switch (screenSize) {
                case Configuration.SCREENLAYOUT_SIZE_LARGE:
                    if (width < 854) {
                        iniHeight = iniHeight + 570;
                    } else if (width < 1201) {
                        Log.e("test", "1200");
                        iniHeight = iniHeight + 740;
                    } else {
                        iniHeight = iniHeight + 780;
                    }
                    break;
                case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                    if (width < 641) {
                        iniHeight = iniHeight + 640;
                    } else if (width < 721) {
                        iniHeight = iniHeight + 695;
                    } else if (width < 769) {
                        iniHeight = iniHeight + 688;
                    } else if (width < 801) {
                        iniHeight = iniHeight + 800;
                    } else if (width < 1081) {
                        iniHeight = iniHeight + 1080;
                    } else if (width < 1201) {
                        iniHeight = iniHeight + 1200;
                    } else if (width < 1441) {
                        iniHeight = iniHeight + 1440;
                    } else {
                        iniHeight = iniHeight + 1200;
                    }
                    break;
                case Configuration.SCREENLAYOUT_SIZE_SMALL:
                    iniHeight = iniHeight + 1400;
                    break;
                default:
                    iniHeight = iniHeight + 1200;
            }

            params.height = iniHeight;
        }

        mAdapter.notifyDataSetChanged();


        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        String toastMsg;

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        previos = (RelativeLayout) view.findViewById(R.id.previos);
        nextButton = (RelativeLayout) view.findViewById(R.id.nextButton);
        roadconfiguration = (LinearLayout) view.findViewById(R.id.roadconfiguration);
        hazads = (LinearLayout) view.findViewById(R.id.hazads);
        rgroup1 = (RadioGroup) view.findViewById(R.id.rgroup1);

        addhazard = (LinearLayout) view.findViewById(R.id.addhazard);
        addhazard.setOnClickListener(this);

        highlow = (RadioGroup) view.findViewById(R.id.highlow);
        residual = (RadioGroup) view.findViewById(R.id.residual);

        anyadd = (EditText) view.findViewById(R.id.anyadd);
        texare = (EditText) view.findViewById(R.id.texare);

        rgroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case R.id.job2R1:
                        Log.e("S", "1");
                        required1 = false;
                        rgroup2.clearCheck();
                        ds.setVisibility(ds.GONE);
                        break;
                    case R.id.job2R2:
                        Log.e("S", "2");
                        required1 = true;
                        ds.setVisibility(ds.VISIBLE);
                        break;
                    case R.id.job2R3:
                        Log.e("S", "3");
                        required1 = true;
                        ds.setVisibility(ds.VISIBLE);
                        break;
                }
            }
        });

        test1 = (RadioGroup) view.findViewById(R.id.test1);
        test2 = (RadioGroup) view.findViewById(R.id.test2);
        test3 = (RadioGroup) view.findViewById(R.id.test3);

        normal1 = (RadioButton) view.findViewById(R.id.normal1);
        normal2 = (RadioButton) view.findViewById(R.id.normal2);
        normal3 = (RadioButton) view.findViewById(R.id.normal3);
        normal4 = (RadioButton) view.findViewById(R.id.normal4);
        normal5 = (RadioButton) view.findViewById(R.id.normal5);
        normal6 = (RadioButton) view.findViewById(R.id.normal6);
        normal7 = (RadioButton) view.findViewById(R.id.normal7);
        normal8 = (RadioButton) view.findViewById(R.id.normal8);

        normal1.setOnClickListener(this);
        normal2.setOnClickListener(this);
        normal3.setOnClickListener(this);
        normal4.setOnClickListener(this);
        normal5.setOnClickListener(this);
        normal6.setOnClickListener(this);
        normal7.setOnClickListener(this);
        normal8.setOnClickListener(this);

        rgroup2 = (RadioGroup) view.findViewById(R.id.rgroup2);
        rgroup4 = (RadioGroup) view.findViewById(R.id.rgroup4);
        rgroup5 = (RadioGroup) view.findViewById(R.id.rgroup5);
        rgroup7 = (RadioGroup) view.findViewById(R.id.rgroup7);
        rgroup8 = (RadioGroup) view.findViewById(R.id.rgroup8);
        rgroup9 = (RadioGroup) view.findViewById(R.id.rgroup9);
        rgroup10 = (RadioGroup) view.findViewById(R.id.rgroup10);
        rgroup11 = (RadioGroup) view.findViewById(R.id.rgroup11);
        rgroup12 = (RadioGroup) view.findViewById(R.id.rgroup12);
        layout3_1 = (RadioGroup) view.findViewById(R.id.layout3_1);
        layout3_2 = (RadioGroup) view.findViewById(R.id.layout3_2);
        layout3_3 = (RadioGroup) view.findViewById(R.id.layout3_3);
        layout3_4 = (RadioGroup) view.findViewById(R.id.layout3_4);
        layout3_5 = (RadioGroup) view.findViewById(R.id.layout3_5);
        layout3_6 = (RadioGroup) view.findViewById(R.id.layout3_6);
        layout3_7 = (RadioGroup) view.findViewById(R.id.layout3_7);
        layout3_8 = (RadioGroup) view.findViewById(R.id.layout3_8);
        layout3_9 = (RadioGroup) view.findViewById(R.id.layout3_9);
        layout3_10 = (RadioGroup) view.findViewById(R.id.layout3_10);
        layout3_11 = (RadioGroup) view.findViewById(R.id.layout3_11);
        layout3_12 = (RadioGroup) view.findViewById(R.id.layout3_12);

        job2R1 = (RadioButton) view.findViewById(R.id.job2R1);
        job2R2 = (RadioButton) view.findViewById(R.id.job2R3);
        job2R3 = (RadioButton) view.findViewById(R.id.job2R3);
        last1 = (RadioButton) view.findViewById(R.id.last1);
        last2 = (RadioButton) view.findViewById(R.id.last2);

        yesnote4 = (RadioButton) view.findViewById(R.id.yesnote4);
        nonote4 = (RadioButton) view.findViewById(R.id.nonote4);
        yesnote3 = (RadioButton) view.findViewById(R.id.yesnote3);
        nonote3 = (RadioButton) view.findViewById(R.id.nonote3);
        yesnote5 = (RadioButton) view.findViewById(R.id.yesnote5);
        nonote5 = (RadioButton) view.findViewById(R.id.nonote5);
        yesnote6 = (RadioButton) view.findViewById(R.id.yesnote6);
        nonote6 = (RadioButton) view.findViewById(R.id.nonote6);
        yesnote7 = (RadioButton) view.findViewById(R.id.yesnote7);
        nonote7 = (RadioButton) view.findViewById(R.id.nonote7);
        yesnote8 = (RadioButton) view.findViewById(R.id.yesnote8);
        nonote8 = (RadioButton) view.findViewById(R.id.nonote8);
        yesnote9 = (RadioButton) view.findViewById(R.id.yesnote9);
        nonote9 = (RadioButton) view.findViewById(R.id.nonote9);
        yesnote10 = (RadioButton) view.findViewById(R.id.yesnote10);
        nonote10 = (RadioButton) view.findViewById(R.id.nonote10);
        yesnote11 = (RadioButton) view.findViewById(R.id.yesnote11);
        nonote11 = (RadioButton) view.findViewById(R.id.nonote11);
        yesnote12 = (RadioButton) view.findViewById(R.id.yesnote12);
        nonote12 = (RadioButton) view.findViewById(R.id.nonote12);

        ifyesz = (EditText) view.findViewById(R.id.ifyesz);

        firstnote = (com.rengwuxian.materialedittext.MaterialEditText) view.findViewById(R.id.firstnote);
        note2 = (com.rengwuxian.materialedittext.MaterialEditText) view.findViewById(R.id.note2);
        note3 = (com.rengwuxian.materialedittext.MaterialEditText) view.findViewById(R.id.note3);
        note4 = (com.rengwuxian.materialedittext.MaterialEditText) view.findViewById(R.id.note4);
        note5 = (com.rengwuxian.materialedittext.MaterialEditText) view.findViewById(R.id.note5);
        note6 = (com.rengwuxian.materialedittext.MaterialEditText) view.findViewById(R.id.note6);
        note7 = (com.rengwuxian.materialedittext.MaterialEditText) view.findViewById(R.id.note7);
        note8 = (com.rengwuxian.materialedittext.MaterialEditText) view.findViewById(R.id.note8);
        note9 = (com.rengwuxian.materialedittext.MaterialEditText) view.findViewById(R.id.note9);
        note10 = (com.rengwuxian.materialedittext.MaterialEditText) view.findViewById(R.id.note10);
        note11 = (com.rengwuxian.materialedittext.MaterialEditText) view.findViewById(R.id.note11);
        note12 = (com.rengwuxian.materialedittext.MaterialEditText) view.findViewById(R.id.note12);
//        note13 = (com.rengwuxian.materialedittext.MaterialEditText) view.findViewById(R.id.note13);

        tcp = (EditText) view.findViewById(R.id.tcp);
        final LinearLayout tcpnoshow = (LinearLayout) view.findViewById(R.id.tcpnoshow);
        layout3_13 = (RadioGroup) view.findViewById(R.id.layout3_13);

        layout3_1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

            }
        });

        layout3_1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case R.id.swmsid1:
                        bnote1 = false;
                        firstnote.setVisibility(firstnote.GONE);
                        break;
                    case R.id.swmsid2:
                        bnote1 = true;
                        firstnote.setVisibility(firstnote.VISIBLE);
                        break;
                }
            }
        });

        layout3_3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case R.id.yesnote3:
                        bnote3 = false;
                        note3.setVisibility(note3.GONE);
                        break;
                    case R.id.nonote3:
                        bnote3 = true;
                        note3.setVisibility(note3.VISIBLE);
                        break;
                }
            }
        });

        layout3_4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case R.id.yesnote4:
                        bnote4 = false;
                        note4.setVisibility(note4.GONE);
                        break;
                    case R.id.nonote4:
                        bnote4 = true;
                        note4.setVisibility(note4.VISIBLE);
                        break;
                }
            }
        });

        layout3_5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case R.id.yesnote5:
                        bnote5 = false;
                        note5.setVisibility(note5.GONE);
                        break;
                    case R.id.nonote5:
                        bnote5 = true;
                        note5.setVisibility(note5.VISIBLE);
                        break;
                }
            }
        });

        layout3_6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case R.id.yesnote6:
                        bnote6 = false;
                        note6.setVisibility(note6.GONE);
                        break;
                    case R.id.nonote6:
                        bnote6 = true;
                        note6.setVisibility(note6.VISIBLE);
                        break;
                }
            }
        });

        layout3_7.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case R.id.yesnote7:
                        bnote7 = false;
                        note7.setVisibility(note7.GONE);
                        break;
                    case R.id.nonote7:
                        bnote7 = true;
                        note7.setVisibility(note7.VISIBLE);
                        break;
                }
            }
        });


        layout3_8.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case R.id.yesnote8:
                        bnote8 = false;
                        note8.setVisibility(note8.GONE);
                        break;
                    case R.id.nonote8:
                        bnote8 = true;
                        note8.setVisibility(note8.VISIBLE);
                        break;
                }
            }
        });

        layout3_9.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case R.id.yesnote9:
                        bnote9 = false;
                        note9.setVisibility(note9.GONE);
                        break;
                    case R.id.nonote9:
                        bnote9 = true;
                        note9.setVisibility(note9.VISIBLE);

                        break;
                }
            }
        });


        layout3_10.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case R.id.yesnote10:
                        bnote10 = false;
                        note10.setVisibility(note10.GONE);
                        break;
                    case R.id.nonote10:
                        bnote10 = true;
                        note10.setVisibility(note10.VISIBLE);
                        break;
                }
            }
        });


        layout3_11.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case R.id.yesnote11:
                        bnote11 = false;
                        note11.setVisibility(note11.GONE);
                        break;
                    case R.id.nonote11:
                        bnote11 = true;
                        note11.setVisibility(note11.VISIBLE);
                        break;
                }
            }
        });


        layout3_12.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case R.id.yesnote12:
                        bnote12 = false;
                        note12.setVisibility(note12.GONE);
                        break;
                    case R.id.nonote12:
                        bnote12 = true;
                        note12.setVisibility(note12.VISIBLE);
                        break;
                }
            }
        });


        layout3_2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case R.id.tcpyes:
                        required3 = true;
                        bnote2 = false;
                        tcpnoshow.setVisibility(tcpnoshow.VISIBLE);
                        note2.setVisibility(note2.GONE);
                        break;
                    case R.id.tcpno:
                        required3 = false;
                        bnote2 = true;
                        tcpnoshow.setVisibility(tcpnoshow.GONE);
                        note2.setVisibility(note2.VISIBLE);
                        break;
                }
            }
        });

        layout3_13.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case R.id.last1:
                        required2 = true;
//                        bnote13 = false;
                        hazads.setVisibility(hazads.VISIBLE);
                        sitecheclistscroll.post(new Runnable() {
                            @Override
                            public void run() {
                                sitecheclistscroll.fullScroll(ScrollView.FOCUS_DOWN);
                            }
                        });
                        break;
                    case R.id.last2:
                        required2 = false;
                        hazads.setVisibility(hazads.GONE);
                        sitecheclistscroll.post(new Runnable() {
                            @Override
                            public void run() {
                                sitecheclistscroll.fullScroll(ScrollView.FOCUS_DOWN);
                            }
                        });
                        break;
                }
            }
        });

        rgroup12 = (RadioGroup) view.findViewById(R.id.rgroup12);
        firstsegmented = (RadioButton) view.findViewById(R.id.firstsegmented);
        secondsegmented = (RadioButton) view.findViewById(R.id.secondsegmented);
        thirdsegmented = (RadioButton) view.findViewById(R.id.thirdsegmented);
        firstlayout = (LinearLayout) view.findViewById(R.id.firstlayout);
        secondlayout = (LinearLayout) view.findViewById(R.id.secondlayout);
        thirdlayout = (LinearLayout) view.findViewById(R.id.thirdlayout);
        ds = (LinearLayout) view.findViewById(R.id.ds);

        fields();
        setCheckedTure();
        return view;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.firstsegmented:
                firstlayout.setVisibility(firstlayout.VISIBLE);
                secondlayout.setVisibility(secondlayout.GONE);
                thirdlayout.setVisibility(thirdlayout.GONE);
                sitecheclistscroll.fullScroll(ScrollView.FOCUS_UP);
                break;
            case R.id.secondsegmented:
                firstlayout.setVisibility(firstlayout.GONE);
                secondlayout.setVisibility(secondlayout.VISIBLE);
                thirdlayout.setVisibility(thirdlayout.GONE);
                sitecheclistscroll.fullScroll(ScrollView.FOCUS_UP);
                break;
            case R.id.thirdsegmented:
                firstlayout.setVisibility(firstlayout.GONE);
                secondlayout.setVisibility(secondlayout.GONE);
                thirdlayout.setVisibility(thirdlayout.VISIBLE);
                sitecheclistscroll.fullScroll(ScrollView.FOCUS_UP);
                break;
            case R.id.normal1:
                test2.clearCheck();
                test3.clearCheck();
                speedLimit = normal1.getText().toString();
                speedlimitid = "0";
                break;
            case R.id.normal2:
                test2.clearCheck();
                test3.clearCheck();
                speedLimit = normal2.getText().toString();
                speedlimitid = "1";
                break;
            case R.id.normal3:
                test2.clearCheck();
                test3.clearCheck();
                speedLimit = normal3.getText().toString();
                speedlimitid = "2";
                break;
            case R.id.normal4:
                test1.clearCheck();
                test3.clearCheck();
                speedLimit = normal4.getText().toString();
                speedlimitid = "3";
                break;
            case R.id.normal5:
                test1.clearCheck();
                test3.clearCheck();
                speedLimit = normal5.getText().toString();
                speedlimitid = "4";
                break;
            case R.id.normal6:
                test1.clearCheck();
                test3.clearCheck();
                speedLimit = normal6.getText().toString();
                speedlimitid = "5";
                break;
            case R.id.normal7:
                test2.clearCheck();
                test1.clearCheck();
                speedLimit = normal7.getText().toString();
                speedlimitid = "6";
                break;
            case R.id.normal8:
                test2.clearCheck();
                test1.clearCheck();
                speedLimit = normal8.getText().toString();
                speedlimitid = "7";
                break;
            case R.id.addhazard:
                if (sites.size() < 4) {
                    addItemtoSitehcheclist();
                    int screenSize = getResources().getConfiguration().screenLayout &
                            Configuration.SCREENLAYOUT_SIZE_MASK;
                    String toastMsg;
                    switch (screenSize) {
                        case Configuration.SCREENLAYOUT_SIZE_LARGE:
                            if (width < 854) {
                                iniHeight = iniHeight + 570;
                            } else if (width < 1201) {
                                Log.e("test", "1200");
                                iniHeight = iniHeight + 740;
                            } else {
                                iniHeight = iniHeight + 780;
                            }
                            break;
                        case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                            if (width < 641) {
                                iniHeight = iniHeight + 640;
                            } else if (width < 721) {
                                iniHeight = iniHeight + 695;
                            } else if (width < 769) {
                                iniHeight = iniHeight + 688;
                                Toast.makeText(getActivity(), "750", Toast.LENGTH_SHORT).show();
                            } else if (width < 801) {
                                iniHeight = iniHeight + 800;
                            } else if (width < 1081) {
                                iniHeight = iniHeight + 1080;
                            } else if (width < 1201) {
                                iniHeight = iniHeight + 1200;
                            } else if (width < 1441) {
                                iniHeight = iniHeight + 1440;
                            } else {
                                iniHeight = iniHeight + 1200;
                            }
                            break;
                        case Configuration.SCREENLAYOUT_SIZE_SMALL:
                            iniHeight = iniHeight + 1400;
                            break;
                        default:
                            iniHeight = iniHeight + 1200;
                    }

                    params.height = iniHeight;
                    Runnable task = new Runnable() {
                        public void run() {
                            sitecheclistscroll.fullScroll(ScrollView.FOCUS_DOWN);
                        }
                    };
                    worker.schedule(task, 200, TimeUnit.MILLISECONDS);
                } else {
                    Toast.makeText(getActivity(), "Only five additional hazard is accepted.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void removeitem(int id, String iddb) {
        removeItems(id, iddb);

        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        String toastMsg;
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                if (width < 854) {
                    iniHeight = iniHeight + 570;
                } else if (width < 1201) {
                    Log.e("test", "1200");
                    iniHeight = iniHeight - 740;
                } else {
                    iniHeight = iniHeight + 780;
                }
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                if (width < 641) {
                    iniHeight = iniHeight - 640;
                } else if (width < 721) {
                    iniHeight = iniHeight - 695;
                } else if (width < 769) {
                    iniHeight = iniHeight - 688;
                } else if (width < 801) {
                    iniHeight = iniHeight - 800;
                } else if (width < 1081) {
                    iniHeight = iniHeight - 1080;
                } else if (width < 1201) {
                    iniHeight = iniHeight - 1200;
                } else if (width < 1441) {
                    iniHeight = iniHeight - 1440;
                } else {
                    iniHeight = iniHeight - 1200;
                }
                break;
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                iniHeight = iniHeight - 1400;
                break;
            default:
                iniHeight = iniHeight - 1200;
        }
        params.height = iniHeight;

        Runnable task = new Runnable() {
            public void run() {
                sitecheclistscroll.fullScroll(ScrollView.FOCUS_DOWN);
            }
        };

        worker.schedule(task, 200, TimeUnit.MILLISECONDS);
        db.removeAdditionalHazard(mJobObject.getJobID(), iddb + "");
    }

    @Override
    public void updateAnyAdditionalHazard(ArrayList<siteChecklistAddmodel> sitews, int position) {
        sites = sitews;
    }

    @Override
    public void controlMeasures(ArrayList<siteChecklistAddmodel> sitews, int position) {
        sites = sitews;
    }

    @Override
    public void initialriskInter(ArrayList<siteChecklistAddmodel> sitews, int position) {
        sites = sitews;
    }

    @Override
    public void resiDualInter(ArrayList<siteChecklistAddmodel> sitews, int position) {
        sites = sitews;
    }

    public void removeItems(int item, String iddb) {
        sites.remove(item);



        mAdapter.notifyDataSetChanged();
    }

    public void addItemtoSitehcheclist() {
        sites.add(new siteChecklistAddmodel());

        db.removeAdditionalHazard2(mJobObject.getJobID());
        for (int i = 0; i < sites.size(); i++) {
            db.addAdditionalHazard(sites.get(i), mJobObject.getJobID(), i + "");
        }
        mAdapter = new AdapterSiteChecklist(db.getAdditionalHazard(mJobObject.getJobID()), getActivity(), this);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        if (i != -1) {
            switch (radioGroup.getId()) {
                case R.id.test1: {
                    if (test1.getCheckedRadioButtonId() != -1) {
                        test2.clearCheck();
                        test3.clearCheck();
                    } else {

                    }
                }
                break;
                case R.id.test2: {
                    if (test2.getCheckedRadioButtonId() != -1) {
                        test1.clearCheck();
                        test3.clearCheck();
                    } else {

                    }
                }
                break;
                case R.id.test3: {
                    if (test3.getCheckedRadioButtonId() != -1) {
                        test2.clearCheck();
                        test1.clearCheck();
                    } else {

                    }
                }
                break;
            }
        }
    }

    public interface OnnextandPre {
        public void onNext();

        public void onPre();
    }

    public void fields() {
        firstsegmented.setOnClickListener(this);
        secondsegmented.setOnClickListener(this);
        thirdsegmented.setOnClickListener(this);
        previos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onPre();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextValidate("0");
                addhazzard();
                //Todo Hazard
            }
        });
    }

    public void addhazzard() {
        String temohig = "", tempresidual = "";
        RadioButton highlowradio, residualradio;
        int radiofirst = highlow.getCheckedRadioButtonId();
        highlowradio = (RadioButton) highlow.findViewById(radiofirst);

        int radiosecond = residual.getCheckedRadioButtonId();
        residualradio = (RadioButton) residual.findViewById(radiosecond);

        if (highlow.getCheckedRadioButtonId() != -1) {
            if (highlow.indexOfChild(highlowradio) == 1) {
                temohig = "Med";
            } else if (highlow.indexOfChild(highlowradio) == 0) {
                temohig = "High";
            } else if (highlow.indexOfChild(highlowradio) == 2) {
                temohig = "Low";
            }
        } else {
            temohig = "";
        }

        if (residual.getCheckedRadioButtonId() != -1) {
            if (residual.indexOfChild(residualradio) == 1) {
                tempresidual = "Med";
            } else if (residual.indexOfChild(residualradio) == 0) {
                tempresidual = "High";
            } else if (residual.indexOfChild(residualradio) == 2) {
                tempresidual = "Low";
            }
        } else {
            tempresidual = "";
        }

        String hazardstring = "[" +
                "{" +
                "\"JobID\":\"" + mJobObject.getJobID() + "\"," +
                "\"AddHazard\":\"" + anyadd.getText().toString() + "\"," +
                "\"InitialRisk\":\"" + temohig + "\"," +
                "\"ControlMeasures\":\"" + texare.getText().toString() + "\"," +
                "\"ResidualRisk\":\"" + tempresidual + "\"" +
                "}" +
                "]";

        Log.e("Hazzard", hazardstring);
        db.hazardtb(mJobObject.getJobID(), hazardstring);
    }

    public void setCheckedTure() {
        String[] m = new String[0];
        Log.e("data return", db.getDummySiteChecklist(mJobObject.getJobID()) + "");

        if (!(db.getDummySiteChecklist(mJobObject.getJobID()) == null)) {
            m = db.getDummySiteChecklist(mJobObject.getJobID()).split("~");

            if (!(m[0].equals("-mc"))) {
                ((RadioButton) rgroup1.getChildAt(Integer.parseInt(m[0]))).setChecked(true);
            }
            if (!(m[1].equals("-mc"))) {
                ((RadioButton) rgroup2.getChildAt(Integer.parseInt(m[1]))).setChecked(true);
            }
            Log.e("sd", "s" + m[2]);
            if (!(m[2].equals("[]"))) {
                String b = m[2].replace("[", "").replace(",]", "");
                for (int c = 0; c < roadconfiguration.getChildCount(); c++) {
                    String[] x = b.split(",");
                    View v = (View) roadconfiguration.getChildAt(c);
                    if (v instanceof CheckBox) {
                        CheckBox s = (CheckBox) v;
                        for (int d = 0; d < x.length; d++) {
                            if (c == Integer.parseInt(x[d])) {
                                s.setChecked(true);
                            }
                        }
                    }
                }
            }

            if (!(m[3].equals("-mc"))) {
                ((RadioButton) rgroup4.getChildAt(Integer.parseInt(m[3]))).setChecked(true);
            }

            if (!(m[4].equals("-mc"))) {
                speedlimitid = m[4];

                Log.e("Normal Road Speed limit", speedlimitid);

                int c = Integer.parseInt(m[4]);
                Log.e("tag", c + "");

                if (c <= 2) {
                    ((RadioButton) test1.getChildAt(Integer.parseInt(m[4]))).setChecked(true);
                    speedLimit = ((RadioButton) test1.getChildAt(Integer.parseInt(m[4]))).getText().toString();
                } else if (c <= 5) {
                    if (c == 3) {
                        c = 0;
                    } else if (c == 4) {
                        c = 1;
                    } else {
                        c = 2;
                    }
                    ((RadioButton) test2.getChildAt(c)).setChecked(true);
                    speedLimit = ((RadioButton) test2.getChildAt(c)).getText().toString();
                } else {
                    if (c == 6) {
                        c = 0;
                    } else {
                        c = 1;
                    }
                    ((RadioButton) test3.getChildAt(c)).setChecked(true);
                    speedLimit = ((RadioButton) test3.getChildAt(c)).getText().toString();
                }

            }

            if (!(m[5].equals("-mc"))) {
                ((RadioButton) rgroup7.getChildAt(Integer.parseInt(m[5]))).setChecked(true);
            }
            if (!(m[6].equals("-mc"))) {
                ((RadioButton) rgroup8.getChildAt(Integer.parseInt(m[6]))).setChecked(true);
            }
            if (!(m[7].equals("-mc"))) {
                ((RadioButton) rgroup9.getChildAt(Integer.parseInt(m[7]))).setChecked(true);
            }
            if (!(m[8].equals("-mc"))) {
                ((RadioButton) rgroup10.getChildAt(Integer.parseInt(m[8]))).setChecked(true);
            }
            if (!(m[9].equals("-mc"))) {
                ((RadioButton) rgroup11.getChildAt(Integer.parseInt(m[9]))).setChecked(true);
            }
            if (!(m[10].equals("-mc"))) {
                ((RadioButton) rgroup12.getChildAt(Integer.parseInt(m[10]))).setChecked(true);
            }
            if (!(m[11].equals("-mc"))) {
                ((RadioButton) layout3_1.getChildAt(Integer.parseInt(m[11]))).setChecked(true);
            }
            if (!(m[12].equals("-mc"))) {
                firstnote.setText(m[12]);
            }
            if (!(m[13].equals("-mc"))) {
                ((RadioButton) layout3_2.getChildAt(Integer.parseInt(m[13]))).setChecked(true);
            }
            if (!(m[14].equals("-mc"))) {
                note2.setText(m[14]);
            }
            if (!(m[15].equals("-mc"))) {
                ((RadioButton) layout3_3.getChildAt(Integer.parseInt(m[15]))).setChecked(true);
            }
            if (!(m[16].equals("-mc"))) {
                note3.setText(m[16]);
            }
            if (!(m[17].equals("-mc"))) {
                ((RadioButton) layout3_4.getChildAt(Integer.parseInt(m[17]))).setChecked(true);
            }
            if (!(m[18].equals("-mc"))) {
                note4.setText(m[18]);
            }
            if (!(m[19].equals("-mc"))) {
                ((RadioButton) layout3_5.getChildAt(Integer.parseInt(m[19]))).setChecked(true);
            }
            if (!(m[20].equals("-mc"))) {
                note5.setText(m[20]);
            }

            if (!(m[21].equals("-mc"))) {
                ((RadioButton) layout3_6.getChildAt(Integer.parseInt(m[21]))).setChecked(true);
            }

            if (!(m[22].equals("-mc"))) {
                note6.setText(m[22]);
            }

            if (!(m[23].equals("-mc"))) {
                ((RadioButton) layout3_7.getChildAt(Integer.parseInt(m[23]))).setChecked(true);
            }

            if (!(m[24].equals("-mc"))) {
                note7.setText(m[24]);
            }

            if (!(m[25].equals("-mc"))) {
                ((RadioButton) layout3_8.getChildAt(Integer.parseInt(m[25]))).setChecked(true);
            }

            if (!(m[26].equals("-mc"))) {
                note8.setText(m[26]);
            }

            if (!(m[27].equals("-mc"))) {
                ((RadioButton) layout3_9.getChildAt(Integer.parseInt(m[27]))).setChecked(true);
            }

            if (!(m[28].equals("-mc"))) {
                note9.setText(m[28]);
            }

            if (!(m[29].equals("-mc"))) {
                ((RadioButton) layout3_10.getChildAt(Integer.parseInt(m[29]))).setChecked(true);
            }
            if (!(m[30].equals("-mc"))) {
                note10.setText(m[30]);
            }

            if (!(m[31].equals("-mc"))) {
                ((RadioButton) layout3_11.getChildAt(Integer.parseInt(m[31]))).setChecked(true);
            }
            if (!(m[32].equals("-mc"))) {
                note11.setText(m[32]);
            }

            if (!(m[33].equals("-mc"))) {
                ((RadioButton) layout3_12.getChildAt(Integer.parseInt(m[33]))).setChecked(true);
            }
            if (!(m[34].equals("-mc"))) {
                note12.setText(m[34]);
            }

            if (!(m[35].equals("-mc"))) {
                ((RadioButton) layout3_13.getChildAt(Integer.parseInt(m[35]))).setChecked(true);

                if (m[35].equals("0")) {
                    Log.e("hazard from db", db.checkhazard(mJobObject.getJobID()));
                    try {
                        JSONArray jsons = new JSONArray(db.checkhazard(mJobObject.getJobID()));
                        for (int d = 0; d < jsons.length(); d++) {
                            JSONObject c = jsons.getJSONObject(d);
                            anyadd.setText(c.getString("AddHazard").toString());
                            texare.setText(c.getString("ControlMeasures").toString());
                            Log.e("sjdhfskdjhf", c.getString("ResidualRisk").toString());
                            if (!(c.getString("InitialRisk").toString().equals(""))) {
                                if (c.getString("InitialRisk").equals("High")) {
                                    ((RadioButton) highlow.getChildAt(0)).setChecked(true);
                                } else if (c.getString("InitialRisk").toString().equals("Med")) {
                                    ((RadioButton) highlow.getChildAt(1)).setChecked(true);
                                } else {
                                    ((RadioButton) highlow.getChildAt(2)).setChecked(true);
                                }
                            }
                            if (!(c.getString("ResidualRisk").toString().equals(""))) {
                                if (c.getString("ResidualRisk").equals("High")) {
                                    ((RadioButton) residual.getChildAt(0)).setChecked(true);
                                } else if (c.getString("ResidualRisk").toString().equals("Med")) {
                                    ((RadioButton) residual.getChildAt(1)).setChecked(true);
                                } else {
                                    ((RadioButton) residual.getChildAt(2)).setChecked(true);
                                }


                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

//            if (!(m[36].equals("-mc"))) {
//                note13.setText(m[36]);
//            }

            if (!(m[36].equals("-mc"))) {
                tcp.setText(m[36]);
            }

            Log.e("TEST TEST TEST GAGO", db.getDummySiteChecklist(mJobObject.getJobID()));
        }
    }


    public void nextValidate(String kanino) {
        boolean m = false;
        String dummyt = "";

        int radioButtonID2 = rgroup2.getCheckedRadioButtonId();
        rb2 = (RadioButton) rgroup2.findViewById(radioButtonID2);

        int radioButtonID4 = rgroup4.getCheckedRadioButtonId();
        rb4 = (RadioButton) rgroup4.findViewById(radioButtonID4);

        int radioButtonID5 = rgroup5.getCheckedRadioButtonId();
        rb5 = (RadioButton) rgroup5.findViewById(radioButtonID5);

        int radioButtonID7 = rgroup7.getCheckedRadioButtonId();
        rb7 = (RadioButton) rgroup7.findViewById(radioButtonID7);

        int radioButtonID8 = rgroup8.getCheckedRadioButtonId();
        rb8 = (RadioButton) rgroup8.findViewById(radioButtonID8);

        int radioButtonID9 = rgroup9.getCheckedRadioButtonId();
        rb9 = (RadioButton) rgroup9.findViewById(radioButtonID9);

        int radioButtonID10 = rgroup10.getCheckedRadioButtonId();
        rb10 = (RadioButton) rgroup10.findViewById(radioButtonID10);

        int radioButtonID11 = rgroup11.getCheckedRadioButtonId();
        rb11 = (RadioButton) rgroup11.findViewById(radioButtonID11);

        int radioButtonID12 = rgroup12.getCheckedRadioButtonId();
        rb12 = (RadioButton) rgroup12.findViewById(radioButtonID12);

        int radioButtonID = rgroup1.getCheckedRadioButtonId();
        rb1 = (RadioButton) rgroup1.findViewById(radioButtonID);

        if (rgroup1.getCheckedRadioButtonId() != -1) {
            returnDatam = "Single_Select:Type of Road^" + rb1.getText().toString() + "~";
            dummyt = dummyt + rgroup1.indexOfChild(rb1) + "~";
        } else {
            m = true;
            returnDatam = "Single_Select:Type of Road^ ~";
            dummyt = dummyt + "-mc~";
        }

        if (rgroup2.getCheckedRadioButtonId() != -1) {
            returnDatam = returnDatam + "Single_Select:No. of lanes (Multilane only)^" + rb2.getText() + "~";
            dummyt = dummyt + rgroup2.indexOfChild(rb2) + "~";
        } else {
            if (required1 == true) {
                m = true;
            }
            returnDatam = returnDatam + "Single_Select:No. of lanes (Multilane only)^None~";
            dummyt = dummyt + "-mc~";
        }
        String dummmys = "", dummmyss = "[";
        dummmys = dummmys + "Multiple_Select:Road Configuration^";

        for (int c = 0; c < roadconfiguration.getChildCount(); c++) {
            View vd = (View) roadconfiguration.getChildAt(c);
            if (vd instanceof CheckBox) {
                CheckBox s = (CheckBox) vd;
                if (s.isChecked()) {
                    if (roadconfiguration.getChildCount() - 1 == c) {
                        dummmys = dummmys + s.getText().toString() + "";
                        dummmyss = dummmyss + c + ",";
                        dc = true;
                    } else {
                        dummmys = dummmys + s.getText().toString() + "|";
                        dummmyss = dummmyss + c + ",";
                        dc = true;
                    }
                }
            }
        }

        dummmys = dummmys + "~";
        dummmys = dummmys.replace("|~", "~");
        returnDatam = returnDatam + dummmys;
        Log.e("sds", returnDatam);
        dummyt = dummyt + dummmyss + "]~";

        if (rgroup4.getCheckedRadioButtonId() != -1) {
            returnDatam = returnDatam + "Single_Select:Road Surface^" + rb4.getText() + "~";
            dummyt = dummyt + rgroup4.indexOfChild(rb4) + "~";
        } else {
            m = true;
            returnDatam = returnDatam + "Single_Select:Road Surface^ ~";
            dummyt = dummyt + "-mc~";
        }

        if (!speedLimit.equals("")) {
            returnDatam = returnDatam + "Single_Select:Normal Road Speed Limit^" + speedLimit + "~";
            dummyt = dummyt + speedlimitid + "~";
        } else {
            m = true;
            returnDatam = returnDatam + "Single_Select:Normal Road Speed Limit^ ~";
            dummyt = dummyt + "-mc~";
        }

        if (rgroup7.getCheckedRadioButtonId() != -1) {
            returnDatam = returnDatam + "Single_Select:Signs / Signals^" + rb7.getText() + "~";
            dummyt = dummyt + rgroup7.indexOfChild(rb7) + "~";
        } else {
            m = true;
            returnDatam = returnDatam + "Single_Select:Signs / Signals^ ~";
            dummyt = dummyt + "-mc~";
        }
        if (rgroup8.getCheckedRadioButtonId() != -1) {
            returnDatam = returnDatam + "Single_Select:Visibility^" + rb8.getText() + "~";
            dummyt = dummyt + rgroup8.indexOfChild(rb8) + "~";
        } else {
            m = true;
            returnDatam = returnDatam + "Single_Select:Visibility^ ~";
            dummyt = dummyt + "-mc~";
        }
        if (rgroup9.getCheckedRadioButtonId() != -1) {
            returnDatam = returnDatam + "Single_Select:Weather^" + rb9.getText() + "~";
            dummyt = dummyt + rgroup9.indexOfChild(rb9) + "~";
        } else {
            m = true;
            returnDatam = returnDatam + "Single_Select:Weather^ ~";
            dummyt = dummyt + "-mc~";
        }
        if (rgroup10.getCheckedRadioButtonId() != -1) {
            returnDatam = returnDatam + "Single_Select:Traffic Control^" + rb10.getText() + "~";
            dummyt = dummyt + rgroup10.indexOfChild(rb10) + "~";
        } else {
            m = true;
            returnDatam = returnDatam + "Single_Select:Traffic Control^ ~";
            dummyt = dummyt + "-mc~";
        }

        if (rgroup11.getCheckedRadioButtonId() != -1) {
            returnDatam = returnDatam + "Single_Select:Lanes Closed^" + rb11.getText() + "~";
            dummyt = dummyt + rgroup11.indexOfChild(rb11) + "~";
        } else {
            m = true;
            returnDatam = returnDatam + "Single_Select:Lanes Closed^ ~";
            dummyt = dummyt + "-mc~";
        }

        if (rgroup12.getCheckedRadioButtonId() != -1) {
            returnDatam = returnDatam + "Single_Select:Workers Clearance (Speed reduced to)^" + rb12.getText() + "~";
            dummyt = dummyt + rgroup12.indexOfChild(rb12) + "~";
        } else {
            m = true;
            returnDatam = returnDatam + "Single_Select:Workers Clearance (Speed reduced to)^ ~";
            dummyt = dummyt + "-mc~";
        }

        int radio1 = layout3_1.getCheckedRadioButtonId();
        rb1s = (RadioButton) layout3_1.findViewById(radio1);

        int radio2 = layout3_2.getCheckedRadioButtonId();
        rb2s = (RadioButton) layout3_2.findViewById(radio2);

        int radio3 = layout3_3.getCheckedRadioButtonId();
        rb3s = (RadioButton) layout3_3.findViewById(radio3);

        int radio4 = layout3_4.getCheckedRadioButtonId();
        rb4s = (RadioButton) layout3_4.findViewById(radio4);

        int radio5 = layout3_5.getCheckedRadioButtonId();
        rb5s = (RadioButton) layout3_5.findViewById(radio5);

        int radio6 = layout3_6.getCheckedRadioButtonId();
        rb6s = (RadioButton) layout3_6.findViewById(radio6);

        int radio7 = layout3_7.getCheckedRadioButtonId();
        rb7s = (RadioButton) layout3_7.findViewById(radio7);

        int radio8 = layout3_8.getCheckedRadioButtonId();
        rb8s = (RadioButton) layout3_8.findViewById(radio8);

        int radio9 = layout3_9.getCheckedRadioButtonId();
        rb9s = (RadioButton) layout3_9.findViewById(radio9);

        int radio10 = layout3_10.getCheckedRadioButtonId();
        rb10s = (RadioButton) layout3_10.findViewById(radio10);

        int radio11 = layout3_11.getCheckedRadioButtonId();
        rb11s = (RadioButton) layout3_11.findViewById(radio11);

        int radio12 = layout3_12.getCheckedRadioButtonId();
        rb12s = (RadioButton) layout3_12.findViewById(radio12);

        int radio13 = layout3_13.getCheckedRadioButtonId();
        rb13s = (RadioButton) layout3_13.findViewById(radio13);

        if (layout3_1.getCheckedRadioButtonId() != -1) {
            returnDatam = returnDatam + "Single_Select:All control measures as per SWMS in place^" + rb1s.getText() + "~";
            dummyt = dummyt + layout3_1.indexOfChild(rb1s) + "~";
        } else {
            m = true;
            returnDatam = returnDatam + "Single_Select:All control measures as per SWMS in place^ ~";
            dummyt = dummyt + "-mc~";
        }

        if ((bnote1 != false)) {
            if (validateInoutBox(firstnote)) {
                m = true;
                returnDatam = returnDatam + "Text:All control measures as per SWMS in place_Note^x~";
                dummyt = dummyt + "-mc~";
            } else {
                returnDatam = returnDatam + "Text:All control measures as per SWMS in place_Note^" + firstnote.getText().toString() + "~";
                dummyt = dummyt + firstnote.getText().toString() + "~";
            }
        } else {
            returnDatam = returnDatam + "Text:All control measures as per SWMS in place_Note^x~";
            dummyt = dummyt + "-mc~";
        }

        if (layout3_2.getCheckedRadioButtonId() != -1) {
            returnDatam = returnDatam + "Single_Select:Signage erected as per SWMS, Manual diagram, Traffic Control Plan - TCP No.^" + rb2s.getText() + "~";
            dummyt = dummyt + layout3_2.indexOfChild(rb2s) + "~";
        } else {
            m = true;
            returnDatam = returnDatam + "Single_Select:Signage erected as per SWMS, Manual diagram, TrafficControl Pla - TCP No.^ ~";
            dummyt = dummyt + "-mc~";
        }


        if ((bnote2 != false)) {
            if (validateInoutBox(note2)) {
                m = true;
                returnDatam = returnDatam + "Text:Signage erected as per SWMS, Manual diagram, Traffic Control Plan - TCP No._Note^x~";
                dummyt = dummyt + "-mc~";
            } else {
                returnDatam = returnDatam + "Text:Signage erected as per SWMS, Manual diagram, Traffic Control Plan - TCP No._Note^" + note2.getText().toString() + "~";
                dummyt = dummyt + note2.getText().toString() + "~";
            }
        } else {
            returnDatam = returnDatam + "Text:Signage erected as per SWMS, Manual diagram, Traffic Control Plan - TCP No._Note^x~";
            dummyt = dummyt + "-mc~";
        }


        if (layout3_3.getCheckedRadioButtonId() != -1) {
            returnDatam = returnDatam + "Single_Select:Has conflicting signage been covered or removed^" + rb3s.getText() + "~";
            dummyt = dummyt + layout3_3.indexOfChild(rb3s) + "~";
        } else {
            m = true;
            returnDatam = returnDatam + "Single_Select:Has conflicting signage been covered or removed^ ~";
            dummyt = dummyt + "-mc~";
        }

        if ((bnote3 != false)) {
            if (validateInoutBox(note3)) {
                m = true;
                returnDatam = returnDatam + "Text:Has conflicting signage been covered or removed_Note^x~";
                dummyt = dummyt + "-mc~";
            } else {
                returnDatam = returnDatam + "Text:Has conflicting signage been covered or removed_Note^" + note3.getText().toString() + "~";
                dummyt = dummyt + note3.getText().toString() + "~";
            }
        } else {
            returnDatam = returnDatam + "Text:Has conflicting signage been covered or removed_Note^x~";
            dummyt = dummyt + "-mc~";
        }


        if (layout3_4.getCheckedRadioButtonId() != -1) {
            returnDatam = returnDatam + "Single_Select:Are signs securely mounted and visible to traffic^" + rb4s.getText() + "~";
            dummyt = dummyt + layout3_4.indexOfChild(rb4s) + "~";

        } else {
            m = true;
            returnDatam = returnDatam + "Single_Select:Are signs securely mounted and visible to traffic^ ~";
            dummyt = dummyt + "-mc~";
        }


        if ((bnote4 != false)) {
            if (validateInoutBox(note4)) {
                m = true;
                returnDatam = returnDatam + "Text:Are signs securely mounted and visible to traffic_Note^x~";
                dummyt = dummyt + "-mc~";
            } else {
                returnDatam = returnDatam + "Text:Are signs securely mounted and visible to traffic_Note^" + note4.getText().toString() + "~";
                dummyt = dummyt + note4.getText().toString() + "~";
            }
        } else {
            returnDatam = returnDatam + "Text:Are signs securely mounted and visible to traffic_Note^x~";
            dummyt = dummyt + "-mc~";
        }

        if (layout3_5.getCheckedRadioButtonId() != -1) {
            returnDatam = returnDatam + "Single_Select:Traffic Controller Ahead / Prepare to stop sign erected^" + rb5s.getText() + "~";
            dummyt = dummyt + layout3_5.indexOfChild(rb5s) + "~";
        } else {
            m = true;
            returnDatam = returnDatam + "Single_Select:Traffic Controller Ahead / Prepare to stop sign erected^ ~";
            dummyt = dummyt + "-mc~";
        }

        if ((bnote5 != false)) {
            if (validateInoutBox(note5)) {
                m = true;
                returnDatam = returnDatam + "Text:Traffic Controller Ahead / Prepare to stop sign erected_Note^x~";
                dummyt = dummyt + "-mc~";
            } else {
                returnDatam = returnDatam + "Text:Traffic Controller Ahead / Prepare to stop sign erected_Note^" + note5.getText().toString() + "~";
                dummyt = dummyt + note5.getText().toString() + "~";
            }
        } else {
            returnDatam = returnDatam + "Text:Traffic Controller Ahead / Prepare to stop sign erected_Note^x~";
            dummyt = dummyt + "-mc~";
        }

        if (layout3_6.getCheckedRadioButtonId() != -1) {
            returnDatam = returnDatam + "Single_Select:PPE is being worn as instructed and as per SWMS^" + rb6s.getText() + "~";
            dummyt = dummyt + layout3_6.indexOfChild(rb6s) + "~";
        } else {
            m = true;
            returnDatam = returnDatam + "Single_Select:PPE is being worm as instructed and as per SWMS^ ~";
            dummyt = dummyt + "-mc~";
        }

        if ((bnote6 != false)) {
            if (validateInoutBox(note6)) {
                m = true;
                returnDatam = returnDatam + "Text:PPE is being worn as instructed and as per SWMS_Note^x~";
                dummyt = dummyt + "-mc~";
            } else {
                returnDatam = returnDatam + "Text:PPE is being worn as instructed and as per SWMS_Note^" + note6.getText().toString() + "~";
                dummyt = dummyt + note6.getText().toString() + "~";
            }
        } else {
            returnDatam = returnDatam + "Text:PPE is being worn as instructed and as per SWMS_Note^x~";
            dummyt = dummyt + "-mc~";
        }

        if (layout3_7.getCheckedRadioButtonId() != -1) {
            returnDatam = returnDatam + "Single_Select:Stop slow bat and hand signals to be used to control traffic^" + rb7s.getText() + "~";
            dummyt = dummyt + layout3_7.indexOfChild(rb7s) + "~";
        } else {
            m = true;
            returnDatam = returnDatam + "Single_Select:Stop slow bat and hand signals to be used to control traffic^ ~";
            dummyt = dummyt + "-mc~";
        }

        if ((bnote7 != false)) {
            if (validateInoutBox(note7)) {
                m = true;
                returnDatam = returnDatam + "Text:Stop slow bat and hand signals to be used to control traffic_Note^x~";
                dummyt = dummyt + "-mc~";
            } else {
                returnDatam = returnDatam + "Text:Stop slow bat and hand signals to be used to control traffic_Note^" + note7.getText().toString() + "~";
                dummyt = dummyt + note7.getText().toString() + "~";
            }
        } else {
            returnDatam = returnDatam + "Text:Stop slow bat and hand signals to be used to control traffic_Note^x~";
            dummyt = dummyt + "-mc~";
        }

        if (layout3_8.getCheckedRadioButtonId() != -1) {
            returnDatam = returnDatam + "Single_Select:Each TC maintains an escape route at all times^" + rb8s.getText() + "~";
            dummyt = dummyt + layout3_8.indexOfChild(rb8s) + "~";
        } else {
            m = true;
            returnDatam = returnDatam + "Single_Select:Each TC maintains an escape route at all times^ ~";
            dummyt = dummyt + "-mc~";
        }

        if ((bnote8 != false)) {
            if (validateInoutBox(note8)) {
                m = true;
                returnDatam = returnDatam + "Text:Each TC maintains an escape route at all times_Note^x~";
                dummyt = dummyt + "-mc~";
            } else {
                returnDatam = returnDatam + "Text:Each TC maintains an escape route at all times_Note^" + note8.getText().toString() + "~";
                dummyt = dummyt + note8.getText().toString() + "~";
            }
        } else {
            returnDatam = returnDatam + "Text:Each TC maintains an escape route at all times_Note^x~";
            dummyt = dummyt + "-mc~";
        }

        if (layout3_9.getCheckedRadioButtonId() != -1) {
            returnDatam = returnDatam + "Single_Select:TC to stand facing traffic and outside projected travel path^" + rb9s.getText() + "~";
            dummyt = dummyt + layout3_9.indexOfChild(rb9s) + "~";
        } else {
            m = true;
            returnDatam = returnDatam + "Single_Select:TC to stand facing traffic and outside projected travel path^ ~";
            dummyt = dummyt + "-mc~";
        }

        if ((bnote9 != false)) {
            if (validateInoutBox(note9)) {
                m = true;
                returnDatam = returnDatam + "Text:TC to stand facing traffic and outside projected travel path_Note^x~";
                dummyt = dummyt + "-mc~";
            } else {
                returnDatam = returnDatam + "Text:TC to stand facing traffic and outside projected travel path_Note^" + note9.getText().toString() + "~";
                dummyt = dummyt + note9.getText().toString() + "~";
            }
        } else {
            returnDatam = returnDatam + "Text:TC to stand facing traffic and outside projected travel path_Note^x~";
            dummyt = dummyt + "-mc~";
        }

        if (layout3_10.getCheckedRadioButtonId() != -1) {
            returnDatam = returnDatam + "Single_Select:Access provided for cyclists, pedestrians, wheelchairs and driveways^" + rb10s.getText() + "~";
            dummyt = dummyt + layout3_10.indexOfChild(rb10s) + "~";
        } else {
            m = true;
            returnDatam = returnDatam + "Single_Select:Access provided for cyclists, pedestrians, wheelchairs and driveways^ ~";
            dummyt = dummyt + "-mc~";
        }

        if ((bnote10 != false)) {
            if (validateInoutBox(note10)) {
                m = true;
                returnDatam = returnDatam + "Text:Access provided for cyclists, pedestrians, wheelchairs and driveways_Note^x~";
                dummyt = dummyt + "-mc~";
            } else {
                returnDatam = returnDatam + "Text:Access provided for cyclists, pedestrians, wheelchairs and driveways_Note^" + note10.getText().toString() + "~";
                dummyt = dummyt + note10.getText().toString() + "~";
            }
        } else {
            returnDatam = returnDatam + "Text:Access provided for cyclists, pedestrians, wheelchairs and driveways_Note^x~";
            dummyt = dummyt + "-mc~";
        }

        if (layout3_11.getCheckedRadioButtonId() != -1) {
            returnDatam = returnDatam + "Single_Select:Maintain safe distance from all plant and equipment^" + rb11s.getText() + "~";
            dummyt = dummyt + layout3_11.indexOfChild(rb11s) + "~";
        } else {
            m = true;
            returnDatam = returnDatam + "Single_Select:Maintain safe distance from all plant and equipment^ ~";
            dummyt = dummyt + "-mc~";
        }

        if ((bnote11 != false)) {
            if (validateInoutBox(note11)) {
                m = true;
                returnDatam = returnDatam + "Text:Maintain safe distance from all plant and equipment_Note^x~";
                dummyt = dummyt + "-mc~";
            } else {
                returnDatam = returnDatam + "Text:Maintain safe distance from all plant and equipment_Note^" + note11.getText().toString() + "~";
                dummyt = dummyt + note11.getText().toString() + "~";
            }
        } else {
            returnDatam = returnDatam + "Text:Maintain safe distance from all plant and equipment_Note^x~";
            dummyt = dummyt + "-mc~";
        }


        if (layout3_12.getCheckedRadioButtonId() != -1) {
            returnDatam = returnDatam + "Single_Select:Is there sufficient room to queue stopped vehicles^" + rb12s.getText() + "~";
            dummyt = dummyt + layout3_12.indexOfChild(rb12s) + "~";
        } else {
            m = true;
            returnDatam = returnDatam + "Single_Select:Is there sufficient room to queue stopped vehicles^ ~";
            dummyt = dummyt + "-mc~";
        }

        if ((bnote12 != false)) {
            if (validateInoutBox(note12)) {
                m = true;
                returnDatam = returnDatam + "Text:Is there sufficient room to queue stopped vehicles_Note^x~";
                dummyt = dummyt + "-mc~";
            } else {
                returnDatam = returnDatam + "Text:Is there sufficient room to queue stopped vehicles_Note^" + note12.getText().toString() + "~";
                dummyt = dummyt + note12.getText().toString() + "~";
            }
        } else {
            returnDatam = returnDatam + "Text:Is there sufficient room to queue stopped vehicles_Note^x~";
            dummyt = dummyt + "-mc~";
        }


        if (layout3_13.getCheckedRadioButtonId() != -1) {
            returnDatam = returnDatam + "Single_Select:Environment - Is there any environmental risks^" + rb13s.getText() + "~";
            dummyt = dummyt + layout3_13.indexOfChild(rb13s) + "~";
            isAdditionaYes = true;
        } else {
            m = true;
            isAdditionaYes = false;
            returnDatam = returnDatam + "Single_Select:Environment Is there any environmental risks^ ~";
            dummyt = dummyt + "-mc~";
        }

        if (required2 == true) {
            if (anyadd.getText().toString().equals("")) {
                Log.e("Log", "1");
                m = true;
            }
            if (texare.getText().toString().equals("")) {
                Log.e("Log", "2");
                m = true;
            }
            if (highlow.getCheckedRadioButtonId() != -1) {
            } else {
                Log.e("Log", "3");
                m = true;
            }
            if (residual.getCheckedRadioButtonId() != -1) {
            } else {
                Log.e("Log", "4");
                m = true;
            }
        }

        if (!tcp.getText().toString().equals("")) {
            returnDatam = returnDatam + "Text:If Yes, please enter the appropriate diagram / TCP No.^" + tcp.getText().toString() + "";
            dummyt = dummyt + tcp.getText().toString() + "";
        } else {
            if (required3 == true) {
                m = true;
            }
            returnDatam = returnDatam + "Text:If Yes, please enter the appropriate diagram / TCP No.^none";
            dummyt = dummyt + "-mc";
        }

        if (kanino.equals("1")) {
            db = new DatabaseHelper(getActivity());
            String c = db.checkExistCreateJObs2(mJobObject.getJobID());
            if (db.checkExistCreateJObs2(mJobObject.getJobID()) == null) {
                db.addCreateJobs(mJobObject.getJobID());
            }
            HashMap<String, String> data = new HashMap<String, String>();
            data.put("second",
                    returnDatam);
            createjob.createSecondItem(data);
            Log.e("returnDatam", returnDatam + "");

            db.updateContact(mJobObject.getJobID(), "", returnDatam, "", "", "0");
            db.addDummySiteChecklist(mJobObject.getJobID(), dummyt);
            db.updateRequirementsDummy(mJobObject.getJobID(), dummyt);

        } else {
            if (dc != false) {
                if ((m == true)) {
                    Toast.makeText(getActivity(), "Please complete the form to proceed.", Toast.LENGTH_SHORT).show();
                } else {
                    if (isAdditionaYes) {
                        if (!(sites.size() < 0)) {
                            boolean gets = false;

                            for (int i = 0; i < sites.size(); i++) {
                                String a = sites.get(i).getAddHazard() + "a";
                                String b = sites.get(i).getControlMeasures() + "b";
                                String c = sites.get(i).getInitialRisk() + "c";
                                String d = sites.get(i).getResidualRisk() + "d";

                                if (a.equals("nulla") || a.equals("a")) {
                                    gets = true;
                                }
                                if (b.equals("nullb") || a.equals("b")) {
                                    gets = true;
                                }
                                if (c.equals("nullc") || a.equals("c")) {
                                    gets = true;
                                }
                                if (d.equals("nulld") || a.equals("d")) {
                                    gets = true;
                                }
                            }

                            if ((gets == true)) {
                                Toast.makeText(getActivity(), "Please complete the form to proceed.", Toast.LENGTH_SHORT).show();
                            } else {
                                HashMap<String, String> data23 = new HashMap<String, String>();
                                data23.put("second",
                                        returnDatam);

                                //Todo Adding data to db
                                for (int i = 0; i < sites.size(); i++) {
                                    db.addAdditionalHazard(sites.get(i), mJobObject.getJobID(), i + "");
                                }

                                createjob.createSecondItem(data23);

                                db = new DatabaseHelper(getActivity());
                                String c = db.checkExistCreateJObs2(mJobObject.getJobID());

                                if (db.checkExistCreateJObs2(mJobObject.getJobID()) == null) {
                                    db.addCreateJobs(mJobObject.getJobID());
                                }
                                HashMap<String, String> data = new HashMap<String, String>();
                                data.put("second",
                                        returnDatam);
                                createjob.createSecondItem(data);

                                db.updateContact(mJobObject.getJobID(), "", returnDatam, "", "", "0");
                                db.addDummySiteChecklist(mJobObject.getJobID(), dummyt);
                                db.updateRequirementsDummy(mJobObject.getJobID(), dummyt);
                                dc = false;
                                mListener.onNext();
                            }

                        } else {

                            HashMap<String, String> data23 = new HashMap<String, String>();
                            data23.put("second",
                                    returnDatam);
                            createjob.createSecondItem(data23);

                            db = new DatabaseHelper(getActivity());
                            String c = db.checkExistCreateJObs2(mJobObject.getJobID());

                            if (db.checkExistCreateJObs2(mJobObject.getJobID()) == null) {
                                db.addCreateJobs(mJobObject.getJobID());
                            }
                            HashMap<String, String> data = new HashMap<String, String>();
                            data.put("second",
                                    returnDatam);
                            createjob.createSecondItem(data);

                            db.updateContact(mJobObject.getJobID(), "", returnDatam, "", "", "0");

                            db.addDummySiteChecklist(mJobObject.getJobID(), dummyt);
                            db.updateRequirementsDummy(mJobObject.getJobID(), dummyt);
                            dc = false;
                            mListener.onNext();
                        }
                    } else {
                        HashMap<String, String> data23 = new HashMap<String, String>();
                        data23.put("second",
                                returnDatam);
                        createjob.createSecondItem(data23);
                        db = new DatabaseHelper(getActivity());
                        String c = db.checkExistCreateJObs2(mJobObject.getJobID());

                        if (db.checkExistCreateJObs2(mJobObject.getJobID()) == null) {
                            db.addCreateJobs(mJobObject.getJobID());
                        }

                        HashMap<String, String> data = new HashMap<String, String>();
                        data.put("second",
                                returnDatam);
                        createjob.createSecondItem(data);
                        db.updateContact(mJobObject.getJobID(), "", returnDatam, "", "", "0");
                        db.addDummySiteChecklist(mJobObject.getJobID(), dummyt);
                        db.updateRequirementsDummy(mJobObject.getJobID(), dummyt);
                        dc = false;
                        mListener.onNext();
                    }
                }
            } else {
                Toast.makeText(getActivity(), "Please complete the form to proceed.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.sitechecklist, menu);
        update = (MenuItem) menu.findItem(R.id.update_checklist);
        savemen = (MenuItem) menu.findItem(R.id.savechecklist);
        update.setVisible(true);
        savemen.setVisible(false);
    }

    public boolean onOptionsItemSelected(MenuItem itemb) {
        switch (itemb.getItemId()) {
            case R.id.update_checklist:
                nextValidate("1");
                addhazzard();
                //Todo Adding data to db
                db.removeAdditionalHazard2(mJobObject.getJobID());
                for (int i = 0; i < sites.size(); i++) {
                    db.addAdditionalHazard(sites.get(i), mJobObject.getJobID(), i + "");
                }
                Gson m = new Gson();
                Log.e("gson return", m.toJson(db.getAdditionalHazard(mJobObject.getJobID())));
                Toast.makeText(getActivity(), "Changes have been saved.", Toast.LENGTH_SHORT).show();
                break;
            case android.R.id.home:
                getActivity().finish();
                break;
        }
        return false;
    }

    public boolean validateInoutBox(EditText v) {
        boolean temp = false;
        if (v.getText().toString().equals("")) {
            temp = true;
        }
        return temp;
    }
}