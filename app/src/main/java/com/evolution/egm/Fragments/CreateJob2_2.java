package com.evolution.egm.Fragments;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.evolution.egm.Utils.SessionManager;
import com.evolution.egm.Models.Job;
import com.evolution.egm.R;

import org.parceler.Parcels;

/**
 * Created by mcclynreyarboleda on 4/13/15.
 */
public class CreateJob2_2 extends Fragment implements View.OnClickListener {

    private static final String ARG_SECTION_NUMBER = "section_number";

    Job mJobObject;
    private String token;
    Bundle extras;
    public SessionManager session;
    public CreateJob2_2() {

    }

    public static CreateJob2_2 newInstance(int sectionNumber) {
        CreateJob2_2 fragment = new CreateJob2_2();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        session = new SessionManager(getActivity());
        token = session.getToken();

        mJobObject = new Job();
        extras = getActivity().getIntent().getExtras();
        if (extras != null) {
            mJobObject = Parcels.unwrap(getActivity().getIntent().getExtras().getParcelable("data"));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_createjob2_2, container, false);
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.test);
        TextView txt1 = new TextView(getActivity());
        txt1.setText("Type of Road");
        txt1.setId(R.id.inputID);
        linearLayout.setBackgroundColor(Color.TRANSPARENT);
        linearLayout.addView(txt1);
        return view;
    }

    @Override
    public void onClick(View v) {

    }
}
