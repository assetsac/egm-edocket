package com.evolution.egm.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.evolution.egm.Utils.ConnectionDetector;
import com.evolution.egm.Utils.Constants;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Utils.SessionManager;
import com.evolution.egm.Interface.APIInterface;
import com.evolution.egm.Models.VideoModel;
import com.evolution.egm.R;
import com.evolution.egm.Activities.VideoViewActivity;
import com.google.gson.Gson;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;
import com.squareup.okhttp.OkHttpClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by mcclynreyarboleda on 4/13/15.
 */
public class VideoTest extends Fragment implements View.OnClickListener {

    LinearLayout takeavideo, selectgalery;

    ArrayList<VideoModel> item = new ArrayList<>();
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE2 = 200;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private static final String TAG = "Video";

    //Video or IMAGe
    DatabaseHelper db;
    public SessionManager session;
    private String token;
    OkHttpClient okHttpClient;
    private Uri fileUri; // file url to store image/video
    String fileloc = null;
    private static final String ARG_SECTION_NUMBER = "section_number";
    MenuItem send;
    Uri uri;

    private ImageView playvideo;
    private ImageView videoView;
    Dialog dialog4;
    LinearLayout novideo;
    ScrollView withvideo;
    EditText subject, note;
    VideoModel video = new VideoModel();
    String videoid;
    boolean ismaylaman;
    boolean videoba;
    private ConnectionDetector connection;

    long totalSize = 0;

    String tempStarttime, tempLocation, tempendtime, tempendLocation;

    public VideoTest() {

    }

    public static VideoTest newInstance(int sectionNumber) {
        VideoTest fragment = new VideoTest();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public void runCommand() {
        Process process = null;
        try {
            process = Runtime.getRuntime().exec("adb shell am start -a android.media.action.VIDEO_CAPTURE  --ei android.intent.extras.CAMERA_FACING 1 --ei android.intent.extras.videoQuality 0   -n com.android.gallery3d/com.android.camera.CameraActivity");
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DatabaseHelper(getActivity());
        session = new SessionManager(getActivity());
        token = session.getToken();
        if (savedInstanceState != null) {
            fileUri = Uri.parse(savedInstanceState.getString("file_uri"));
        }
        connection = new ConnectionDetector(getActivity());
        setHasOptionsMenu(true);
        runCommand();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_videotest, container, false);

        takeavideo = (LinearLayout) view.findViewById(R.id.takeapicture);
        selectgalery = (LinearLayout) view.findViewById(R.id.selectgalery);

        novideo = (LinearLayout) view.findViewById(R.id.novideo);
        withvideo = (ScrollView) view.findViewById(R.id.withvideo);

        takeavideo.setOnClickListener(this);
        selectgalery.setOnClickListener(this);

        videoView = (ImageView) view.findViewById(R.id.videoview);
        playvideo = (ImageView) view.findViewById(R.id.playvideo);

        playvideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uri = Uri.parse("file://" + fileloc);
                Intent viewIntent = new Intent("android.intent.action.VIEW", uri);
                viewIntent.setDataAndType(uri, "video/*");
                startActivity(viewIntent);
            }
        });

        subject = (EditText) view.findViewById(R.id.subject);
        note = (EditText) view.findViewById(R.id.note);

        subject.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                db.updatesubject(subject.getText().toString(), "1");
                Log.e("update", "yes" + subject.getText().toString());
            }
        });

        note.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                db.updatenote(note.getText().toString(), "1");
                Log.e("update n", "yes" + note.getText().toString());
            }
        });

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Checking camera availability
        if (!isDeviceSupportCamera()) {
            Toast.makeText(getActivity(),
                    "Sorry! Your device doesn't support camera",
                    Toast.LENGTH_LONG).show();
            // will close the app if the device does't have camera
        }

        dialog4 = new Dialog(getActivity(), R.style.FullHeightDialog);
        dialog4.setCanceledOnTouchOutside(false);
        dialog4.setContentView(R.layout.loading);
        dialog4.contentMargin(0, 0, 0, -25);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!(db.getAllVideos().size() <= 0)) {
            Glide.with(getActivity()).load(db.getAllVideos().get(0).getVideo_url()).into(videoView);
            fileloc = db.getAllVideos().get(0).getVideo_url();
            novideo.setVisibility(novideo.GONE);
            withvideo.setVisibility(withvideo.VISIBLE);
            selectgalery.setClickable(true);
            subject.setText(db.getAllVideos().get(0).getSubject());
            note.setText(db.getAllVideos().get(0).getNote());
            video = db.getAllVideos().get(0);
            selectgalery.setAlpha(1);
            ismaylaman = true;
            videoba = true;
        } else {
            novideo.setVisibility(novideo.VISIBLE);
            withvideo.setVisibility(withvideo.GONE);
            selectgalery.setClickable(false);
            selectgalery.setAlpha((float) 0.2);
            ismaylaman = false;
            videoba = false;
        }
    }

    private boolean isDeviceSupportCamera() {
        if (getActivity().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.takeapicture:

                if ((db.getAllVideos().size() <= 0)) {
                    Dialog.Builder builder = null;
                    builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                        @Override
                        public void onPositiveActionClicked(DialogFragment fragment2) {
                            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                            fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);
                            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY,
                                    MediaRecorder.OutputFormat.MPEG_4);
                            intent.putExtra("EXTRA_VIDEO_QUALITY", 0);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//                                    intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, 25491520L);
                            intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 120);
                            intent.putExtra("android.intent.extras.CAMERA_FACING", 0);
                            startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
                            Time now = new Time();
                            now.setToNow();
                            long time = System.currentTimeMillis();
                            tempStarttime = time + "";
                            tempLocation = getGPS();
                            HashMap<String, String> data = new HashMap<String, String>();
                            data.put("tempStarttime", tempStarttime);
                            data.put("tempLocation", tempLocation);
                            session.createVideoInfo(data);
                            super.onPositiveActionClicked(fragment2);
                        }
                    };

                    ((SimpleDialog.Builder) builder).message("Please set the camera resolution to low quality mode.")
                            .title("Camera")
                            .positiveAction("OK");
                    DialogFragment fragment11 = DialogFragment.newInstance(builder);
                    fragment11.setCancelable(false);
                    fragment11.show(getFragmentManager(), null);

                } else {
                    Dialog.Builder builder4 = null;
                    builder4 = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                        @Override
                        public void onPositiveActionClicked(DialogFragment fragment) {

                            Dialog.Builder builder = null;
                            builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                                @Override
                                public void onPositiveActionClicked(DialogFragment fragment2) {
                                    Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                                    fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);
                                    intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY,
                                            MediaRecorder.OutputFormat.MPEG_4);
                                    intent.putExtra("EXTRA_VIDEO_QUALITY", 0);
                                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//                                    intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, 25491520L);
                                    intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 120);
                                    intent.putExtra("android.intent.extras.CAMERA_FACING", 0);
                                    startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
                                    Time now = new Time();
                                    now.setToNow();
                                    long time = System.currentTimeMillis();
                                    tempStarttime = time + "";
                                    tempLocation = getGPS();
                                    HashMap<String, String> data = new HashMap<String, String>();
                                    data.put("tempStarttime", tempStarttime);
                                    data.put("tempLocation", tempLocation);
                                    session.createVideoInfo(data);
                                    super.onPositiveActionClicked(fragment2);
                                }
                            };

                            ((SimpleDialog.Builder) builder).message("Please set the camera resolution to low quality mode.")
                                    .title("Camera")
                                    .positiveAction("OK");
                            DialogFragment fragment11 = DialogFragment.newInstance(builder);
                            fragment11.setCancelable(false);
                            fragment11.show(getFragmentManager(), null);

                            super.onPositiveActionClicked(fragment);
                        }

                        @Override
                        public void onNegativeActionClicked(DialogFragment fragment) {
                            super.onNegativeActionClicked(fragment);
                        }
                    };

                    ((SimpleDialog.Builder) builder4).message("Retake video?")
                            .title("Retake")
                            .positiveAction("OK")
                            .negativeAction("CANCEL");
                    DialogFragment fragment5 = DialogFragment.newInstance(builder4);

                    fragment5.setCancelable(false);
                    fragment5.show(getFragmentManager(), null);
                }


                break;
            case R.id.selectgalery:

                Dialog.Builder builder2 = null;
                builder2 = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                    @Override
                    public void onPositiveActionClicked(DialogFragment fragment) {

                        db.removeVideo();
                        novideo.setVisibility(novideo.VISIBLE);
                        withvideo.setVisibility(withvideo.GONE);
                        selectgalery.setClickable(false);
                        selectgalery.setAlpha((float) 0.2);
                        ismaylaman = false;
                        videoba = false;

                        super.onPositiveActionClicked(fragment);
                    }

                    @Override
                    public void onNegativeActionClicked(DialogFragment fragment) {
                        super.onNegativeActionClicked(fragment);
                    }
                };

                ((SimpleDialog.Builder) builder2).message("Delete video and data?")
                        .title("Delete")
                        .positiveAction("Ok")
                        .negativeAction("CANCEL");
                DialogFragment fragment3 = DialogFragment.newInstance(builder2);

                fragment3.setCancelable(false);
                fragment3.show(getFragmentManager(), null);


//                Intent intent2 = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
//                fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);
//                intent2.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
//                intent2.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
////                intent2.putExtra(MediaStore.EXTRA_SIZE_LIMIT, 5491520L);
//                intent2.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 119);
//                startActivityForResult(intent2, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);

                break;
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }


    private void launchUploadActivity() {
        Intent i = new Intent(getActivity(), VideoViewActivity.class);
        i.putExtra("filePath", fileUri.getPath());
        startActivity(i);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
        outState.putString("file_uri", fileUri + "");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                db.removeVideo();
                if (!fileUri.getPath().isEmpty()) {
                    long time = System.currentTimeMillis();
                    String files = fileUri.getPath() + "";

                    VideoModel videotest = new VideoModel();

                    videotest.setVideo_url(files);
                    videotest.setStartCapture(session.getTemStartime());
                    videotest.setStartLocation(session.getTemStarloc());
                    videotest.setEndLocation(getGPS());
                    videotest.setEndCapture("" + time);
                    tempendtime = time + "";
                    tempendLocation = getGPS();
                    item.add(videotest);

                    db.addVideo(videotest);
                    Toast.makeText(getActivity(),
                            "Video Added", Toast.LENGTH_SHORT)
                            .show();
                    Glide.with(getActivity()).load(fileUri.getPath()).into(videoView);
                    fileloc = fileUri.getPath();
                    video = videotest;
                    ismaylaman = true;
                    videoba = true;

                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getActivity(),
                        "Cancelled", Toast.LENGTH_SHORT)
                        .show();
                tempStarttime = null;
                tempLocation = null;
                ismaylaman = false;
            } else {
            }
        } else if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE2) {
            Uri selectedImageUri = data.getData();
            if (!fileUri.getPath().isEmpty()) {
                VideoModel videotest = new VideoModel();
                String files = fileUri.getPath() + "";
                Log.e("file loc", files);
                videotest.setVideo_url(files);
                videotest.setGeotag("14.584618333333333, 121.07596333333335");
                item.add(videotest);
                db.addVideo(videotest);
                Toast.makeText(getActivity(),
                        "Video Added", Toast.LENGTH_SHORT)
                        .show();
                video = videotest;
            }
        }
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "AndroidFileUpload");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create "
                        + "AndroidFileUpload" + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }
        return mediaFile;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_video, menu);
        send = (MenuItem) menu.findItem(R.id.save);
//        savemen = (MenuItem) menu.findItem(R.id.savechecklist);
//        updatechecklist.setVisible(false);
//        if (ismaylaman) {
        send.setVisible(true);
//        } else {
//            send.setVisible(false);
//        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.save:
                if (connection.isConnectedToInternet()) {
                    if (videoba) {
                        if ((subject.getText().toString().equals("")) || (note.getText().toString().equals(""))) {
                            Toast.makeText(getActivity(), "Please complete the form.", Toast.LENGTH_SHORT).show();
                        } else {
                            uploadVideo();
//                            uploadFile();
//                            splitVideo();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Please complete the form.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "No Internet Connection.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public String getFileSize() {
        String re = null;
        try {
            File file = new File(fileloc);
            long length = file.length();
            length = length / 1024;
            System.out.println("File Path : " + file.getPath() + ", File size : " + length + " KB");
            re = length + "";
        } catch (Exception e) {
            System.out.println("File not found : " + e.getMessage() + e);
        }
        return re;
    }

    private OkHttpClient getClient() {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(18, TimeUnit.MINUTES);
        client.setReadTimeout(18, TimeUnit.MINUTES);
        return client;
    }

    public void uploadVideo() {
        Log.e("Filesize", getFileSize());
        dialog4.show();
        Map<String, String> params = new HashMap<>();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setClient(new OkClient(getClient()))
                .setEndpoint(Constants.API_URL)
                .build();

//        TypedFile typedFile = new TypedFile("multipart/form-data", new File("/storage/emulated/0/Pictures/AndroidFileUpload/test.mp4"));
        TypedFile typedFile = new TypedFile("multipart/form-data", new File(fileloc));
        APIInterface interfacem = restAdapter.create(APIInterface.class);

        Log.e("to send",
                db.getAllVideos().get(0).getSubject() + "\n" +
                        db.getAllVideos().get(0).getNote() + "\n" +
                        db.getAllVideos().get(0).getStartCapture() + "\n" +
                        session.getToken() + "\n" +
                        db.getAllVideos().get(0).getEndCapture() + "\n" +
                        db.getAllVideos().get(0).getStartLocation() + "\n" +
                        db.getAllVideos().get(0).getEndLocation() + "\n" +
                        typedFile);

        interfacem.uploadVideos(session.getToken(),
                Constants.REQUEST_JOBINFO,
                db.getAllVideos().get(0).getSubject(),
                db.getAllVideos().get(0).getNote(),
                db.getAllVideos().get(0).getStartCapture(),
                db.getAllVideos().get(0).getEndCapture(),
                db.getAllVideos().get(0).getStartLocation(),
                db.getAllVideos().get(0).getEndLocation(),
                typedFile,
                new Callback<Object>() {
                    @Override
                    public void success(Object o, Response response) {
                        String jsonObjectResponse = new Gson().toJson(o);
                        Log.e("respone", jsonObjectResponse);
                        try {
                            JSONObject jsonObject = new JSONObject(jsonObjectResponse.toString());
                            String status = jsonObject.getString("status");
                            if (status.equals("true")) {
                                Dialog.Builder builder = null;
                                builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {

                                    @Override
                                    public void onPositiveActionClicked(DialogFragment fragment) {
                                        db.removeVideo();
                                        novideo.setVisibility(novideo.VISIBLE);
                                        withvideo.setVisibility(withvideo.GONE);
                                        selectgalery.setClickable(false);
                                        selectgalery.setAlpha((float) 0.2);
                                        ismaylaman = false;
                                        videoba = false;
                                        deleteimageData(fileloc);
                                        super.onPositiveActionClicked(fragment);
                                    }

                                    @Override
                                    public void onNegativeActionClicked(DialogFragment fragment) {
                                        super.onNegativeActionClicked(fragment);
                                    }
                                };
                                ((SimpleDialog.Builder) builder).message("Video has been successfully uploaded!")
                                        .title("Video")
                                        .positiveAction("OK");
                                DialogFragment fragment = DialogFragment.newInstance(builder);
                                fragment.setCancelable(false);
                                fragment.show(getFragmentManager(), null);
                                dialog4.hide();
                            } else {
                                retryit();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog4.hide();
                        }
                    }

                    @Override
                    public void failure(RetrofitError e) {
                        Log.e("ERROR", e.toString());
                        retryit();
                    }
                });
    }

    public void deleteimageData(String url) {
        Log.e(TAG, "deleteimageData: " + url);
        String[] separated = url.split("/");
        String finame =  separated[separated.length -1];

        File file = new File(url.replace(finame,""), finame);
        boolean deleted = file.delete();
        if(deleted){
            Log.e(TAG, "Deleted: ");
        }else{
            Log.e(TAG, "Error while deleting: ");
        }
    }

    public void retryit() {

        Dialog.Builder builder = null;
        builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                dialog4.hide();
                uploadVideo();
                super.onPositiveActionClicked(fragment);
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                super.onNegativeActionClicked(fragment);
            }
        };

        ((SimpleDialog.Builder) builder).message("Please try again.")
                .title("Error while uploading")
                .positiveAction("RETRY")
                .negativeAction("CANCEL");
        DialogFragment fragment = DialogFragment.newInstance(builder);

        fragment.setCancelable(false);
        fragment.show(getFragmentManager(), null);

        dialog4.hide();
    }

    private String getGPS() {
        String location = "";
        LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = lm.getProviders(true);
        /* Loop over the array backwards, and if you get an accurate location, then break out the loop*/
        Location l = null;

        for (int i = providers.size() - 1; i >= 0; i--) {
            l = lm.getLastKnownLocation(providers.get(i));
            if (l != null) break;
        }
        double[] gps = new double[2];
        if (l != null) {
            gps[0] = l.getLatitude();
            gps[1] = l.getLongitude();
            location = l.getLatitude() + ", " + l.getLongitude();
        } else {
            location = "-27.4996872,153.0354553";
        }
        return location;
    }

}