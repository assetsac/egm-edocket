package com.evolution.egm.Fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.evolution.egm.Adapters.AdapterFleetmembers;
import com.evolution.egm.Utils.SessionManager;
import com.evolution.egm.Models.FleetNames;
import com.evolution.egm.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mcclynreyarboleda on 4/13/15.
 */
public class CreateJob5 extends Fragment {
    private ListView listview;
    private AdapterFleetmembers adapter;
    private List<FleetNames> item = new ArrayList<>();
    public int countfield = 1;
    RelativeLayout nextfirst, nextButton, previos, save;
    MenuItem cancel, selectall, add, deselectall;
    private static final String ARG_SECTION_NUMBER = "section_number";

    public OnnextandPre mListener;
    private String token;
    public SessionManager session;
    public CreateJob5() {

    }

    public static CreateJob5 newInstance(int sectionNumber) {

        CreateJob5 fragment = new CreateJob5();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem itemb) {
        switch (itemb.getItemId()) {
            case R.id.additem:

                FleetNames m = new FleetNames();
                m.setFleetid("" + countfield);
                m.setFleetname("TEST");
                item.add(m);
                countfield++;
                adapter.updateList();

                return false;
            case R.id.deleleitem:

                if (add.isVisible()==true) {
                    showCheckboxes();
                }
                for (int x = 0; x < adapter.getCount(); x++) {
                    CheckBox isdelete = (CheckBox) listview.getChildAt(x).findViewById(R.id.isdelete);
                    if (isdelete.isChecked() == true) {
                        countfield--;
                        Log.e("checked", "ckeck == " + x + " listitem == " + countfield);
                        item.remove(item.get(x));
                        listview.invalidateViews();
                    }
                }

                return true;
            case R.id.cancel:
                for (int x = 0; x < adapter.getCount(); x++) {
                    CheckBox isdelete = (CheckBox) listview.getChildAt(x).findViewById(R.id.isdelete);
                    isdelete.setVisibility(isdelete.GONE);
                    Log.d("sd", "" + x);
                }
                cancel.setVisible(false);
                selectall.setVisible(false);
                deselectall.setVisible(false);
                add.setVisible(true);

                break;

            case R.id.selectall:
                selectall.setVisible(false);
                deselectall.setVisible(true);

                for (int x = 0; x < adapter.getCount(); x++) {
                    CheckBox isdelete = (CheckBox) listview.getChildAt(x).findViewById(R.id.isdelete);
                    isdelete.setChecked(true);
                    Log.d("sd", "" + x);
                }

                break;
            case R.id.deselectall:

                selectall.setVisible(true);
                deselectall.setVisible(false);

                for (int x = 0; x < adapter.getCount(); x++) {
                    CheckBox isdelete = (CheckBox) listview.getChildAt(x).findViewById(R.id.isdelete);
                    isdelete.setChecked(false);
                    Log.d("sd", "" + x);
                }
                break;
            default:
                break;
        }
        return false;
    }


    public void showCheckboxes() {

        cancel.setVisible(true);
        selectall.setVisible(true);
        add.setVisible(false);

        for (int x = 0; x < adapter.getCount(); x++) {
            CheckBox isdelete = (CheckBox) listview.getChildAt(x).findViewById(R.id.isdelete);
            isdelete.setVisibility(isdelete.VISIBLE);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        generateDummyField();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_main_page, menu);
        cancel = (MenuItem) menu.findItem(R.id.cancel);
        add = (MenuItem) menu.findItem(R.id.additem);
        selectall = (MenuItem) menu.findItem(R.id.selectall);
        deselectall = (MenuItem) menu.findItem(R.id.deselectall);
        cancel.setVisible(false);
        selectall.setVisible(false);
        deselectall.setVisible(false);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_createjob5, container, false);
        previos = (RelativeLayout) view.findViewById(R.id.previos);
        nextButton = (RelativeLayout) view.findViewById(R.id.nextButton);
        listview = (ListView) view.findViewById(R.id.listview);
        adapter = new AdapterFleetmembers(item, getActivity());

        previos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onPre();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onNext();
            }
        });

        listview.setAdapter(adapter);
        return view;
    }


    public void generateDummyField() {

        FleetNames m = new FleetNames();
        m.setFleetid("" + countfield);
        m.setFleetname("TEST");
        item.add(m);
        countfield++;


    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        session = new SessionManager(getActivity());
        token = session.getToken();

        try {
            mListener = (OnnextandPre) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }

    public interface OnnextandPre {
        // TODO: Update argument type and name
        public void onNext();
        public void onPre();
    }


}
