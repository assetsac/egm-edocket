package com.evolution.egm.Fragments;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.evolution.egm.Adapters.AdapterSiteChecklistSignature;
import com.evolution.egm.Utils.ConnectionDetector;
import com.evolution.egm.Utils.Constants;
import com.evolution.egm.Utils.CreateJobSharedPreference;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Utils.SessionManager;
import com.evolution.egm.Interface.APIInterface;
import com.evolution.egm.Interface.SiteChecklistSignatureInterface;
import com.evolution.egm.Models.Job;
import com.evolution.egm.Models.SiteCheckListSignatureModel;
import com.evolution.egm.R;
import com.google.gson.Gson;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;
import com.squareup.okhttp.OkHttpClient;

import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

/**
 * Created by McClynrey on 3/1/2016.
 */
public class SiteChecklistSignature extends Fragment implements SiteChecklistSignatureInterface {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private String token;
    public SessionManager session;
    private ConnectionDetector connection;
    public OnnextandPre mListener;
    OkHttpClient okHttpClient;
    Job mJobObject;
    com.rey.material.app.Dialog dialogdd;
    DatabaseHelper db;
    ArrayList<SiteCheckListSignatureModel> mDataset;
    MenuItem updatechecklist, savemen;
    CreateJobSharedPreference createjob;

    final CharSequence ifsubjob = ".";

    public static SiteChecklistSignature newInstance(int sectionNumber) {
        SiteChecklistSignature fragment = new SiteChecklistSignature();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public SiteChecklistSignature() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(60 * 1000, TimeUnit.MILLISECONDS);
        connection = new ConnectionDetector(getActivity());
        db = new DatabaseHelper(getActivity());
        setHasOptionsMenu(true);

        dialogdd = new com.rey.material.app.Dialog(getActivity(), R.style.FullHeightDialog);
        dialogdd.setCanceledOnTouchOutside(false);
        dialogdd.setContentView(R.layout.loadingsyncsite);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sitechecklist_signature, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.tclist);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        Bundle extras = getActivity().getIntent().getExtras();
        if (extras != null) {
            mJobObject = Parcels.unwrap(getActivity().getIntent().getExtras().getParcelable("data"));
        }
        mDataset = new ArrayList<>();
        for (int c = 0; c < db.getallTimesheets(mJobObject.getJobID()).getTimesheets().size(); c++) {
            SiteCheckListSignatureModel site = new SiteCheckListSignatureModel();
            site.setJobID(mJobObject.getJobID());
            site.setTCName(db.getallTimesheets(mJobObject.getJobID()).getTimesheets().get(c).getName());
            site.setAttachedOn("");
            site.setAttachment("");
            mDataset.add(site);
        }
        mAdapter = new AdapterSiteChecklistSignature(mDataset, getActivity(), this);
        mRecyclerView.setAdapter(mAdapter);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.sitechecklist, menu);
        updatechecklist = (MenuItem) menu.findItem(R.id.update_checklist);
        savemen = (MenuItem) menu.findItem(R.id.savechecklist);
        updatechecklist.setVisible(false);
        savemen.setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem itemb) {
        switch (itemb.getItemId()) {
            case R.id.savechecklist:
                Gson gson = new Gson();
                if (!gson.toJson(mDataset).contains("\"Attachment\":\"\"")) {
                    com.rey.material.app.Dialog.Builder builder = null;
                    builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                        @Override
                        public void onPositiveActionClicked(DialogFragment fragment) {
                            if (!connection.isConnectedToInternet()) {
                                db.addreUploadJob(mJobObject.getJobID());
                                dialogdd.dismiss();
                                mListener.saving();
                            } else {
                                syncSiteChecklist();
                            }
                            super.onPositiveActionClicked(fragment);
                        }

                        @Override
                        public void onNegativeActionClicked(DialogFragment fragment) {
                            super.onNegativeActionClicked(fragment);
                        }
                    };
                    ((SimpleDialog.Builder) builder).message("Site Checklist form will be uploaded and locked from editing. Would you like to continue?")
                            .title("Upload Site Checklist")
                            .positiveAction("OK")
                            .negativeAction("CANCEL");

                    DialogFragment fragment = DialogFragment.newInstance(builder);
                    fragment.show(getFragmentManager(), null);

                } else {
                    Toast.makeText(getActivity(), "Please complete all the signatures.", Toast.LENGTH_LONG).show();
                }
                break;
            case android.R.id.home:
                mListener.onPre();
                break;
        }
        return false;
    }

    @Override
    public void updateSignature(ArrayList<SiteCheckListSignatureModel> mDataset) {
        this.mDataset = mDataset;
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        session = new SessionManager(getActivity());
        token = session.getToken();
        createjob = new CreateJobSharedPreference(getActivity());
        try {
            mListener = (OnnextandPre) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public interface OnnextandPre {
        public void onNext();

        public void onPre();

        void saving();
    }

    public String getAllAdditionalHazard() {
        Gson mac = new Gson();
        String re = "";
        String de = db.checkhazard(mJobObject.getJobID()).replace("]", "");
        if (db.checkhazard(mJobObject.getJobID()).contains("\"AddHazard\":\"\"")) {
            re = "[]";
        } else {
            if (db.getAdditionalHazard(mJobObject.getJobID()).size() != 0) {
                de = de + ",";
            }
            re = de + mac.toJson(db.getAdditionalHazard(mJobObject.getJobID())).replace("[", "");
        }
        return re;
    }

    public String getSiteChecklist() {
        String temp = "";
        if (db.getSiteChecklist(mJobObject.getJobID()) == null) {

        } else {
            temp = db.getSiteChecklist(mJobObject.getJobID());
        }
        return temp;
    }

    public void syncSiteChecklist() {
        dialogdd.show();
        boolean cc = mJobObject.getJobID().contains(ifsubjob);
        Log.e("teststeset", getAllAdditionalHazard());
        String Checklist2 = getSiteChecklist().replace("|~", "~");
        String Checklist = Checklist2.replace("^x", "^n/a");
        Map<String, String> params = new HashMap<>();
        params.put("operatorID", mJobObject.getOperatorID().toString());
        params.put("jobID", mJobObject.getJobID());
        params.put("timesheets", "[]");
        params.put("assets", "[]");
        params.put("requirements", "[]");
        params.put("dockets", "[]");
        params.put("siteCheckList", Checklist);
        params.put("signageAudit", "[]");
        params.put("tcAttachment", "[]");
        params.put("orderNumber", mJobObject.getOrderNumber());
        if (cc) {
            params.put("parentJob", mJobObject.getId());
        } else {
            params.put("parentJob", "");
        }
        params.put("additionalHazards", getAllAdditionalHazard().replace("\n", ""));

        if (!db.getotherfield(mJobObject.getJobID()).equals("wala")) {
            String data = db.getotherfield(mJobObject.getJobID());
            String[] arr = data.split("~");
            params.put("clientEmail", "");
            params.put("clientName", "");
        } else {
            params.put("clientEmail", "");
            params.put("clientName", "");
        }

        params.put("geotag", "-27.4996872,153.0354553");
        String hashmap = "operatorID :" + params.get("operatorID").toString() + "\n\n" +
                "jobID :" + params.get("jobID").toString() + "\n\n" +
                "assets :" + params.get("assets").toString() + "\n\n" +
                "tcAttachment :" + params.get("tcAttachment").toString() + "\n\n" +
                "timesheets :" + params.get("timesheets").toString() + "\n\n" +
                "requirements :" + params.get("requirements").toString() + "\n\n" +
                "dockets :" + params.get("dockets").toString() + "\n\n" +
                "siteCheckList :" + params.get("siteCheckList").toString() + "\n\n" +
                "signageAudit :" + params.get("signageAudit").toString() + "\n\n" +
                "additionalHazards :" + params.get("additionalHazards").toString() + "\n\n" +
                "parentJob :" + params.get("parentJob").toString() + "\n\n" +
                "clientEmail :" + params.get("clientEmail").toString() + "\n\n" +
                "geotag :" + "14.584456, 121.075952\n\n";

        try {
            File myFile = new File("/sdcard/csv-" + mJobObject.getJobID() + ".txt");
            myFile.createNewFile();
            FileOutputStream fOut = new FileOutputStream(myFile);
            OutputStreamWriter myOutWriter =
                    new OutputStreamWriter(fOut);
            myOutWriter.append(hashmap);
            myOutWriter.close();
            fOut.close();
        } catch (Exception e) {
            Toast.makeText(getActivity(), e.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setClient(new OkClient(okHttpClient))
                .setEndpoint(Constants.API_URL)
                .build();
        APIInterface upload = restAdapter.create(APIInterface.class);
        upload.siteChecklistSync(session.getToken(), Constants.REQUEST_JOBINFO, params, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                String jsonObjectResponse = new Gson().toJson(o);
                Log.e("Output", "" + jsonObjectResponse.toString());
                JSONObject jsonObject = null;
                dialogdd.dismiss();
                try {
                    jsonObject = new JSONObject(jsonObjectResponse.toString());
                    if (jsonObject.get("status").toString().equals("false")) {
                        Toast.makeText(getActivity(), "Uploading site checklist was unsuccessful.", Toast.LENGTH_SHORT).show();
                        dialogdd.dismiss();
                        mListener.saving();
                        Gson gson = new Gson();
                        HashMap<String, String> data = new HashMap<String, String>();
                        data.put("signatures",
                                gson.toJson(mDataset) + "]");
                        createjob.createSignatureArea(data);
                        db.updateContact(mJobObject.getJobID(), "", "", gson.toJson(mDataset) + "]", "", "1");
                    } else {
                        Toast.makeText(getActivity(), "Site Checklist has been uploaded.", Toast.LENGTH_SHORT).show();
                        dialogdd.dismiss();
                        mListener.saving();
                        Gson gson = new Gson();
                        HashMap<String, String> data = new HashMap<String, String>();
                        data.put("signatures",
                                gson.toJson(mDataset) + "]");
                        createjob.createSignatureArea(data);
                        db.updateContact(mJobObject.getJobID(), "", "", gson.toJson(mDataset) + "]", "", "1");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Response r = error.getResponse();
                switch (error.getKind()) {
                    case NETWORK:
                        // TODO something
                        Log.e("ifsuccess", "failure:UNEXPECTED" + error.toString());
                        break;
                    case UNEXPECTED:
                        Log.e("ifsuccess", "failure:UNEXPECTED" + error.toString());
                        throw error;
                    case HTTP:
                        switch (r.getStatus()) {
                            case 401:
                                Log.e("ifsuccess", "failure:401" + error.toString());
                                break;
                            case 404:
                                // TODO something
                                Log.e("ifsuccess", "failure:404" + error.toString());
                                break;
                            case 500:
                                Log.e("ifsuccess", "failure:500" + error.toString());
                                // TODO something
                                break;
                        }
                        Log.e("error ", "error while connectiong to HTTP" + r.getStatus());
                        break;
                    default:
                        Log.e("error ", "error while connectiong to HTTP" + r.getStatus());
                        break;
                }

                Toast.makeText(getActivity(), "Uploading site checklist was unsuccessful.", Toast.LENGTH_SHORT).show();
                dialogdd.dismiss();
                mListener.saving();
                Gson gson = new Gson();
                HashMap<String, String> data = new HashMap<String, String>();
                data.put("signatures",
                        gson.toJson(mDataset) + "]");
                createjob.createSignatureArea(data);
                db.updateContact(mJobObject.getJobID(), "", "", gson.toJson(mDataset) + "]", "", "1");
            }
        });

    }
}
