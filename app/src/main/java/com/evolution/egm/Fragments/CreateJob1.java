package com.evolution.egm.Fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.evolution.egm.Utils.CreateJobSharedPreference;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Utils.DateHelper;
import com.evolution.egm.Utils.SessionManager;
import com.evolution.egm.Models.Job;
import com.evolution.egm.R;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.parceler.Parcels;

import java.util.HashMap;

/**
 * Created by mcclynreyarboleda on 4/13/15.
 */
public class CreateJob1 extends Fragment implements View.OnClickListener {
    String firstGroup = "", secondGroup = "", thirdGroup = "", fourthGroup = "", fifthGroup = "";
    public String selection;
    LinearLayout jobdiscription, loadingarea;
    private String token;
    public SessionManager session;
    public String JobID;
    TextView jobidtxt, CompanyNametxt, datetxt, Addresstxt, depottxt, authorisedby, sitecontact;
    RelativeLayout nextfirst;
    CreateJobSharedPreference createjob;
    View view;
    DateHelper date;
    TextView subjobdots;
    TextView editSub;
    MaterialEditText yestxt1, yestxt2, yestxt3, yestxt4, yestxt5;
    Job mJobObject;
    Bundle extras;
    RadioButton job1R1No, job1R1Yes, job1R2No, job1R2Yes, job1R3No, job1R3Yes, job1R4No, job1R4Yes, job1R5No, job1R5Yes;
    public OnFirstFragmentListener mListener;
    String returnData;
    DatabaseHelper db;

    public interface Test {
        public void onTrigger();
    }

    private static final String ARG_SECTION_NUMBER = "section_number";

    public CreateJob1() {

    }

    public static CreateJob1 newInstance(int sectionNumber) {
        CreateJob1 fragment = new CreateJob1();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        session = new SessionManager(getActivity());
        token = session.getToken();
        db = new DatabaseHelper(getActivity());
        createjob = new CreateJobSharedPreference(getActivity());
        try {
            mListener = (OnFirstFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        extras = getActivity().getIntent().getExtras();

        mJobObject = new Job();
        date = new DateHelper();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_createjob1, container, false);

        TextView clicktest = (TextView) view.findViewById(R.id.clicktest);

        nextfirst = (RelativeLayout) view.findViewById(R.id.nextfirst);
        editSub = (TextView) view.findViewById(R.id.editSub);
        subjobdots = (TextView) view.findViewById(R.id.subjobdots);

        yestxt1 = (MaterialEditText) view.findViewById(R.id.yestxt1);
        yestxt2 = (MaterialEditText) view.findViewById(R.id.yestxt2);
        yestxt3 = (MaterialEditText) view.findViewById(R.id.yestxt3);
        yestxt4 = (MaterialEditText) view.findViewById(R.id.yestxt4);
        yestxt5 = (MaterialEditText) view.findViewById(R.id.yestxt5);

        Bundle extras = getActivity().getIntent().getExtras();
        if(extras == null){

        }else {
            if (getActivity().getIntent().getStringExtra("SubJob").equals("k")) {
                editSub.setVisibility(editSub.VISIBLE);
                subjobdots.setVisibility(subjobdots.VISIBLE);
            } else {
                editSub.setVisibility(editSub.GONE);
                subjobdots.setVisibility(subjobdots.GONE);
            }
        }

        job1R1No = (RadioButton) view.findViewById(R.id.job1R1No);
        job1R1Yes = (RadioButton) view.findViewById(R.id.job1R1Yes);

        job1R2No = (RadioButton) view.findViewById(R.id.job1R2No);
        job1R2Yes = (RadioButton) view.findViewById(R.id.job1R2Yes);


        job1R3No = (RadioButton) view.findViewById(R.id.job1R3No);
        job1R3Yes = (RadioButton) view.findViewById(R.id.job1R3Yes);


        job1R4No = (RadioButton) view.findViewById(R.id.job1R4No);
        job1R4Yes = (RadioButton) view.findViewById(R.id.job1R4Yes);

        job1R5No = (RadioButton) view.findViewById(R.id.job1R5No);
        job1R5Yes = (RadioButton) view.findViewById(R.id.job1R5Yes);


        job1R1No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstGroup = "No";
            }
        });

        job1R1Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstGroup = "Yes";
            }
        });

        job1R2No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                secondGroup = "No";
            }
        });

        job1R2Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                secondGroup = "Yes";
            }
        });

        job1R3No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                thirdGroup = "No";
            }
        });

        job1R3Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                thirdGroup = "Yes";
            }
        });

        job1R4No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fourthGroup = "No";
            }
        });

        job1R4Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fourthGroup = "Yes";
            }
        });

        job1R5No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fifthGroup = "No";
            }
        });

        job1R5Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fifthGroup = "Yes";
            }
        });

        jobidtxt = (TextView) view.findViewById(R.id.jobid);
        CompanyNametxt = (TextView) view.findViewById(R.id.CompanyName);
        datetxt = (TextView) view.findViewById(R.id.date);
        Addresstxt = (TextView) view.findViewById(R.id.Address);

        depottxt = (TextView) view.findViewById(R.id.depottxt);
        authorisedby = (TextView) view.findViewById(R.id.authorisedby);
        sitecontact = (TextView) view.findViewById(R.id.sitecontact);
        jobdiscription = (LinearLayout) view.findViewById(R.id.jobdiscription);
        loadingarea = (LinearLayout) view.findViewById(R.id.loadingarea);

        if (extras != null) {
            mJobObject = Parcels.unwrap(getActivity().getIntent().getExtras().getParcelable("data"));
            jobidtxt.setText(mJobObject.getJobID());
            CompanyNametxt.setText(mJobObject.getCompanyName());
            datetxt.setText(date.miliSecondConverter(mJobObject.getStartDateTime(), "EEE, MMM d, yy"));
            Addresstxt.setText(mJobObject.getAddress());
            depottxt.setText(mJobObject.getDepot());
            authorisedby.setText(mJobObject.getAuthorizedPerson());
            sitecontact.setText(mJobObject.getContactPerson());
        }

        jobdiscription.setVisibility(jobdiscription.VISIBLE);
        nextfirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                firstGroup = firstGroup + "::" + yestxt1.getText().toString();
                secondGroup = secondGroup + "::" + yestxt2.getText().toString();
                thirdGroup = thirdGroup + "::" + yestxt3.getText().toString();
                fourthGroup = fourthGroup + "::" + yestxt4.getText().toString();
                fifthGroup = fifthGroup + "::" + yestxt5.getText().toString();

                HashMap<String, String> data = new HashMap<String, String>();
                returnData = "Traffic Control Vehicles: " + yestxt1.getText() + ", Arrowboard: " + yestxt2.getText() + ", Bump Truck: " + yestxt3.getText() + ", VMS: " + yestxt4.getText() + ", Police Special Duty:" + yestxt5.getText();
                Log.e("returnData", returnData);
                data.put("first", returnData);
                createjob.createFirstItem(data);

                mListener.onFirst(data);

            }
        });
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.job1R1No:
                this.selection = "No";
                break;
        }
    }

    public void getCheckBox() {
        Log.e("test log", "test fragment to activity");
    }


    public interface OnFirstFragmentListener {
        // TODO: Update argument type and name
        public void onFirst(HashMap<String, String> data);
    }

}
