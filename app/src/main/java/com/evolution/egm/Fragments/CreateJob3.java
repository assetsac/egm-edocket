package com.evolution.egm.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.evolution.egm.Utils.ConnectionDetector;
import com.evolution.egm.Utils.Constants;
import com.evolution.egm.Utils.CreateJobSharedPreference;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Utils.Encoder;
import com.evolution.egm.Utils.SessionManager;
import com.evolution.egm.Interface.APIInterface;
import com.evolution.egm.Models.Job;
import com.evolution.egm.Models.Timesheet;
import com.evolution.egm.R;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;
import com.squareup.okhttp.OkHttpClient;

import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

/**
 * Created by mcclynreyarboleda on 4/13/15.
 */
public class CreateJob3 extends Fragment implements View.OnClickListener {

    MenuItem updatechecklist, savemen;
    RelativeLayout nextfirst, nextButton, previos;
    LinearLayout firstsignature, signature2, signature3, signature4, signature5, signature6, signature7, signature8, signature9, signature10;
    EditText namefield, namefield2, namefield3, namefield4, namefield5, namefield6, namefield7, namefield8, namefield9, namefield10;

    CreateJobSharedPreference createjob;
    private static final String ARG_SECTION_NUMBER = "section_number";
    private String token;
    public SessionManager session;
    private ConnectionDetector connection;
    OkHttpClient okHttpClient;
    Job mJobObject;
    DatabaseHelper db;
    final CharSequence ifsubjob = ".";
    String ret;
    public OnnextandPre mListener;
    String Jobs;
    int c = 0;
    private ArrayList<Timesheet> timesheetmem = new ArrayList<Timesheet>();
    public String sig1, sig2, sig3, sig4, sig5, sig6, sig7, sig8, sig9, sig10;
    public String sigString1 = "", sigString2 = "", sigString3 = "", sigString4 = "", sigString5 = "", sigString6 = "", sigString7 = "", sigString8 = "", sigString9 = "", sigString10 = "";
    boolean f = true;
    int counter = 0;
    com.rey.material.app.Dialog dialogdd;
    ProgressBar progresssaving;
    TextView statusbaritem;
    TextView statuspercentage;

    LinearLayout z1, z2, z3, z4, z5, z6, z7, z8, z9, z10;

    public CreateJob3() {

    }

    public static CreateJob3 newInstance(int sectionNumber) {
        CreateJob3 fragment = new CreateJob3();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(60 * 1000, TimeUnit.MILLISECONDS);
        connection = new ConnectionDetector(getActivity());
        dialogdd = new com.rey.material.app.Dialog(getActivity(), R.style.FullHeightDialog);
        setHasOptionsMenu(true);
        db = new DatabaseHelper(getActivity());
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_createjob3, container, false);
        Bundle extras = getActivity().getIntent().getExtras();

        if (extras != null) {
            mJobObject = Parcels.unwrap(getActivity().getIntent().getExtras().getParcelable("data"));
        }

        previos = (RelativeLayout) view.findViewById(R.id.previos);
        nextButton = (RelativeLayout) view.findViewById(R.id.nextButton);

        z1 = (LinearLayout) view.findViewById(R.id.z1);
        z2 = (LinearLayout) view.findViewById(R.id.z2);
        z3 = (LinearLayout) view.findViewById(R.id.z3);
        z4 = (LinearLayout) view.findViewById(R.id.z4);
        z5 = (LinearLayout) view.findViewById(R.id.z5);
        z6 = (LinearLayout) view.findViewById(R.id.z7);
        z7 = (LinearLayout) view.findViewById(R.id.z8);
        z8 = (LinearLayout) view.findViewById(R.id.z8);
        z9 = (LinearLayout) view.findViewById(R.id.z9);
        z10 = (LinearLayout) view.findViewById(R.id.z10);

        namefield = (EditText) view.findViewById(R.id.namefield);
        namefield2 = (EditText) view.findViewById(R.id.namefield2);
        namefield3 = (EditText) view.findViewById(R.id.namefiel3);
        namefield4 = (EditText) view.findViewById(R.id.namefield4);
        namefield5 = (EditText) view.findViewById(R.id.namefield5);
        namefield6 = (EditText) view.findViewById(R.id.namefield6);
        namefield7 = (EditText) view.findViewById(R.id.namefield7);
        namefield8 = (EditText) view.findViewById(R.id.namefield8);
        namefield9 = (EditText) view.findViewById(R.id.namefield9);
        namefield10 = (EditText) view.findViewById(R.id.namefield10);

        timesheetmem = db.getallTimesheets(mJobObject.getJobID()).getTimesheets();
        for (int c = 0; c < timesheetmem.size(); c++) {
            switch (c) {
                case 0:
                    namefield.setText(timesheetmem.get(c).getName());
                    namefield.setEnabled(false);
                    z1.setVisibility(z1.VISIBLE);
                    f = false;
                    break;
                case 1:
                    namefield2.setText(timesheetmem.get(c).getName());
                    namefield2.setEnabled(false);
                    z2.setVisibility(z2.VISIBLE);
                    f = false;
                    break;
                case 2:
                    namefield3.setText(timesheetmem.get(c).getName());
                    namefield3.setEnabled(false);
                    z3.setVisibility(z3.VISIBLE);
                    f = false;
                    break;
                case 3:
                    namefield4.setText(timesheetmem.get(c).getName());
                    namefield4.setEnabled(false);
                    z4.setVisibility(z4.VISIBLE);
                    f = false;
                    break;
                case 4:
                    namefield5.setText(timesheetmem.get(c).getName());
                    namefield5.setEnabled(false);
                    z5.setVisibility(z5.VISIBLE);
                    f = false;
                    break;
                case 5:
                    namefield6.setText(timesheetmem.get(c).getName());
                    namefield6.setEnabled(false);
                    z6.setVisibility(z6.VISIBLE);
                    f = false;
                    break;
                case 6:
                    namefield7.setText(timesheetmem.get(c).getName());
                    namefield7.setEnabled(false);
                    z7.setVisibility(z7.VISIBLE);
                    f = false;
                    break;
                case 7:
                    namefield8.setText(timesheetmem.get(c).getName());
                    namefield8.setEnabled(false);
                    z8.setVisibility(z8.VISIBLE);
                    f = false;
                    break;
                case 8:
                    namefield9.setText(timesheetmem.get(c).getName());
                    namefield9.setEnabled(false);
                    z9.setVisibility(z9.VISIBLE);
                    f = false;
                    break;
                case 9:
                    namefield10.setText(timesheetmem.get(c).getName());
                    namefield10.setEnabled(false);
                    z10.setVisibility(z10.VISIBLE);
                    f = false;
                    break;
            }
        }
        previos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onPre();
            }
        });
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        firstsignature = (LinearLayout) view.findViewById(R.id.firstsignature);
        signature2 = (LinearLayout) view.findViewById(R.id.signature2);
        signature3 = (LinearLayout) view.findViewById(R.id.signature3);
        signature4 = (LinearLayout) view.findViewById(R.id.signature4);
        signature5 = (LinearLayout) view.findViewById(R.id.signature5);
        signature6 = (LinearLayout) view.findViewById(R.id.signature6);
        signature7 = (LinearLayout) view.findViewById(R.id.signature7);
        signature8 = (LinearLayout) view.findViewById(R.id.signature8);
        signature9 = (LinearLayout) view.findViewById(R.id.signature9);
        signature10 = (LinearLayout) view.findViewById(R.id.signature10);

        firstsignature.setOnClickListener(this);
        signature2.setOnClickListener(this);
        signature3.setOnClickListener(this);
        signature4.setOnClickListener(this);
        signature5.setOnClickListener(this);
        signature6.setOnClickListener(this);
        signature7.setOnClickListener(this);
        signature8.setOnClickListener(this);
        signature9.setOnClickListener(this);
        signature10.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {

        dialogdd.setCanceledOnTouchOutside(false);
        dialogdd.setContentView(R.layout.loadingsyncsite);

        statusbaritem = (TextView) dialogdd.findViewById(R.id.statusbaritem);
        statuspercentage = (TextView) dialogdd.findViewById(R.id.statuspercentage);
        progresssaving = (ProgressBar) dialogdd.findViewById(R.id.progressBar7);

        progresssaving.getProgressDrawable().setColorFilter(Color.LTGRAY, PorterDuff.Mode.SRC_IN);
        progresssaving.setProgress(5);
        dialogdd.contentMargin(0, 0, 0, -25);

        final Dialog dialog = new Dialog(getActivity(), R.style.FullHeightDialog);
        dialog.setContentView(R.layout.dialog_signature);
        final LinearLayout cancel = (LinearLayout) dialog.findViewById(R.id.cancel);
        final LinearLayout save = (LinearLayout) dialog.findViewById(R.id.save);
        final SignaturePad mSignaturePad = (SignaturePad) dialog.findViewById(R.id.signaturearea);
        final LinearLayout clear = (LinearLayout) dialog.findViewById(R.id.clear);
        final TextView signaturename = (TextView) dialog.findViewById(R.id.signaturename);
        final ImageView signatureprev = (ImageView) dialog.findViewById(R.id.signatureprev);

        clear.setEnabled(false);
        save.setEnabled(true);
        switch (v.getId()) {
            case R.id.firstsignature:
                if (!namefield.getText().toString().equals("")) {
                    signaturename.setText(namefield.getText().toString());
                    if (sigString1.equals("")) {
                        signatureprev.setVisibility(signatureprev.GONE);
                        mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
                    } else {
                        byte[] decodedString = Base64.decode(sigString1, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        signatureprev.setImageBitmap(decodedByte);
                        clear.setEnabled(true);
                        signatureprev.setVisibility(signatureprev.VISIBLE);
                        mSignaturePad.setVisibility(mSignaturePad.GONE);
                    }
                    save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (clear.isEnabled()) {
                                Log.e("tes", sigString1);
                                if (!sigString1.equals("")) {
                                    dialog.hide();
                                } else {
                                    Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                                    String m = Encoder.encodeTobase64(signatureBitmap);
                                    if (addSignatureToGallery(signatureBitmap, namefield.getText().toString())) {
                                        long millisStart = Calendar.getInstance().getTimeInMillis();
                                        sigString1 = m;

                                        sig1 = "{" +
                                                "\"JobID\": \"" + mJobObject.getJobID() + "\"," +
                                                "\"TCName\": \"" + namefield.getText().toString() + "\"," +
                                                "\"AttachedOn\": \"" + millisStart + "\"," +
                                                "\"Attachment\": \"" + m + "\"" +
                                                "        }";
                                        c = 1;
                                        f = true;
                                        Toast.makeText(getActivity(), "Signature has  been saved!", Toast.LENGTH_SHORT).show();
                                        dialog.hide();
                                        firstsignature.setBackgroundResource(R.color.greencolor);
                                    } else {
                                        Toast.makeText(getActivity(), "Unable to store the signature", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            } else {
                                sig1 = "";
                                sigString1 = "";
                                c = 0;
                                f = false;
                                dialog.hide();
                                firstsignature.setBackgroundResource(R.color.skyblue);
                            }
                        }
                    });
                    clear.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mSignaturePad.clear();
                            clear.setEnabled(false);
                            signatureprev.setVisibility(signatureprev.GONE);
                            mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
                        }
                    });
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
                        @Override
                        public void onSigned() {
                            save.setEnabled(true);
                            clear.setEnabled(true);
                        }

                        @Override
                        public void onClear() {
                            sig1 = "";
                            c = 0;
                            signatureprev.setVisibility(signatureprev.GONE);
                            f = false;
                            sigString1 = "";
                            firstsignature.setBackgroundResource(R.color.skyblue);
                            clear.setEnabled(false);
                        }
                    });
                    dialog.show();
                } else {
                    Toast.makeText(getActivity(), "Please add your name first.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.signature2:
                if (!namefield2.getText().toString().equals("")) {
                    signaturename.setText(namefield2.getText().toString());
                    if (sigString2.equals("")) {
                        signatureprev.setVisibility(signatureprev.GONE);
                        mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
                    } else {
                        byte[] decodedString = Base64.decode(sigString2, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        signatureprev.setImageBitmap(decodedByte);
                        clear.setEnabled(true);
                        signatureprev.setVisibility(signatureprev.VISIBLE);
                        mSignaturePad.setVisibility(mSignaturePad.GONE);
                    }
                    save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (clear.isEnabled()) {
                                Log.e("tes", sigString2);
                                if (!sigString2.equals("")) {
                                    dialog.hide();
                                } else {
                                    Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                                    String m = Encoder.encodeTobase64(signatureBitmap);
                                    if (addSignatureToGallery(signatureBitmap, namefield2.getText().toString())) {
                                        long millisStart = Calendar.getInstance().getTimeInMillis();
                                        sigString2 = m;
                                        sig2 = "{" +
                                                "\"JobID\": \"" + mJobObject.getJobID() + "\"," +
                                                "\"TCName\": \"" + namefield2.getText().toString() + "\"," +
                                                "\"AttachedOn\": \"" + millisStart + "\"," +
                                                "\"Attachment\": \"" + m + "\"" +
                                                "        }";
                                        c++;
                                        f = true;
                                        Toast.makeText(getActivity(), "Signature has  been saved!", Toast.LENGTH_SHORT).show();
                                        dialog.hide();
                                        signature2.setBackgroundResource(R.color.greencolor);
                                    } else {
                                        Toast.makeText(getActivity(), "Unable to store the signature", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            } else {
                                sig2 = "";
                                sigString2 = "";
                                c--;
                                f = false;
                                dialog.hide();
                                signature2.setBackgroundResource(R.color.skyblue);
                            }

                        }
                    });
                    clear.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mSignaturePad.clear();
                            clear.setEnabled(false);
                            signatureprev.setVisibility(signatureprev.GONE);
                            mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
                        }
                    });
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
                        @Override
                        public void onSigned() {
                            save.setEnabled(true);
                            clear.setEnabled(true);
                        }

                        @Override
                        public void onClear() {
                            sig2 = "";
                            c--;
                            signatureprev.setVisibility(signatureprev.GONE);
                            f = false;
                            sigString2 = "";
                            signature2.setBackgroundResource(R.color.skyblue);
                            clear.setEnabled(false);
                        }
                    });
                    dialog.show();
                } else {
                    Toast.makeText(getActivity(), "Please add your name first.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.signature3:
                if (!namefield3.getText().toString().equals("")) {
                    signaturename.setText(namefield3.getText().toString());

                    if (sigString3.equals("")) {
                        signatureprev.setVisibility(signatureprev.GONE);
                        mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
                    } else {
                        byte[] decodedString = Base64.decode(sigString3, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        signatureprev.setImageBitmap(decodedByte);
                        clear.setEnabled(true);
                        signatureprev.setVisibility(signatureprev.VISIBLE);
                        mSignaturePad.setVisibility(mSignaturePad.GONE);
                    }

                    save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (clear.isEnabled()) {
                                Log.e("tes", sigString3);
                                if (!sigString3.equals("")) {
                                    dialog.hide();
                                } else {
                                    Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                                    String m = Encoder.encodeTobase64(signatureBitmap);
                                    if (addSignatureToGallery(signatureBitmap, namefield3.getText().toString())) {
                                        long millisStart = Calendar.getInstance().getTimeInMillis();
                                        sigString3 = m;

                                        sig3 = "{" +
                                                "\"JobID\": \"" + mJobObject.getJobID() + "\"," +
                                                "\"TCName\": \"" + namefield3.getText().toString() + "\"," +
                                                "\"AttachedOn\": \"" + millisStart + "\"," +
                                                "\"Attachment\": \"" + m + "\"" +
                                                "        }";
                                        c++;

                                        f = true;
                                        Toast.makeText(getActivity(), "Signature has  been saved!", Toast.LENGTH_SHORT).show();
                                        dialog.hide();
                                        signature3.setBackgroundResource(R.color.greencolor);
                                    } else {
                                        Toast.makeText(getActivity(), "Unable to store the signature", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            } else {
                                sig3 = "";
                                sigString3 = "";
                                c--;
                                f = false;
                                dialog.hide();
                                signature3.setBackgroundResource(R.color.skyblue);
                            }

                        }
                    });
                    clear.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mSignaturePad.clear();
                            clear.setEnabled(false);
                            signatureprev.setVisibility(signatureprev.GONE);
                            mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
                        }
                    });
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
                        @Override
                        public void onSigned() {
                            save.setEnabled(true);
                            clear.setEnabled(true);
                        }

                        @Override
                        public void onClear() {
                            sig3 = "";
                            c--;
                            signatureprev.setVisibility(signatureprev.GONE);
                            f = false;
                            sigString3 = "";
                            signature3.setBackgroundResource(R.color.skyblue);
//                            save.setEnabled(false);
                            clear.setEnabled(false);
                        }
                    });
                    dialog.show();
                } else {
                    Toast.makeText(getActivity(), "Please add your name first.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.signature4:
                if (!namefield4.getText().toString().equals("")) {
                    signaturename.setText(namefield4.getText().toString());
                    if (sigString4.equals("")) {
                        signatureprev.setVisibility(signatureprev.GONE);
                        mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
                    } else {
                        byte[] decodedString = Base64.decode(sigString4, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        signatureprev.setImageBitmap(decodedByte);
                        clear.setEnabled(true);
                        signatureprev.setVisibility(signatureprev.VISIBLE);
                        mSignaturePad.setVisibility(mSignaturePad.GONE);
                    }
                    save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (clear.isEnabled()) {
                                Log.e("tes", sigString4);
                                if (!sigString4.equals("")) {
                                    dialog.hide();
                                } else {
                                    Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                                    String m = Encoder.encodeTobase64(signatureBitmap);
                                    if (addSignatureToGallery(signatureBitmap, namefield4.getText().toString())) {
                                        long millisStart = Calendar.getInstance().getTimeInMillis();
                                        sigString4 = m;

                                        sig4 = "{" +
                                                "\"JobID\": \"" + mJobObject.getJobID() + "\"," +
                                                "\"TCName\": \"" + namefield4.getText().toString() + "\"," +
                                                "\"AttachedOn\": \"" + millisStart + "\"," +
                                                "\"Attachment\": \"" + m + "\"" +
                                                "        }";
                                        c++;
                                        f = true;
                                        Toast.makeText(getActivity(), "Signature has  been saved!", Toast.LENGTH_SHORT).show();
                                        dialog.hide();
                                        signature4.setBackgroundResource(R.color.greencolor);
                                    } else {
                                        Toast.makeText(getActivity(), "Unable to store the signature", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            } else {
                                sig4 = "";
                                sigString4 = "";
                                c--;
                                f = false;
                                dialog.hide();
                                signature4.setBackgroundResource(R.color.skyblue);
                            }

                        }
                    });
                    clear.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mSignaturePad.clear();
                            clear.setEnabled(false);
                            signatureprev.setVisibility(signatureprev.GONE);
                            mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
                        }
                    });
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
                        @Override
                        public void onSigned() {
                            save.setEnabled(true);
                            clear.setEnabled(true);
                        }

                        @Override
                        public void onClear() {
                            sig4 = "";
                            c--;
                            signatureprev.setVisibility(signatureprev.GONE);
                            f = false;
                            sigString4 = "";
                            signature4.setBackgroundResource(R.color.skyblue);
                            clear.setEnabled(false);
                        }
                    });
                    dialog.show();
                } else {
                    Toast.makeText(getActivity(), "Please add your name first.", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.signature5:
                if (!namefield5.getText().toString().equals("")) {
                    signaturename.setText(namefield5.getText().toString());
                    if (sigString5.equals("")) {
                        signatureprev.setVisibility(signatureprev.GONE);
                        mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
                    } else {
                        byte[] decodedString = Base64.decode(sigString5, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        signatureprev.setImageBitmap(decodedByte);
                        clear.setEnabled(true);
                        signatureprev.setVisibility(signatureprev.VISIBLE);
                        mSignaturePad.setVisibility(mSignaturePad.GONE);
                    }
                    save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (clear.isEnabled()) {
                                Log.e("tes", sigString5);
                                if (!sigString5.equals("")) {
                                    dialog.hide();
                                } else {
                                    Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                                    String m = Encoder.encodeTobase64(signatureBitmap);
                                    if (addSignatureToGallery(signatureBitmap, namefield5.getText().toString())) {
                                        long millisStart = Calendar.getInstance().getTimeInMillis();
                                        sigString5 = m;

                                        sig5 = "{" +
                                                "\"JobID\": \"" + mJobObject.getJobID() + "\"," +
                                                "\"TCName\": \"" + namefield5.getText().toString() + "\"," +
                                                "\"AttachedOn\": \"" + millisStart + "\"," +
                                                "\"Attachment\": \"" + m + "\"" +
                                                "        }";
                                        c++;

                                        f = true;
                                        Toast.makeText(getActivity(), "Signature has  been saved!", Toast.LENGTH_SHORT).show();
                                        dialog.hide();
                                        signature5.setBackgroundResource(R.color.greencolor);
                                    } else {
                                        Toast.makeText(getActivity(), "Unable to store the signature", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            } else {
                                sig5 = "";
                                sigString5 = "";
                                c--;
                                f = false;
                                dialog.hide();
                                signature5.setBackgroundResource(R.color.skyblue);
                            }

                        }
                    });
                    clear.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mSignaturePad.clear();
                            clear.setEnabled(false);
                            signatureprev.setVisibility(signatureprev.GONE);
                            mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
                        }
                    });
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
                        @Override
                        public void onSigned() {
                            save.setEnabled(true);
                            clear.setEnabled(true);
                        }

                        @Override
                        public void onClear() {
                            sig5 = "";
                            c--;
                            signatureprev.setVisibility(signatureprev.GONE);
                            f = false;
                            sigString5 = "";
                            signature5.setBackgroundResource(R.color.skyblue);
                            clear.setEnabled(false);
                        }
                    });
                    dialog.show();
                } else {
                    Toast.makeText(getActivity(), "Please add your name first.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.signature6:
                if (!namefield6.getText().toString().equals("")) {
                    signaturename.setText(namefield6.getText().toString());
                    if (sigString6.equals("")) {
                        signatureprev.setVisibility(signatureprev.GONE);
                        mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
                    } else {
                        byte[] decodedString = Base64.decode(sigString6, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        signatureprev.setImageBitmap(decodedByte);
                        clear.setEnabled(true);
                        signatureprev.setVisibility(signatureprev.VISIBLE);
                        mSignaturePad.setVisibility(mSignaturePad.GONE);
                    }
                    save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (clear.isEnabled()) {
                                Log.e("tes", sigString6);
                                if (!sigString6.equals("")) {
                                    dialog.hide();
                                } else {
                                    Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                                    String m = Encoder.encodeTobase64(signatureBitmap);
                                    if (addSignatureToGallery(signatureBitmap, namefield6.getText().toString())) {
                                        long millisStart = Calendar.getInstance().getTimeInMillis();
                                        sigString6 = m;

                                        sig6 = "{" +
                                                "\"JobID\": \"" + mJobObject.getJobID() + "\"," +
                                                "\"TCName\": \"" + namefield6.getText().toString() + "\"," +
                                                "\"AttachedOn\": \"" + millisStart + "\"," +
                                                "\"Attachment\": \"" + m + "\"" +
                                                "        }";
                                        c++;

                                        f = true;
                                        Toast.makeText(getActivity(), "Signature has  been saved!", Toast.LENGTH_SHORT).show();
                                        dialog.hide();
                                        signature6.setBackgroundResource(R.color.greencolor);
                                    } else {
                                        Toast.makeText(getActivity(), "Unable to store the signature", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            } else {
                                sig6 = "";
                                sigString6 = "";
                                c--;
                                f = false;
                                dialog.hide();
                                signature6.setBackgroundResource(R.color.skyblue);
                            }

                        }
                    });
                    clear.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mSignaturePad.clear();
                            clear.setEnabled(false);
                            signatureprev.setVisibility(signatureprev.GONE);
                            mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
                        }
                    });
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
                        @Override
                        public void onSigned() {
                            save.setEnabled(true);
                            clear.setEnabled(true);
                        }

                        @Override
                        public void onClear() {
                            sig6 = "";
                            c--;
                            signatureprev.setVisibility(signatureprev.GONE);
                            f = false;
                            sigString6 = "";
                            signature6.setBackgroundResource(R.color.skyblue);
                            clear.setEnabled(false);
                        }
                    });
                    dialog.show();
                } else {
                    Toast.makeText(getActivity(), "Please add your name first.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.signature7:

                if (!namefield7.getText().toString().equals("")) {
                    signaturename.setText(namefield7.getText().toString());
                    if (sigString7.equals("")) {
                        signatureprev.setVisibility(signatureprev.GONE);
                        mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
                    } else {
                        byte[] decodedString = Base64.decode(sigString7, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        signatureprev.setImageBitmap(decodedByte);
                        clear.setEnabled(true);
                        signatureprev.setVisibility(signatureprev.VISIBLE);
                        mSignaturePad.setVisibility(mSignaturePad.GONE);
                    }
                    save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (clear.isEnabled()) {
                                Log.e("tes", sigString7);
                                if (!sigString7.equals("")) {
                                    dialog.hide();
                                } else {
                                    Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                                    String m = Encoder.encodeTobase64(signatureBitmap);
                                    if (addSignatureToGallery(signatureBitmap, namefield7.getText().toString())) {
                                        long millisStart = Calendar.getInstance().getTimeInMillis();
                                        sigString7 = m;

                                        sig7 = "{" +
                                                "\"JobID\": \"" + mJobObject.getJobID() + "\"," +
                                                "\"TCName\": \"" + namefield7.getText().toString() + "\"," +
                                                "\"AttachedOn\": \"" + millisStart + "\"," +
                                                "\"Attachment\": \"" + m + "\"" +
                                                "        }";
                                        c++;

                                        f = true;
                                        Toast.makeText(getActivity(), "Signature has  been saved!", Toast.LENGTH_SHORT).show();
                                        dialog.hide();
                                        signature7.setBackgroundResource(R.color.greencolor);
                                    } else {
                                        Toast.makeText(getActivity(), "Unable to store the signature", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            } else {
                                sig7 = "";
                                sigString7 = "";
                                c--;
                                f = false;
                                dialog.hide();
                                signature7.setBackgroundResource(R.color.skyblue);
                            }

                        }
                    });
                    clear.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mSignaturePad.clear();
                            clear.setEnabled(false);
                            signatureprev.setVisibility(signatureprev.GONE);
                            mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
                        }
                    });
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
                        @Override
                        public void onSigned() {
                            save.setEnabled(true);
                            clear.setEnabled(true);
                        }

                        @Override
                        public void onClear() {
                            sig7 = "";
                            c--;
                            signatureprev.setVisibility(signatureprev.GONE);
                            f = false;
                            sigString7 = "";
                            signature7.setBackgroundResource(R.color.skyblue);
                            clear.setEnabled(false);
                        }
                    });
                    dialog.show();
                } else {
                    Toast.makeText(getActivity(), "Please add your name first.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.signature8:
                if (!namefield8.getText().toString().equals("")) {
                    signaturename.setText(namefield8.getText().toString());
                    if (sigString8.equals("")) {
                        signatureprev.setVisibility(signatureprev.GONE);
                        mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
                    } else {
                        byte[] decodedString = Base64.decode(sigString8, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        signatureprev.setImageBitmap(decodedByte);
                        clear.setEnabled(true);
                        signatureprev.setVisibility(signatureprev.VISIBLE);
                        mSignaturePad.setVisibility(mSignaturePad.GONE);
                    }
                    save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (clear.isEnabled()) {
                                Log.e("tes", sigString8);
                                if (!sigString8.equals("")) {
                                    dialog.hide();
                                } else {
                                    Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                                    String m = Encoder.encodeTobase64(signatureBitmap);
                                    if (addSignatureToGallery(signatureBitmap, namefield8.getText().toString())) {
                                        long millisStart = Calendar.getInstance().getTimeInMillis();
                                        sigString8 = m;

                                        sig8 = "{" +
                                                "\"JobID\": \"" + mJobObject.getJobID() + "\"," +
                                                "\"TCName\": \"" + namefield8.getText().toString() + "\"," +
                                                "\"AttachedOn\": \"" + millisStart + "\"," +
                                                "\"Attachment\": \"" + m + "\"" +
                                                "        }";
                                        c++;

                                        f = true;
                                        Toast.makeText(getActivity(), "Signature has  been saved!", Toast.LENGTH_SHORT).show();
                                        dialog.hide();
                                        signature8.setBackgroundResource(R.color.greencolor);
                                    } else {
                                        Toast.makeText(getActivity(), "Unable to store the signature", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            } else {
                                sig8 = "";
                                sigString8 = "";
                                c--;
                                f = false;
                                dialog.hide();
                                signature8.setBackgroundResource(R.color.skyblue);
                            }

                        }
                    });
                    clear.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mSignaturePad.clear();
                            clear.setEnabled(false);
                            signatureprev.setVisibility(signatureprev.GONE);
                            mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
                        }
                    });
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
                        @Override
                        public void onSigned() {
                            save.setEnabled(true);
                            clear.setEnabled(true);
                        }

                        @Override
                        public void onClear() {
                            sig8 = "";
                            c--;
                            signatureprev.setVisibility(signatureprev.GONE);
                            f = false;
                            sigString8 = "";
                            signature8.setBackgroundResource(R.color.skyblue);
//                            save.setEnabled(false);
                            clear.setEnabled(false);
                        }
                    });
                    dialog.show();
                } else {
                    Toast.makeText(getActivity(), "Please add your name first.", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.signature9:
                if (!namefield9.getText().toString().equals("")) {
                    signaturename.setText(namefield9.getText().toString());
                    if (sigString9.equals("")) {
                        signatureprev.setVisibility(signatureprev.GONE);
                        mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
                    } else {
                        byte[] decodedString = Base64.decode(sigString9, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        signatureprev.setImageBitmap(decodedByte);
                        clear.setEnabled(true);
                        signatureprev.setVisibility(signatureprev.VISIBLE);
                        mSignaturePad.setVisibility(mSignaturePad.GONE);
                    }
                    save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (clear.isEnabled()) {
                                Log.e("tes", sigString9);
                                if (!sigString9.equals("")) {
                                    dialog.hide();
                                } else {
                                    Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                                    String m = Encoder.encodeTobase64(signatureBitmap);
                                    if (addSignatureToGallery(signatureBitmap, namefield9.getText().toString())) {
                                        long millisStart = Calendar.getInstance().getTimeInMillis();
                                        sigString9 = m;

                                        sig9 = "{" +
                                                "\"JobID\": \"" + mJobObject.getJobID() + "\"," +
                                                "\"TCName\": \"" + namefield9.getText().toString() + "\"," +
                                                "\"AttachedOn\": \"" + millisStart + "\"," +
                                                "\"Attachment\": \"" + m + "\"" +
                                                "        }";
                                        c++;

                                        f = true;
                                        Toast.makeText(getActivity(), "Signature has  been saved!", Toast.LENGTH_SHORT).show();
                                        dialog.hide();
                                        signature9.setBackgroundResource(R.color.greencolor);
                                    } else {
                                        Toast.makeText(getActivity(), "Unable to store the signature", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            } else {
                                sig9 = "";
                                sigString9 = "";
                                c--;
                                f = false;
                                dialog.hide();
                                signature9.setBackgroundResource(R.color.skyblue);
                            }

                        }
                    });
                    clear.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mSignaturePad.clear();
                            clear.setEnabled(false);
                            signatureprev.setVisibility(signatureprev.GONE);
                            mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
                        }
                    });
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
                        @Override
                        public void onSigned() {
                            save.setEnabled(true);
                            clear.setEnabled(true);
                        }

                        @Override
                        public void onClear() {
                            sig9 = "";
                            c--;
                            signatureprev.setVisibility(signatureprev.GONE);
                            f = false;
                            sigString9 = "";
                            signature9.setBackgroundResource(R.color.skyblue);
//                            save.setEnabled(false);
                            clear.setEnabled(false);
                        }
                    });
                    dialog.show();
                } else {
                    Toast.makeText(getActivity(), "Please add your name first.", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.signature10:

                if (!namefield10.getText().toString().equals("")) {
                    signaturename.setText(namefield10.getText().toString());
                    if (sigString10.equals("")) {
                        signatureprev.setVisibility(signatureprev.GONE);
                        mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
                    } else {
                        byte[] decodedString = Base64.decode(sigString10, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        signatureprev.setImageBitmap(decodedByte);
                        clear.setEnabled(true);
                        signatureprev.setVisibility(signatureprev.VISIBLE);
                        mSignaturePad.setVisibility(mSignaturePad.GONE);
                    }
                    save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (clear.isEnabled()) {
                                Log.e("tes", sigString10);
                                if (!sigString10.equals("")) {
                                    dialog.hide();
                                } else {
                                    Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                                    String m = Encoder.encodeTobase64(signatureBitmap);
                                    if (addSignatureToGallery(signatureBitmap, namefield10.getText().toString())) {
                                        long millisStart = Calendar.getInstance().getTimeInMillis();
                                        sigString10 = m;

                                        sig10 = "{" +
                                                "\"JobID\": \"" + mJobObject.getJobID() + "\"," +
                                                "\"TCName\": \"" + namefield10.getText().toString() + "\"," +
                                                "\"AttachedOn\": \"" + millisStart + "\"," +
                                                "\"Attachment\": \"" + m + "\"" +
                                                "        }";
                                        c++;

                                        f = true;
                                        Toast.makeText(getActivity(), "Signature has  been saved!", Toast.LENGTH_SHORT).show();
                                        dialog.hide();
                                        signature10.setBackgroundResource(R.color.greencolor);
                                    } else {
                                        Toast.makeText(getActivity(), "Unable to store the signature", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            } else {
                                sig10 = "";
                                sigString10 = "";
                                c--;
                                f = false;
                                dialog.hide();
                                signature10.setBackgroundResource(R.color.skyblue);
                            }

                        }
                    });
                    clear.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mSignaturePad.clear();
                            clear.setEnabled(false);
                            signatureprev.setVisibility(signatureprev.GONE);
                            mSignaturePad.setVisibility(mSignaturePad.VISIBLE);
                        }
                    });
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
                        @Override
                        public void onSigned() {
                            save.setEnabled(true);
                            clear.setEnabled(true);
                        }

                        @Override
                        public void onClear() {
                            sig10 = "";
                            c--;
                            signatureprev.setVisibility(signatureprev.GONE);
                            f = false;
                            sigString10 = "";
                            signature10.setBackgroundResource(R.color.skyblue);
                            clear.setEnabled(false);
                        }
                    });
                    dialog.show();
                } else {
                    Toast.makeText(getActivity(), "Please add your name first.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.sitechecklist, menu);
        updatechecklist = (MenuItem) menu.findItem(R.id.update_checklist);
        savemen = (MenuItem) menu.findItem(R.id.savechecklist);
        updatechecklist.setVisible(false);
        savemen.setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem itemb) {
        switch (itemb.getItemId()) {
            case R.id.savechecklist:

                counter = 0;

                ret = "[";
                if (sig1 != null) {
                    ret = ret + sig1 + ",";
                    counter++;
                }
                if (sig2 != null) {
                    ret = ret+ sig2;
                    counter++;
                }
                if (sig3 != null) {
                    ret = ret + sig3 + ",";
                    counter++;
                }
                if (sig4 != null) {
                    ret = ret + sig4 + ",";
                    counter++;
                }
                if (sig5 != null) {
                    ret = ret +  sig5 + ",";
                    counter++;
                }
                if (sig6 != null) {
                    ret = ret +  sig6 + ",";
                    counter++;
                }
                if (sig7 != null) {
                    ret = ret + sig7 + ",";
                    counter++;
                }
                if (sig8 != null) {
                    ret = ret +  sig8 + ",";
                    counter++;
                }
                if (sig9 != null) {
                    ret = ret +sig9 + ",";
                    counter++;
                }
                if (sig10 != null) {
                    ret = ret + sig10 + ",";
                    counter++;
                }

                Log.e("return", counter + ret +"]");

                Log.e("size", db.getallTimesheets(mJobObject.getJobID()).getTimesheets().size() + " -==- " + c);
                if (db.getallTimesheets(mJobObject.getJobID()).getTimesheets().size() == counter) {
                    savesig();
                } else {
                    Toast.makeText(getActivity(), "All Traffic Controllers must sign before proceeding.", Toast.LENGTH_LONG).show();
                }
                break;
            case android.R.id.home:
                c = 0;
                sigString1 = "";
                sigString2 = "";
                sigString3 = "";
                sigString4 = "";
                sigString5 = "";
                sigString6 = "";
                sigString7 = "";
                sigString8 = "";
                sigString9 = "";
                sigString10 = "";
                ret = null;
                sig1 = null;
                sig2 = null;
                sig3 = null;
                sig4 = null;
                sig5 = null;
                sig6 = null;
                sig7 = null;
                sig8 = null;
                sig9 = null;
                sig10 = null;

                firstsignature.setBackgroundResource(R.color.skyblue);
                signature2.setBackgroundResource(R.color.skyblue);
                signature3.setBackgroundResource(R.color.skyblue);
                signature4.setBackgroundResource(R.color.skyblue);
                signature5.setBackgroundResource(R.color.skyblue);
                signature6.setBackgroundResource(R.color.skyblue);
                signature7.setBackgroundResource(R.color.skyblue);
                signature8.setBackgroundResource(R.color.skyblue);
                signature9.setBackgroundResource(R.color.skyblue);
                signature10.setBackgroundResource(R.color.skyblue);

                mListener.onPre();
                break;
        }
        return false;
    }

    public void savesig() {
        ret = "[";
        if (sig1 != null) {
            ret = ret + sig1;
        }
        if (sig2 != null) {
            ret = ret + "," + sig2;
        }
        if (sig3 != null) {
            ret = ret + "," + sig3;
        }
        if (sig4 != null) {
            ret = ret + "," + sig4;
        }
        if (sig5 != null) {
            ret = ret + "," + sig5;
        }
        if (sig6 != null) {
            ret = ret + "," + sig6;
        }
        if (sig7 != null) {
            ret = ret + "," + sig7;
        }
        if (sig8 != null) {
            ret = ret + "," + sig8;
        }
        if (sig9 != null) {
            ret = ret + "," + sig9;
        }
        if (sig10 != null) {
            ret = ret + "," + sig10;
        }

        Log.e("return", ret + "]");
        if (sig1 != null) {
            com.rey.material.app.Dialog.Builder builder = null;
            builder = new SimpleDialog.Builder(R.style.SimpleDialogLight) {
                @Override
                public void onPositiveActionClicked(DialogFragment fragment) {
                    if (!connection.isConnectedToInternet()) {
                        db.addreUploadJob(mJobObject.getJobID());
                    } else {
                        syncSiteChecklist();
                    }
                    super.onPositiveActionClicked(fragment);
                }

                @Override
                public void onNegativeActionClicked(DialogFragment fragment) {
                    super.onNegativeActionClicked(fragment);
                }
            };

            ((SimpleDialog.Builder) builder).message("Saving this will lock the Site Checlist form from editing. Do you still want to continue?")
                    .title("Saving Site Checklist")
                    .positiveAction("OK")
                    .negativeAction("CANCEL");

            DialogFragment fragment = DialogFragment.newInstance(builder);
            fragment.show(getFragmentManager(), null);

        } else {
            Toast.makeText(getActivity(), "All Traffic Controller must sign before proceeding.", Toast.LENGTH_LONG).show();
        }
    }

    public String getSiteChecklist() {
        String temp = "";
        if (db.getSiteChecklist(mJobObject.getJobID()) == null) {

        } else {
            temp = db.getSiteChecklist(mJobObject.getJobID());
        }
        return temp;
    }

    public String getAllAdditionalHazard() {
        Gson mac = new Gson();
        String re = mac.toJson(db.getAdditionalHazard(mJobObject.getJobID()));
        return re;
    }

    public void syncSiteChecklist() {
        dialogdd.show();

        boolean cc = mJobObject.getJobID().contains(ifsubjob);
        Log.e("teststeset", getAllAdditionalHazard());
        String Checklist2 = getSiteChecklist().replace("|~", "~");
        String Checklist = Checklist2.replace("^x", "^n/a");
        Map<String, String> params = new HashMap<>();

        params.put("operatorID", mJobObject.getOperatorID().toString());
        params.put("jobID", mJobObject.getJobID());
        params.put("timesheets", "[]");
        params.put("assets", "[]");
        params.put("requirements", "[]");
        params.put("dockets", "[]");
        params.put("siteCheckList", Checklist);
        params.put("signageAudit", "[]");
        params.put("tcAttachment", "[]");

        if (cc) {
            params.put("parentJob", mJobObject.getId());
        } else {
            params.put("parentJob", "");
        }

        params.put("additionalHazards", getAllAdditionalHazard().replace("\n", ""));
        if (!db.getotherfield(mJobObject.getJobID()).equals("wala")) {
            String data = db.getotherfield(mJobObject.getJobID());
            String[] arr = data.split("~");
            params.put("clientEmail", "");
            params.put("clientName", "");
        } else {
            params.put("clientEmail", "");
            params.put("clientName", "");
        }
        params.put("geotag", session.getlocation());
        String hashmap = "operatorID :" + params.get("operatorID").toString() + "\n\n" +
                "jobID :" + params.get("jobID").toString() + "\n\n" +
                "assets :" + params.get("assets").toString() + "\n\n" +
                "tcAttachment :" + params.get("tcAttachment").toString() + "\n\n" +
                "timesheets :" + params.get("timesheets").toString() + "\n\n" +
                "requirements :" + params.get("requirements").toString() + "\n\n" +
                "dockets :" + params.get("dockets").toString() + "\n\n" +
                "siteCheckList :" + params.get("siteCheckList").toString() + "\n\n" +
                "signageAudit :" + params.get("signageAudit").toString() + "\n\n" +
                "additionalHazards :" + params.get("additionalHazards").toString() + "\n\n" +
                "parentJob :" + params.get("parentJob").toString() + "\n\n" +
                "clientEmail :" + params.get("clientEmail").toString() + "\n\n" +
                "geotag :" + session.getlocation();

        try {
            File myFile = new File("/sdcard/csv-" + mJobObject.getJobID() + ".txt");
            myFile.createNewFile();
            FileOutputStream fOut = new FileOutputStream(myFile);
            OutputStreamWriter myOutWriter =
                    new OutputStreamWriter(fOut);
            myOutWriter.append(hashmap);
            myOutWriter.close();
            fOut.close();
        } catch (Exception e) {
            Toast.makeText(getActivity(), e.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setClient(new OkClient(okHttpClient))
                .setEndpoint(Constants.API_URL)
                .build();
        APIInterface upload = restAdapter.create(APIInterface.class);
        upload.siteChecklistSync(session.getToken(), Constants.REQUEST_JOBINFO, params, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                String jsonObjectResponse = new Gson().toJson(o);
                Log.e("Output", "" + jsonObjectResponse.toString());
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(jsonObjectResponse.toString());
                    if (jsonObject.get("status").toString().equals("false")) {
                        Toast.makeText(getActivity(), "Uploading site checklist was unsuccessful.", Toast.LENGTH_SHORT).show();
                        dialogdd.dismiss();
                    }else{
                        dialogdd.dismiss();
                        mListener.saving();
                        HashMap<String, String> data = new HashMap<String, String>();
                        data.put("signatures",
                                ret + "]");
                        createjob.createSignatureArea(data);
                        db.updateContact(mJobObject.getJobID(), "", "", ret + "]", "", "1");
                        Toast.makeText(getActivity(), "Site Checklist has been uploaded.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("ifsuccess", "failure: Wrong" + error.toString());
                Toast.makeText(getActivity(), "Error while uploading site checklist", Toast.LENGTH_SHORT).show();
                dialogdd.dismiss();
            }
        });

    }


    public String getSignageAudit() {
        String temp = "";
        if (db.getSignageAudt(mJobObject.getJobID()) == null) {

        } else {
            temp = db.getSignageAudt(mJobObject.getJobID());
        }
        return temp;
    }

    public File getAlbumStorageDir(String albumName) {
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        }
        return file;
    }

    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        stream.close();
    }

    public boolean addSignatureToGallery(Bitmap signature, String filename) {
        boolean result = false;
        try {
            File photo = new File(getAlbumStorageDir("Evolution"), String.format(filename + ".jpg", System.currentTimeMillis()));
            saveBitmapToJPG(signature, photo);
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(photo);
            mediaScanIntent.setData(contentUri);
            getActivity().sendBroadcast(mediaScanIntent);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        session = new SessionManager(getActivity());
        token = session.getToken();
        createjob = new CreateJobSharedPreference(getActivity());
        try {
            mListener = (OnnextandPre) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public interface OnnextandPre {
        public void onNext();

        public void onPre();

        void saving();
    }
}
