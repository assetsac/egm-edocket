package com.evolution.egm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.evolution.egm.Activities.LoginActivity;
import com.evolution.egm.Activities.MainPage;
import com.evolution.egm.Utils.AlarmReceiver;
import com.evolution.egm.Utils.SessionManager;

public class SplashScreen extends AppCompatActivity implements Animation.AnimationListener {

    private final int SPLASH_DISPLAY_LENGTH = 2500;
    private SessionManager session;
    ImageView logo;
    Animation animFadein;

    // alarm0
    private Context context;

    //alarl close
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        session = new SessionManager(this);
        logo = (ImageView) findViewById(R.id.logo);
        animFadein = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_in);
        animFadein.setAnimationListener(this);
        logo.startAnimation(animFadein);
        logo.setVisibility(logo.VISIBLE);
        splashScreen();

        this.context = this;
        Intent alarm = new Intent(this.context, AlarmReceiver.class);
        boolean alarmRunning = (PendingIntent.getBroadcast(this.context, 0, alarm, PendingIntent.FLAG_NO_CREATE) != null);

        if (!alarmRunning) {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this.context, 0, alarm, 0);
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), 15000, pendingIntent);
        }
    }

    public void splashScreen() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mainIntent;
                if (session.getToken() != "") {
                    mainIntent = new Intent(getApplicationContext(), MainPage.class);
                } else {
                    mainIntent = new Intent(getApplicationContext(), LoginActivity.class);
                }
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(mainIntent);
                finish();
                overridePendingTransition(0, 0);
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
