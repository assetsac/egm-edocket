package com.evolution.egm.Models;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by mcclynreyarboleda on 6/2/15.
 */
@Parcel
public class CheckList {
    String Description, Id, SectionID, SequenceNo, ValueType;

    @SerializedName("CheckListItemOptions")
    ArrayList<CheckListItemOptions> options;

    public CheckList() {

    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getSectionID() {
        return SectionID;
    }

    public void setSectionID(String sectionID) {
        SectionID = sectionID;
    }

    public String getSequenceNo() {
        return SequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        SequenceNo = sequenceNo;
    }

    public String getValueType() {
        return ValueType;
    }

    public void setValueType(String valueType) {
        ValueType = valueType;
    }

    public ArrayList<CheckListItemOptions> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<CheckListItemOptions> options) {
        this.options = options;
    }
}
