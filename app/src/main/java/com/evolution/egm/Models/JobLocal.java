package com.evolution.egm.Models;

/**
 * Created by mcclynreyarboleda on 6/2/15.
 */

public class JobLocal {
    String id, json;

    public JobLocal() {
    }

    public JobLocal(String id, String json) {
        this.id = id;
        this.json = json;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }
}
