package com.evolution.egm.Models;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by mcclynreyarboleda on 4/15/15.
 */
@Parcel
public class Job {

    String AdditionalNotes, AuthorizerID, Code, ContactID, DepotID, EndDateTime, ForemanID, Id, OperatorID, OrderNumber, ParentJobID, StartDateTime, StartType, JobID, JobCode, ContactPerson, AuthorizedPerson, Address, Depot, CompanyName, StartDateTimeFormat, EndDateTimeFormat, State, ContactMobilePhone, ContactPhone, ContactEmail;
    String JobShiftStartDateTime, JobShiftStartDateTimeFormat, JobShiftEndDateTime, JobShiftEndDateTimeFormat, CreateDateTime, CreateDateTimeFormat;
    @SerializedName("Timesheets")
    ArrayList<Timesheet> timesheets;

    @SerializedName("Requirements")
    ArrayList<ModelRequirements> mRequirements;

    @SerializedName("DocketAttachments")
    ArrayList<DocketAttachments> docketAttachments;

    public Job() {

    }

    public Job(String additionalNotes, String authorizerID, String code, String contactID, String depotID, String endDateTime, String foremanID, String id, String operatorID, String orderNumber, String parentJobID, String startDateTime, String startType, String jobID, String jobCode, String contactPerson, String authorizedPerson, String address, String depot, String companyName, String startDateTimeFormat, String endDateTimeFormat, String state, ArrayList<Timesheet> timesheets, ArrayList<ModelRequirements> mRequirements,ArrayList<DocketAttachments> docketAttachments, String contactEmail, String contactPhone, String contactMobilePhone,String JobShiftStartDateTime, String JobShiftStartDateTimeFormat, String JobShiftEndDateTime, String JobShiftEndDateTimeFormat, String CreateDateTime, String CreateDateTimeFormat ) {
        AdditionalNotes = additionalNotes;
        AuthorizerID = authorizerID;
        Code = code;
        ContactID = contactID;
        DepotID = depotID;
        EndDateTime = endDateTime;
        ForemanID = foremanID;
        Id = id;
        OperatorID = operatorID;
        OrderNumber = orderNumber;
        ParentJobID = parentJobID;
        StartDateTime = startDateTime;
        StartType = startType;
        JobID = jobID;
        JobCode = jobCode;
        ContactPerson = contactPerson;
        AuthorizedPerson = authorizedPerson;
        Address = address;
        Depot = depot;
        CompanyName = companyName;
        StartDateTimeFormat = startDateTimeFormat;
        EndDateTimeFormat = endDateTimeFormat;
        State = state;
        ContactEmail = contactEmail;
        ContactPhone = contactPhone;
        ContactMobilePhone = contactMobilePhone;
        this.timesheets = timesheets;
        this.mRequirements = mRequirements;
        this.docketAttachments = docketAttachments;
        this.CreateDateTimeFormat = CreateDateTimeFormat;
        this.JobShiftStartDateTimeFormat = JobShiftStartDateTimeFormat;
        this.JobShiftEndDateTime = JobShiftEndDateTime;
        this.JobShiftEndDateTimeFormat = JobShiftEndDateTimeFormat;
        this.CreateDateTime = CreateDateTime;
        this.CreateDateTimeFormat = CreateDateTimeFormat;
    }

    public String getContactMobilePhone() {
        return ContactMobilePhone;
    }

    public void setContactMobilePhone(String contactMobilePhone) {
        ContactMobilePhone = contactMobilePhone;
    }

    public String getContactPhone() {
        return ContactPhone;
    }

    public void setContactPhone(String contactPhone) {
        ContactPhone = contactPhone;
    }

    public String getContactEmail() {
        return ContactEmail;
    }

    public void setContactEmail(String contactEmail) {
        ContactEmail = contactEmail;
    }

    public String getAdditionalNotes() {
        return AdditionalNotes;
    }

    public void setAdditionalNotes(String additionalNotes) {
        AdditionalNotes = additionalNotes;
    }

    public String getAuthorizerID() {
        return AuthorizerID;
    }

    public void setAuthorizerID(String authorizerID) {
        AuthorizerID = authorizerID;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getContactID() {
        return ContactID;
    }

    public void setContactID(String contactID) {
        ContactID = contactID;
    }

    public String getDepotID() {
        return DepotID;
    }

    public void setDepotID(String depotID) {
        DepotID = depotID;
    }

    public String getEndDateTime() {
        return EndDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        EndDateTime = endDateTime;
    }

    public String getForemanID() {
        return ForemanID;
    }

    public void setForemanID(String foremanID) {
        ForemanID = foremanID;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getOperatorID() {
        return OperatorID;
    }

    public void setOperatorID(String operatorID) {
        OperatorID = operatorID;
    }

    public String getOrderNumber() {
        return OrderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        OrderNumber = orderNumber;
    }

    public String getParentJobID() {
        return ParentJobID;
    }

    public void setParentJobID(String parentJobID) {
        ParentJobID = parentJobID;
    }

    public String getStartDateTime() {
        return StartDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        StartDateTime = startDateTime;
    }

    public String getStartType() {
        return StartType;
    }

    public void setStartType(String startType) {
        StartType = startType;
    }

    public String getJobID() {
        return JobID;
    }

    public void setJobID(String jobID) {
        JobID = jobID;
    }

    public String getJobCode() {
        return JobCode;
    }

    public void setJobCode(String jobCode) {
        JobCode = jobCode;
    }

    public String getContactPerson() {
        return ContactPerson;
    }

    public void setContactPerson(String contactPerson) {
        ContactPerson = contactPerson;
    }

    public String getAuthorizedPerson() {
        return AuthorizedPerson;
    }

    public void setAuthorizedPerson(String authorizedPerson) {
        AuthorizedPerson = authorizedPerson;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getDepot() {
        return Depot;
    }

    public void setDepot(String depot) {
        Depot = depot;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getStartDateTimeFormat() {
        return StartDateTimeFormat;
    }

    public void setStartDateTimeFormat(String startDateTimeFormat) {
        StartDateTimeFormat = startDateTimeFormat;
    }

    public String getEndDateTimeFormat() {
        return EndDateTimeFormat;
    }

    public void setEndDateTimeFormat(String endDateTimeFormat) {
        EndDateTimeFormat = endDateTimeFormat;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public ArrayList<Timesheet> getTimesheets() {
        return timesheets;
    }

    public void setTimesheets(ArrayList<Timesheet> timesheets) {
        this.timesheets = timesheets;
    }

    public ArrayList<ModelRequirements> getmRequirements() {
        return mRequirements;
    }

    public void setmRequirements(ArrayList<ModelRequirements> mRequirements) {
        this.mRequirements = mRequirements;
    }

    public String getJobShiftStartDateTime() {
        return JobShiftStartDateTime;
    }

    public void setJobShiftStartDateTime(String jobShiftStartDateTime) {
        JobShiftStartDateTime = jobShiftStartDateTime;
    }

    public String getJobShiftStartDateTimeFormat() {
        return JobShiftStartDateTimeFormat;
    }

    public void setJobShiftStartDateTimeFormat(String jobShiftStartDateTimeFormat) {
        JobShiftStartDateTimeFormat = jobShiftStartDateTimeFormat;
    }

    public String getJobShiftEndDateTime() {
        return JobShiftEndDateTime;
    }

    public void setJobShiftEndDateTime(String jobShiftEndDateTime) {
        JobShiftEndDateTime = jobShiftEndDateTime;
    }

    public String getJobShiftEndDateTimeFormat() {
        return JobShiftEndDateTimeFormat;
    }

    public void setJobShiftEndDateTimeFormat(String jobShiftEndDateTimeFormat) {
        JobShiftEndDateTimeFormat = jobShiftEndDateTimeFormat;
    }

    public String getCreateDateTime() {
        return CreateDateTime;
    }

    public void setCreateDateTime(String createDateTime) {
        CreateDateTime = createDateTime;
    }

    public String getCreateDateTimeFormat() {
        return CreateDateTimeFormat;
    }

    public void setCreateDateTimeFormat(String createDateTimeFormat) {
        CreateDateTimeFormat = createDateTimeFormat;
    }

    public ArrayList<DocketAttachments> getDocketAttachments() {
        return docketAttachments;
    }

    public void setDocketAttachments(ArrayList<DocketAttachments> docketAttachments) {
        this.docketAttachments = docketAttachments;
    }
}