package com.evolution.egm.Models;

import org.parceler.Parcel;

/**
 * Created by mcarboleda on 22/03/2016.
 */
@Parcel
public class SubJobCounter {
    String JobId, Counter;

    public SubJobCounter() {
    }

    public SubJobCounter(String jobId, String counter) {
        JobId = jobId;
        Counter = counter;
    }

    public String getJobId() {
        return JobId;
    }

    public void setJobId(String jobId) {
        JobId = jobId;
    }

    public String getCounter() {
        return Counter;
    }

    public void setCounter(String counter) {
        Counter = counter;
    }
}
