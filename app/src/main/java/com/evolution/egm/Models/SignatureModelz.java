package com.evolution.egm.Models;

import org.parceler.Parcel;

/**
 * Created by mcarboleda on 11/07/2016.
 */
@Parcel
public class SignatureModelz {
    String AttachedOn;
    String Attachment;
    String JobID;
    String TCName;

    public SignatureModelz() {
    }

    public SignatureModelz(String attachedOn, String attachment, String jobID, String TCName) {
        AttachedOn = attachedOn;
        Attachment = attachment;
        JobID = jobID;
        this.TCName = TCName;
    }

    public String getAttachedOn() {
        return AttachedOn;
    }

    public void setAttachedOn(String attachedOn) {
        AttachedOn = attachedOn;
    }

    public String getAttachment() {
        return Attachment;
    }

    public void setAttachment(String attachment) {
        Attachment = attachment;
    }

    public String getJobID() {
        return JobID;
    }

    public void setJobID(String jobID) {
        JobID = jobID;
    }

    public String getTCName() {
        return TCName;
    }

    public void setTCName(String TCName) {
        this.TCName = TCName;
    }
}
