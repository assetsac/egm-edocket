package com.evolution.egm.Models;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by mcclynreyarboleda on 5/29/15.
 */
@Parcel
public class ModelRequirementsObjects {
    @SerializedName("requirements")
    ArrayList<ModelRequirements> mRequirements;

    public ModelRequirementsObjects() {
    }

    public ModelRequirementsObjects(ArrayList<ModelRequirements> mRequirements) {
        this.mRequirements = mRequirements;
    }

    public ArrayList<ModelRequirements> getmRequirements() {
        return mRequirements;
    }

    public void setmRequirements(ArrayList<ModelRequirements> mRequirements) {
        this.mRequirements = mRequirements;
    }
}
