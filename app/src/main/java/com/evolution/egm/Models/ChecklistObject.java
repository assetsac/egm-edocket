package com.evolution.egm.Models;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by mcclynreyarboleda on 6/2/15.
 */
@Parcel
public class ChecklistObject {
    @SerializedName("siteChecklist")
    ArrayList<CheckList> checkList;

    public ChecklistObject() {
    }

    public ArrayList<CheckList> getCheckList() {
        return checkList;
    }

    public void setCheckList(ArrayList<CheckList> checkList) {
        this.checkList = checkList;
    }
}
