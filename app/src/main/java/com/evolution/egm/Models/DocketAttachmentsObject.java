package com.evolution.egm.Models;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by mcgwapo on 10/15/15.
 */
@Parcel
public class DocketAttachmentsObject {
    @SerializedName("DocketAttachments")
    ArrayList<DocketAttachments> docketAttachments;

    public DocketAttachmentsObject() {

    }

    public DocketAttachmentsObject(ArrayList<DocketAttachments> docketAttachments) {
        this.docketAttachments = docketAttachments;
    }

    public ArrayList<DocketAttachments> getDocketAttachments() {
        return docketAttachments;
    }

    public void setDocketAttachments(ArrayList<DocketAttachments> docketAttachments) {
        this.docketAttachments = docketAttachments;
    }
}
