package com.evolution.egm.Models;

/**
 * Created by mcgwapo on 10/9/15.
 */
public class PhotoItem {
    String id, image_url;
    boolean isSelected;
    String isUploaded;


    public PhotoItem() {

    }

    public PhotoItem(String id, String image_url, String isUploaded) {
        this.id = id;
        this.image_url = image_url;
        this.isUploaded = isUploaded;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getIsUploaded() {
        return isUploaded;
    }

    public void setIsUploaded(String isUploaded) {
        this.isUploaded = isUploaded;
    }
}
