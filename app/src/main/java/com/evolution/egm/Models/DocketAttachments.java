package com.evolution.egm.Models;

import org.parceler.Parcel;

/**
 * Created by mcgwapo on 10/15/15.
 */
@Parcel
public class DocketAttachments {

    String Caption, ExternalLink, ItemId, JobId, RowID, AppLink;

    public DocketAttachments() {
    }

    public DocketAttachments(String caption, String externalLink, String itemId, String jobId, String rowID, String appLink) {
        Caption = caption;
        ExternalLink = externalLink;
        ItemId = itemId;
        JobId = jobId;
        RowID = rowID;
        AppLink = appLink;
    }

    public String getCaption() {
        return Caption;
    }

    public void setCaption(String caption) {
        Caption = caption;
    }

    public String getExternalLink() {
        return ExternalLink;
    }

    public void setExternalLink(String externalLink) {
        ExternalLink = externalLink;
    }

    public String getItemId() {
        return ItemId;
    }

    public void setItemId(String itemId) {
        ItemId = itemId;
    }

    public String getJobId() {
        return JobId;
    }

    public void setJobId(String jobId) {
        JobId = jobId;
    }

    public String getRowID() {
        return RowID;
    }

    public void setRowID(String rowID) {
        RowID = rowID;
    }

    public String getAppLink() {
        return AppLink;
    }

    public void setAppLink(String appLink) {
        AppLink = appLink;
    }
}
