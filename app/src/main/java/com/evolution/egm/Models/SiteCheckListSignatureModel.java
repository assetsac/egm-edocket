package com.evolution.egm.Models;

import org.parceler.Parcel;

/**
 * Created by McClynrey on 3/1/2016.
 */
@Parcel
public class SiteCheckListSignatureModel {
    String JobID, TCName, AttachedOn, Attachment;

    public SiteCheckListSignatureModel() {
    }

    public SiteCheckListSignatureModel(String jobID, String TCName, String attachedOn, String attachment) {
        JobID = jobID;
        this.TCName = TCName;
        AttachedOn = attachedOn;
        Attachment = attachment;
    }


    public String getJobID() {
        return JobID;
    }

    public void setJobID(String jobID) {
        JobID = jobID;
    }

    public String getTCName() {
        return TCName;
    }

    public void setTCName(String TCName) {
        this.TCName = TCName;
    }

    public String getAttachedOn() {
        return AttachedOn;
    }

    public void setAttachedOn(String attachedOn) {
        AttachedOn = attachedOn;
    }

    public String getAttachment() {
        return Attachment;
    }

    public void setAttachment(String attachment) {
        Attachment = attachment;
    }
}
