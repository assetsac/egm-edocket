package com.evolution.egm.Models;

import org.parceler.Parcel;

/**
 * Created by mcgwapo on 11/9/15.
 */

@Parcel
public class SignagesMeterModel {
    String _id;
    String jobid;
    String image;
    String metress;
    String quantity;
    String after_care;
    String metress2;
    String quantity2;
    String typemode;

    String typemo;
    boolean isSelected;

    public SignagesMeterModel() {

    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getJobid() {
        return jobid;
    }

    public void setJobid(String jobid) {
        this.jobid = jobid;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMetress() {
        return metress;
    }

    public void setMetress(String metress) {
        this.metress = metress;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getAfter_care() {
        return after_care;
    }

    public void setAfter_care(String after_care) {
        this.after_care = after_care;
    }

    public String getMetress2() {
        return metress2;
    }

    public void setMetress2(String metress2) {
        this.metress2 = metress2;
    }

    public String getQuantity2() {
        return quantity2;
    }

    public void setQuantity2(String quantity2) {
        this.quantity2 = quantity2;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getTypemo() {
        return typemo;
    }

    public void setTypemo(String typemo) {
        this.typemo = typemo;
    }

    public String getTypemode() {
        return typemode;
    }

    public void setTypemode(String typemode) {
        this.typemode = typemode;
    }
}
