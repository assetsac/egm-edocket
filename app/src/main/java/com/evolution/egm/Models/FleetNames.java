package com.evolution.egm.Models;

/**
 * Created by mcclynreyarboleda on 5/14/15.
 */
public class FleetNames {
    public FleetNames() {

    }
    boolean isChecked;
    String fleetid, fleetname;

    public FleetNames(String fleetid, String fleetname, boolean isChecked) {
        this.fleetid = fleetid;
        this.fleetname = fleetname;
        this.isChecked = isChecked;
    }


    public String getFleetid() {
        return fleetid;
    }

    public void setFleetid(String fleetid) {
        this.fleetid = fleetid;
    }

    public String getFleetname() {
        return fleetname;
    }

    public void setFleetname(String fleetname) {
        this.fleetname = fleetname;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setIsChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

}
