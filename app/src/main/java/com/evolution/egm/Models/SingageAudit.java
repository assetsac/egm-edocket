package com.evolution.egm.Models;

import org.parceler.Parcel;

/**
 * Created by mcgwapo on 11/24/15.
 */
@Parcel
public class SingageAudit {

    String _id, JobID, RegoNo, Landmark, CarriageWay, Direction, TimeErected, TimeCollected, TimeChecked1, TimeChecked2, TimeChecked3, TimeChecked4, AuditSigns, IsAfterCare,ErectedBy, CollectedBy;

    public SingageAudit() {
    }

    public SingageAudit(String _id, String jobID, String regoNo, String landmark, String carriageWay, String direction, String timeErected, String timeCollected, String timeChecked1, String timeChecked2, String timeChecked3, String timeChecked4, String auditSigns, String isAfterCare, String erectedBy, String collectedBy) {
        this._id = _id;
        JobID = jobID;
        RegoNo = regoNo;
        Landmark = landmark;
        CarriageWay = carriageWay;
        Direction = direction;
        TimeErected = timeErected;
        TimeCollected = timeCollected;
        TimeChecked1 = timeChecked1;
        TimeChecked2 = timeChecked2;
        TimeChecked3 = timeChecked3;
        TimeChecked4 = timeChecked4;
        AuditSigns = auditSigns;
        IsAfterCare = isAfterCare;
        ErectedBy = erectedBy;
        CollectedBy = collectedBy;
    }


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getJobID() {
        return JobID;
    }

    public void setJobID(String jobID) {
        JobID = jobID;
    }

    public String getRegoNo() {
        return RegoNo;
    }

    public void setRegoNo(String regoNo) {
        RegoNo = regoNo;
    }

    public String getLandmark() {
        return Landmark;
    }

    public void setLandmark(String landmark) {
        Landmark = landmark;
    }

    public String getCarriageWay() {
        return CarriageWay;
    }

    public void setCarriageWay(String carriageWay) {
        CarriageWay = carriageWay;
    }

    public String getDirection() {
        return Direction;
    }

    public void setDirection(String direction) {
        Direction = direction;
    }

    public String getTimeErected() {
        return TimeErected;
    }

    public void setTimeErected(String timeErected) {
        TimeErected = timeErected;
    }

    public String getTimeCollected() {
        return TimeCollected;
    }

    public void setTimeCollected(String timeCollected) {
        TimeCollected = timeCollected;
    }

    public String getTimeChecked1() {
        return TimeChecked1;
    }

    public void setTimeChecked1(String timeChecked1) {
        TimeChecked1 = timeChecked1;
    }

    public String getTimeChecked2() {
        return TimeChecked2;
    }

    public void setTimeChecked2(String timeChecked2) {
        TimeChecked2 = timeChecked2;
    }

    public String getTimeChecked3() {
        return TimeChecked3;
    }

    public void setTimeChecked3(String timeChecked3) {
        TimeChecked3 = timeChecked3;
    }

    public String getTimeChecked4() {
        return TimeChecked4;
    }

    public void setTimeChecked4(String timeChecked4) {
        TimeChecked4 = timeChecked4;
    }

    public String getAuditSigns() {
        return AuditSigns;
    }

    public void setAuditSigns(String auditSigns) {
        AuditSigns = auditSigns;
    }

    public String getIsAfterCare() {
        return IsAfterCare;
    }

    public void setIsAfterCare(String isAfterCare) {
        IsAfterCare = isAfterCare;
    }

    public String getErectedBy() {
        return ErectedBy;
    }

    public void setErectedBy(String erectedBy) {
        ErectedBy = erectedBy;
    }

    public String getCollectedBy() {
        return CollectedBy;
    }

    public void setCollectedBy(String collectedBy) {
        CollectedBy = collectedBy;
    }
}
