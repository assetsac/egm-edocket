package com.evolution.egm.Models;

/**
 * Created by mcclynreyarboleda on 5/25/15.
 */
public class CreateJobFisrt {

    String firstGroup, secondGroup, thirdGroup, fourthGroup, fifthGroup;

    public CreateJobFisrt() {

    }

    public CreateJobFisrt(String firstGroup, String secondGroup, String thirdGroup, String fourthGroup, String fifthGroup) {
        this.firstGroup = firstGroup;
        this.secondGroup = secondGroup;
        this.thirdGroup = thirdGroup;
        this.fourthGroup = fourthGroup;
        this.fifthGroup = fifthGroup;
    }

    public String getFirstGroup() {
        return firstGroup;
    }

    public void setFirstGroup(String firstGroup) {
        this.firstGroup = firstGroup;
    }

    public String getSecondGroup() {
        return secondGroup;
    }

    public void setSecondGroup(String secondGroup) {
        this.secondGroup = secondGroup;
    }

    public String getThirdGroup() {
        return thirdGroup;
    }

    public void setThirdGroup(String thirdGroup) {
        this.thirdGroup = thirdGroup;
    }

    public String getFourthGroup() {
        return fourthGroup;
    }

    public void setFourthGroup(String fourthGroup) {
        this.fourthGroup = fourthGroup;
    }

    public String getFifthGroup() {
        return fifthGroup;
    }

    public void setFifthGroup(String fifthGroup) {
        this.fifthGroup = fifthGroup;
    }

}
