package com.evolution.egm.Models;

import org.parceler.Parcel;

/**
 * Created by mcarboleda on 28/04/2016.
 */
@Parcel
public class CheckManstatStatus {
    String status, message;

    public CheckManstatStatus() {

    }

    public CheckManstatStatus(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
