package com.evolution.egm.Models;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by mcgwapo on 11/16/15.
 */
@Parcel
public class SignagesObject {
    @SerializedName("SignagesMeterModel")
    ArrayList<SignagesMeterModel> signages;

    public SignagesObject() {
    }

    public SignagesObject(ArrayList<SignagesMeterModel> signages) {
        this.signages = signages;
    }

    public ArrayList<SignagesMeterModel> getSignages() {
        return signages;
    }

    public void setSignages(ArrayList<SignagesMeterModel> signages) {
        this.signages = signages;
    }
}
