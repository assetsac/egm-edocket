package com.evolution.egm.Models;

import org.parceler.Parcel;

/**
 * Created by mcarboleda on 26/10/2016.
 */

@Parcel
public class Stocklist_Kit_Model {
    String _id;
    String type;
    String brand;
    String lenght;
    String size;
    String description;
    String category;
    String stocklist_type;
    boolean isChecked;
    String jobID;

    public Stocklist_Kit_Model() {

    }

    public Stocklist_Kit_Model(String _id, String type, String brand, String lenght, String size, String description, String category, String stocklist_type, boolean isChecked, String jobID) {
        this._id = _id;
        this.type = type;
        this.brand = brand;
        this.lenght = lenght;
        this.size = size;
        this.description = description;
        this.category = category;
        this.stocklist_type = stocklist_type;
        this.isChecked = isChecked;
        this.jobID = jobID;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getLenght() {
        return lenght;
    }

    public void setLenght(String lenght) {
        this.lenght = lenght;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getStocklist_type() {
        return stocklist_type;
    }

    public void setStocklist_type(String stocklist_type) {
        this.stocklist_type = stocklist_type;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getJobID() {
        return jobID;
    }

    public void setJobID(String jobID) {
        this.jobID = jobID;
    }
}
