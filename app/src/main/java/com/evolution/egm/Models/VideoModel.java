package com.evolution.egm.Models;

import org.parceler.Parcel;

/**
 * Created by mcarboleda on 27/05/2016.
 */
@Parcel
public class VideoModel {
    String _id ,jobid, video_url, isSeleted, dateUploaded, geotag, subject, note, startCapture, endCapture, startLocation, endLocation;

    public VideoModel() {

    }

    public VideoModel(String _id, String jobid, String video_url, String isSeleted, String dateUploaded, String geotag, String subject, String note, String startCapture, String endCapture, String startLocation, String endLocation) {
        this._id = _id;
        this.jobid = jobid;
        this.video_url = video_url;
        this.isSeleted = isSeleted;
        this.dateUploaded = dateUploaded;
        this.geotag = geotag;
        this.subject = subject;
        this.note = note;
        this.startCapture = startCapture;
        this.endCapture = endCapture;
        this.startLocation = startLocation;
        this.endLocation = endLocation;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getJobid() {
        return jobid;
    }

    public void setJobid(String jobid) {
        this.jobid = jobid;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getIsSeleted() {
        return isSeleted;
    }

    public void setIsSeleted(String isSeleted) {
        this.isSeleted = isSeleted;
    }

    public String getDateUploaded() {
        return dateUploaded;
    }

    public void setDateUploaded(String dateUploaded) {
        this.dateUploaded = dateUploaded;
    }

    public String getGeotag() {
        return geotag;
    }

    public void setGeotag(String geotag) {
        this.geotag = geotag;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStartCapture() {
        return startCapture;
    }

    public void setStartCapture(String startCapture) {
        this.startCapture = startCapture;
    }

    public String getEndCapture() {
        return endCapture;
    }

    public void setEndCapture(String endCapture) {
        this.endCapture = endCapture;
    }

    public String getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(String startLocation) {
        this.startLocation = startLocation;
    }

    public String getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(String endLocation) {
        this.endLocation = endLocation;
    }
}
