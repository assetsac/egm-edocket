package com.evolution.egm.Models;

/**
 * Created by mcclynreyarboleda on 4/15/15.
 */
public class SearchResults {

    public SearchResults() {
    }

    public SearchResults(String id, String name, String user_id, String status) {
        this.id = id;
        this.name = name;
        this.user_id = user_id;
        this.status = status;
    }

    String id, name, user_id, status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
