package com.evolution.egm.Models;

import org.parceler.Parcel;

/**
 * Created by mcclynreyarboleda on 5/21/15.
 */
@Parcel
public class Timesheet {
    boolean isSelected;

    String ContactID, WorkerID, EndDateTime, Id, JobID, ShiftLocation, StartDateTime, StartType, StartDateTimeFormat, EndDateTimeFormat, Name, Description;
    String JobShiftStartDateTime, JobShiftStartDateTimeFormat, JobShiftEndDateTime, JobShiftEndDateTimeFormat, CreateDateTime, CreateDateTimeFormat;

    public String getWorkerID() {
        return WorkerID;
    }

    public void setWorkerID(String workerID) {
        WorkerID = workerID;
    }

    public Timesheet() {

    }

    public Timesheet(boolean isSelected, String contactID, String workerID, String endDateTime, String id, String jobID, String shiftLocation, String startDateTime, String startType, String startDateTimeFormat, String endDateTimeFormat, String name, String description, String jobShiftStartDateTime, String jobShiftStartDateTimeFormat, String jobShiftEndDateTime, String jobShiftEndDateTimeFormat, String createDateTime, String createDateTimeFormat) {
        this.isSelected = isSelected;
        ContactID = contactID;
        WorkerID = workerID;
        EndDateTime = endDateTime;
        Id = id;
        JobID = jobID;
        ShiftLocation = shiftLocation;
        StartDateTime = startDateTime;
        StartType = startType;
        StartDateTimeFormat = startDateTimeFormat;
        EndDateTimeFormat = endDateTimeFormat;
        Name = name;
        Description = description;
        JobShiftStartDateTime = jobShiftStartDateTime;
        JobShiftStartDateTimeFormat = jobShiftStartDateTimeFormat;
        JobShiftEndDateTime = jobShiftEndDateTime;
        JobShiftEndDateTimeFormat = jobShiftEndDateTimeFormat;
        CreateDateTime = createDateTime;
        CreateDateTimeFormat = createDateTimeFormat;
    }

    public String getContactID() {
        return ContactID;
    }

    public void setContactID(String contactID) {
        ContactID = contactID;
    }

    public String getEndDateTime() {
        return EndDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        EndDateTime = endDateTime;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getJobID() {
        return JobID;
    }

    public void setJobID(String jobID) {
        JobID = jobID;
    }

    public String getShiftLocation() {
        return ShiftLocation;
    }

    public void setShiftLocation(String shiftLocation) {
        ShiftLocation = shiftLocation;
    }

    public String getStartDateTime() {
        return StartDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        StartDateTime = startDateTime;
    }

    public String getStartType() {
        return StartType;
    }

    public void setStartType(String startType) {
        StartType = startType;
    }

    public String getStartDateTimeFormat() {
        return StartDateTimeFormat;
    }

    public void setStartDateTimeFormat(String startDateTimeFormat) {
        StartDateTimeFormat = startDateTimeFormat;
    }

    public String getEndDateTimeFormat() {
        return EndDateTimeFormat;
    }

    public void setEndDateTimeFormat(String endDateTimeFormat) {
        EndDateTimeFormat = endDateTimeFormat;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }



    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }


    public String getJobShiftStartDateTime() {
        return JobShiftStartDateTime;
    }

    public void setJobShiftStartDateTime(String jobShiftStartDateTime) {
        JobShiftStartDateTime = jobShiftStartDateTime;
    }

    public String getJobShiftStartDateTimeFormat() {
        return JobShiftStartDateTimeFormat;
    }

    public void setJobShiftStartDateTimeFormat(String jobShiftStartDateTimeFormat) {
        JobShiftStartDateTimeFormat = jobShiftStartDateTimeFormat;
    }

    public String getJobShiftEndDateTime() {
        return JobShiftEndDateTime;
    }

    public void setJobShiftEndDateTime(String jobShiftEndDateTime) {
        JobShiftEndDateTime = jobShiftEndDateTime;
    }

    public String getJobShiftEndDateTimeFormat() {
        return JobShiftEndDateTimeFormat;
    }

    public void setJobShiftEndDateTimeFormat(String jobShiftEndDateTimeFormat) {
        JobShiftEndDateTimeFormat = jobShiftEndDateTimeFormat;
    }

    public String getCreateDateTime() {
        return CreateDateTime;
    }

    public void setCreateDateTime(String createDateTime) {
        CreateDateTime = createDateTime;
    }

    public String getCreateDateTimeFormat() {
        return CreateDateTimeFormat;
    }

    public void setCreateDateTimeFormat(String createDateTimeFormat) {
        CreateDateTimeFormat = createDateTimeFormat;
    }
}
