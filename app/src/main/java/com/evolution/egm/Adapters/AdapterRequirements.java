package com.evolution.egm.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.evolution.egm.Models.ModelRequirements;
import com.evolution.egm.R;

import java.util.ArrayList;


/**
 * Created by mcclynreyarboleda on 4/29/15.
 */
public class AdapterRequirements extends RecyclerView.Adapter<AdapterRequirements.ViewHolder> {
    public ArrayList<ModelRequirements> mDataset;
    public Context context;

    public AdapterRequirements(ArrayList<ModelRequirements> myDataset, Context context) {
        mDataset = myDataset;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.requirementitems, null);
        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        ModelRequirements m = mDataset.get(position);

        viewHolder.tvtinfo_text.setText(m.getDescription());
        viewHolder.counter.setText(m.getQuantity());
    }
    @Override
    public int getItemCount() {
        return (null != mDataset ? mDataset.size() : 0);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvtinfo_text, counter;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            tvtinfo_text = (TextView) itemLayoutView
                    .findViewById(R.id.requirementname);
            counter = (TextView) itemLayoutView
                    .findViewById(R.id.counter);

        }
    }
}
