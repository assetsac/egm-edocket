package com.evolution.egm.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.TextView;

import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Utils.DateHelper;
import com.evolution.egm.Interface.TimesheetEvents;
import com.evolution.egm.Models.Timesheet;
import com.evolution.egm.R;
import com.rey.material.app.Dialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 * Created by mcclynreyarboleda on 4/29/15.
 */
public class Employeelist_Duplicate extends RecyclerView.Adapter<Employeelist_Duplicate.ViewHolder> implements View.OnClickListener {
    public ArrayList<Timesheet> mDataset;
    DatabaseHelper db;
    public String workerid;
    public Context context;
    private TimesheetEvents callback;
    DateHelper date;
    Dialog dialog;

    public Employeelist_Duplicate(Context context) {
        this.callback = (TimesheetEvents) context;
        this.context = context;
    }

    public Employeelist_Duplicate(ArrayList<Timesheet> myDataset, Context context, String ID) {
        mDataset = myDataset;
        this.context = context;
        this.workerid = ID;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {

        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.duplicateemployee, null);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        dialog = new Dialog(parent.getContext(), R.style.FullHeightDialog2);
        date = new DateHelper();
        return viewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        db = new DatabaseHelper(context);
        final Timesheet m = mDataset.get(position);
        viewHolder.tvtinfo_text.setText(m.getName());
        viewHolder.addtoduplicate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    m.setIsSelected(true);
                    Log.e("s", "True Checked");
                } else {
                    m.setIsSelected(false);
                    Log.e("a", "False Checked");
                }
            }
        });
        viewHolder.tvtinfo_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.setContentView(R.layout.timesheetduplicateinfo);
                TextView txttravelstart, txttravelfinish, traveltodates, traveltodatef, traveltotal;
                TextView onsitestartdate, onsitestarttime, onsitenddate, onsitendtime, onsitetotalfinal;
                TextView mealstartdate, mealstarttime, mealenddate, mealendtime, mealtotalfinal;
                TextView travelfromstartdate, travelfromstarttime, travelfromenddate, travelfromendtime, travelfromtotalfinal;
                TextView traveltofinalkm, kilomitertravelfrom;


                traveltodates = (TextView) dialog.findViewById(R.id.traveltodates);
                traveltodatef = (TextView) dialog.findViewById(R.id.traveltodatef);
                txttravelstart = (TextView) dialog.findViewById(R.id.txttravelstart);
                txttravelfinish = (TextView) dialog.findViewById(R.id.txttravelfinish);
                traveltotal = (TextView) dialog.findViewById(R.id.traveltotal);
                traveltofinalkm = (TextView) dialog.findViewById(R.id.traveltofinalkm);

                onsitestartdate = (TextView) dialog.findViewById(R.id.onsitestartdate);
                onsitestarttime = (TextView) dialog.findViewById(R.id.onsitestarttime);
                onsitenddate = (TextView) dialog.findViewById(R.id.onsitenddate);
                onsitendtime = (TextView) dialog.findViewById(R.id.onsitendtime);
                onsitetotalfinal = (TextView) dialog.findViewById(R.id.onsitetotalfinal);

                mealstartdate = (TextView) dialog.findViewById(R.id.mealstartdate);
                mealstarttime = (TextView) dialog.findViewById(R.id.mealstarttime);
                mealenddate = (TextView) dialog.findViewById(R.id.mealenddate);
                mealendtime = (TextView) dialog.findViewById(R.id.mealendtime);
                mealtotalfinal = (TextView) dialog.findViewById(R.id.mealtotalfinal);

                travelfromstartdate = (TextView) dialog.findViewById(R.id.travelfromstartdate);
                travelfromstarttime = (TextView) dialog.findViewById(R.id.travelfromstarttime);
                travelfromenddate = (TextView) dialog.findViewById(R.id.travelfromenddate);
                travelfromendtime = (TextView) dialog.findViewById(R.id.travelfromendtime);
                travelfromtotalfinal = (TextView) dialog.findViewById(R.id.travelfromtotalfinal);
                kilomitertravelfrom = (TextView) dialog.findViewById(R.id.kilomitertravelfrom);

                ImageButton closetime = (ImageButton) dialog.findViewById(R.id.closetime);
                TextView timesheetownername = (TextView) dialog.findViewById(R.id.timesheetownername);
                timesheetownername.setText(m.getName());

                String timeshet = db.getAlllocalTimesheet(m.getJobID(), m.getContactID());
                Log.e("mac inn", timeshet);
                String[] splitter = timeshet.split("~");

                if (!splitter[0].equals("null")) {
                    traveltodates.setText(date.miliSecondConverter(splitter[0], "dd MMMM yyyy"));
                    txttravelstart.setText(date.miliSecondConverter(splitter[0], "HH:mm"));
                }
                if (!splitter[1].equals("null")) {
                    traveltodatef.setText(date.miliSecondConverter(splitter[1], "dd MMMM yyyy"));
                    txttravelfinish.setText(date.miliSecondConverter(splitter[1], "HH:mm"));
                    traveltotal.setText(getTimeDifference(date.miliSecondConverter(splitter[0], "dd MMMM yyyy HH:mm"), date.miliSecondConverter(splitter[1], "dd MMMM yyyy HH:mm")));
                }
                if (!splitter[2].equals("null")) {
                    traveltofinalkm.setText(splitter[2]);
                }
                if (!splitter[3].equals("null")) {
                    onsitestartdate.setText(date.miliSecondConverter(splitter[3], "dd MMMM yyyy"));
                    onsitestarttime.setText(date.miliSecondConverter(splitter[3], "HH:mm"));
                }
                if (!splitter[6].equals("null")) {
                    onsitenddate.setText(date.miliSecondConverter(splitter[6], "dd MMMM yyyy"));
                    onsitendtime.setText(date.miliSecondConverter(splitter[6], "HH:mm"));
                    onsitetotalfinal.setText(getTimeDifference(date.miliSecondConverter(splitter[3], "dd MMMM yyyy HH:mm"), date.miliSecondConverter(splitter[6], "dd MMMM yyyy HH:mm")));
                }
                if (!splitter[4].equals("null")) {
                    mealstartdate.setText(date.miliSecondConverter(splitter[4], "dd MMMM yyyy"));
                    mealstarttime.setText(date.miliSecondConverter(splitter[4], "HH:mm"));
                }
                if (!splitter[5].equals("null")) {
                    mealenddate.setText(date.miliSecondConverter(splitter[5], "dd MMMM yyyy"));
                    mealendtime.setText(date.miliSecondConverter(splitter[5], "HH:mm"));
                    mealtotalfinal.setText(getTimeDifference(date.miliSecondConverter(splitter[4], "dd MMMM yyyy HH:mm"), date.miliSecondConverter(splitter[5], "dd MMMM yyyy HH:mm")));
                }
                if (!splitter[7].equals("null")) {
                    travelfromstartdate.setText(date.miliSecondConverter(splitter[7], "dd MMMM yyyy"));
                    travelfromstarttime.setText(date.miliSecondConverter(splitter[7], "HH:mm"));
                }
                if (!splitter[8].equals("null")) {
                    travelfromenddate.setText(date.miliSecondConverter(splitter[8], "dd MMMM yyyy"));
                    travelfromendtime.setText(date.miliSecondConverter(splitter[8], "HH:mm"));
                    travelfromtotalfinal.setText(getTimeDifference(date.miliSecondConverter(splitter[7], "dd MMMM yyyy HH:mm"), date.miliSecondConverter(splitter[8], "dd MMMM yyyy HH:mm")));
                }
                if (!splitter[9].equals("null")) {
                    kilomitertravelfrom.setText(splitter[9]);
                }

                closetime.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
            }
        });

        Log.e("Return", db.getAlllocalTimesheet(m.getJobID(), m.getContactID()));
    }

    @Override
    public int getItemCount() {
        return (null != mDataset ? mDataset.size() : 0);
    }

    @Override
    public void onClick(View view) {

    }

    public  ArrayList<Timesheet>  getallSelectedEmployee(){
        return mDataset;
    }

    public void removeItem(int position) {
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvtinfo_text;
        com.rey.material.widget.CheckBox addtoduplicate;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            addtoduplicate = (com.rey.material.widget.CheckBox) itemLayoutView.findViewById(R.id.addtoduplicate);

            tvtinfo_text = (TextView) itemLayoutView
                    .findViewById(R.id.fleetname);
        }
    }

    public String getTimeDifference(String first, String second) {
        long diffMinutes = 0;
        long diffHours = 0;
        SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy HH:mm");
        Date d1 = null;
        Date d2 = null;
        try {
            d1 = format.parse(first);
            d2 = format.parse(second);
            long diff = d2.getTime() - d1.getTime();
            diffMinutes = diff / (60 * 1000) % 60;
            diffHours = diff / (60 * 60 * 1000);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return diffHours + "h  " + diffMinutes + "m";
    }


    public String  getWorkerID(){
        return workerid;
    }
}
