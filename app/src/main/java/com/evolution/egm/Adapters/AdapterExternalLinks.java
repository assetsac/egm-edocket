package com.evolution.egm.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.evolution.egm.Models.DocketAttachments;
import com.evolution.egm.R;

import java.util.ArrayList;


/**
 * Created by mcclynreyarboleda on 4/29/15.
 */
public class AdapterExternalLinks extends RecyclerView.Adapter<AdapterExternalLinks.ViewHolder> {
    public Context context;
    ArrayList<DocketAttachments> docketAttachments;

    public AdapterExternalLinks(ArrayList<DocketAttachments> docketAttachments, Context context) {
        this.docketAttachments = docketAttachments;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.externallinkitem, null);

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        viewHolder.tvtinfo_text.setText(docketAttachments.get(position).getCaption());
        viewHolder.imageView.setImageResource(R.mipmap.jdexternallink);
    }

    @Override
    public int getItemCount() {
        return docketAttachments.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvtinfo_text;
        public ImageView imageView;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            tvtinfo_text = (TextView) itemLayoutView
                    .findViewById(R.id.info_text);

            imageView = (ImageView) itemLayoutView.findViewById(R.id.imageView);


        }
    }
}
