package com.evolution.egm.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Models.SignagesObject;
import com.evolution.egm.R;

import java.util.ArrayList;


/**
 * Created by mcclynreyarboleda on 4/29/15.
 */
public class AdapterSignagesList extends RecyclerView.Adapter<AdapterSignagesList.ViewHolder> implements View.OnClickListener {
    public String[] mDataset;
    public Context context;
    SignagesObject listitem;
    boolean vv = false;
    public int[] mIconSet;
    public DatabaseHelper db;
    ArrayList<String> selectedItem = new ArrayList<String>();

    public AdapterSignagesList(SignagesObject listitem, Context context) {
        this.context = context;
        this.listitem = listitem;
        db = new DatabaseHelper(context);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {

        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.photositemsigns, null);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        Glide.with(context).load(Integer.parseInt(listitem.getSignages().get(position).getImage())).into(viewHolder.phoitem);

        viewHolder.phoitem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewHolder.selectme.getVisibility() == viewHolder.selectme.VISIBLE) {
                    if (listitem.getSignages().get(position).isSelected() != true) {
                        viewHolder.selectme.setVisibility(viewHolder.selectme.GONE);
                        listitem.getSignages().get(position).setIsSelected(false);
                    } else {
                        viewHolder.selectme.setVisibility(viewHolder.selectme.GONE);
                        listitem.getSignages().get(position).setIsSelected(false);
                    }
                } else {
                    viewHolder.selectme.setVisibility(viewHolder.selectme.VISIBLE);
                    listitem.getSignages().get(position).setIsSelected(true);
                }
            }
        });
        if (listitem.getSignages().get(position).isSelected()) {
            viewHolder.selectme.setVisibility(viewHolder.selectme.VISIBLE);
        } else {
            viewHolder.selectme.setVisibility(viewHolder.selectme.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return listitem.getSignages().size();
    }

    @Override
    public void onClick(View view) {

    }

    public SignagesObject listofSelectedItem() {
        return listitem;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView removeImage;
        public ImageView phoitem;
        public LinearLayout selectme;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            phoitem = (ImageView) itemLayoutView.findViewById(R.id.phoitem);
            removeImage = (TextView) itemLayoutView.findViewById(R.id.removeImage);
            selectme = (LinearLayout) itemLayoutView.findViewById(R.id.selectme);
        }
    }
}
