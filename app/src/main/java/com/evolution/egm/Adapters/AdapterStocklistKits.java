package com.evolution.egm.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.evolution.egm.Interface.StocklistViewInterface;
import com.evolution.egm.Models.ModelRequirements;
import com.evolution.egm.Models.Stocklist_Kit_Model;
import com.evolution.egm.R;
import com.evolution.egm.Utils.DatabaseHelper;

import org.parceler.Parcels;

import java.util.ArrayList;


/**
 * Created by mcclynreyarboleda on 4/29/15.
 */
public class AdapterStocklistKits extends RecyclerView.Adapter<AdapterStocklistKits.VersionViewHolder> {
    DatabaseHelper db;
    public ArrayList<Stocklist_Kit_Model> mDataset;
    public Context context;
    StocklistViewInterface openstock;
    OnItemClickListener clickListener;

    public AdapterStocklistKits(ArrayList<Stocklist_Kit_Model> myDataset, Context context) {
        mDataset = myDataset;
        this.context = context;
        this.openstock = (StocklistViewInterface) context;
    }

    @Override
    public VersionViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.stocklistitem_kits, viewGroup, false);
        VersionViewHolder viewHolder = new VersionViewHolder(view);
        db = new DatabaseHelper(context);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(VersionViewHolder versionViewHolder, final int i) {
        final Stocklist_Kit_Model m = mDataset.get(i);

        versionViewHolder.stocktype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openstock.viewstocks(m);
            }
        });
        versionViewHolder.stocktype.setText(m.getType());
        versionViewHolder.setSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    db.setCheckStocks("" + m.get_id(), b);
                } else {
                    db.setCheckStocks("" + m.get_id(), b);
                }
            }
        });
        versionViewHolder.setSelected.setChecked(m.isChecked());
//        if (m.isChecked()) {
//        }else{
//
//        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    class VersionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView stocktype;
        CheckBox setSelected;

        public VersionViewHolder(View itemView) {
            super(itemView);

            stocktype = (TextView) itemView
                    .findViewById(R.id.stocktype);
            setSelected = (CheckBox) itemView
                    .findViewById(R.id.setSelected);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(v, getPosition());
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

}
