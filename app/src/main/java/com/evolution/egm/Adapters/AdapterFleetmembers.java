package com.evolution.egm.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.evolution.egm.Models.FleetNames;
import com.evolution.egm.R;
import com.github.gcacace.signaturepad.views.SignaturePad;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Mac on 5/13/2015.
 */

public class AdapterFleetmembers extends BaseAdapter implements View.OnClickListener {
    private List<FleetNames> item;
    private LayoutInflater inflater;
    Context context;
    CheckBox isdelete;
    public HashMap<String, String> textValues = new HashMap<String, String>();


    public void AdapterInputsfields(Context context) {

    }

    public AdapterFleetmembers(List<FleetNames> item, Context context) {
        this.item = item;
        this.context = context;
    }

    @Override
    public int getCount() {
        return item.size();
    }

    @Override
    public Object getItem(int location) {
        return location;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolderItem {
        TextView number;
        EditText namefield;
        LinearLayout setSignature;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolderItem viewHolder;

        FleetNames m = item.get(position);
        if (convertView == null) {

            // inflate the layout
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.createjobinputsitems, parent, false);

            viewHolder = new ViewHolderItem();
            viewHolder.number = (TextView) convertView.findViewById(R.id.number);

            viewHolder.namefield = (EditText) convertView.findViewById(R.id.namefield);
            viewHolder.setSignature = (LinearLayout) convertView.findViewById(R.id.setSignature);


            // store the holder with the view.
            convertView.setTag(viewHolder);
            convertView.setTag(R.id.number, viewHolder.number);
            convertView.setTag(R.id.namefield, viewHolder.namefield);
            convertView.setTag(R.id.setSignature, viewHolder.setSignature);

            viewHolder.number.setText(m.getFleetid());
            isdelete = (CheckBox) convertView.findViewById(R.id.isdelete);

            viewHolder.setSignature.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final Dialog dialog = new Dialog(context);

                    dialog.setContentView(R.layout.dialog_signature);
                    dialog.setTitle("Signature Area");

                    final Button cancel = (Button) dialog.findViewById(R.id.cancel);
                    final Button save = (Button) dialog.findViewById(R.id.save);

                    final SignaturePad mSignaturePad = (SignaturePad) dialog.findViewById(R.id.signaturearea);
                    save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });

                    final Button clear = (Button) dialog.findViewById(R.id.clear);
                    clear.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mSignaturePad.clear();
                        }
                    });


                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
                        @Override
                        public void onSigned() {
                            save.setEnabled(true);
                            clear.setEnabled(true);
                        }

                        @Override
                        public void onClear() {
                            save.setEnabled(false);
                            clear.setEnabled(false);
                        }
                    });

                    dialog.show();
                }

            });

        } else {
            // just use the viewHolder
            viewHolder = (ViewHolderItem) convertView.getTag();

            viewHolder.number.setText(m.getFleetid());
            isdelete = (CheckBox) convertView.findViewById(R.id.isdelete);

            viewHolder.setSignature.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final Dialog dialog = new Dialog(context);

                    dialog.setContentView(R.layout.dialog_signature);
                    dialog.setTitle("Signature Area");

                    final Button cancel = (Button) dialog.findViewById(R.id.cancel);
                    final Button save = (Button) dialog.findViewById(R.id.save);

                    final SignaturePad mSignaturePad = (SignaturePad) dialog.findViewById(R.id.signaturearea);
                    save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });

                    final Button clear = (Button) dialog.findViewById(R.id.clear);
                    clear.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mSignaturePad.clear();
                        }
                    });


                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
                        @Override
                        public void onSigned() {
                            save.setEnabled(true);
                            clear.setEnabled(true);
                        }

                        @Override
                        public void onClear() {
                            save.setEnabled(false);
                            clear.setEnabled(false);
                        }
                    });

                    dialog.show();
                }

            });
        }


        if (convertView != null) {
            viewHolder.namefield.addTextChangedListener(new GenericTextWatcher(viewHolder.namefield));
        }

        viewHolder.namefield.setTag("namefield" + position);

        return convertView;

    }

    public void updateList() {
        notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {

    }

    public void checkAllCheckBox(int position) {
        isdelete.setVisibility(isdelete.VISIBLE);
    }

    public void hideCheckBox() {
        isdelete.setVisibility(isdelete.GONE);
    }

    private class GenericTextWatcher implements TextWatcher {

        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {

            String text = editable.toString();
            //save the value for the given tag :
            AdapterFleetmembers.this.textValues.put(view.getTag().toString(), editable.toString());
        }
    }


    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        }
        return file;
    }

    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        stream.close();
    }

    public boolean addSignatureToGallery(Bitmap signature) {
        boolean result = false;
        try {
            File photo = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.jpg", System.currentTimeMillis()));
            saveBitmapToJPG(signature, photo);
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(photo);
            mediaScanIntent.setData(contentUri);
            context.sendBroadcast(mediaScanIntent);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }


}
