package com.evolution.egm.Adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Models.Job;
import com.evolution.egm.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by mcclynreyarboleda on 4/1/15.
 */
public class AdapterJObs extends RecyclerView.Adapter<AdapterJObs.ViewHolder> {
    public ArrayList<Job> mDataset;
    public Context context;
    Job mJobObject;

    DatabaseHelper db;

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterJObs(ArrayList<Job> myDataset, Context cotext) {
        mDataset = myDataset;
        this.context = cotext;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.jobsitem, null);
        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    public void resetData() {
        mDataset.clear();
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        mJobObject = mDataset.get(position);

        db = new DatabaseHelper(context);
        if (db.checkExistCreateJObs(mJobObject.getId()) != null) {

            DateFormat formatter = new SimpleDateFormat("EEEE, dd MMMM yyyy HH:mm");
            long milliSeconds = Long.parseLong(mJobObject.getStartDateTime());
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(milliSeconds);

            viewHolder.JobID.setText(mJobObject.getJobID());
            viewHolder.CompanyName.setText(mJobObject.getCompanyName());
            viewHolder.Address.setText(mJobObject.getAddress());
            String dm = formatter.format(calendar.getTime());

            viewHolder.CreateTime.setText(mJobObject.getJobShiftStartDateTimeFormat().replace("-", " ").substring(0, mJobObject.getJobShiftStartDateTimeFormat().length() - 8));
            String toview = mJobObject.getJobShiftStartDateTimeFormat().replace("-", " ").substring(mJobObject.getJobShiftStartDateTimeFormat().length() - 8);
            viewHolder.jobtimetime.setText(toview.substring(0, toview.length() - 3));

        } else {

            DateFormat formatter = new SimpleDateFormat("EEEE, dd MMMM yyyy HH:mm");
            long milliSeconds = Long.parseLong(mJobObject.getStartDateTime());
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(milliSeconds);

            viewHolder.JobID.setText(mJobObject.getJobID());
            viewHolder.CompanyName.setText(mJobObject.getCompanyName());
            viewHolder.Address.setText(mJobObject.getAddress());
            String dm = formatter.format(calendar.getTime());

            viewHolder.CreateTime.setText(mJobObject.getJobShiftStartDateTimeFormat().replace("-", " ").substring(0, mJobObject.getJobShiftStartDateTimeFormat().length() - 8));
            String toview = mJobObject.getJobShiftStartDateTimeFormat().replace("-", " ").substring(mJobObject.getJobShiftStartDateTimeFormat().length() - 8);
            viewHolder.jobtimetime.setText(toview.substring(0,toview.length() - 3));

        }
    }

    public Job getItem(int position) {
        return mDataset.get(position);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return (null != mDataset ? mDataset.size() : 0);
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView JobID, CompanyName, CreateTime, Address, jobtimetime;
        ImageView listicon;

        public ViewHolder(View itemLayoutView) {

            super(itemLayoutView);

            JobID = (TextView) itemLayoutView
                    .findViewById(R.id.JobID);
            listicon = (ImageView) itemLayoutView.findViewById(R.id.listicon);

            CompanyName = (TextView) itemLayoutView
                    .findViewById(R.id.CompanyName);
            Address = (TextView) itemLayoutView
                    .findViewById(R.id.Address);
            CreateTime = (TextView) itemLayoutView
                    .findViewById(R.id.CreateTime);

            jobtimetime = (TextView) itemLayoutView
                    .findViewById(R.id.jobtimetime);

        }

        @Override
        public void onClick(View v) {

        }
    }


    public void updateLists(ArrayList arraylist) {
        mDataset = arraylist;
        notifyDataSetChanged();
    }
}