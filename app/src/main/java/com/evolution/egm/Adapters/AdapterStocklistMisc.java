package com.evolution.egm.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.evolution.egm.Interface.StocklistViewInterface;
import com.evolution.egm.Models.Stocklist_Kit_Model;
import com.evolution.egm.R;

import java.util.ArrayList;


/**
 * Created by mcclynreyarboleda on 4/29/15.
 */
public class AdapterStocklistMisc extends RecyclerView.Adapter<AdapterStocklistMisc.VersionViewHolder> {

    public ArrayList<Stocklist_Kit_Model> mDataset;
    public Context context;
    StocklistViewInterface openstock;
    AdapterStocklistMisc.OnItemClickListener clickListener;

    public AdapterStocklistMisc(ArrayList<Stocklist_Kit_Model> myDataset, Context context) {
        mDataset = myDataset;
        this.context = context;
        this.openstock = (StocklistViewInterface) context;
    }


    @Override
    public AdapterStocklistMisc.VersionViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.stocklistitem_misc, viewGroup, false);
        AdapterStocklistMisc.VersionViewHolder viewHolder = new AdapterStocklistMisc.VersionViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AdapterStocklistMisc.VersionViewHolder versionViewHolder, final int i) {
        final Stocklist_Kit_Model m = mDataset.get(i);

        versionViewHolder.stocktype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openstock.viewstocks(m);
            }
        });
        versionViewHolder.editstoskcs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openstock.editStocks(m);
            }
        });
        versionViewHolder.deletestoskcs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openstock.deleteStocks(m);

            }
        });
        versionViewHolder.stocktype.setText(m.getType());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    class VersionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView stocktype;
        public ImageView editstoskcs, deletestoskcs;

        public VersionViewHolder(View itemView) {
            super(itemView);

            stocktype = (TextView) itemView
                    .findViewById(R.id.stocktype);
            editstoskcs = (ImageView) itemView
                    .findViewById(R.id.editstoskcs);
            deletestoskcs = (ImageView) itemView
                    .findViewById(R.id.deletestoskcs);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(v, getPosition());
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final AdapterStocklistMisc.OnItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }
}