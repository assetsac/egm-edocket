package com.evolution.egm.Adapters;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.evolution.egm.Models.FleetSignature;
import com.evolution.egm.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by mcclynreyarboleda on 4/29/15.
 */
public class AdapterInputsfields extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private List<FleetSignature> item = new ArrayList<>();
    private int counterfield = 0;

    public AdapterInputsfields(Context context, List<FleetSignature> item) {
        this.context = context;
        this.item = item;
    }

    @Override
    public int getCount() {
        return item.size();
    }

    @Override
    public Object getItem(int location) {
        return location;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.createjobinputsitems, null);
        EditText textname = (EditText) convertView.findViewById(R.id.textname);
        TextView number = (TextView) convertView.findViewById(R.id.inputID);
        FleetSignature m = item.get(position);
        number.setText(m.getId());
        textname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {

                    if (!((counterfield + 1) != getCount())) {
                        generateField();
                    }

                } else {

                    removeItem();

                }
            }
        });

        return convertView;
    }

    public void generateField() {

        FleetSignature m = new FleetSignature();
        m.setId("" + counterfield);
        item.add(m);
        counterfield++;
        notifyDataSetChanged();

    }

    public void removeItem() {

    }


}
