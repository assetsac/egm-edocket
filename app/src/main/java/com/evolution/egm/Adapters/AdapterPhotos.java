package com.evolution.egm.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.evolution.egm.Activities.Photo;
import com.evolution.egm.Utils.DatabaseHelper;
import com.evolution.egm.Interface.PhotoInterface;
import com.evolution.egm.Models.PhotoItem;
import com.evolution.egm.R;

import java.util.ArrayList;


/**
 * Created by mcclynreyarboleda on 4/29/15.
 */
public class AdapterPhotos extends RecyclerView.Adapter<AdapterPhotos.ViewHolder> implements View.OnClickListener {
    public String[] mDataset;
    public int[] mIconSet;
    public Context context;
    public PhotoInterface pListener;
    public Photo ph;
    public ArrayList<PhotoItem> items;
    DatabaseHelper db;
    BitmapFactory.Options options;
    boolean vv = false;
    Bitmap bitmap;
    ArrayList<String> selectedItem = new ArrayList<String>();

    public AdapterPhotos(ArrayList<PhotoItem> item, Context context) {
        this.items = item;
        this.context = context;
        this.pListener = (PhotoInterface) context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.photositem, null);
        db = new DatabaseHelper(context);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);

        options = new BitmapFactory.Options();
        ph = new Photo();
        return viewHolder;

    }
    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        Glide.with(context).load(items.get(position).getImage_url()).into(viewHolder.phoitem);
        viewHolder.removeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pListener.deletePhoto(items.get(position).getImage_url());
            }
        });

        viewHolder.phoitem.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                selectedItem.add(items.get(position).getImage_url());
                viewHolder.selectme.setVisibility(viewHolder.selectme.VISIBLE);
                countSelectedShowMenu(selectedItem);
                vv = true;
                return false;
            }
        });

        viewHolder.phoitem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedItem.size() == 0) {
                    pListener.viewPhoto(position + "");
                } else {
                    if (viewHolder.selectme.getVisibility() == viewHolder.selectme.VISIBLE) {
                        if (vv != true) {
                            viewHolder.selectme.setVisibility(viewHolder.selectme.GONE);
                            selectedItem.remove(items.get(position).getImage_url());
                            countSelectedShowMenu(selectedItem);
                        }
                    } else {
                        viewHolder.selectme.setVisibility(viewHolder.selectme.VISIBLE);
                        selectedItem.add(items.get(position).getImage_url());
                        countSelectedShowMenu(selectedItem);
                        vv = false;
                    }
                }
            }
        });
    }

    public void RestArrayList(){
        selectedItem.clear();
    }
    public ArrayList getAllSlectedITem() {
        return selectedItem;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onClick(View view) {

    }

    public void countSelectedShowMenu(ArrayList selectedItem) {
        if (selectedItem.size() == 0) {
            pListener.showHideMenu(false);
        } else {
            pListener.showHideMenu(true);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView removeImage;
        public ImageView phoitem;
        public LinearLayout selectme;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            phoitem = (ImageView) itemLayoutView.findViewById(R.id.phoitem);
            removeImage = (TextView) itemLayoutView.findViewById(R.id.removeImage);
            selectme = (LinearLayout) itemLayoutView.findViewById(R.id.selectme);
        }
    }
}
