package com.evolution.egm.Adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.evolution.egm.Models.Testingmode;
import com.evolution.egm.R;

import java.util.ArrayList;

/**
 * Created by mcclynreyarboleda on 4/1/15.
 */
public class TestingAdapter extends RecyclerView.Adapter<TestingAdapter.ViewHolder> {
    public ArrayList<Testingmode> mDataset;
    public Context context;


    public TestingAdapter(ArrayList<Testingmode> myDataset, Context cotext) {
        mDataset = myDataset;
        this.context = cotext;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.testingitem, null);

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;

    }

    public void resetData() {
        mDataset.clear();
    }
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Testingmode activitites = mDataset.get(position);

    }
    public void ref() {
        notifyDataSetChanged();
    }
    public Testingmode getItem(int position) {
        return mDataset.get(position);
    }


    @Override
    public int getItemCount() {
        return (null != mDataset ? mDataset.size() : 0);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView description;
        public  ImageView imageView;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
        }

        @Override
        public void onClick(View v) {

        }
    }



}