package com.evolution.egm.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.evolution.egm.R;


/**
 * Created by mcclynreyarboleda on 4/29/15.
 */
public class AdapterNSW extends RecyclerView.Adapter<AdapterNSW.ViewHolder> {
    public int[] mIconSet;
    public Context context;
    String id;

    public AdapterNSW(int[] mIconSet, Context context) {

        this.mIconSet = mIconSet;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.nswitems, null);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

    }

    @Override
    public int getItemCount() {
        return mIconSet.length;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView avatar;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            avatar = (ImageView) itemLayoutView
                    .findViewById(R.id.nswicons);
        }
    }
}
